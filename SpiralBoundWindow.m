//
//  SpiralWindow.m
//  SpiralBound
//
//  Created by Philip White on 2/1/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "SpiralBoundWindow.h"
#import "SBWFrameView.h"
#import "TOWFrameView.h"
#import "SBController.h"
#import "SPRTextView.h"
#import "SBTitleView.h"
#import "SBConstants.h"

@implementation SpiralBoundWindow

+ (NSSize)minSize
{
    return NSMakeSize(150,80);
}

- (id)initWithContentRect:(NSRect)contentRect style: (SBWindowStyle)style organizer: (SPROrganizer*)organizer
{
	self = [super initWithContentRect:contentRect styleMask:NSBorderlessWindowMask
					   backing: NSBackingStoreBuffered defer:NO];
	
	if (self != nil)
    {
        [self setOpaque:NO];
        [self setBackgroundColor:[NSColor clearColor]];
		[self setHasShadow:YES];
		[self setMinSize:[SpiralBoundWindow minSize]];
		[self setReleasedWhenClosed:NO];
		
		NSRect bounds = [self frame];
		bounds.origin=NSZeroPoint;
		id frameView;
		
		if (style == SBSpiralBoundStyle)
		{
			frameView = [[SBWFrameView alloc] initWithFrame:bounds organizer:organizer];
		}
		else
		{
			frameView = [[TOWFrameView alloc] initWithFrame:bounds organizer:organizer];
		}
		
		[self setContentView: frameView];
		
		//make the buttons in the toolbar faded out
		//the main window will get a becomeMainWindow message and will set the buttons
		//to the non faded out version
		{
			NSView *toolbar = [(id)[self contentView] toolbarView];
			
			[[toolbar viewWithTag:kTrashButtonTag] setImage: [NSImage imageNamed:@"TrashDisabled"]];
			if ([[self contentView] isKindOfClass:[SBWFrameView class]])
			{
				[[toolbar viewWithTag:kLeftArrowTag] setImage: [NSImage imageNamed:@"LeftArrowDisabled"]];
				[[toolbar viewWithTag:kRightArrowTag] setImage: [NSImage imageNamed:@"RightArrowDisabled"]];
				[[toolbar viewWithTag:kSheetButtonTag] setImage: [NSImage imageNamed:@"SheetDisabled"]];
			}
			else
			{
				[[toolbar viewWithTag:kReturnToNotebookButtonTag] setImage: [NSImage imageNamed:@"NotebookDisabled"]];
			}
		}
	}
    return self;
	
}

-(void)dealloc
{
	[NSApp removeWindowsItem:self];
}

- (BOOL)canBecomeKeyWindow
{
	return YES;
}

- (BOOL)canBecomeMainWindow
{
	return YES;
}

- (NSRect)contentRectForFrameRect:(NSRect)windowFrame
{
    return windowFrame;
}

+ (NSRect)frameRectForContentRect:(NSRect)windowContentRect
						styleMask:(NSUInteger)windowStyle
{
	return windowContentRect;
}

-(SBTitleView*)titleView
{
	return [(SBProtoFrameView*)[self contentView] titleView];
}

- (NSView*)mainView
{
	return [(SBProtoFrameView*)[self contentView] mainView];
}

-(NSView*)documentView
{
	return [(SBProtoFrameView*)[self contentView] documentView];
}

- (SPRTodoListView*)mainTodoListView
{
	return [(SBProtoFrameView*)[self contentView] mainTodoListView];
}

- (void)setMainTodoListView:(SPRTodoListView*)tv
{
	[(SBProtoFrameView*)[self contentView] setMainTodoListView:tv];
}

- (SPRTextView*)mainTextView
{
	return (SPRTextView*)[(SBProtoFrameView*)[self contentView] mainTextView];
}

- (void)setMainTextView: (SPRTextView*)newTextView
{
	[(SBProtoFrameView*)[self contentView] setMainTextView: newTextView];
}

- (void)showMainTextView
{
	[(id)[self contentView] showMainTextView];
}

- (void)showMainTodoListView
{
	[(id)[self contentView] showMainTodoListView];
}

- (void)setTitle: (NSString*)title
{
	SBProtoFrameView *view = (SBProtoFrameView*)[self contentView];
	[[view titleView] setStringValue: title];
	[super setTitle: title];
	[NSApp changeWindowsItem:self title:title filename:NO];
}



- (void)becomeMainWindow
{
	[super becomeMainWindow];
	
	[self setIsTitleBold:YES];
	
	//make the buttons in the toolbar not faded out
	NSView *toolbar = [(id)[self contentView] toolbarView];
	
	[[toolbar viewWithTag:kTrashButtonTag] setImage: [NSImage imageNamed:@"Trash"]];
	if ([[self contentView] isKindOfClass:[SBWFrameView class]])
	{
		[[toolbar viewWithTag:kLeftArrowTag] setImage: [NSImage imageNamed:@"LeftArrow"]];
		[[toolbar viewWithTag:kRightArrowTag] setImage: [NSImage imageNamed:@"RightArrow"]];
		[[toolbar viewWithTag:kSheetButtonTag] setImage: [NSImage imageNamed:@"Sheet"]];
	}
	else
	{
		[[toolbar viewWithTag:kReturnToNotebookButtonTag] setImage: [NSImage imageNamed:@"Notebook"]];
	}
	
	[[self contentView] setNeedsDisplay:YES];
}

- (void)resignMainWindow
{
	[super resignMainWindow];
	
	if ((id)[self firstResponder] != (id)[(id)[self mainView] documentView])
		[self makeFirstResponder:[self contentView]];
	
	[self setIsTitleBold:NO];
	[(id)[self contentView] setColors];
	
	//make the buttons in the toolbar faded out
	NSView *toolbar = [(id)[self contentView] toolbarView];
	
	[[toolbar viewWithTag:kTrashButtonTag] setImage: [NSImage imageNamed:@"TrashDisabled"]];
	if ([[self contentView] isKindOfClass:[SBWFrameView class]])
	{
		[[toolbar viewWithTag:kLeftArrowTag] setImage: [NSImage imageNamed:@"LeftArrowDisabled"]];
		[[toolbar viewWithTag:kRightArrowTag] setImage: [NSImage imageNamed:@"RightArrowDisabled"]];
		[[toolbar viewWithTag:kSheetButtonTag] setImage: [NSImage imageNamed:@"SheetDisabled"]];
	}
	else
	{
		[[toolbar viewWithTag:kReturnToNotebookButtonTag] setImage: [NSImage imageNamed:@"NotebookDisabled"]];
	}
	
	
	[[self contentView] setNeedsDisplay:YES];
}

static NSTimeInterval AnimationRate = 0.20;

- (void)setFrame:(NSRect)windowFrame display:(BOOL)displayViews animate:(BOOL)performAnimation rate:(NSTimeInterval)rate
{
	NSTimeInterval oldValue = AnimationRate;
	AnimationRate = rate;
	[super setFrame:windowFrame display:displayViews animate:performAnimation];
	AnimationRate = oldValue;
}

- (NSTimeInterval)animationResizeTime:(NSRect)newWindowFrame
{
	return AnimationRate;
}

- (void)setIsTitleBold: (BOOL)isBold
{
	[[(id)[self contentView] titleView] setBold:isBold];
}

- (void)buldge
{
	NSRect windowFrame = [self frame];
	windowFrame = NSInsetRect(windowFrame, -2, -2);
	[self setFrame:windowFrame display: YES animate: YES rate:0.04];
	windowFrame = NSInsetRect(windowFrame, 2, 2);
	[self setFrame:windowFrame display: YES animate: YES rate:0.04];
}

#define FLOATER_LEVEL NSFloatingWindowLevel

-(IBAction)toggleFloater:(id)sender
{
	[self setFloater:![self isFloater]];
	[sender setState:[self isFloater] ? NSOnState : NSOffState];
}

- (void)setFloater:(BOOL)floater
{
	int windowLevel;
	if (floater)
	{
		windowLevel = FLOATER_LEVEL;
	}
	else
	{
		windowLevel = NSNormalWindowLevel;
	}
	[self setLevel:windowLevel];
}

- (BOOL)isFloater
{
	return ([self level] == FLOATER_LEVEL);
}

//just for menu validation
-(IBAction)dummy:(id)sender
{

}

- (void)setSecurityButtonState:(int)state
{
	[(id)[self contentView] setSecurityButtonState:state];
}

@end
