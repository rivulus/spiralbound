//
//  PageBrowser+Filtering_SBPro.h
//  CommonSpiralbound
//
//  Created by Philip on 5/18/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "PageBrowser_SBPro.h"

@interface PageBrowser (Filtering)


-(NSUInteger)pageIndexForCellIndex:(NSUInteger)indexIn;
-(void)loadUnfiltered;
-(void)refilterSelectedPage;
-(void)filter;
-(void)filterAction:(id)sender;
-(IBAction)toggleFilterSetting:(id)sender;
@end
