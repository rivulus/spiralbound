//
//  NSString+RIVSearch.m
//  DeleteMe
//
//  Created by Philip on 5/19/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import "NSString+RIVSearch.h"


@implementation NSString (RIVSearch)

-(NSArray*)rangesForSearchString:(NSString*)str options:(CFStringCompareFlags)options wholeWords:(BOOL)ww
{
	CFArrayRef results = CFStringCreateArrayWithFindResults(NULL,
															(CFStringRef)self,
															(CFStringRef)str,
															CFRangeMake(0,[self length]),
															options);
	
	
	if (results == NULL)
		return [NSArray array];
	
	//convert CFArrayRef of pointers to CFRanges into NSArray of NSValues containing NSRanges
	int count = CFArrayGetCount(results);
	NSMutableArray *array = [NSMutableArray arrayWithCapacity:count];
	if (count > 0)
	{
		CFRange **ranges = malloc(sizeof(CFRange*)*count);
		CFArrayGetValues(results,CFRangeMake(0, count),(const void **)ranges);
		
		int i;
		for (i=0; i<count; i++)
		{
			NSRange r = NSMakeRange(ranges[i]->location,ranges[i]->length);
			[array addObject:[NSValue valueWithRange:r]];
		}
		
		free(ranges);
		
		if (ww) //whole words only
		{
			array = (id)[self wholeWordRangesFromRanges:array];
			//NOTE, treat array as IMMUTABLE from now on
		}
	}
	
	CFRelease(results);
	
	return array;
}

-(NSArray*)wholeWordRangesFromRanges:(NSArray*)inRanges
{
	NSMutableArray *outRanges = [NSMutableArray array];
	
	id e = [inRanges objectEnumerator];
	id robj;
	
	CFLocaleRef locale = CFLocaleCopyCurrent();
	CFStringTokenizerRef tokenizer = CFStringTokenizerCreate(NULL,
															 (CFStringRef)self,
															 CFRangeMake(0,[self length]),
															 kCFStringTokenizerUnitWord,
															 locale);
	
	while (robj=[e nextObject])
	{
		NSRange r = [robj rangeValue];
		
		if (CFStringTokenizerGoToTokenAtIndex(tokenizer, r.location) != kCFStringTokenizerTokenNone)
		{
			CFRange tokenRange = CFStringTokenizerGetCurrentTokenRange(tokenizer);
			if (tokenRange.location != r.location)
				continue;
		}
		
		if (CFStringTokenizerGoToTokenAtIndex(tokenizer, r.location+r.length) != kCFStringTokenizerTokenNone)
		{
			CFRange tokenRange = CFStringTokenizerGetCurrentTokenRange(tokenizer);
			if (tokenRange.location != r.location+r.length)
				continue;
		}
		
		//if we get here, the range r doesn't break any word tokens
		[outRanges addObject: robj];
	}
	
	CFRelease(tokenizer);
	CFRelease(locale);
	
	return outRanges;
}

@end
