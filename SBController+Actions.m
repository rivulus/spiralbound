//
//  SBController+Actions.m
//  SpiralBound Pro
//
//  Created by Philip White on 5/4/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

//#import "../SpiralBound 1.1/Exporter.h"
#import "SBController+Actions.h"
#import "SPRPage.h"
#import "SPRInAppPurchases.h"
#import "SPROrganizer.h"
#import "SPROrganizer+Sorting.h"

#import "SPRPreferencesController.h"
#import "HelpController.h"

#import "SPRNotebook.h"
#import "RIVTools.h"
#import "SpiralBoundWindow.h"

#import "SPRSearchEngine.h"

#import "RIVTools.h"

#import "PageSorter_SBPro.h"
#import "NewNotebookController_SBPro.h"

#import "SPRInAppPurchases.h"

@implementation SBController (Actions)

-(IBAction)showTipJar:(id)sender
{
	[[SPRInAppPurchases sharedInstance] showUI];
}

-(IBAction)showNewNotebookPanel:(id)sender
{
	[[NewNotebookController sharedController] showWindow:nil];
}

-(IBAction)showImportFromStickiesPanel:(id)sender
{
	[[NewNotebookController sharedController] showAsImportFromStickies];
}

-(IBAction)showImportFromSpiralBoundPanel:(id)sender
{
	[[NewNotebookController sharedController] showAsImportFromSpiralBound];
}

-(IBAction)showAllNotebooks:(id)sender
{
	[SPROrganizer showAll];
}

-(IBAction)showPageSorter:(id)sender
{
	[[PageSorter sharedSorter] showWindow:nil];
}

-(IBAction)showFindWindow:(id)sender
{
	[[SPRSearchEngine soleInstance] showWindow:nil];
}

-(IBAction)findNext:(id)sender
{
	[[SPRSearchEngine soleInstance] findNext:nil];
}

-(IBAction)findPrevious:(id)sender
{
	[[SPRSearchEngine soleInstance] findPrevious:nil];
}

-(IBAction)showPreferencesWindow:(id)sender
{
	[[SPRPreferencesController sharedPreferencesController] showWindow:nil];
}

-(IBAction)importNotebook:(id)sender
{
	[[NewNotebookController sharedController] importSavedNotebook];
}

-(IBAction)showSpiralBoundHelp:(id)sender
{
	[[HelpController sharedHelpController] showWindow:nil];
}

-(IBAction)contactRivulus:(id)sender
{
	NSURL *url;
	
    // Create the URL.
	
    url = [NSURL URLWithString:@"mailto:rivulus.sw@me.com"
		   "?subject=Customer%20Support%20Question"
		   //"&body=Share%20and%20Enjoy"
		   ];
		   
    if (url == nil)
	{
		NSRunCriticalAlertPanel(@"Error Occurred", @"Unable to generate email", @"OK", nil, nil);
		return;
	}
	
    // Open the URL.
    [[NSWorkspace sharedWorkspace] openURL:url];
}

- (IBAction)cycleThroughWindows:(id)sender
{
	NSArray *windows = [SPROrganizer orderedArrayOfWindows];
	
	NSUInteger nextIndex;
	
	NSUInteger currentIndex = [windows indexOfObject:[NSApp mainWindow]];
	
	if (currentIndex == NSNotFound || currentIndex == [windows count]-1)
		nextIndex = 0;
	else
		nextIndex = currentIndex+1;

	[[windows objectAtIndex:nextIndex] makeKeyAndOrderFront:nil];
}

-(IBAction)saveImmediately:(id)sender
{	
	[SPROrganizer saveOrganizers];
	SetUnsavedChanges(NO);
}

-(IBAction)hideOtherNotebooks:(id)sender
{
	[SPROrganizer hideNonkeyNotebooks];
}

-(void)checkForUpdateQuietly
{
	RIVCheckForUpdate(YES);
}

-(IBAction)checkForUpdate:(id)sender
{
	RIVCheckForUpdate(NO);
}

@end
