//
//  SPRPreferencesController.h
//  SpiralBound
//
//  Created by Philip White on 2/12/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

extern BOOL ShouldAnimate;

@class SpiralBoundWindow;

@interface SPRPreferencesController : NSWindowController
{	
	IBOutlet NSMatrix *mainColorRadioButtons;
	IBOutlet NSPanel *setPasswordPanel;
	IBOutlet NSPanel *changePasswordPanel;
	IBOutlet NSPanel *removePasswordPanel;
	IBOutlet NSTextView *fontSampleView;
	
	//controller set variables
	NSString *fontName;
	id fontSize; //use intValue
	NSColor *textColor;
	
	NSString *oldPassword;
	NSString *newPassword1;
	NSString *newPassword2;
	BOOL newPasswordsAreEqual;
	BOOL oldPasswordIsValid;
	BOOL newPasswordsAreEqualAndOldPasswordIsValid;
	
	BOOL hasPassword;
	BOOL useKeychain;
	BOOL keychainIsLocked;
	
	BOOL checkForUpdates;
	NSString *updateCheckFrequency;
	NSArray *notebookTitles; //for bindings
	
    IBOutlet NSTabView *tabView;
}

+ (SPRPreferencesController*)sharedPreferencesController;
+ (NSString*)defaultDataFolderPath;

- (void)applicationWillTerminate;

//fonts
- (void)makeTextAttributes;

- (NSString *)fontName;
- (void)setFontName:(NSString *)value;

- (id)fontSize;
- (void)setFontSize:(id)value;

- (NSColor *)textColor;
- (void)setTextColor:(NSColor *)value;

//data folder path
- (IBAction)chooseDataFolderPath:(id)sender;
- (void)resetDataFolderPath;
- (IBAction)resetDataFolderPath:(id)sender;
- (NSString*)dataFolderPath;
- (void)deleteOldDataFolder: (NSString*)oldPath;

//passwords
-(IBAction)setPasswordFromButton:(id)sender;
-(IBAction)changePasswordFromButton:(id)sender;
-(IBAction)removePasswordFromButton:(id)sender;

//panel showers
-(IBAction)setPassword:(id)sender;
-(IBAction)changePassword:(id)sender;
-(IBAction)removePassword:(id)sender;
-(IBAction)closeSheet:(id)sender;

- (void)clearSecureInstances;

- (NSString *)oldPassword;
- (void)setOldPassword:(NSString *)value;

- (NSString *)newPassword1;
- (void)setNewPassword1:(NSString *)value;

- (NSString *)newPassword2;
- (void)setNewPassword2:(NSString *)value;

- (BOOL)newPasswordsAreEqual;
- (void)setNewPasswordsAreEqual:(BOOL)value;

- (BOOL)oldPasswordIsValid;
- (void)setOldPasswordIsValid:(BOOL)value;

- (BOOL)newPasswordsAreEqualAndOldPasswordIsValid;
- (void)setNewPasswordsAreEqualAndOldPasswordIsValid:(BOOL)value;

- (BOOL)hasPassword;
- (void)setHasPassword:(BOOL)value;

- (BOOL)useKeychain;
- (void)setUseKeychain:(BOOL)value;

- (BOOL)keychainIsLocked;
- (void)setKeychainIsLocked:(BOOL)value;

//password tools

- (void)storePassword:(NSString*)password;
- (BOOL)isPasswordValid:(NSString*)password;
-(NSData*)roll:(NSData*)inData;
-(void)switchPasswordToKeychain;

+ (void)setupDefaults;

//other
- (IBAction)showTopicalHelp:(id)sender;

-(IBAction)resetPage:(id)sender;
-(IBAction)resetAllPreferences:(id)sender;

-(IBAction)checkForUpdate:(id)sender;

- (NSArray *)notebookTitles;
- (void)setNotebookTitles:(NSArray *)value;

@end
