//
//  SPROrganizer+Security.h
//  SpiralBound Pro
//
//  Created by Philip White on 5/9/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SPROrganizer.h"

@interface SPROrganizer (Security)

+(void)lockAllPages;
+(void)unlockAllPages;
-(void)lockAllPages;
-(void)unlockAllPages;
-(IBAction)lockPage:(id)sender;
//-(void)setSecurityForVisiblePages;
-(BOOL)areAnyPagesSecured;
@end
