//
//  SPRTodoItem.m
//  CocoaJunk
//
//  Created by Philip on 5/20/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import "SPRTodoItemView.h"
#import "SPRTodoItemTextView.h"
#import "SPRTodoItemCheckbox.h"
#import "SPRTodoListView.h"
#import "DefaultsKeys.h"
#import "SBConstants.h"

@implementation SPRTodoItemView

#define YMargin 3.0
#define XMargin 0.0

static BOOL StrikeOut;

+(void)initialize
{
	StrikeOut = [[NSUserDefaults standardUserDefaults] boolForKey:DefaultsPreferencesStrikeOutCheckedItem];
	
	[[NSUserDefaults standardUserDefaults] addObserver:self
											forKeyPath:DefaultsPreferencesStrikeOutCheckedItem
											   options:NSKeyValueObservingOptionNew
											   context:NULL];
}

+ (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([keyPath isEqualToString:DefaultsPreferencesStrikeOutCheckedItem])
	{
		StrikeOut = [[change objectForKey:NSKeyValueChangeNewKey] boolValue];
	}
}

-(BOOL)isFlipped {return YES;}

-(id)initWithFrame:(NSRect)frame textContainer:(NSTextContainer*)tc state:(BOOL)state parentList:(SPRTodoListView*)p
{
	self = [super initWithFrame:frame];
	
	if (self)
	{
		NSRect checkBoxFrame = {0,0,24,24};
		checkBox = [[SPRTodoItemCheckbox alloc] initWithFrame:checkBoxFrame itemView:self];
		[checkBox setChecked:state];
		[self addSubview:checkBox];
		
		NSRect tvFrame = {checkBoxFrame.size.width+2.0,0,frame.size.width-checkBoxFrame.size.width,frame.size.height};
		tvFrame = NSInsetRect(tvFrame, XMargin, YMargin);
		textView = [[SPRTodoItemTextView alloc] initWithFrame:tvFrame textContainer: tc];
		[textView setParentList:parentList];
		[textView setDrawsBackground:NO];
		[textView setHorizontallyResizable:NO];
		[textView setVerticallyResizable:YES];
		[self addSubview:textView];
		[textView setPostsFrameChangedNotifications:YES];
		[textView setMinSize:NSMakeSize(tvFrame.size.width,24-2*YMargin)];
		[textView setMaxSize:NSMakeSize(CGFLOAT_MAX,CGFLOAT_MAX)];
		[textView setAutoresizingMask:NSViewWidthSizable];
		parentList=p;
		
		//register for frame did change notifications for the text view
		[[NSNotificationCenter defaultCenter] addObserver:self
												selector:@selector(textViewFrameDidChange:)
												name:NSViewFrameDidChangeNotification
												object:textView];
		
		[[NSUserDefaults standardUserDefaults] addObserver:self
												forKeyPath:DefaultsPreferencesStrikeOutCheckedItem
												   options:0
												   context:NULL];
		
		[[tc layoutManager] setDelegate:self];
	}
	return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	[self setNeedsDisplay:YES];
}

-(void)setState:(BOOL)s
{
	[checkBox setChecked:s];
}

static BOOL InFrameChange=NO;

-(void)textViewFrameDidChange:(NSNotification*)note
{
    if (InFrameChange)
        return;
    
    InFrameChange=YES;
	NSRect frame = [self frame];
	frame.size.height = [textView frame].size.height;
	[self setFrame:frame];
	[parentList itemDidResize:self];
    InFrameChange=NO;
}

-(CGFloat)necessaryHeight
{
	[textView sizeToFit];
	return [textView frame].size.height+2*YMargin;
}

- (SPRTodoItemTextView *)textView
{
    return textView;
}

- (void)setTextView:(SPRTodoItemTextView *)value
{
    if (textView != value)
	{
        textView = [value copy];
    }
}

-(SPRTodoListView*)parentList
{
	return parentList;
}

-(void)dealloc
{
	[[NSUserDefaults standardUserDefaults] removeObserver:self forKeyPath:DefaultsPreferencesStrikeOutCheckedItem];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark NSLayoutManagerDelegate

- (NSDictionary *)layoutManager:(NSLayoutManager *)layoutManager shouldUseTemporaryAttributes:(NSDictionary *)attrs forDrawingToScreen:(BOOL)toScreen atCharacterIndex:(NSUInteger)charIndex effectiveRange:(NSRangePointer)effectiveCharRange
{
	if (!toScreen)
		return nil; //default Cocoa behavior
	
	if (StrikeOut && [checkBox isChecked])
	{
		if (charIndex==0 &&
			[[[layoutManager textStorage] string] hasPrefix:SPRInvisibleAttributeHolder])
		{
			//effectiveCharRange->location++;
			effectiveCharRange->length=1;
			return attrs;
		}
				
		NSMutableDictionary *modAttrs = [attrs mutableCopy];
		
		[modAttrs setObject:[NSNumber numberWithUnsignedInt:NSUnderlineStyleThick] forKey:NSStrikethroughStyleAttributeName];
		//[modAttrs setObject:[NSColor redColor] forKey:NSStrikethroughColorAttributeName];
		return modAttrs;
	}
	else
	{
		return attrs;
	}
}

@end

@implementation NSView (SPRTodoItemView)

-(SPRTodoItemView*)parentTodoItemView
{
	id v = [self superview];
	
	do
	{
		if ([v isKindOfClass:[SPRTodoItemView class]])
			return v;
	} while ((v = [v superview]));
	return nil;
}

@end
