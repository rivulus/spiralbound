//
//  SPRMultipleNotebookPurchaseAppeal.m
//  SpiralBound Pro
//
//  Created by Philip White on 4/9/18.
//

#import "SPRMultipleNotebookPurchaseAppeal.h"
#import "SPRInformationPopover.h"
#import "SPRProgressWindow.h"

@interface SPRMultipleNotebookPurchaseAppeal ()
{
	IBOutlet NSButton *_tipButton;
	IBOutlet NSButton *_bigTipButton;
	IBOutlet NSButton *_extraBigTipButton;
	SPRPurchaseAppealResult _result;
	SPRPurchaseAppealResultCallback _completion;
}

- (IBAction)makePurchase:(id)sender;

@end

@implementation SPRMultipleNotebookPurchaseAppeal

- (instancetype)init
{
	self = [self initWithWindow:nil];
	
	return self;
}

- (void)showAsModalWithCompletion:(SPRPurchaseAppealResultCallback)appealCompletion
{
	_completion = appealCompletion;
	
	[self setButtonTitles];
	NSWindow *window = [self window];
	[window center];
	[window makeKeyAndOrderFront:nil];
}

- (NSNibName)windowNibName
{
	return @"SPRMultipleNotebooksPurchaseAppeal";
}

- (void)closeSheet
{
	NSWindow *window = [self window];
	NSWindow *sheetWindow = [[window sheets] firstObject];
	
	if (sheetWindow != nil)
	{
		[window endSheet:sheetWindow];
	}
}

- (void)reset
{
	[self closeSheet];
}

- (void)finish
{
	_completion = nil;
	NSWindow *window = [self window];
	NSWindow *sheetWindow = [[window sheets] firstObject];
	
	if (sheetWindow != nil)
	{
		[window endSheet:sheetWindow];
	}
	
	[window close];
}

- (void)callCompletion
{
	if (_completion != nil)
	{
		_completion(_result);
	}
}

- (void)showProgressIndicator
{
	SPRProgressWindow *progress = [[SPRProgressWindow alloc] init];
	NSWindow *progressWindow = [progress window];
	
	[[self window] beginSheet:progressWindow completionHandler:nil];
}

- (IBAction)makePurchase:(id)sender
{
	if (sender == _tipButton)
	{
		_result = SPRPurchaseAppealTip;
	} else if (sender == _bigTipButton)
	{
		_result = SPRPurchaseAppealBigTip;
	} else if (sender == _extraBigTipButton)
	{
		_result = SPRPurchaseAppealExtraBigTip;
	}
	[self showProgressIndicator];
	[self callCompletion];
}

- (void)setTipPrice:(NSString *)price
{
	_tipPrice = [price copy];
}

- (void)setBigTipPrice:(NSString *)price
{
	_bigTipPrice = [price copy];
}

- (void)setExtraBigTipPrice:(NSString *)price
{
	_extraBigTipPrice = [price copy];
}

- (void)setButtonTitles
{
	[self window]; // Load connections
	NSString *titleFormat = NSLocalizedString(@"TipButtonTitle", nil);
	NSString *title = [NSString stringWithFormat:titleFormat, _tipPrice];
	[_tipButton setTitle:title];
	
	titleFormat = NSLocalizedString(@"BigTipButtonTitle", nil);
	title = [NSString stringWithFormat:titleFormat, _bigTipPrice];
	[_bigTipButton setTitle:title];
	
	titleFormat = NSLocalizedString(@"ExtraBigTipButtonTitle", nil);
	title = [NSString stringWithFormat:titleFormat, _extraBigTipPrice];
	[_extraBigTipButton setTitle:title];
}

@end
