//
//  SPRTextView.h
//  SpiralBound
//
//  Created by Philip White on 2/12/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

//#import <QTKit/QTKit.h>

#import <Cocoa/Cocoa.h>
@class SPRLockableTextStorage, SPRCalculatorHandler;
@interface SPRTextView : NSTextView <NSTextViewDelegate>
{
	SPRCalculatorHandler *calculatorHandler;
	
	BOOL hasPotentialEquation;
	NSRange potentialEquationRange;
	NSUInteger potentialEquationRHSLength;
	
	
	NSImage *imageClipping; //this is cached between writeablePasteboardTypes and writeSelectionToPasteboard
	
	NSButton *lockButton;
	NSTextStorage *savedStorage;
}


//- (void)showFindIndicatorForRange:(NSRange)charRange;
/*- (BOOL)isScrambled;
- (void)setIsScrambled:(BOOL)value;
- (BOOL)isLocallyScrambled;
- (void)setIsLocallyScrambled:(BOOL)value;
- (void)processScrambling;*/
//- (void)setButtonAndMenuItem;
- (void)keyDownSkipCalculator:(NSEvent *)theEvent;
-(void)replaceTextStorage:(SPRLockableTextStorage*)newTS;



@end
