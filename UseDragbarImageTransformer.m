//
//  UseDragbarImageTransformer.m
//  SpiralBound Pro
//
//  Created by Philip White on 5/22/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "UseDragbarImageTransformer.h"


@implementation UseDragbarImageTransformer

+ (void)initialize
{
	NSImage *image = [NSImage imageNamed:@"RegularSample.tiff"];
	[image setName:@"RegularSample"];
	
	image = [NSImage imageNamed:@"SimpleSample.tiff"];
	[image setName:@"SimpleSample"];
}

+ (Class)transformedValueClass
{
	return [NSImage class];
}

+ (BOOL)allowsReverseTransformation { return NO; }

- (id)transformedValue:(id)value
{
    if ([value boolValue] == YES)
		return [NSImage imageNamed:@"SimpleSample"];
	else
		return [NSImage imageNamed:@"RegularSample"];
}


@end
