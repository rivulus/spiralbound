//
//  SBController.m
//  SpiralBound
//
//  Created by Philip White on 2/2/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "SBController.h"
#import "SPRInAppPurchases.h"
#import "SPROrganizer.h"
#import "SPROrganizer+Security.h"
#import "SpiralBoundWindow.h"
#import "SPRNotebook.h"
#import "SPRPage.h"
#import "SBProtoFrameView.h"
#import "SBWFrameView.h"
#import "SPRTextView.h"
#import "SBConstants.h"
#import "SBController+Actions.h"

#import "SPRPreferencesController.h"

#import "StringIsNotEmptyTransformer.h"
#import "UseDragbarTransformer.h"
#import "UseDragbarImageTransformer.h"
#import "DoesNotEqualTwoTransformer.h"
#import "PrecisionTransformer.h"
#import "DateTransformer.h"
#import "ValueTransformers.h"
#import "Inquisitor.h"

#import "NewNotebookController_SBPro.h"
#import "PageSorter_SBPro.h"

#import <ExceptionHandling/ExceptionHandlingDefines.h>
#import <ExceptionHandling/NSExceptionHandler.h>
#import "ErrorController.h"
#import "SBServices.h"

#import "Exporter.h"
#import "DefaultsKeys.h"
#import "NotificationNames.h"

SBController *gSBController;

const BOOL ShowPattern=NO;

BOOL Inactive=NO;
time_t LastEventTime=0;
time_t LastSavedTime;

NSTimer *UpdateTimer=nil, *GraceTimer=nil;

BOOL ShouldAnimate = YES;

static void DisplayReconfigurationCallback (CGDirectDisplayID display, u_int32_t flags, void *userInfo);
OSStatus QuitApplicationProcessWithPID(pid_t pid);

//#ifdef DEBUG
//	static NSTimeInterval LaunchTime;
//#endif


@implementation SBController

+ (void) initialize
{	
	if (self != [SBController class])
		return;
	
//#ifdef DEBUG
//	LaunchTime = [NSDate timeIntervalSinceReferenceDate];
//#endif
		
	//value transformers
	StringIsNotEmptyTransformer *stringIsNotEmptyTransformer = [[StringIsNotEmptyTransformer alloc] init];
	[NSValueTransformer setValueTransformer:stringIsNotEmptyTransformer forName:@"StringIsNotEmptyTransformer"];
	
	UseDragbarTransformer *useDragbarTransformer = [[UseDragbarTransformer alloc] init];
	[NSValueTransformer setValueTransformer:useDragbarTransformer forName:@"UseDragbarTransformer"];
	
	UseDragbarImageTransformer *useDragbarImageTransformer = [[UseDragbarImageTransformer alloc] init];
	[NSValueTransformer setValueTransformer:useDragbarImageTransformer forName:@"UseDragbarImageTransformer"];
	
	DoesNotEqualTwoTransformer *doesNotEqualTwoTransformer = [[DoesNotEqualTwoTransformer alloc] init];
	[NSValueTransformer setValueTransformer:doesNotEqualTwoTransformer forName:@"DoesNotEqualTwoTransformer"];
	
	PrecisionTransformer *precisionTransformer = [[PrecisionTransformer alloc] init];
	[NSValueTransformer setValueTransformer:precisionTransformer forName:@"PrecisionTransformer"];
	
	DateTransformer *dateTransformer = [[DateTransformer alloc] init];
	[NSValueTransformer setValueTransformer:dateTransformer forName:@"DateTransformer"];
	
	CheckIntervalTransformer *checkIntervalTransformer = [[CheckIntervalTransformer alloc] init];
	[NSValueTransformer setValueTransformer:checkIntervalTransformer forName:@"CheckIntervalTransformer"];
	
	[SPRPreferencesController setupDefaults];
}

- (id)init
{
	self = [super init];
	if (self)
	{
		gSBController = self;
	}
	
	return self;
}

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark NSApplicationDelegate


-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //remove the 2nd,third,and fourth items having to do with updates and registration from the application menu
    NSMenu *appMenu = [[[NSApp mainMenu] itemAtIndex:0] submenu];
    [appMenu removeItemAtIndex:1];
    [appMenu removeItemAtIndex:1];
    [appMenu removeItemAtIndex:1];
	
	NSUserDefaults *stud = [NSUserDefaults standardUserDefaults];
	
	//register for display configuration callbacks
	CGDisplayRegisterReconfigurationCallback(DisplayReconfigurationCallback, NULL);
	
	//used to be in +initialize
	{
		LastSavedTime = time(0);
		
		//make sure preferences sets up before anything else
		//[SBConstants setupConstants];			//this class now uses the +load method instead
		[SPRPreferencesController sharedPreferencesController];
	}
	
	//used to be in -init
	{
#if EXCEPTIONS_ON
	#ifndef DEBUG
		[[NSExceptionHandler defaultExceptionHandler] setDelegate:[ErrorController sharedController]];
		[[NSExceptionHandler defaultExceptionHandler] setExceptionHandlingMask:NSLogUncaughtExceptionMask+NSHandleUncaughtExceptionMask];
	#endif	
#endif
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(notebookTitleDidChange:)
													 name:NotebookTitleDidChangeNotification
												   object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(notebookCountDidChange:)
													 name:NotebookCountDidChangeNotification
												   object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(notebookDidHide:)
													 name:NotebookDidHideNotification
												   object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(notebookDidBecomeKey:)
													 name:NotebookDidBecomeKeyNotification
												   object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(notebookDidResignKey:)
													 name:NotebookDidResignKeyNotification
												   object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(windowDidBecomeMain:)
													 name:NSWindowDidBecomeMainNotification
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(windowDidResignMain:)
													 name:NSWindowDidResignMainNotification
												   object:nil];
												   
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(pageSecurityDidChange:)
													 name:PageSecurityDidChangeNotification
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(mainWindowPageDidChange:)
													 name:MainWindowPageDidChangeNotification
												   object:nil];
	}


#ifdef DEBUG
	[[NSBundle bundleWithPath:@"/Library/Frameworks/FScript.framework"] load];
	[NSClassFromString(@"FScriptMenuItem") performSelector: @selector(insertInMainMenu)];
	
	[[NSBundle bundleWithPath:@"/Library/Frameworks/Nu.framework"] load];
	[[NSClassFromString(@"Nu") performSelector:@selector(parser)]
	 performSelector:@selector(parseEval:) withObject:@"(load \"console\")"];
	[NSClassFromString(@"NuConsoleWindowController") new];
	
#endif 

	#ifdef BETA
		const NSTimeInterval originTime = 265829112.0;
		const NSTimeInterval betaInterval = 24.0*60.0*60.0*30.0;
		
		NSTimeInterval interval = [[NSDate date] timeIntervalSinceReferenceDate];
		
		if (interval < originTime)
		{
			NSRunAlertPanel(@"Beta Version Alert",@"Your clock appears to be set incorrectly.",@"Quit", nil, nil);
			ExitToShell();
		}
		else if (interval > originTime+betaInterval)
		{
			NSRunAlertPanel(@"Beta Version Alert",@"This beta version has expired.",@"Quit", nil, nil);
			ExitToShell();
		}
		else
		{
			NSRunAlertPanel(@"Beta Version Alert", @"This beta version will expire in %u days", @"OK", nil, nil, (NSUInteger)(((originTime+betaInterval)-interval)/(60.0*60.0*24.0)));
		}
		
	#endif

	LastEventTime = time(NULL); //just to make sure this always has a value
	
	//before loading data, let's check to see if the user requires as password at launch
	if ([stud boolForKey:DefaultsPreferencesRequirePasswordAtLaunch])
	{
		if ([[Inquisitor sharedInquisitor] challenge] == NO)
		{
			exit(0);
			return;
		}
	}
	
	//before loading data, check to see if the user is still using non keychain password
	if ([stud boolForKey:@"UseKeychain"] == NO)
	{
		[[SPRPreferencesController sharedPreferencesController] switchPasswordToKeychain];
	}
	
/*DATA GETS LOADED HERE*/
	[SPROrganizer organizers]; //call this to get it to initialize
	
	[self startInactivityTimer];
	[self startSaveTimer];
	
	//check to see if the user has been given the option about importing data from SpiralBound and Stickies
	{
		
		if ([stud boolForKey:@"UserHasBeenAskedToImportData"] == NO)
		{
			[stud setBool:YES forKey:@"UserHasBeenAskedToImportData"];
			
			NSString *titleString = @"Data Available to Import",
			*contentString=nil;
			BOOL sbDataExists = [SPRNotebook originalSBDataExists], stickiesDataExists = [SPRNotebook stickiesDataExists];
			
			if (sbDataExists && stickiesDataExists)
			{
				contentString = @"You appear to have data from both Stickies and SpiralBound that can be imported into SpiralBound Pro. Choose \"Import SpiralBound Data\" or \"Import Stickies Data\" from the File Menu to import this data.";
			}
			else if (sbDataExists)
			{
				contentString = @"You appear to have data from SpiralBound that can be imported into SpiralBound Pro. Choose \"Import SpiralBound Data\" from the File Menu to import this data.";
			}
			else if (stickiesDataExists)
			{
				contentString = @"You appear to have data from Stickies that can be imported into SpiralBound Pro. Choose \"Import Stickies Data\" from the File Menu to import this data.";
			}
			
			if (sbDataExists || stickiesDataExists)
				NSRunAlertPanel(titleString, contentString, @"OK", nil, nil);
		}
		
	}

	[[NSColorPanel sharedColorPanel] setShowsAlpha:YES];
	
	//register for Services now
	[NSApp setServicesProvider:[SBServices sharedProvider]];
	NSUpdateDynamicServices();
	
	[[NSNotificationCenter defaultCenter] addObserverForName:SPRInAppPurchaseTipJarIsAvailable object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
		[_tipJarMenuItem setEnabled:YES];
		[_tipJarMenuItem setTarget:self];
		[_tipJarMenuItem setAction:@selector(showTipJar:)];
	}];
	
	[[SPRInAppPurchases sharedInstance] run];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
	NSEnumerator *enumerator = [[SPRNotebook notebooks] objectEnumerator];
	SPRNotebook *n;
	while (n = [enumerator nextObject])
	{
		[[n pages] makeObjectsPerformSelector:@selector(autotitle)];
	}
	
	[SPROrganizer saveOrganizers];
	[[SPRPreferencesController sharedPreferencesController] applicationWillTerminate];
}

-(BOOL)application:(NSApplication *)theApplication openFile:(NSString *)filename
{
    NSLog(@"application:openfile:");
	//since this might be called before the application delegate has been sent -applicationDidFinishLaunch,
	//have this performed once the run loop is really going
	//of course we need to copy filename so it doesn't get autoreleased before that message is sent
    NSString *copiedFName = [filename copy];
	
	id target = [NewNotebookController sharedController];
	
	[target performSelector:@selector(importSavedPagesFromFile:) withObject:copiedFName afterDelay:0.0];
	
	//lets autorelease this later, much later
	//[copiedFName performSelector:@selector(autorelease) withObject:nil afterDelay:60.0];
	//always return YES, risky?
	return YES;
}

#pragma mark NOTIFICATIONS

- (void)notebookCountDidChange:(NSNotification*)notification
{
	[self updateNotebookMenu];
	
	if (![[SPROrganizer notebookTitles] containsObject:
		  [[NSUserDefaults standardUserDefaults] objectForKey:DefaultsPreferencesServicesDefaultNotebookNameKey]])
	{
		[[NSUserDefaults standardUserDefaults] removeObjectForKey:DefaultsPreferencesServicesDefaultNotebookNameKey];
		[[NSUserDefaults standardUserDefaults] setBool:NO forKey:DefaultsPreferencesServicesAlwaysUseNotebookKey];
	}
}

- (void)notebookTitleDidChange:(NSNotification*)notification
{
	//the title changed on the notebook set as default in services
	if ([[[notification userInfo] objectForKey:@"OldTitle"] isEqualToString:
		 [[NSUserDefaults standardUserDefaults] objectForKey:DefaultsPreferencesServicesDefaultNotebookNameKey]])
	{
		[[NSUserDefaults standardUserDefaults] setObject:[[notification userInfo] objectForKey:@"NewTitle"]
												  forKey:DefaultsPreferencesServicesDefaultNotebookNameKey];
	}
}

- (void)notebookDidHide:(NSNotification*)notification
{
	SPROrganizer *organizer = [notification object];
	NSString *title = [organizer title];
	
	NSMenuItem *item = [notebookMenu itemWithTitle:title];
	
	if (item == nil)
		return;			//this means that the item hasn't been created yet
	
	NSMutableAttributedString *attrTitle = [[NSMutableAttributedString alloc] initWithString: [item title]];
	
	NSRange range = {0,[attrTitle length]};
	NSFont *italicFont = [[NSFontManager sharedFontManager] convertFont: [NSFont fontWithName:@"Helvetica" size:14.0] toHaveTrait: NSItalicFontMask]; 
	[attrTitle addAttribute: NSFontAttributeName value: italicFont range: range];
	
	[item setAttributedTitle:attrTitle];
	
}


-(void)notebookDidBecomeKey:(NSNotification *)notification
{
	SPROrganizer *organizer = [notification object];
	NSString *title = [organizer title];
	
	NSMenuItem *item = [notebookMenu itemWithTitle:title];
	
	if (item == nil)
		return;			//this means that the item hasn't been created yet
	
	NSMutableAttributedString *attrTitle = [[NSMutableAttributedString alloc] initWithString: [item title]];
	
	NSRange range = {0,[attrTitle length]};
	NSFont *boldFont = [[NSFontManager sharedFontManager] convertFont: [NSFont menuFontOfSize:14.0] toHaveTrait: NSBoldFontMask];
	
	
	[attrTitle addAttribute: NSFontAttributeName value: boldFont range: range];
	
	[item setAttributedTitle:attrTitle];
	
}

-(void)notebookDidResignKey:(NSNotification *)notification
{
	SPROrganizer *organizer = [notification object];
	NSString *title = [organizer title];
	
	NSMenuItem *item = [notebookMenu itemWithTitle:title];
	
	if (item == nil)
		return;			//this means that the item hasn't been created yet
	
	[item setAttributedTitle:nil];
}



- (void)updateNotebookMenu
{
	//start by deleting all items after the delimiter
	NSEnumerator *enumerator = [[notebookMenu itemArray] reverseObjectEnumerator];
	NSMenuItem *item;
	
	while (item = [enumerator nextObject])
	{
		if ([item isSeparatorItem])
			break;
	}
	
	NSUInteger firstItemIndex = [notebookMenu indexOfItem:item]+1;
	NSUInteger iterations = [notebookMenu numberOfItems]-firstItemIndex;
	
	while (iterations--)
		[notebookMenu removeItemAtIndex:firstItemIndex];
	
	//now add the new items
	enumerator = [[SPROrganizer menuItemsForOrganizers] objectEnumerator];
	
	while (item = [enumerator nextObject])
		[notebookMenu addItem: item];
}

-(void)pageSecurityDidChange:(NSNotification*)notification
{
	SPRPage *page = [notification object];
	
	NSWindow *window = [[SPROrganizer organizerForPage:page] windowForPage:page];
	
	if ([window isKeyWindow])
	{
		[self setSecureMenuItemTitleForPage:page];
	}
}

-(void)mainWindowPageDidChange:(NSNotification*)notification
{
	SPROrganizer *org = [SPROrganizer organizerForNotebook:[notification object]];
	
	NSWindow *window = [org mainWindow];
	
	if ([window isKeyWindow])
	{
		[self setSecureMenuItemTitleForPage:[org mainWindowPage]];
	}
}

-(void)setSecureMenuItemTitleForPage:(SPRPage*)page
{
	NSString *title;
	if ([page isSecure])
		title = [NSString stringWithFormat:@"Unlock Page \"%@\"", [page title]];
	else
		title = [NSString stringWithFormat:@"Lock Page \"%@\"", [page title]];
	[secureMenuItem setTitle:title];
}

-(void)windowDidBecomeMain:(NSNotification*)note
{
	SpiralBoundWindow *window = (SpiralBoundWindow*)[note object];
	
	if (![window isKindOfClass:[SpiralBoundWindow class]])
		return;
	
	[floatMenuItem setState:[(SpiralBoundWindow*)window isFloater]];
	
	SPRPage *page = [[SPROrganizer organizerForWindow:[note object]] pageForWindow:[note object]];
	if (page)
		[self setSecureMenuItemTitleForPage:page];
}

-(void)windowDidResignMain:(NSNotification*)note
{
	[floatMenuItem setState:NSOffState];
}


#pragma mark SECURITY & INACTIVITY

-(void)applicationDidBecomeActive:(NSNotification*)aNotification
{
	Inactive = NO;
	if ([self allPagesAreSecured])
	{
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		BOOL requirePassword = [defaults boolForKey: @"NeedsPasswordToUnscramble"];
		
		if (requirePassword == YES)
		{
			BOOL validated = [[Inquisitor sharedInquisitor] challengeWithoutCancel];
			
			if (validated == NO)
				return;
		}
		
		[SPROrganizer unlockAllPages];
		[self setAllPagesAreSecured:NO];
	}
}


- (void)applicationDidResignActive:(NSNotification *)aNotification
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	BOOL shouldScramble = [defaults boolForKey:@"ShouldScramble"];
	BOOL scrambleImmediately = [defaults boolForKey:@"ScrambleImmediately"];
	
	if (shouldScramble && scrambleImmediately)
	{
		[SPROrganizer lockAllPages];
		[self setAllPagesAreSecured:YES];
	}
	
	if ([[Inquisitor sharedInquisitor] isChallenging])
		[[Inquisitor sharedInquisitor] stopChallenging];
}


- (BOOL)allPagesAreSecured
{
    return allPagesAreSecured;
}

- (void)setAllPagesAreSecured:(BOOL)value
{
    if (allPagesAreSecured != value)
	{
        allPagesAreSecured = value;
    }
}

-(void)startInactivityTimer
{
	[NSTimer scheduledTimerWithTimeInterval: 10 target: self selector: @selector(monitorInactivity:) userInfo:nil repeats:YES];	
}

-(void)monitorInactivity:(id)userinfo
{
	NSUInteger timeElapsed = time(0)-LastEventTime;
	
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	
	if (timeElapsed >= [userDefaults integerForKey:@"ScramblingDelay"]*60)
	{
		Inactive=YES;
		
		if ([userDefaults boolForKey: @"ShouldScramble"])
		{
			[SPROrganizer lockAllPages];
			[self setAllPagesAreSecured:YES];
		}
	}
}

- (BOOL)canImportStickies
{
	return [SPRNotebook stickiesDataExists];
}

- (BOOL)canImportSpiralBound
{
	return [SPRNotebook originalSBDataExists];
}

#pragma mark autosave
- (void)startSaveTimer
{
	[NSTimer scheduledTimerWithTimeInterval:20 target:self selector:@selector(saveIfNeeded:) userInfo:nil repeats:YES];
}

- (void)saveIfNeeded:(id)userInfo
{
	if (HasUnsavedChanges())
	{
		[SPROrganizer saveOrganizers];
		SetUnsavedChanges(NO);
	}
}



-(BOOL)validateMenuItem:(NSMenuItem *)menuItem
{
	NSString *title = [menuItem title];
	if ([title isEqualToString:@"Email Quickly"])
	{
		return [[NSUserDefaults standardUserDefaults] boolForKey:@"UserCanEmailQuickly"];
	}
	else if ([title isEqualToString:@"Import From Stickies…"])
	{
		return [self canImportStickies];
	}
	else if ([title isEqualToString:@"Import From SpiralBound…"])
	{
		return [self canImportSpiralBound];
	}
	else
	{
		return YES;
	}

}

@end

static void DisplayReconfigurationCallback (CGDirectDisplayID display, u_int32_t flags, void *userInfo)
{
	if (!(flags & kCGDisplayBeginConfigurationFlag)) //we're only interested when configuration has already changed
	{
		//post a custom notification
		//not that for each display event this notification will likely get posted once for each display
		//but the notification queue will by defaults coalesce notifications with identical objects and names
		NSNotification *note = [NSNotification notificationWithName:SPRDisplayConfigurationDidChange object:gSBController];
		[[NSNotificationQueue defaultQueue] enqueueNotification:note
												   postingStyle:NSPostWhenIdle];
	}
}

OSStatus QuitApplicationProcessWithPID(pid_t pid)
{
    AppleEvent evt, res;
    AEDesc errDesc;
    OSStatus err;
	
    // build and send a 'quit' event
    err = AEBuildAppleEvent(kCoreEventClass, kAEQuitApplication,
                            typeKernelProcessID,
                            &pid, sizeof(pid),
                            kAutoGenerateReturnID,
                            kAnyTransactionID,
                            &evt, NULL, "");
    if (err) return err;
	err = AESendMessage(&evt, &res, kAEWaitReply, kAEDefaultTimeout);
	AEDisposeDesc(&evt);
    // note: process may quit without replying
	if (err == connectionInvalid) return noErr;
	if (err) return err;
	// check if reply event contains an error number, e.g. userCanceledErr
	err = AEGetParamDesc(&res, keyErrorNumber, typeSInt32, &errDesc);
	if (err == noErr) {
		AEGetDescData(&errDesc, &err, sizeof(err));
		AEDisposeDesc(&res);
    } else if (err == errAEDescNotFound)
		err = noErr;
    return err;
}

void ShowEasterEggForColor(NSColor *color)
{
	NSString *text=nil, *button;
	
	if ([color isEqualTo:SBBoldPink])
	{
		text = @"Why settle for pale pink when you can have robust pink, pink in the extreme, a really pink pink, or punk pink, if you will?  Pink should not be a pale shade of red but rather a unique pink personified by a color conscious public not willing to settle for a lesser shade of sameness but a unique shade eccentric in its execution and bold in its expression.";
		button = @"What a peculiar thing to say!";
	}
	else if ([color isEqualTo:SBBoldRed])
	{
		text = @"Red is the color that is bold enough and bright enough that only the most powerful among us will dare to drape themselves in it.";
		button = @"How odd!";
	}
	else if ([color isEqualTo:SBBoldGreen])
	{
		text = @"Ain't it grand to be green, to paraphrase a song by Kermit.  Green is not the color of mean, as in greenie meanie but the color of grass and growth and grinches.  Well, maybe not grinches, but you get the idea.";
		button = @"Curious, isn't it?";
	}
	else if ([color isEqualTo:SBBoldYellow])
	{
		text = @"Yellow is the color of Mellow.  In the case of the mellow mushroom, mellow means a mind meandering in mush - the mush of too many mushrooms!";
		button = @"Hmmmmmm.";
	}
	if (text != nil)
	{
		NSRunAlertPanel(@"Hello! What's this?", text, button, nil, nil);
	}
}

//GetUnregisteredPattern(NSColor *color)
#define INSET 6
NSColor *Zfj49fjl3EijP(NSColor *mainColor)
{
	static NSColor *cachedPattern = nil;
	static NSColor *cachedForColor = nil;
	
	if ([cachedForColor isEqualTo:mainColor]) //behaves correctly if cachedForColor == nil
		return cachedPattern;
	
	
	NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"Unregistered Unregistered"];
	NSRange range = {0, 25}, range1 = {0,12}, range2 = {13,12};
	NSColor *color1 = [[NSColor whiteColor] blendedColorWithFraction:0.4 ofColor:[NSColor redColor]];
	NSColor *color2 = [[NSColor whiteColor] blendedColorWithFraction:0.4 ofColor:[NSColor cyanColor]];
	
	[string addAttribute: NSFontAttributeName value: [NSFont userFontOfSize:22.0] range: range];
	[string addAttribute: NSForegroundColorAttributeName value: color1 range: range1];
	[string addAttribute: NSForegroundColorAttributeName value: color2 range: range2];
	
	NSSize imageSize;
	imageSize = [string size];
	
	imageSize.width += 2*INSET;
	imageSize.height += 2*INSET;
	
	NSImage *image = [[NSImage alloc] initWithSize:imageSize];
	
	[image lockFocus];
	{
		[mainColor set];
		NSRectFill(NSMakeRect(0,0,imageSize.width, imageSize.height));
		
		[string drawAtPoint: NSMakePoint(INSET,INSET)];
	}
	[image unlockFocus];
	
	cachedPattern = [NSColor colorWithPatternImage:image];
	cachedForColor = mainColor;
	
	
	return cachedPattern;
}
#undef INSET
