//
//  SPRTodoListView.m
//  CocoaJunk
//
//  Created by Philip on 5/20/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import "SPRTodoListView.h"
#import "SPRTodoItemView.h"
#import "SPRTodoItemTextView.h"
#import "SBConstants.h"
#import "SPRLockableTextStorage.h"
#import "SPRTodoItemCheckbox.h"

#define YMargin 4

NSString *SPRPasteboardTypeTodoItem = @"SPRPasteboardTypeTodoItem";

@implementation SPRTodoListView

- (NSArray*)supportedDragTypes
{
	static NSArray *types=nil;
	if (types == nil)
	{
		types = [[NSArray alloc] initWithObjects:SPRPasteboardTypeTodoItem,
												 NSRTFDPboardType,
												 NSRTFPboardType,
												 NSStringPboardType,
												 nil];
	}
	return types;
}

- (NSArray*)supportedDropTypes
{
	static NSArray *types=nil;
	if (types == nil)
	{
		types = [[NSArray alloc] initWithObjects:SPRPasteboardTypeTodoItem,nil];
	}
	return types;
}

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
	{
		items = [NSMutableArray new];
		textStorages = [NSMutableArray new];
		layoutManagers = [NSMutableArray new];
		dropIndex=UINT32_MAX;
		[self registerForDraggedTypes: [self supportedDropTypes]];
		//fillerView = [[SPRTodoFillerView alloc] initWithTodoListView:self];
		//[self setPostsFrameChangedNotifications:YES];
		
    }
    return self;
}

-(void)addTextStorage:(NSTextStorage*)newStorage state:(BOOL)s
{
	[textStorages addObject:newStorage];
	
	NSLayoutManager *layoutManager;
	layoutManager = [[NSLayoutManager alloc] init];
	[newStorage addLayoutManager:layoutManager];
	
	NSRect cFrame = NSMakeRect(0,0,[self bounds].size.width,CGFLOAT_MAX);
	NSTextContainer *container;
	
	container = [[NSTextContainer alloc]
				 initWithContainerSize:cFrame.size];
	[container setWidthTracksTextView:YES];
	[layoutManager addTextContainer:container];
	[layoutManagers addObject:layoutManager];
	
	SPRTodoItemView *item = [[SPRTodoItemView alloc] initWithFrame:cFrame
													 textContainer:container
															 state:s
														parentList:self];
	[items addObject:item];
	
	[item setAutoresizingMask:NSViewWidthSizable];
	
	[self addSubview:item];
	[self sizeAndPlaceItems];
	NSResponder *fr = [[self window] firstResponder];
	if ([fr isKindOfClass:[NSView class]] && [(NSView*)fr isDescendantOf:self])
		[[self window] makeFirstResponder:[item textView]];
}

-(void)setState:(BOOL)s forTextStorage:(NSTextStorage*)storage
{
	NSUInteger i = [textStorages indexOfObjectIdenticalTo:storage];
	
	if (i==NSNotFound)
		[NSException raise:@"SPRTodoListView" format:@"Could not find textStorage in setState:forTextStorage:"];
	
	[(SPRTodoItemView*)[items objectAtIndex:i] setState:s];
	[[items objectAtIndex:i] setNeedsDisplay:YES];
}


-(BOOL)isFlipped {return YES;}

-(BOOL)becomeFirstResponder
{
	//select the first item
	[[self window] makeFirstResponder:[[items objectAtIndex:0] textView]];
	
	return YES;
}

-(void)sizeAndPlaceItems
{
	CGFloat y=YMargin;
	CGFloat width = [self bounds].size.width;
	for (SPRTodoItemView *view in items)
	{
		CGFloat height = [view necessaryHeight];
		[view setFrame:NSMakeRect(0,y,width,height)];
		y+=height;
	}
	
	//now set self's frame height to y;
	NSRect frame = [self frame];
	frame.size.height=y+2*YMargin;
	
	float minY = [[self enclosingScrollView] contentSize].height;
	if (frame.size.height < minY)
		frame.size.height = minY;
	
	[self setFrame:frame];
	[self setNeedsDisplay:YES];
}

-(void)itemDidResize:(SPRTodoItemView*)item
{
	if (ignoreResize)
		return;
	
	CGFloat width = [self bounds].size.width;
	CGFloat y;
	NSUInteger itemIndex;
	if (item == nil)
	{
		y=YMargin;
		itemIndex=0;
	}
	else
	{
		itemIndex = [items indexOfObjectIdenticalTo:item];
		if (itemIndex == 0)
		{
			y=YMargin;
		}
		else
		{
			NSRect prevF = [[items objectAtIndex:itemIndex-1] frame];
			y = prevF.origin.y+prevF.size.height;
		}
	}
	
	for (;itemIndex<[items count];itemIndex++)
	{
		SPRTodoItemView *item = [items objectAtIndex:itemIndex];
		NSRect newFrame = {0,y,width,[item necessaryHeight]};
		[item setFrame:newFrame];
		y += newFrame.size.height;
	}
	
	//now set self's frame height to y;
	NSRect frame = [self frame];
	frame.size.height=y+2*YMargin;
	
	float minY = [[self enclosingScrollView] contentSize].height;
	if (frame.size.height < minY)
		frame.size.height = minY;
	
	[self setFrame:frame];
	[self setNeedsDisplay:YES];
}

-(BOOL)isOpaque
{
	return YES;
}

-(void)mouseDown:(NSEvent*)event
{
	NSPoint local_point = [self convertPoint:[event locationInWindow] fromView:nil];
	NSRect lowestItemFrame = [[items lastObject] frame];
	
	//if there is a click below the last item, select the last item
	if (local_point.y >= lowestItemFrame.origin.y+lowestItemFrame.size.height)
	{
		NSTextView *tv = [[items lastObject] textView];
		[[self window] makeFirstResponder: tv];
		NSTextStorage *ts = [textStorages lastObject];
		[tv setSelectedRange:NSMakeRange([ts length],0)];
	}
}

- (void)drawRect:(NSRect)dirtyRect
{
	//fill in the top and bottom margins
	NSRect bounds = [self bounds];
	[primaryColor set];
	NSRectFill(bounds);
	
	BOOL which=YES;
	for (id item in items)
	{
		if (which)
			[primaryColor set];
		else
			[secondaryColor set];
		which = !which;
		NSRectFill([item frame]);
	}
	

	if (dropIndex != UINT32_MAX)
	{
		//draw the drop-style line
		NSBezierPath *line = [NSBezierPath bezierPath];
		CGFloat y;
		
		if (dropIndex > [items count])
			dropIndex = [items count];
		
		if (dropIndex == 0)
		{
			y=YMargin;
		}
		else
		{
			NSRect r = [[items objectAtIndex:dropIndex-1] frame];
			y = r.origin.y+r.size.height;
		}
		[line setLineWidth:2.0];
		[line appendBezierPathWithOvalInRect:NSMakeRect(3,y-3,6,6)];
		[line moveToPoint:NSMakePoint(9,y)];
		[line lineToPoint:NSMakePoint([self bounds].size.width,y)];
		[[NSColor blueColor] set];
		[line stroke];
	}
}

#pragma mark NSDraggingDestination & Related

- (BOOL)wantsPeriodicDraggingUpdates {return YES;}

- (NSDragOperation)draggingEntered:(id < NSDraggingInfo >)sender
{
	NSPoint point = [self convertPoint:[sender draggingLocation] fromView:nil];
	NSDragOperation dragOp;
	if ([self setDropIndexFromPoint:point])
		dragOp = [sender draggingSourceOperationMask];
	else
		dragOp = NSDragOperationNone;
	
	[self unregisterTextViewDraggedTypes];
	[self setNeedsDisplay:YES];
	return dragOp;
}

- (NSDragOperation)draggingUpdated:(id < NSDraggingInfo >)sender
{
	NSPoint point = [self convertPoint:[sender draggingLocation] fromView:nil];
	NSDragOperation dragOp;
	if ([self setDropIndexFromPoint:point])
		dragOp = [sender draggingSourceOperationMask];
	else
		dragOp = NSDragOperationNone;
	
	
	[self setNeedsDisplay:YES];
	return dragOp;
}

- (void)draggingExited:(id < NSDraggingInfo >)sender
{
	dropIndex=UINT32_MAX;
	
	[self reregisterTextViewDraggedTypes];
	[self setNeedsDisplay:YES];
}

- (void)draggingEnded:(id < NSDraggingInfo >)sender
{
	dropIndex=UINT32_MAX;
	
	[self reregisterTextViewDraggedTypes];
	[self setNeedsDisplay:YES];
}

- (BOOL)performDragOperation:(id < NSDraggingInfo >)sender
{	
	NSPasteboard *pboard = [sender draggingPasteboard];
	NSString *bestType = [pboard availableTypeFromArray:[self supportedDropTypes]];
	
	//if (bestType == nil)
	//	return NO;
	
	NSData *data = [pboard dataForType:bestType];
	
	NSDragOperation dragOp = [sender draggingSourceOperationMask];
	id src = [sender draggingSource];
	
	NSUInteger srcIndex = [textStorages indexOfObjectIdenticalTo:[src dragSourceItemTextStorage]];
	BOOL interListDrag = srcIndex == NSNotFound;
	
	if ((dragOp & NSDragOperationCopy) || interListDrag)
	{
		//in this case don't move, create a new item
		[delegate todoListView:self writeDroppedData:data ofType:bestType toIndex:dropIndex];
		
		if (interListDrag && (dragOp & NSDragOperationMove)) //then delete the source
		{
			//delete the item
			[src deleteDragSourceItem];
		}
	}
	else //a move within one list
	{
		[delegate todoListView:self moveDroppedData:data ofType:bestType toIndex:dropIndex fromIndex:srcIndex];
	}

	dropIndex = UINT32_MAX;
	[self reregisterTextViewDraggedTypes];
	[src clearDragSourceItem];
	[self setNeedsDisplay:YES];
	
	return YES;
}


#pragma mark NSDraggingSource & NSPasteboardDelegate & Related
-(void)startDragForEvent:(NSEvent*)event todoItemView:(SPRTodoItemView*)tiv
{
	NSBitmapImageRep *imageRep = [[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
																		 pixelsWide:[self bounds].size.width
																		 pixelsHigh:[self bounds].size.height
																	  bitsPerSample:8
																	samplesPerPixel:4
																		   hasAlpha:YES
																		   isPlanar:NO
																	 colorSpaceName:NSCalibratedRGBColorSpace
																		bytesPerRow:4*[self bounds].size.width
																	   bitsPerPixel:4*8];
	
	
	[tiv cacheDisplayInRect:[self bounds] toBitmapImageRep:imageRep];
	
	
	NSImage *dragImage = [[NSImage alloc] initWithSize:[self bounds].size];
	[dragImage addRepresentation:imageRep];
	
	NSPoint dragPoint = [tiv frame].origin;
	dragPoint.y += [tiv frame].size.height;
	
	NSUInteger i = [items indexOfObjectIdenticalTo:tiv];
	if (i == NSNotFound)
		[NSException raise:@"SPRTodoListView" format:@"Could not find item"];
	
	NSPasteboard *pboard = [NSPasteboard pasteboardWithName:NSDragPboard];
	
	[pboard declareTypes:[self supportedDragTypes] owner:self];
	
	dragSourceItemTextStorage = [textStorages objectAtIndex:i];
	
	[self dragImage:dragImage
				 at:dragPoint
			 offset:NSZeroSize
			  event:event
		 pasteboard:pboard
			 source:self
		  slideBack:YES];
	
	//now clear the pboard to keep Cocoa from asking for the data at quits
	[pboard declareTypes:[NSArray array] owner:nil];
}


- (NSDragOperation)draggingSourceOperationMaskForLocal:(BOOL)isLocal
{
	if (isLocal)
	{
		if ([[NSApp currentEvent] modifierFlags] & NSAlternateKeyMask)
			return NSDragOperationCopy;
		else
			return NSDragOperationMove;
	}
	else
	{
		return NSDragOperationCopy;
	}
}


- (void)pasteboard:(NSPasteboard *)sender provideDataForType:(NSString *)type
{
	NSUInteger i = [textStorages indexOfObjectIdenticalTo:dragSourceItemTextStorage];
	if (i == NSNotFound)
	{
		[NSException raise:@"SPRTodoListViewException" format:@"Failed to locate source item in drag. -draggedImage:endedAt:…"];
		return;
	}
	
	NSData *d;
	
	d = [delegate todoListView:self dataForPBoardType:type forDragFromItemAtIndex:i];
	[sender setData:d forType:type];
}

/*END OF NSDraggingSource & NSPasteboardDelegate*/

- (BOOL)setDropIndexFromPoint:(NSPoint)point
{
	NSUInteger i;
	NSUInteger newDropIndex=UINT32_MAX;
	
	int c = [items count];
	
	if (c == 0)
	{
		newDropIndex=0;
	}
	else
	{
		NSRect lastItemFrame = [[items lastObject] frame];
		float itemsTop = [[items objectAtIndex:0] frame].origin.y,
			  itemsBottom = lastItemFrame.origin.y + lastItemFrame.size.height;
			  
		if (point.y < itemsTop)
		{
			newDropIndex = 0;
		}
		else if (point.y > itemsBottom)
		{
			newDropIndex = c;
		}
		else
		{
			for (i=0;i<c;i++)
			{
				NSRect r = [[items objectAtIndex:i] frame];
				if (NSPointInRect(point,r))
				{
					if (point.y < r.origin.y+r.size.height/2.0)
						newDropIndex=i;
					else
						newDropIndex=i+1;
					break;
				}
			}
		}
	}
	
	dropIndex=newDropIndex;
	
	return dropIndex != UINT32_MAX;
}

-(void)unregisterTextViewDraggedTypes
{
	if ([items count] > 0 && savedTextViewDragTypes==nil)
	{
		savedTextViewDragTypes = [[[[items objectAtIndex:0] textView] registeredDraggedTypes] copy];
		
		for (id item in items)
			[[item textView] unregisterDraggedTypes];
	}
}

-(void)reregisterTextViewDraggedTypes
{
	if (savedTextViewDragTypes)
	{
		for (id item in items)
			[[item textView] registerForDraggedTypes:savedTextViewDragTypes];
	
		savedTextViewDragTypes = nil;
	}
}

#pragma mark MISC

-(void)selectNext:(id)sender wrap:(BOOL)wrap
{
	NSInteger index = [items indexOfObjectIdenticalTo:[self itemWithTextView:sender]];
	SPRTodoItemView *item=nil;
	
	if (index != [items count]-1) //don't select next if the bottom view is selected
	{
		item = [items objectAtIndex:index+1];
	}
	else if (wrap) //but select the top one if wrapping is we should wrap
	{
		item = [items objectAtIndex:0];
	}
	
	if (item)
	{
		[[self window] makeFirstResponder:[item textView]];
		[[item textView] setSelectedRange:NSMakeRange(0,0)]; //actually will end up being (1,0) if the invisible character is there
		[self scrollItemToVisible:item];
	}
}

-(void)selectPrevious:(id)sender wrap:(BOOL)wrap
{
	NSInteger index = [items indexOfObjectIdenticalTo:[self itemWithTextView:sender]];
	SPRTodoItemView *item=nil;
	
	if (index != 0) //don't select previous if the top view is selected
	{
		item = [items objectAtIndex:index-1];
	}
	else if (wrap) //but select the top one if wrapping is we should wrap
	{
		item = [items objectAtIndex:[items count]-1];
	}
	
	if (item)
	{
		[[self window] makeFirstResponder:[item textView]];
		[[item textView] setSelectedRange:NSMakeRange([[[item textView] textStorage] length], 0)];
		[self scrollItemToVisible:item];
	}		
}

-(void)saveSelection
{
	if (!overrideSelection)
	{
		NSResponder *selectedView = [[self window] firstResponder];
		if ([selectedView isKindOfClass:[SPRTodoItemTextView class]])
		{
			savedSelectionStorage = [(id)selectedView textStorage];
			//savedCharacterSelections = [[(id)selectedView selectedRanges] retain];
			savedCharacterSelection = [(id)selectedView selectedRange];
		}
		else
		{
			hasSavedSelection=NO;
			savedSelectionStorage = nil;
			return;
		}
	}	
	hasSavedSelection=YES;
	//if (!NSLocationInRange(savedCharacterSelection.location+savedCharacterSelection.length,
	//					  NSMakeRange(0,[savedSelectionStorage length])))	//then just select the first character
		//savedCharacterSelection = NSMakeRange(0,0);
		//savedCharacterSelections = [[NSArray alloc] initWithObjects:[NSValue valueWithRange:NSMakeRange(0,0)],nil];
}

-(void)loadSelection
{
	if (hasSavedSelection)
	{
		BOOL chooseLast = YES;
		NSTextView *selectedTextView=nil;
		
		if (overrideSelection && overrideIndex < [items count])
		{
			//try to use the index in overrideIndex to set the active item
			selectedTextView = [[items objectAtIndex:overrideIndex] textView];
			chooseLast = NO;
		}
		else if (hasSavedSelection && savedSelectionStorage)
		{
			//find the item with the textstorage
			NSUInteger i;
			for (i=0; i<[textStorages count]; i++)
			{
				if ([textStorages objectAtIndex:i] == savedSelectionStorage)
					break;
			}
			
			if (i < [textStorages count]) //meaning it was found
			{
				selectedTextView = [[items objectAtIndex:i] textView];
				chooseLast = NO;
			}
		}
		
		if (chooseLast)
		{
			selectedTextView = [[items lastObject] textView];
		}
		
		[[self window] makeFirstResponder:selectedTextView];
		
		if (savedCharacterSelection.location+savedCharacterSelection.length >
			[[selectedTextView textStorage] length])
			savedCharacterSelection=NSMakeRange(0,0);
			
		[selectedTextView setSelectedRange:savedCharacterSelection];
		//[selectedTextView setSelectedRanges:savedCharacterSelections];
		
		//scroll to visible
		NSTextStorage *ts = [selectedTextView textStorage];
		NSRange wholeRange = NSMakeRange(0,[ts length]);
		[self scrollToVisibleSubrange:wholeRange index:[textStorages indexOfObjectIdenticalTo:[selectedTextView textStorage]]];
	}
	
	hasSavedSelection=NO;
	overrideSelection=NO;
	savedSelectionStorage=nil;
	//[savedCharacterSelections release];
	//savedCharacterSelections=nil;
	savedCharacterSelection=NSMakeRange(0,0);
}

-(void)returnPressed:(SPRTodoItemTextView*)tv
{
	SPRTodoItemView *item=nil;
	NSUInteger i;
	for (i=0; i<[items count]; i++)
	{
		item = [items objectAtIndex:i];
		if ([item textView] == tv)
			break;
	}
	
	if ( i == [items count] )
		[NSException raise:@"SPRTodoListViewException (-returnPressed:)" format:@"Failed to find item for text view."];
	
	//check if the associated textstorage is locked. if so, return without doing anything
	if ([[textStorages objectAtIndex:i] locked])
		return;
	
	//from the selection, determine what text will go with the new todo item
	//two possibilities, one selection range of zero length, take the rest of the field and use that text
	//OR one or more selections of nonzero length, take the selected text
	NSArray *ranges = [tv selectedRanges];
	NSMutableAttributedString *textForNew = [NSMutableAttributedString new];
	NSTextStorage *ts = [tv textStorage];
	NSRange firstRange = [[ranges objectAtIndex:0] rangeValue];
	
	[ts deleteCharactersInRange:firstRange];
	NSRange rest = {firstRange.location, [ts length]-firstRange.location};
	[textForNew setAttributedString:[ts attributedSubstringFromRange:rest]];
	[ts deleteCharactersInRange:rest];

	if ([ts length] >= 1 && [[ts string] characterAtIndex:[ts length]-1] == '\n')
		[ts deleteCharactersInRange:NSMakeRange([ts length]-1,1)];
	else if ([textForNew length] >= 1 && [[textForNew string] characterAtIndex:0] == '\n')
		[textForNew deleteCharactersInRange:NSMakeRange(0,1)];
	
	//if the length of textForNew is zero, insert into that string the SPRInvisibleAttributesHolder with the attributes
	//that are active at the end of the original string (ts)
	if ([textForNew length] == 0 && [ts length] > 0)
	{
		NSDictionary *attrs = [ts attributesAtIndex:[ts length]-1 effectiveRange:NULL];
		[textForNew setAttributedString:SPRGetAttributeHolder(attrs)];
	}
	
	overrideIndex = i+1; //mark to select the next view
	overrideSelection=YES;
	savedCharacterSelection = NSMakeRange(0,0);
	//savedCharacterSelections = [[NSArray alloc] initWithObjects:[NSValue valueWithRange:NSMakeRange(0,0)],nil];
	[delegate todoListView:self createNewItemAtIndex:i+1 withContent:textForNew];
}

-(BOOL)backspacePressed:(SPRTodoItemTextView*)tv
{
	SPRTodoItemView *item=nil;
	NSUInteger i;
	for (i=0; i<[items count]; i++)
	{
		item = [items objectAtIndex:i];
		if ([item textView] == tv)
			break;
	}
	
	if ( i == [items count] )
		[NSException raise:@"SPRTodoListViewException (-returnPressed:)" format:@"Failed to find item for text view."];
	
	if (i == 0 && [[textStorages objectAtIndex:i] length] > 0)
	{
		return NO;
	}
	
	//if there is any text left in the item, preserve it
	NSMutableAttributedString *pc = [NSMutableAttributedString new];
	[pc setAttributedString: [tv textStorage]];
	
	if ([[pc mutableString] hasPrefix:SPRInvisibleAttributeHolder])
		[pc deleteCharactersInRange:[[pc mutableString] rangeOfString:SPRInvisibleAttributeHolder]];
		
	if (i > 0)
	{
		overrideIndex = i-1; //mark to select PREVIOUS
		//savedCharacterSelections = [[NSArray alloc] initWithObjects:[NSValue valueWithRange:NSMakeRange([[textStorages objectAtIndex:i-1] length],0)],nil];
		savedCharacterSelection = NSMakeRange([[textStorages objectAtIndex:i-1] length],0);
	}
	else
	{
		overrideIndex = 0;
		savedCharacterSelection = NSMakeRange(0,0);
		//savedCharacterSelections = [[NSArray alloc] initWithObjects:[NSValue valueWithRange:NSMakeRange(0,0)],nil];
	}
	
	overrideSelection=YES;
	
	[delegate todoListView:self deleteItemAtIndex:i preserveContents:pc];
	return YES;
}

-(void)deleteDragSourceItem
{
	NSUInteger i = [textStorages indexOfObjectIdenticalTo:dragSourceItemTextStorage];
	if (i == NSNotFound)
		[NSException raise:@"SPRTodoListViewException" format:@"Failed to locate source item in drag. -draggedImage:endedAt:…"];
	
	[delegate todoListView:self deleteItemAtIndex:i preserveContents:nil];
}

-(void)clearDragSourceItem
{
	dragSourceItemTextStorage=nil;
}

-(void)checkBoxClicked:(SPRTodoItemCheckbox*)checkBox
{
	NSUInteger i;
	for (i=0; i<[items count]; i++)
	{
		if ([checkBox isDescendantOf:[items objectAtIndex:i]])
			break;
	}
	NSAssert(i != [items count], @"Could not determine item from check box in SPRTodoListView -checkBoxClicked:");
	
	[delegate todoListView:self toggleItemAtIndex:i];
	[[items objectAtIndex:i] setNeedsDisplay:YES];
}

-(SPRTodoItemView*)itemWithTextView:(SPRTodoItemTextView*)tv
{
	for (SPRTodoItemView *i in items)
	{
		if ([i textView] == tv)
			return i;
	}
	
	[NSException raise:@"SPRTodoListViewException" format:@"Failed to find item for text view."];
	return nil;
}

- (void)dealloc
{
	NSUInteger i;
	for (i=0; i < [textStorages count];i++)
		[[textStorages objectAtIndex:i] removeLayoutManager:[layoutManagers objectAtIndex:i]];
	
	//[fillerView release];
}

-(void)setPrimaryBackgroundColor:(NSColor*)c
{
	primaryColor = c;
	/*[secondaryColor release];
	
	
	primaryColor = [[c colorUsingColorSpaceName:NSDeviceRGBColorSpace] retain];
	
	if (primaryColor == nil) //it's a texture
	{
		primaryColor = [c retain];
		secondaryColor = [c retain];
		return;
	}
	
	CGFloat h,s,v;
	[primaryColor getHue:&h saturation:&s brightness:&v alpha:NULL];
	if (v<0.85)
		v *= 1/0.85;
	else
		v *= 0.85;
	secondaryColor = [[NSColor colorWithCalibratedHue:h saturation:s brightness:v alpha:1.0] retain];*/
	[self setNeedsDisplay:YES];
}

-(void)setSecondaryBackgroundColor:(NSColor*)c
{
	secondaryColor = c;
	[self setNeedsDisplay:YES];
}

+(NSColor*)calculateSecondaryColor:(NSColor*)c
{
	CGFloat h,s,v;
	[[c colorUsingColorSpaceName:NSCalibratedRGBColorSpace]
		getHue:&h saturation:&s brightness:&v alpha:NULL];
	if (v<0.85)
		v *= 1/0.90;
	else
		v *= 0.90;
		
	return [NSColor colorWithCalibratedHue:h saturation:s brightness:v alpha:1.0];
}

-(void)removeAllItems
{
	ignoreResize = YES;
	for (NSView *v in items)
		[v removeFromSuperview];
	[items removeAllObjects];
	
	NSUInteger i;
	for (i=0; i < [textStorages count];i++)
		[[textStorages objectAtIndex:i] removeLayoutManager:[layoutManagers objectAtIndex:i]];
	
	[textStorages removeAllObjects];
	[layoutManagers removeAllObjects];
	ignoreResize = NO;
}

-(void)setDelegate:(id)del
{
	delegate = del;
}

-(void)subrange:(NSRange*)subrange andIndex:(NSUInteger*)index forRange:(NSRange)inRange
{
	NSRange r = inRange;
	NSUInteger i, c = [items count];
	NSUInteger rangeOffset=0; //the total length of the text of all todo items up to the first character of the current item;
	for (i=0; i<c; i++)
	{
		NSTextStorage *t = [textStorages objectAtIndex:i];
		if (r.location >= rangeOffset && r.location < rangeOffset+[t length])
		{
			*subrange = NSMakeRange(r.location-rangeOffset,r.length);
			*index = i;
			return;
		}
		rangeOffset += [t length];
	}
	//allow for one special case; after the last character
	if (r.location == rangeOffset)
	{
		*subrange = NSMakeRange(0,r.length);
		*index = c-1;
		return;
	}
	
	//if we got here, it wasn't found
	*index = NSNotFound;
	*subrange = NSMakeRange(NSNotFound,0);
}

-(void)addTemporaryHighlightForRange:(NSRange)r
{
	NSColor *hc = [NSColor selectedTextBackgroundColor];
	NSUInteger i, c = [items count];
	NSUInteger rangeOffset=0; //the total length of the text of all todo items up to the first character of the current item;
	for (i=0; i<c; i++)
	{
		NSTextStorage *t = [textStorages objectAtIndex:i];
		if (r.location >= rangeOffset && r.location < rangeOffset+[t length])
		{
			NSLayoutManager *l = [layoutManagers objectAtIndex:i];
			NSRange adjustedRange = {r.location-rangeOffset,r.length};
			
			[l addTemporaryAttribute:NSBackgroundColorAttributeName value:hc forCharacterRange:adjustedRange];
			return;
		}
		
		rangeOffset += [t length];
	}
}

-(void)removeTemporaryHighlights
{
	NSUInteger i, c = [items count];
	for (i=0; i<c; i++)
	{
		NSTextStorage *t = [textStorages objectAtIndex:i];
		NSLayoutManager *l = [layoutManagers objectAtIndex:i];
		NSRange totalRange = NSMakeRange(0,[t length]);
		[l removeTemporaryAttribute:NSBackgroundColorAttributeName forCharacterRange:totalRange];
	}
}

-(void)scrollToVisibleSubrange:(NSRange)r index:(NSUInteger)i
{
	NSPoint scrollToPoint;
	//first, check to see if the item is completely visible, if not, make as much of it viewable as possible
	SPRTodoItemView *item = [items objectAtIndex:i];
	NSClipView *clipView = [[self enclosingScrollView] contentView]; 
	
	BOOL visible = CGRectContainsRect(NSRectToCGRect([clipView documentVisibleRect]),
									  NSRectToCGRect([item frame]));
	
	if (visible)
		return; //no scrolling needed
	
	//scrollToPoint = [item frame].origin; //this is just the coarse scrolling
	
	NSPoint scrollInItemPoint = {0,0};
	
	//now get the point that needs to be scrolled to within the item
	NSLayoutManager *lm = [layoutManagers objectAtIndex:i];
	NSTextView *tv = [item textView];
	NSTextContainer *tc = [tv textContainer];
	scrollInItemPoint.y = [tv textContainerOrigin].y; //scroll to the beginning of the text container
	
	//get the glyphrange from the character range
	NSRange glyphRange = [lm glyphRangeForCharacterRange:r actualCharacterRange:NULL];
	
	//now get the bounding rect of the range
	NSRect bRect = [lm boundingRectForGlyphRange:glyphRange inTextContainer:tc];
	
	//and add the y origin of that to the the scrollToPoint
	scrollInItemPoint.y += bRect.origin.y;
	
	//now convert that point to coordinates of self
	scrollToPoint = [self convertPoint:scrollInItemPoint fromView:tv];
	
	
	//now do the actual scrolling
	scrollToPoint.y += scrollInItemPoint.y;
	scrollToPoint = [clipView constrainScrollPoint:scrollToPoint];
	[clipView scrollToPoint:scrollToPoint];
}

- (void)scrollRangeToVisible:(NSRange)range
{
	NSUInteger i;
	NSRange r;
	[self subrange:&r andIndex:&i forRange:range];
	
	if (i == NSNotFound)
		[NSException raise:@"SPRTodoListViewException" format:@"scrollRangeToVisible: passed invalid range: %@", NSStringFromRange(range)];
	
	[self scrollToVisibleSubrange:r index:i];
}

-(void)scrollItemToVisible:(SPRTodoItemView*)item
{
	NSUInteger i = [items indexOfObject:item];
	
	NSAssert(i!=NSNotFound, @"-scrollItemToVisible passed invalid itemView");
	
	NSRange r = {0,[[textStorages objectAtIndex:i] length]};
	[self scrollToVisibleSubrange:r index:i];
}

- (void)setSelectedRange:(NSRange)range
{
	NSUInteger index;
	NSRange r;
	[self subrange:&r andIndex:&index forRange:range];
	
	if (index == NSNotFound)
		[NSException raise:@"SPRTodoListViewException" format:@"setSelectedRange: passed invalid range: %@", NSStringFromRange(range)];
	
	//unselect all other ranges
	NSUInteger i, c = [items count];
	for (i=0; i<c; i++)
	{
		if (i != index)
			[[[items objectAtIndex:i] textView] setSelectedRange:NSMakeRange(0,0)];
	}
	
	NSTextView *tv = [[items objectAtIndex:index] textView];
	[tv setSelectedRange:r];
}

- (void)showFindIndicatorForRange:(NSRange)range
{
	NSUInteger i;
	NSRange r;
	[self subrange:&r andIndex:&i forRange:range];
	
	if (i == NSNotFound)
		[NSException raise:@"SPRTodoListViewException" format:@"showFindIndicatorForRange: passed invalid range: %@", NSStringFromRange(range)];
	
	NSTextView *tv = [[items objectAtIndex:i] textView];
	[tv showFindIndicatorForRange:r];
}

- (NSTextStorage*)dragSourceItemTextStorage
{ return dragSourceItemTextStorage; }

-(void)unselectOtherItems:(SPRTodoItemView*)newlySelectedItem
{
	for (SPRTodoItemView *item in items)
	{
		if (item != newlySelectedItem)
		{
			[[item textView] setSelectedRange:NSMakeRange(0,0)];
		}
	}
}

@end

@implementation NSView (SPRTodoListView)

-(SPRTodoListView*)parentTodoListView
{
	id v = [self superview];
	
	do
	{
		if ([v isKindOfClass:[SPRTodoItemView class]])
			return [v parentList];
	} while ((v = [v superview]));
	return nil;
}

@end
/*
@implementation SPRTodoFillerView

-(id)initWithTodoListView:(SPRTodoListView*)tlv
{
	self = [super initWithFrame:NSMakeRect(0,0,0,0)];
	if (self)
	{
		todoListView = tlv;
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(updateSize:)
													 name:NSViewFrameDidChangeNotification
												   object:tlv];
		[self setHidden:YES];
		[[tlv superview] addSubview: self];
	}
	
	return self;
}

-(void)mouseDown:(NSEvent*)event
{
	NSBeep();
}

-(void)updateSize:(NSNotification*)note
{
	NSRect tlvFrame = [todoListView frame],
		   superBounds = [[todoListView superview] bounds];
	
	if (tlvFrame.origin.y+tlvFrame.size.height < superBounds.origin.y+superBounds.size.height)
	{
		NSRect newFrame = superBounds;
		newFrame.origin.y = tlvFrame.origin.y+tlvFrame.size.height;
		newFrame.size.height = superBounds.origin.y+superBounds.size.height - (tlvFrame.origin.y+tlvFrame.size.height);
		[self setHidden:NO];
	}
	else
	{
		[self setHidden:YES];
	}
}

@end*/
