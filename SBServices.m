//
//  SBServices.m
//  SpiralBound Pro
//
//  Created by Philip on 11/5/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "SBServices.h"
#import "SPROrganizer.h"
#import "DefaultsKeys.h"

#define CANCEL_BUTTON 0xFFFF




@implementation SBServices

+(SBServices*)sharedProvider
{
	static SBServices *sharedProvider=nil;
	
	if (sharedProvider == nil)
	{
		sharedProvider = [[self alloc] initWithWindowNibName:@"Services"];
	}
	return sharedProvider;
}

-(void)makePageFromTextService:(NSPasteboard*)pboard
					  userData:(NSString*)userData
						 error:(NSString**)error
{

	NSString *availableType;
	NSArray *allowableTypes = [NSArray arrayWithObjects:NSRTFDPboardType,
							   NSRTFPboardType,
							   NSStringPboardType,
							   nil];
	
	availableType = [pboard availableTypeFromArray:allowableTypes];
	
    if (availableType == nil)
    {
        [self doMakePageError];
        return;
    }
	
	NSData *pboardData = [pboard dataForType:availableType];
	
	if (pboardData == nil)
    {
        [self doMakePageError];
        return;
    }
        
	id string = [[NSAttributedString alloc] initWithData:pboardData
												 options:nil
									  documentAttributes:NULL
												   error:NULL];
	
	SPROrganizer *org;

	//determine whether to show the dialog or not
	BOOL showDialog=YES;
	NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
	
	if ([ud boolForKey:DefaultsPreferencesServicesAlwaysUseNotebookKey])
	{
		//find the notebook in which to make the new page
		NSString *notebookName = [ud stringForKey:DefaultsPreferencesServicesDefaultNotebookNameKey] ;
		
		if (notebookName != nil)
		{
			org = [SPROrganizer organizerForTitle:notebookName];
			
			if (org != nil)
			{
				showDialog = NO;
			}
		}
		
		if (showDialog)
		{
			//the user defaults are invalid, reset them
			[ud setBool:NO forKey:DefaultsPreferencesServicesAlwaysUseNotebookKey];
			//[ud removeObjectForKey:DefaultsPreferencesServicesDefaultNotebookNameKey];
		}
	}
	
	if (showDialog)
	{
		NSWindow *dialog = [self window];
		[notebookListButton removeAllItems];
		[notebookListButton addItemsWithTitles:[SPROrganizer notebookTitles]];
		
		NSString *itemToSelect = [ud stringForKey:DefaultsPreferencesServicesDefaultNotebookNameKey];
		if (itemToSelect==nil)
			itemToSelect = [[SPROrganizer keyOrganizer] title];
			
		if (itemToSelect==nil)
			itemToSelect = [[SPROrganizer organizers] objectAtIndex:0];
		
		[[SPROrganizer organizerForTitle:itemToSelect] setKey:YES];
		
		[notebookListButton selectItemWithTitle:itemToSelect];
		[NSApp activateIgnoringOtherApps:YES];
		int result = [NSApp runModalForWindow:dialog];
		
		if (result == CANCEL_BUTTON)
		{
			[dialog orderOut:nil];
			return;
		}
		[dialog orderOut:nil];
		org = [SPROrganizer organizerForTitle:[[notebookListButton selectedItem] title]];
	}
	
	[org newPageWithAttributedString:string];
	[NSApp activateIgnoringOtherApps:YES];
	
	return;
}

-(void)doMakePageError
{
    [NSApp activateIgnoringOtherApps:YES];
    NSRunAlertPanel(@"Unable to Perform Service",@"SpiralBound Pro was unable to perform this service because it was provided with invalid data or no data at all.",@"OK",nil,nil);
}

-(IBAction)selectionChanged:(id)sender
{
	[[SPROrganizer organizerForTitle:[[notebookListButton selectedItem] title]] raise:nil];
}

-(IBAction)ok:(id)sender
{
	[NSApp stopModalWithCode:0];
}

-(IBAction)cancel:(id)sender
{
	[NSApp stopModalWithCode:CANCEL_BUTTON];
}

@end
