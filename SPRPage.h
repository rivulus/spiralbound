//
//  SPRPage.h
//  SpiralBound
//
//  Created by Philip White on 2/5/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

extern BOOL HasUnsavedChanges(void);
extern void SetUnsavedChanges(BOOL);

@class SPRPlainPage, SPRLockableTextStorage;
@class SPRTodoPage;

@interface SPRPage : NSObject
{
	NSUInteger	hash;
	NSString *title;
	BOOL secure;
	BOOL titled;
}

+(void)initialize;
+(void)beginEncodingWithoutSMarker;
+(void)endEncodingWithoutSMarker;
+(void)beginDecodingWithUniqueHashes;
+(void)endDecodingWithUniqueHashes;
+(NSUInteger)uniqueHash;
+(void)registerHash:(NSUInteger)newHash;
+(void)unregisterHash:(NSUInteger)hash;

-(id)init;
-(id)initWithIntroText:(NSAttributedString*)t;
-(void)dealloc;
-(id)initWithCoder:(NSCoder *)decoder;
-(void)encodeWithCoder:(NSCoder *)encoder;
-(id)copyWithZone:(NSZone *)zone;
-(NSUInteger)hash;
-(SPRLockableTextStorage*)contentText;
-(NSString*)contentsAsString;
-(NSAttributedString*)contentsAsAttributedString;
-(void)setContentText:(NSAttributedString*)value;
-(NSString *)title;
-(void)setTitle:(NSString *)value;
-(BOOL)isSecure;
-(void)setSecure:(BOOL)value;
-(void)setTitled:(BOOL)value;
-(BOOL)isTitled;
-(void)autotitle;
-(void)autotitleWithString:(NSString*)s; //this generates a title from the string s. String s does not necessarily become the title

@end
