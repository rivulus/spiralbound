//
//  UseDragbarTransformer.m
//  SpiralBound Pro
//
//  Created by Philip White on 5/21/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "UseDragbarTransformer.h"


@implementation UseDragbarTransformer

+ (Class)transformedValueClass
{
	return [NSNumber class];
}

+ (BOOL)allowsReverseTransformation { return YES; }

- (id)transformedValue:(id)value
{
    if ([value boolValue] == YES)
		return [NSNumber numberWithInt:1];
	else
		return [NSNumber numberWithInt:0];
}

- (id)reverseTransformedValue:(id)value
{
	if ([value intValue] == 1)
		return [NSNumber numberWithBool:YES];
	else
		return [NSNumber numberWithBool:NO];
}

@end
