//
//  SPRTodoItemSampleView.h
//  CommonSpiralbound
//
//  Created by Philip on 6/17/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SPRTodoItemView.h"

@class SPRLockableTextStorage;
@interface SPRTodoItemSampleView : SPRTodoItemView
{
	SPRLockableTextStorage *textStorage;
}

@end
