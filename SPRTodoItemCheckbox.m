//
//  SPRTodoItemCheckbox.m
//  CocoaJunk
//
//  Created by Philip on 5/21/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import "SPRTodoItemCheckbox.h"
#import "SPRTodoItemView.h"
#import "SPRTodoItemTextView.h"
#import "SPRTodoListView.h"
#import "DefaultsKeys.h"
#import "SPRLockableTextStorage.h"

@implementation SPRTodoItemCheckbox


static int MarkStyleForGlyph;

//use the class method getters for these
static NSColor *MarkColor=nil;
static NSBezierPath *MarkGlyph = nil;

+(void)initialize
{
	if (self == [SPRTodoItemCheckbox class])
	{
		[[NSUserDefaults standardUserDefaults] addObserver:self
												forKeyPath:DefaultsPreferencesMarkStyle
												   options:NSKeyValueObservingOptionNew
												   context:NULL];
		
		[[NSUserDefaults standardUserDefaults] addObserver:self
												forKeyPath:DefaultsPreferencesMarkColor
												   options:NSKeyValueObservingOptionNew
												   context:NULL];
	}
}

+ (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([keyPath isEqualToString:DefaultsPreferencesMarkStyle])
	{
		MarkStyleForGlyph = [[change objectForKey:NSKeyValueChangeNewKey] intValue];
		[self GenerateGlyph];
	}
	else if ([keyPath isEqualToString:DefaultsPreferencesMarkColor])
	{
		NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:DefaultsPreferencesMarkColor];
		MarkColor = [NSUnarchiver unarchiveObjectWithData:data];
	}
}

static const unichar Marks[] = {0x2713,  //check
	0x2717,  //hand-drawn x
	0x25a3,	 //filled
	0x2715,  //neat x
	0x273d,  //asterisk
	0x263a}; //smiley face

+(void)GenerateGlyph
{
	
	unichar mark = Marks[MarkStyleForGlyph];
	
	NSTextStorage *ts = [[NSTextStorage alloc] initWithString:[NSString stringWithFormat:@"%C",mark]];
	
	NSLayoutManager *lm;
	lm = [[NSLayoutManager alloc] init];
	[ts addLayoutManager:lm];        
	
	NSRect cFrame = NSMakeRect(0,0,100,100);//arbitrary
	NSTextContainer *tc;
	
	tc = [[NSTextContainer alloc]
		  initWithContainerSize:cFrame.size];
	[lm addTextContainer:tc];
	
	NSGlyph g = [lm glyphAtIndex:[lm glyphIndexForCharacterAtIndex:0]];
	
	NSFont *f = [ts attribute:NSFontAttributeName atIndex:0 effectiveRange:NULL];
	MarkGlyph = [NSBezierPath new];
	[MarkGlyph moveToPoint:NSMakePoint(0.0,0.0)];
	[MarkGlyph appendBezierPathWithGlyph:g inFont:f];
}

+(NSBezierPath*)markGlyph
{
	if (MarkGlyph == nil)
	{
		MarkStyleForGlyph = [[NSUserDefaults standardUserDefaults] integerForKey:DefaultsPreferencesMarkStyle];
		[self GenerateGlyph];
	}
	
	return MarkGlyph;
}

+(NSColor*)markColor
{
	if (MarkColor == nil)
	{
		NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:DefaultsPreferencesMarkColor];
		MarkColor = [NSUnarchiver unarchiveObjectWithData:data];
	}
	return MarkColor;
}

+ (unichar)markCharacter
{
	return Marks[MarkStyleForGlyph];
}

- (id)initWithFrame:(NSRect)frame itemView:(SPRTodoItemView*)iv
{
    self = [super initWithFrame:frame];
    if (self)
	{
		itemView = iv;
		[[NSUserDefaults standardUserDefaults] addObserver:self
												forKeyPath:DefaultsPreferencesMarkStyle
												   options:0
												   context:NULL];
		
		[[NSUserDefaults standardUserDefaults] addObserver:self
												forKeyPath:DefaultsPreferencesMarkColor
												   options:0
												   context:NULL];
    }
    return self;
}

-(void)dealloc
{
	[[NSUserDefaults standardUserDefaults] removeObserver:self forKeyPath:DefaultsPreferencesMarkStyle];
	[[NSUserDefaults standardUserDefaults] removeObserver:self forKeyPath:DefaultsPreferencesMarkColor];
	
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	[self setNeedsDisplay:YES];
}

- (void)setChecked:(BOOL)yn
{
	if (checked != yn)
	{
		checked = yn;
		[self setNeedsDisplay:YES];
	}
}

- (BOOL)isChecked
{
	return checked;
}

-(void)mouseDown:(NSEvent*)event
{
	if ([(id)[[itemView textView] textStorage] locked])
		return;
	
	beingClicked = YES;
	[self setNeedsDisplay:YES];
}

- (BOOL)shouldDelayWindowOrderingForEvent:(NSEvent *)theEvent
{
	return YES;
}

- (void)mouseDragged:(NSEvent *)theEvent
{
	if ([(id)[[itemView textView] textStorage] locked])
		return;
	
	beingClicked=NO;
	[self setNeedsDisplay:YES];
	[[self parentTodoListView] startDragForEvent:theEvent todoItemView:(id)[self superview]];
}

- (void)mouseUp:(NSEvent *)theEvent
{
	if ([(id)[[itemView textView] textStorage] locked])
		return;
	
	beingClicked = NO;
	
	 if (NSPointInRect([self convertPoint:[theEvent locationInWindow] fromView:nil], [self bounds]))
		 [[self parentTodoListView] checkBoxClicked:self];
	
	[self setNeedsDisplay:YES];
}

- (void)drawRect:(NSRect)dirtyRect
{
	
	[[NSColor blackColor] set];
	
	NSRect boxRect = NSInsetRect([self bounds],4,4);
	NSBezierPath *boxPath = [NSBezierPath bezierPathWithRoundedRect:boxRect xRadius:0.5 yRadius:0.5];
	[boxPath setLineWidth:1.0];
	//CGFloat dash[] = {2.5,0.75};
	//[boxPath setLineDash:dash count:2 phase:0.0];
	[boxPath stroke];
	if (checked || beingClicked)
	{
		NSColor *markColor = [SPRTodoItemCheckbox markColor];
		NSBezierPath *b = [SPRTodoItemCheckbox markGlyph];
		//scale the path to the bounds while retaining the aspect ratio
		NSRect destRect = NSInsetRect([self bounds],4,4);
		NSRect pathRect = [b bounds];
		
		CGFloat xTrans = destRect.origin.x-pathRect.origin.x,
				yTrans = destRect.origin.y-pathRect.origin.y,
				xScale = destRect.size.width/pathRect.size.width,
				yScale = destRect.size.height/pathRect.size.height;
		
		NSAffineTransform *xform = [NSAffineTransform transform];
		[xform translateXBy:xTrans yBy:yTrans];
		[xform scaleXBy:xScale yBy:yScale];
		
		[b transformUsingAffineTransform:xform];
		if (beingClicked)
			[[markColor colorWithAlphaComponent:0.3] set];
		else
			[markColor set];
		[b fill];
	}
}

- (BOOL)acceptsFirstMouse:(NSEvent *)theEvent
{
	return YES;
}

@end
