//
//  SBWFrameView.h
//  SpiralBound
//
//  Created by Philip White on 2/1/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SBProtoFrameView.h"


@interface SBWFrameView : SBProtoFrameView
{
}

+ (void)initialize;

//overriden methods
-(NSImage*)dragbarImage;
-(NSNib*)nib;


-(NSRect)titleBarRect;

-(void)drawBorder;

//forwarders
- (IBAction)nextPageFromMenu:(id)sender;
- (IBAction)previousPageFromMenu:(id)sender;

@end
