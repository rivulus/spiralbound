//
//  NSDictionary+RIVConvenience.h
//  CommonSpiralbound
//
//  Created by Philip on 5/25/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface NSDictionary (RIVConvenience)

-(BOOL)boolForKey:(id)k;
-(NSRect)rectForKey:(id)k;
-(NSRange)rangeForKey:(id)k;

@end


@interface NSMutableDictionary (RIVConvenience)

-(void)setBool:(BOOL)b forKey:(id)k;
-(void)setRect:(NSRect)r forKey:(id)k;
-(void)setRange:(NSRange)r forKey:(id)k;

@end

