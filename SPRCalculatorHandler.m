//
//  SPRCalculatorHandler.m
//  CommonSpiralbound
//
//  Created by Philip on 5/28/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import "SPRCalculatorHandler.h"
#import "SPRTextView.h"
#import "Calculator.h"
#import "KeyRecordField.h"
#import "DefaultsKeys.h"

@implementation SPRCalculatorHandler

-(id)initWithTextView:(SPRTextView*)tv
{
	self = [super init];
	
	if (self)
	{
		textView = tv;
		//[textView setDelegate:self];
	}
	return self;
}

//register to receive notifications when the window ceases to be key. That will allow us to resolve any equations that are up in the air
-(void)textViewDidMoveToWindow
{
	
	NSWindow *window = [textView window];
	if (window)
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(windowDidResignKey:)
													 name:NSWindowDidResignKeyNotification
												   object:window];
}

-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)windowDidResignKey:(NSNotification *)note
{
	[self finalizePotentialEquation];
}

//this will simply note if the new changes warrant a call to the calculator
//which will be handled by textDidChange:
- (BOOL)textView:(NSTextView *)aTextView shouldChangeTextInRange:(NSRange)affectedCharRange replacementString:(NSString *)replacementString
{
	if ([[NSUserDefaults standardUserDefaults] boolForKey:@"CalculatorIsEnabled"] == NO)
	{
		calculatorShouldBeCalled = NO;
		return YES;
	}
	
	NSUInteger length = [replacementString length];
	if (length > 0)
	{
		//don't let it change if there is a character immediately after the equals sign
		
		if ( [replacementString characterAtIndex:length-1] == '=' ) //last character in replacementString
		{
			/*
			NSUInteger indexOfNextCharacter = affectedCharRange.location+affectedCharRange.length;
			NSString *totalString = [textView string];
			
			if ([totalString length] > indexOfNextCharacter)
			{
				unichar character = [totalString characterAtIndex:indexOfNextCharacter];
				if (character != '\a' && [[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember: character] == NO)
				{
					
					calculatorShouldBeCalled = NO;
					return YES;
				}
			}*/
			
			
			calculatorShouldBeCalled = YES;
			return YES;
		}
	}	
	
	calculatorShouldBeCalled = NO;
	
	return YES;
}

- (BOOL)tryToSolveEquationInTextView:(NSTextView*)view
{
	NSRange selection = [textView selectedRange];
	if (selection.length != 0)
		return NO;
	
	NSString *string = [[textView string] substringToIndex:selection.location];
	
	Calculator *calculator = [Calculator sharedCalculator];
	//break it down by various criteria
	//first the triple space criterion
	
	NSString *substring = [[string componentsSeparatedByString:@"="] lastObject];
	substring = [[substring componentsSeparatedByString:@"   "] lastObject];
	substring = [[substring componentsSeparatedByString:@"\t"] lastObject];
	substring = [[substring componentsSeparatedByString:@"\n\n"] lastObject];
	
	NSString *result = [calculator calculateWithString: substring allowOneTokenEquation:NO];
	
	if (result == nil)
	{
		substring = [[substring componentsSeparatedByString:@"\n"] lastObject];
		result = [calculator calculateWithString: substring allowOneTokenEquation:NO];
		
		if (result == nil)
		{
			substring = [[substring componentsSeparatedByString:@"  "] lastObject];
			result = [calculator calculateWithString: substring allowOneTokenEquation:NO];
			
			if (result == nil)
			{
				substring = [[substring componentsSeparatedByString:@" "] lastObject];
				result = [calculator calculateWithString: substring allowOneTokenEquation:NO];
			}
		}
	}
	
	if (result == nil)
		return NO;
	
	NSString *resultWithEquals = [@"=" stringByAppendingString:result];
	
	NSTextStorage *storage = [textView textStorage];
	
	NSAttributedString *attributedResult = [[NSAttributedString alloc] initWithString: resultWithEquals
																			attributes: [storage attributesAtIndex: selection.location-1
																									effectiveRange: nil]];
	
	
	if ([textView shouldChangeTextInRange:NSMakeRange(selection.location,0)
					replacementString:[attributedResult string]])
	{
		[storage beginEditing];
		[storage insertAttributedString: attributedResult
								atIndex:selection.location];
		[storage endEditing];
		[view didChangeText];
	}
	else
	{
		return NO;
	}
	
	return YES;
}

- (void)textDidChange:(NSNotification *)aNotification
{	
	
	if (calculatorShouldBeCalled == NO)
		return;
	
	NSRange selection = [textView selectedRange];
	
	if (selection.length != 0 || selection.location == 0)
		return;
	
	NSUInteger lastCharacterIndex = selection.location-1; //I can't envision a time where the selection would not be zero leng
	NSString *string = [textView string];
	//unichar lastCharacter = [string characterAtIndex:lastCharacterIndex];
	
	Calculator *calculator = [Calculator sharedCalculator];
	//break it down by various criteria
	//first the triple space criterion
	
	//we'll need this substringToEquals to determine where exactly the equation is when we
	//want to highlight it
	NSString *substringToEquals = [string substringToIndex:lastCharacterIndex];
	NSString *substring = [[substringToEquals componentsSeparatedByString:@"="] lastObject];
	substring = [[substring componentsSeparatedByString:@"   "] lastObject];
	substring = [[substring componentsSeparatedByString:@"\t"] lastObject];
	substring = [[substring componentsSeparatedByString:@"\n\n"] lastObject];
	
	NSString *result = [calculator calculateWithString: substring allowOneTokenEquation:YES];
	
	if (result == nil)
	{
		substring = [[substring componentsSeparatedByString:@"\n"] lastObject];
		result = [calculator calculateWithString: substring allowOneTokenEquation:YES];
		
		if (result == nil)
		{
			substring = [[substring componentsSeparatedByString:@"  "] lastObject];
			result = [calculator calculateWithString: substring allowOneTokenEquation:YES];
			
			if (result == nil)
			{
				substring = [[substring componentsSeparatedByString:@" "] lastObject];
				result = [calculator calculateWithString: substring allowOneTokenEquation:YES];
			}
		}
	}
	
	if (result != nil)
	{
		//get the correct attributes for the results and insert it into the textstorage
		NSTextStorage *storage = [textView textStorage];
		
		NSAttributedString *attributedResult = [[NSAttributedString alloc] initWithString: result
																			   attributes: [storage attributesAtIndex: lastCharacterIndex
																									   effectiveRange: nil]];
		
		if ([textView shouldChangeTextInRange:NSMakeRange(lastCharacterIndex+1,0)
							replacementString:[attributedResult string]])
		{
			[storage beginEditing];
			[storage insertAttributedString: attributedResult
									atIndex: lastCharacterIndex+1];
			[storage endEditing];
			[textView didChangeText];
		}
		else
		{
			return;
		}
		
		//determine the range of the equation plus result and highlight it in a suitable color
		NSRange equationRange = [substringToEquals rangeOfString:substring options: NSBackwardsSearch];
		//that's just the range of the left hand side of the equation
		equationRange.length += 1 + [result length]; //one for the equals sign
		
		
		[self markPotentialEquationWithRange: equationRange rhsLength: [result length]];
	}
	
}


- (BOOL)keyDown:(NSEvent*)event inView:(SPRTextView*)view
{
	NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
	if ([ud boolForKey: @"CalculatorIsEnabled"] == NO)
		return NO;
	
	//NSString *characters = [event characters];
	if ([self hasPotentialEquation] == NO)
	{
		if ( [ud boolForKey:@"EnterKeyCompletesEquation"] &&
			[KeyRecordField isKeyDownEqualToKeyCode:event])
		{
			return [self tryToSolveEquationInTextView:textView];
		}
		
		return NO;
	}
	
	BOOL handled;
	//NSUInteger rhsLength = [view potentialEquationRHSLength];
	
	if ([KeyRecordField isKeyDownEqualToKeyCode:event])
	{
		[self finalizePotentialEquation];
		handled = YES;
		
		//apply a permanent attributed to the whole equation range to mark it as an equation
		//TO DO
	}
	else
	{
		[self erasePotentialEquation];
		handled = NO;
	}
	return handled;
	
}


- (NSRange)textView:(NSTextView *)aTextView willChangeSelectionFromCharacterRange:(NSRange)oldSelectedCharRange toCharacterRange:(NSRange)newSelectedCharRange
{
	[self finalizePotentialEquation];
	
	return newSelectedCharRange;
}

-(void)markPotentialEquationWithRange:(NSRange)range rhsLength:(NSUInteger)rhsLength
{
	hasPotentialEquation = YES;
	potentialEquationRange = range;
	potentialEquationRHSLength = rhsLength;
	
	NSLayoutManager *layoutManager = [textView layoutManager];
	
	//get the colors from the defaults
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSColor *equationHighlightColor =
	[NSUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"EquationHighlightColor"]];
	NSColor *solutionHighlightColor = 
	[NSUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"SolutionHighlightColor"]];
	
	NSDictionary *lhsAttributes = [NSDictionary dictionaryWithObject:equationHighlightColor
															  forKey:NSForegroundColorAttributeName];
	NSDictionary *rhsAttributes = [NSDictionary dictionaryWithObject:solutionHighlightColor
															  forKey:NSForegroundColorAttributeName];														  
	
	[layoutManager setTemporaryAttributes: lhsAttributes
						forCharacterRange: NSMakeRange(range.location,range.length-rhsLength)];
	
	[layoutManager setTemporaryAttributes: rhsAttributes
						forCharacterRange: NSMakeRange(range.location+range.length-rhsLength, rhsLength)];
}

-(BOOL)hasPotentialEquation
{
	return hasPotentialEquation;
}

-(NSRange)potentialEquationRange
{
	return potentialEquationRange;
}

-(NSUInteger)potentialEquationRHSLength
{
	return potentialEquationRHSLength;
}

-(void)erasePotentialEquation
{
	if (hasPotentialEquation == NO)
		return;
	
	NSRange rangeToDelete = NSMakeRange(potentialEquationRange.location+potentialEquationRange.length-potentialEquationRHSLength,
										potentialEquationRHSLength);
	
	[[textView layoutManager] removeTemporaryAttribute:NSForegroundColorAttributeName
								 forCharacterRange:potentialEquationRange];
	[textView setSelectedRange:NSMakeRange(rangeToDelete.location,0)];
	if ([textView shouldChangeTextInRange:rangeToDelete
					replacementString:@""])
	{
		[[textView textStorage] beginEditing];
		[[textView textStorage] deleteCharactersInRange:rangeToDelete];
		[[textView textStorage] endEditing];
		[textView didChangeText];
	}
	hasPotentialEquation = NO;
}

-(void)finalizePotentialEquation
{
	if (!hasPotentialEquation)
		return;
	
	//it's important to set this before calling setSelectedRange
	//because the delegate method will cause problems otherwise 
	hasPotentialEquation = NO;
	
	[[textView layoutManager] removeTemporaryAttribute:NSForegroundColorAttributeName
								 forCharacterRange:potentialEquationRange];
	[textView setSelectedRange:NSMakeRange(potentialEquationRange.location+potentialEquationRange.length,0)];
	
	//check if the user wants the result in the clipboard
	if ([[NSUserDefaults standardUserDefaults] boolForKey:DefaultsPreferencesPutCalculatorResultInClipboard])
	{
		NSString *answer = [[[textView textStorage] string] substringWithRange:
								NSMakeRange(potentialEquationRange.location+potentialEquationRange.length-potentialEquationRHSLength,
											potentialEquationRHSLength)];
		
		NSPasteboard *pboard = [NSPasteboard generalPasteboard];
		[pboard declareTypes:[NSArray arrayWithObject:NSStringPboardType]
					   owner:nil];
		[pboard setData:[answer dataUsingEncoding:NSUTF8StringEncoding]
				forType:NSStringPboardType];
	}
}


@end
