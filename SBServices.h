//
//  SBServices.h
//  SpiralBound Pro
//
//  Created by Philip on 11/5/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SBServices : NSWindowController

{
	IBOutlet NSPopUpButton *notebookListButton;
}

+(SBServices*)sharedProvider;
-(void)makePageFromTextService:(NSPasteboard*)pboard
					  userData:(NSString*)userData
						 error:(NSString**)error;
-(IBAction)ok:(id)sender;
-(IBAction)cancel:(id)sender;
-(IBAction)selectionChanged:(id)sender;
@end
