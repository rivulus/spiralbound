//
//  SPRTodoItemSampleView.m
//  CommonSpiralbound
//
//  Created by Philip on 6/17/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import "SPRTodoItemSampleView.h"
#import "SPRTodoItemCheckbox.h"
#import "SPRTodoItemTextView.h"
#import "SPRLockableTextStorage.h"

@implementation SPRTodoItemSampleView

-(id)initWithFrame:(NSRect)rect
{
	textStorage = [[SPRLockableTextStorage alloc] initWithString:@"Sample To Do Item"];
	
	NSLayoutManager *layoutManager;
	layoutManager = [[NSLayoutManager alloc] init];
	[textStorage addLayoutManager:layoutManager];
	
	NSRect cFrame = {0,0,200,100}; //dummy size, gets resized by super
	NSTextContainer *container;
	
	container = [[NSTextContainer alloc]
				 initWithContainerSize:cFrame.size];
	[layoutManager addTextContainer:container];
	
	self = [super initWithFrame:rect
				  textContainer:container
						  state:YES
					 parentList:(id)self]; //this class implements a checkBoxClicked: method
	
	if (self)
	{
		[textView setSelectable:NO];
	}
	return self;
}


//these next three methods let this class masquerade as the list view itself
-(void)checkBoxClicked:(SPRTodoItemCheckbox*)box
{
	[box setChecked:![box isChecked]];
	[self setNeedsDisplay:YES];
}


-(void)startDragForEvent:(NSEvent*)event todoItemView:(SPRTodoItemView*)tiv
{
	//do nothing
}

-(void)itemDidResize:(SPRTodoItemView*)item
{
	//do nothing
}


@end
