//
//  SBTitleView.m
//  SpiralBound
//
//  Created by Philip White on 2/13/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "SBTitleView.h"
#import "NSView+Organizer.h"
#import "SPROrganizer+Actions.h"
//#import "SBController+Data.h"
#import "SBConstants.h"

const float BigInset = 52;
const float LittleInset = 28;

@implementation SBTitleView

- (BOOL)acceptsFirstMouse:(NSEvent *)theEvent
{
	return YES;
}

- (void)awakeFromNib
{
	/****THIS CHECK NEEDS TO CHANGE*****/
	if ([[[self superview] subviews] count] == 3) //it's torn off
		inset = LittleInset;
	else
		inset = BigInset;
	
	if ([[self cell] respondsToSelector:@selector(setLineBreakMode:)]) 
		[[self cell] setLineBreakMode: NSLineBreakByTruncatingTail];
}

//note that the normal renaming process start with an action method to this object
//-doneEditing:
//which tells the Organizer to rename the page associated with the window
//which tells the SPRPage to rename itself
//which posts a notification
//which the organizer picks up and sends a setTitle message to the associated window
//which calls this setStringValue: where the correct attributes are set
//circuitous perhaps, but I think logical and effective
- (void)setStringValue:(NSString *)plainString
{
	//this seems to be necessary for 10.4
	[[self superview] setNeedsDisplayInRect: [self frame]];

	NSMutableAttributedString *string = [[self attributedStringValue] mutableCopy];
	
	NSRange range = NSMakeRange(0,[string length]);
	[string replaceCharactersInRange:range withString:plainString];
	
	[self setAttributedStringValue:string];
	[self sizeAndPosition];
}

- (void)mouseDown:(NSEvent *)theEvent
{
	if ([theEvent clickCount] == 2)
	{
		[self rename];
	}
	else
	{
		[[self nextResponder] mouseDown: theEvent];
	}
}

- (void)rename
{
	[self setEditable:YES];
	[self selectText:nil];
	[self setAction:@selector(doneEditing:)];
	[self setTarget: self];
	
	
	//make the field as large as possible temporarily
	float width = [[self superview] bounds].size.width - inset*2;
	NSRect frame = [self frame];
	float xloc = [[self superview] bounds].size.width/2 - width/2;
	frame.origin.x = xloc;
	frame.size.width = width;
	[self setFrame:frame];
	[self setNeedsDisplay:YES];
}

- (void)doneEditing:(id)sender
{
	[self sizeAndPosition];
	[self setEditable:NO];
	[[self organizer] setTitle: [self stringValue] window:(id)[self window]];		//see NSView+Organizer
	[self setBold:YES];
}

- (void)sizeAndPosition
{
	[self sizeToFit];
	NSRect frame = [self frame];
	if (frame.size.width < 16)
		frame.size.width=16;
	else if (frame.size.width + 2*inset >= [[self superview] bounds].size.width)
		frame.size.width = [[self superview] bounds].size.width - 2*inset;
	float xloc = [[self superview] bounds].size.width/2 - frame.size.width/2;
	frame.origin.x = xloc;
	[self setFrame:frame];
	[self setNeedsDisplay:YES];
}

- (BOOL)bold
{
    return bold;
}

- (void)setBold:(BOOL)value
{
     bold = value;
	NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithAttributedString: [self attributedStringValue]];
		
		NSFont *font;
		NSColor *color;
		if (value == YES) //make it bold
		{
			font = [NSFont boldSystemFontOfSize:0];
			color = [NSColor blackColor];
		}
		else
		{
			font = [NSFont systemFontOfSize:0];
			color = [NSColor darkGrayColor];
		}
		NSRange range = NSMakeRange(0,[string length]);
		
		[string addAttribute:NSFontAttributeName value:font range: range];
		[string addAttribute:NSForegroundColorAttributeName value:color range:range];
		
		[self setAttributedStringValue:string];
		
	
		[self sizeAndPosition];
}

- (void)resizeWithOldSuperviewSize:(NSSize)oldBoundsSize
{
	[super resizeWithOldSuperviewSize:oldBoundsSize];
	
	[self sizeToFit];
	NSRect frame = [self frame];
	if (frame.size.width < 16)
		frame.size.width=16;
	else if (frame.size.width + 2*inset >= [[self superview] bounds].size.width)
		frame.size.width = [[self superview] bounds].size.width - 2*inset;
	float xloc = [[self superview] bounds].size.width/2 - frame.size.width/2;
	frame.origin.x = xloc;
	[self setFrame:frame];
	
}

@end
