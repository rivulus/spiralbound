//
//  HelpController.h
//  SpiralBound
//
//  Created by Philip White on 2/27/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Webkit/Webkit.h>

@interface HelpController : NSWindowController
{
	NSAttributedString *help;
	//IBOutlet NSTextView *helpView;
	IBOutlet WebView *webView;
}

+(id)sharedHelpController;
-(NSAttributedString*)help;


@end
