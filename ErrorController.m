//
//  ErrorController.m
//  SpiralBound 1.1
//
//  Created by Philip on 8/5/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "ErrorController.h"
#if MAC_OS_X_VERSION_MIN_REQUIRED >= MAC_OS_X_VERSION_10_5
#include <execinfo.h>
#endif


@implementation ErrorController

static ErrorController *SharedController=nil;

+(ErrorController*)sharedController
{
	if (SharedController==nil)
	{
		SharedController = [[self alloc] init];
	}
	
	return SharedController;
}

-(id)init
{
	self = [super initWithWindowNibName:@"Error"];
	
	return self;
}

- (BOOL)shouldDisplayException:(NSException *)exception
{	
	NSString* name = [exception name];
    NSString* reason = [exception reason];
    
    if ([name isEqualToString:NSImageCacheException] ||
        [name isEqualToString:@"GIFReadingException"] ||
        [name isEqualToString:@"NSRTFException"] ||
        ([name isEqualToString:@"NSInternalInconsistencyException"] && [reason hasPrefix:
																		@"lockFocus"])
        )
    {
        return NO;
    }
    
	//this is the epson printer driver hack
	{
		NSMutableString *trace = [NSMutableString string];
		void *stack[128];
		int count,i;
		count = backtrace(stack,128);
		
		char **symbols;
		symbols = backtrace_symbols(stack,count);
		
		for (i=0; i<count; i++)
		{
			[trace appendFormat:@"%s\n", symbols[i]];
		}
		free(symbols);
		
		if ([trace rangeOfString:@"CERP"].location != NSNotFound &&
			[trace rangeOfString:@"PDEC"].location != NSNotFound)
			return NO;
	}
	
    return YES;
}

-(BOOL)exceptionHandler:(NSExceptionHandler *)sender shouldHandleException:(NSException *)exception mask:(NSUInteger)aMask
{
	NSLog(@"*********ErrorController exceptionHandler:shouldHandleException:mask:*********");
	
	if ([self shouldDisplayException:exception])
	{
		[self showReportForException:exception];
		
	}
	/*else
	{
		return NO;
	}*/
	
	return YES;
}

- (BOOL)exceptionHandler:(NSExceptionHandler *)sender shouldLogException:(NSException *)exception mask:(NSUInteger)aMask
{
	return YES;
}

-(void)showReportForException:(NSException*)exception
{
	NSMutableString *exceptionInfo = [NSMutableString string];
	
	[self window];
	
	[userField setString:@"Please describe here what you were doing when this window appeared. This information will help Rivulus to eliminate this bug in the next release"];
	
	NSDictionary *bundleInfo = [[NSBundle mainBundle] infoDictionary];
    NSProcessInfo *processInfo = [NSProcessInfo processInfo];
    NSOperatingSystemVersion osVersion = [processInfo operatingSystemVersion];
	
	[exceptionInfo appendFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@", @"--------------BEGIN EXCEPTION REPORT--------------\n\n",
												 [exception name], @"\n\n",
												 [exception reason], @"\n\n",
												 @"--------------BEGIN MISC INFO---------------\n\n",
												 @"System = ", [NSString stringWithFormat:@"%li.%li.%li",(long)osVersion.majorVersion,(long)osVersion.minorVersion,osVersion.patchVersion], @"\n",
												 @"Application = ", [bundleInfo objectForKey:@"CFBundleName"], @"\n",
												 @"Version = ", [bundleInfo objectForKey: @"CFBundleVersion"], @"\n\n",
												 @"--------------BEGIN STACK TRACE--------------\n\n"];
	
	//now get and format the stack trace
#if MAC_OS_X_VERSION_MIN_REQUIRED >= MAC_OS_X_VERSION_10_5 //meaning OS X 10.5 or later
	void *stack[128];
	int count,i;
	count = backtrace(stack,128);
	
	char **symbols;
	symbols = backtrace_symbols(stack,count);
	
	for (i=0; i<count; i++)
	{
		[exceptionInfo appendFormat:@"%s\n", symbols[i]];
	}
	free(symbols);
#else
	NSString *stackTrace = [[exception userInfo] objectForKey: NSStackTraceKey];
	
	if (stackTrace != nil)
	{
		NSString *atosPath = @"/usr/bin/atos";
		
		if ([[NSFileManager defaultManager] fileExistsAtPath:atosPath])
		{
			NSMutableArray *arguments = [NSMutableArray array];
			//"-p pid"
			[arguments addObject:@"-p"];
			[arguments addObject:[NSString stringWithFormat:@"%i", [[NSProcessInfo processInfo] processIdentifier]]];
			
			[arguments addObjectsFromArray:[stackTrace componentsSeparatedByString:@" "]];
			
			
			NSTask *atos = [[NSTask alloc] init];
			
			if (atos != nil)
			{
				[atos setLaunchPath:atosPath];
				[atos setArguments:arguments];
				
				NSPipe *outputPipe = [NSPipe pipe];
				[atos setStandardOutput:outputPipe];
				[atos launch];
				[atos waitUntilExit];
				
				if ([atos terminationStatus] == 0) //if success
				{
					NSData *outputData = [[outputPipe fileHandleForReading] availableData];
					NSString *output = [[NSString alloc] initWithBytes:[outputData bytes] length:[outputData length] encoding:NSUTF8StringEncoding];
					
					[exceptionInfo appendString: output];
				}
			}
			[atos release];
			
		}
		else
		{
			[exceptionInfo appendString: stackTrace];
		}

	}

#endif
	[reportField setString:exceptionInfo];
	
	[[self window] center];
	[[self window] makeKeyAndOrderFront:nil];
	[NSApp runModalForWindow:[self window]];
}

-(IBAction)sendReport:(id)sender
{
	[NSApp stopModal];
	[[self window] orderOut:nil];
	
	NSURL *url;
	
    // Create the URL.
	
	NSString *urlString = [NSString stringWithFormat: @"mailto:rivulus.sw@me.com"
						   "?subject=Error Report"
						   "&body=%@\n\n---------------------------------------------------------------\n\n%@",
						   [userField string], [reportField string]];
	
	
    url = [NSURL URLWithString: [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	
    if (url == nil)
	{
		NSRunCriticalAlertPanel(@"Error Occurred", @"Unable to generate email", @"OK", nil, nil);
		return;
	}
	
    //Open the URL.
    [[NSWorkspace sharedWorkspace] openURL:url];
}

-(IBAction)dontSend:(id)sender
{
	[NSApp stopModal];
	[[self window] orderOut:nil];
}

@end
