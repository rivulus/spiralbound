//
//  SPRPage.m
//  SpiralBound
//
//  Created by Philip White on 2/5/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "SPRPage.h"
#import "SPRPlainPage.h"
#import "NotificationNames.h"
#import "SBConstants.h"

#define NO_HASH UINT32_MAX

static NSMutableSet *sHashSet;
static BOOL DecodeWithUniqueHashes=NO;
static BOOL EncodeWithoutSMarker=NO;

BOOL UnsavedChanges=NO;

void SetUnsavedChanges(BOOL value)
{
	UnsavedChanges = value;
}

BOOL HasUnsavedChanges(void)
{
	return UnsavedChanges;
}

@implementation SPRPage

+(void)initialize
{
	//make an empty hash set
	if (!sHashSet)
		sHashSet = [[NSMutableSet alloc] init];
}

+(void)beginEncodingWithoutSMarker
{
	if (EncodeWithoutSMarker)
		@throw([NSException exceptionWithName:@"+beginEncodingWithoutSMarker exception"
									   reason:@"Already Encoding Without SMarkers"
									 userInfo:nil]);
	
	EncodeWithoutSMarker = YES;
}

+(void)endEncodingWithoutSMarker
{
	if (!EncodeWithoutSMarker)
		@throw([NSException exceptionWithName:@"+endEncodingWithoutSMarker exception"
									   reason:@"Not Encoding Without SMarkers"
									 userInfo:nil]);
	
	EncodeWithoutSMarker = NO;
}

+(void)beginDecodingWithUniqueHashes
{
	if (DecodeWithUniqueHashes)
		@throw([NSException exceptionWithName:@"+beginDecodingWithUniqueHashes exception"
									   reason:@"Already Decoding With Unique Hashes"
									 userInfo:nil]);
	
	DecodeWithUniqueHashes = YES;
}

+(void)endDecodingWithUniqueHashes
{
	if (!DecodeWithUniqueHashes)
		@throw([NSException exceptionWithName:@"+endDecodingWithUniqueHashes exception"
									   reason:@"Not Decoding With Unique Hashes"
									 userInfo:nil]);
	
	
	DecodeWithUniqueHashes = NO;
}

+(NSUInteger)uniqueHash
{

	//brute force
	BOOL loop=YES;
	NSUInteger potentialHash;
	for (potentialHash = 0; loop; potentialHash++)
	{
		NSNumber *objValue = [NSNumber numberWithUnsignedInteger:potentialHash];
		if (![sHashSet containsObject:objValue])
			break;
	}
	
	//potentialHash should be unique
	[sHashSet addObject:[NSNumber numberWithInteger:potentialHash]];
	return potentialHash;
}

+(void)registerHash:(NSUInteger)newHash
{
	NSNumber *hashNumber = [NSNumber numberWithUnsignedInteger:newHash];
	
	//check that it's unique
	if ([sHashSet containsObject:hashNumber])
		@throw([NSException exceptionWithName:@"Duplicate Hash" reason:@"Dreadful Error! registerHash: encountered duplicate hash." userInfo:nil]);
	
	[sHashSet addObject:hashNumber];
}


+(void)unregisterHash:(NSUInteger)hash
{
	NSNumber *hashNumber = [NSNumber numberWithInteger:hash];
	NSEnumerator *enumerator = [sHashSet objectEnumerator];
	NSNumber *number;
	
	while (number = [enumerator nextObject])
	{
		if ([number isEqualToNumber:hashNumber])
		{
			[sHashSet removeObject:number];
			return;
		}
	}
}


-(id)init
{
	if ([self class] == [SPRPage class])	//return a subclass instead
	{
		hash=NO_HASH; //set this to prevent the value 0 from being removed from the hash set in -dealloc
		return [[SPRPlainPage alloc] init];
	}
	
	self = [super init];
	
	if (self != nil)
	{
		hash = [SPRPage uniqueHash];
		title = @"untitled";
		secure=NO;
	
		SetUnsavedChanges(YES);
	}
	return self;
}


-(id)initWithIntroText:(NSAttributedString*)t
{
	if ([self class] == [SPRPage class])	//return a subclass instead
	{
		hash=NO_HASH; //set this to prevent the value 0 from being removed from the hash set in -dealloc
		return [[SPRPlainPage alloc] initWithIntroText:t];
	}
	
	self = [super init];
	
	if (self != nil)
	{
		hash = [SPRPage uniqueHash];
		title = @"untitled";
		secure=NO;
		
		SetUnsavedChanges(YES);
	}
	return self;
}

-(void)dealloc
{
	if (hash != NO_HASH) //this NO_HASH value exists because anytime an instance of SPRPage is created
						 //it is deallocated before a hash is generated for it. In it's place an SPRPlainPage
						 //instance is created
		[SPRPage unregisterHash:hash];
}


- (id)initWithCoder:(NSCoder *)decoder
{
	if ([self class] == [SPRPage class])
	{
		[NSException raise:@"SPRPageException"
					format:@"Attempted to instantiate abstract class SPRPage in -initWithCoder:"];
	}
	
	if (DecodeWithUniqueHashes == YES)
	{
		hash = [SPRPage uniqueHash];
	}
	else
	{
		hash = [decoder decodeIntForKey:@"Hash"];
		[SPRPage registerHash:hash];
	}
	
	title = [decoder decodeObjectForKey:@"Title"];
	if (title == nil)
		title = @"untitled";
	
	if (![decoder containsValueForKey:@"Titled"])
		titled = YES;
	else
		titled = [decoder decodeBoolForKey:@"Titled"];
	
	secure = [decoder decodeBoolForKey:@"Secure"];
	
	return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
	if ([self class] == [SPRPage class])
	{
		[NSException raise:@"SPRPageException"
					format:@"Attempted to encode abstract class SPRPage in -encodeWithCoder:"];
	}
	
	[encoder encodeInteger:hash forKey:@"Hash"];
	[encoder encodeObject:title forKey:@"Title"];
	[encoder encodeBool:titled forKey:@"Titled"];
	
	if (EncodeWithoutSMarker)
		[encoder encodeBool:NO forKey:@"Secure"];
	else
		[encoder encodeBool:secure forKey:@"Secure"];
}

- (id)copyWithZone:(NSZone *)zone
{
	NSAssert([self class]!=[SPRPage class],@"Class not subclassed properly");

	SPRPage *newPage = [[[self class] allocWithZone:zone] init];
	[newPage setTitle:title];
	[newPage setSecure:secure];
	return newPage;
}

- (NSUInteger)hash
{
    return hash;
}

-(SPRLockableTextStorage*)contentText
{
	[NSException raise:@"SPRPageException"
				format:@"Tried to called -contentText on page of class: %@", NSStringFromClass([self class])];
	return nil;
}

-(void)setContentText:(NSAttributedString*)value
{
	[NSException raise:@"SPRPageException"
				format:@"Tried to called -setContentText: on page of class: %@", NSStringFromClass([self class])];
}

-(NSString*)contentsAsString
{
	[NSException raise:@"SPRPageException"
				format:@"Unimplemented -contentsAsString in class: %@", NSStringFromClass([self class])];
	return nil;
}

-(NSAttributedString*)contentsAsAttributedString
{
	[NSException raise:@"SPRPageException"
				format:@"Unimplemented -contentsAsAttributedString in class: %@", NSStringFromClass([self class])];
	return nil;
}

- (NSString *)title
{
    return title;
}

- (void)setTitle:(NSString *)value
{
	//remove newlines(replace with spaces) and SPRInvisibleAttributeHolders
	
	value = [value stringByReplacingOccurrencesOfString:SPRInvisibleAttributeHolder
											 withString:@""];
	value = [value stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    if (title != value)
	{
		
        title = [value copy];
		
		[self setTitled:YES];
		[[NSNotificationQueue defaultQueue] enqueueNotification:
		 [NSNotification notificationWithName:PageTitleDidChangeNotification object:self]
												   postingStyle:NSPostASAP];
    }
}

- (BOOL)isSecure
{
    return secure;
}

- (void)setSecure:(BOOL)value
{
    if (secure != value)
	{
        secure = value;
		[[NSNotificationQueue defaultQueue] enqueueNotification:
		 [NSNotification notificationWithName:PageSecurityDidChangeNotification																																			object:self]
												   postingStyle:NSPostASAP];
    }
}

-(void)setTitled:(BOOL)value
{
	titled = value;
}

-(BOOL)isTitled
{
	return titled;
}

-(void)autotitle
{
	[NSException raise:@"SPRPageException"
				format:@"Called -autotitle, -autotitle unimplemented. Class: %@",
						NSStringFromClass([self class])];
}

-(void)autotitleWithString:(NSString*)s
{
	const static int maxLength=30;
	
	//NSMutableCharacterSet *trimSet = [NSMutableCharacterSet whitespaceAndNewlineCharacterSet];
	//because of ambiguities in 10.4 do this instead
	NSMutableCharacterSet *trimSet = [[NSCharacterSet whitespaceAndNewlineCharacterSet] mutableCopy];
	
	[trimSet formUnionWithCharacterSet:[NSMutableCharacterSet controlCharacterSet]];
	[trimSet formUnionWithCharacterSet:[NSCharacterSet characterSetWithCharactersInString:[NSString stringWithFormat:@"%C", 0xFFFC]]];
	
	NSArray *lines = [[s stringByTrimmingCharactersInSet:
					   trimSet]
					  componentsSeparatedByString:@"\n"];
	if ([lines count] == 0)
		return;
	
	if ([[lines objectAtIndex:0] length] == 0)
		return;
	
	NSString *string = [lines objectAtIndex:0];
	BOOL needsEllipsis=NO;
	
	if ([string length] > maxLength)
	{
		if ([[NSCharacterSet alphanumericCharacterSet] characterIsMember:[string characterAtIndex:maxLength]])
			needsEllipsis = YES;
		
		string = [string substringWithRange:NSMakeRange(0, maxLength)];
		
        NSUInteger untrimmedLength = [string length];
        string = [string stringByTrimmingCharactersInSet:
                  [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
		if (untrimmedLength > [string length])
			needsEllipsis = NO;
	}
	
	if (needsEllipsis)
		string = [string stringByAppendingFormat:@"%C",0x2026];
	
	[self setTitle:string];
}

#ifdef DEBUG
+(id)hashSet
{
	return sHashSet;
}
#endif

@end
