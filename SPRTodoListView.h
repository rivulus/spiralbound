//
//  SPRTodoListView.h
//  CocoaJunk
//
//  Created by Philip on 5/20/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

extern NSString *SPRPasteboardTypeTodoItem;

@class SPRTodoItemView,SPRTodoItemTextView,SPRTodoItemCheckbox,SPRTodoFillerView;
@interface SPRTodoListView : NSView
{
	//SPRTodoFillerView *fillerView;
	NSUInteger dropIndex; //set to UINT32_MAX to not draw
	NSMutableArray *items;
	NSMutableArray *textStorages;
	NSMutableArray *layoutManagers; //we need these so we can tell the corresponding textstorage to release it
									//when done
	NSColor *primaryColor, *secondaryColor;
	id delegate;
	
	BOOL hasSavedSelection;
	NSTextStorage *savedSelectionStorage;
	BOOL overrideSelection; //flag this to NOT set savedSelection on calls to -saveSelection. Instead only hasSavedSelection will be marked
	NSUInteger overrideIndex;
	//NSArray *savedCharacterSelections;
	NSRange savedCharacterSelection;
	
	BOOL ignoreResize; //set this when deleting items
	
	/*Dragging*/
	NSTextStorage *dragSourceItemTextStorage;
	NSArray *savedTextViewDragTypes;
    
}
-(NSArray*)supportedDragTypes;
-(NSArray*)supportedDropTypes;
-(void)addTextStorage:(NSTextStorage*)newStorage state:(BOOL)s;
-(void)setState:(BOOL)s forTextStorage:(NSTextStorage*)storage;
-(void)sizeAndPlaceItems;
-(void)itemDidResize:(SPRTodoItemView*)item;
-(void)selectNext:(id)sender wrap:(BOOL)wrap;
-(void)selectPrevious:(id)sender wrap:(BOOL)wrap;
-(SPRTodoItemView*)itemWithTextView:(SPRTodoItemTextView*)tv;
-(BOOL)setDropIndexFromPoint:(NSPoint)point;
-(void)unregisterTextViewDraggedTypes;
-(void)reregisterTextViewDraggedTypes;
-(void)setPrimaryBackgroundColor:(NSColor*)c;
-(void)setSecondaryBackgroundColor:(NSColor*)c;
+(NSColor*)calculateSecondaryColor:(NSColor*)c;
-(void)removeAllItems;
-(void)setDelegate:(id)del;
-(void)saveSelection;
-(void)loadSelection;
-(void)returnPressed:(SPRTodoItemTextView*)tv;
-(BOOL)backspacePressed:(SPRTodoItemTextView*)tv;
-(void)checkBoxClicked:(SPRTodoItemCheckbox*)checkBox;
-(void)startDragForEvent:(NSEvent*)event todoItemView:(SPRTodoItemView*)tiv;
-(void)deleteDragSourceItem;
-(void)clearDragSourceItem;
-(void)subrange:(NSRange*)subrange andIndex:(NSUInteger*)index forRange:(NSRange)inRange;
-(void)addTemporaryHighlightForRange:(NSRange)r;		//the range here is such that if all of the items text was treated as one string, concatenated without
														//delimiters
-(void)unselectOtherItems:(SPRTodoItemView*)newlySelectedItem;
-(void)removeTemporaryHighlights;

-(void)scrollToVisibleSubrange:(NSRange)r index:(NSUInteger)i;
-(void)scrollItemToVisible:(SPRTodoItemView*)item;

/*NSText and NSTextView messages. Used by SPRSearchEngine*/
//see notes on addTemporaryHighlightsForRange: about the ranges
- (void)scrollRangeToVisible:(NSRange)range;
- (void)setSelectedRange:(NSRange)range;
- (void)showFindIndicatorForRange:(NSRange)range;
- (NSTextStorage*)dragSourceItemTextStorage;
@end

@interface SPRTodoListView (Delegate)

-(void)todoListView:(SPRTodoListView*)tlv createNewItemAtIndex:(NSUInteger)index withContent:(NSAttributedString*)newText;
-(void)todoListView:(SPRTodoListView*)tlv deleteItemAtIndex:(NSUInteger)index preserveContents:(NSAttributedString*)contents;
-(void)todoListView:(SPRTodoListView*)tlv toggleItemAtIndex:(NSUInteger)index;
-(NSData*)todoListView:(SPRTodoListView*)tlv dataForPBoardType:(NSString*)type forDragFromItemAtIndex:(NSUInteger)index;
-(BOOL)todoListView:(SPRTodoListView*)tlv writeDroppedData:(NSData*)data ofType:(NSString*)type toIndex:(NSUInteger)index;
-(void)todoListView:(SPRTodoListView*)tlv moveDroppedData:(NSData*)data ofType:(NSString*)type toIndex:(NSUInteger)dstIndex fromIndex:(NSUInteger)srcIndex;

@end

@interface NSView (SPRTodoListView)

-(SPRTodoListView*)parentTodoListView;

@end
/*
@interface SPRTodoFillerView : NSView
{
	SPRTodoListView *todoListView; //not retained
}

-(id)initWithTodoListView:(SPRTodoListView*)tlv;
@end*/
