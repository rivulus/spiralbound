//
//  SBController.h
//  SpiralBound
//
//  Created by Philip White on 2/2/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SBTypes.h"

@class SBController, SpiralBoundWindow, SPRPage;
extern SBController *gSBController;
//extern SpiralBoundWindow *MainWindow;
//extern NSMutableDictionary *PageToPlaceTable;
//extern NSMutableDictionary *PageToSecurityTable;
extern time_t LastEventTime;
extern BOOL Inactive;
extern BOOL ShouldAnimate;
extern NSTimer *UpdateTimer;

extern time_t LastSavedTime;

@interface SBController : NSObject
{
	NSColor *equationColor;
	NSColor *solutionColor;
	
	BOOL allPagesAreSecured;
	
	
	//NSNumber *countOfPagesInStack;
	IBOutlet NSMenu *notebookMenu;
	
	BOOL mainWindowIsFloater;
	
	IBOutlet NSMenuItem *floatMenuItem;
	IBOutlet NSMenuItem *secureMenuItem;
	IBOutlet NSMenuItem *_tipJarMenuItem;
}



- (BOOL)allPagesAreSecured;
- (void)setAllPagesAreSecured:(BOOL)value;

- (void)applicationDidBecomeActive:(NSNotification*)aNotification;

- (void)notebookCountDidChange:(NSNotification*)notification;


- (void)updateNotebookMenu;

- (void)startInactivityTimer;

- (BOOL)canImportStickies;

- (BOOL)canImportSpiralBound;

- (void)startSaveTimer;

-(void)windowDidBecomeMain:(NSNotification*)note;
-(void)windowDidResignMain:(NSNotification*)note;

-(void)setSecureMenuItemTitleForPage:(SPRPage*)page;

@end


void ShowEasterEggForColor(NSColor *color);
#define GetUnregisteredPattern Zfj49fjl3EijP
NSColor *Zfj49fjl3EijP(NSColor *color);
extern const BOOL ShowPattern;
