//
//  NSAttributedString+Additions.m
//  SpiralBound Pro
//
//  Created by Philip White on 5/27/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "NSAttributedString+Additions.h"


@implementation NSAttributedString (RIVAdditions)

-(BOOL)attribute:(NSString*)attribute existsInRange:(NSRange)range andReturnsTrueForSelector:(SEL)selector
{
	id value;
	
	NSUInteger index;
	
	for (index=range.location; index < range.location+range.length; )
	{
		NSRange effectiveRange;
		value = [self attribute:attribute atIndex:index effectiveRange:&effectiveRange];
		if (objc_msgSend(value, selector))
			return YES;
		
		index = effectiveRange.location+effectiveRange.length;
	}
	return NO;
}

-(NSData*)dataUsingDocumentType:(NSString*)doctype
{
	NSDictionary *docAttrs = [NSDictionary dictionaryWithObject:doctype
														 forKey:NSDocumentTypeDocumentAttribute];
	NSData *data = [self dataFromRange:NSMakeRange(0,[self length])
					documentAttributes:docAttrs
								 error:NULL];
	return data;
}

@end
