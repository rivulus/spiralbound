//
//  KeyRecordField.m
//  KeyStroker
//
//  Created by Philip White on 6/15/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "KeyRecordField.h"

int keyCode;

extern NSString *AcceptEquationKey;

NSCharacterSet *VisibleSet;
NSString *SymbolLookup[72];


/*
			//NSInsertCharFunctionKey = 0xF73D,
			//NSDeleteCharFunctionKey = 0xF73E,
			//NSPrevFunctionKey = 0xF73F,
			//NSNextFunctionKey = 0xF740,
			//NSSelectFunctionKey = 0xF741,
			//NSExecuteFunctionKey = 0xF742,
			//NSUndoFunctionKey = 0xF743,
			//NSRedoFunctionKey = 0xF744,
			//NSFindFunctionKey = 0xF745,
			//NSHelpFunctionKey = 0xF746,
			//NSModeSwitchFunctionKey = 0xF747
*/
@implementation KeyRecordField

+ (void)initialize
{
	SymbolLookup[0] = [[NSString alloc] initWithFormat:@"%C", 0x2191];	//NSUpArrowFunctionKey = 0xF700,
	SymbolLookup[1] = [[NSString alloc] initWithFormat:@"%C", 0x2193];	//NSDownArrowFunctionKey = 0xF701,
	SymbolLookup[2] = [[NSString alloc] initWithFormat:@"%C", 0x2190];	//NSLeftArrowFunctionKey = 0xF702,
	SymbolLookup[3] = [[NSString alloc] initWithFormat:@"%C", 0x2192];	//NSRightArrowFunctionKey = 0xF703,
	SymbolLookup[4] = @"F1";
	SymbolLookup[5] = @"F2";
	SymbolLookup[6] = @"F3";
	SymbolLookup[7] = @"F4";
	SymbolLookup[8] = @"F5";
	SymbolLookup[9] = @"F6";
	SymbolLookup[10] = @"F7";
	SymbolLookup[11] = @"F8";
	SymbolLookup[12] = @"F9";
	SymbolLookup[13] = @"F10";
	SymbolLookup[14] = @"F11";
	SymbolLookup[15] = @"F12";
	//skip F13 through F35
	SymbolLookup[39] = @"Insert";
	SymbolLookup[40] = @"Delete";
	SymbolLookup[41] = @"Home";
	SymbolLookup[42] = @"Begin";
	SymbolLookup[43] = @"End";
	SymbolLookup[44] = @"Page Up";
	SymbolLookup[45] = @"Page Down";
	SymbolLookup[46] = @"Prnt Scrn";
	SymbolLookup[47] = @"Scrl Lck";
	SymbolLookup[48] = @"Pause";
	SymbolLookup[49] = @"Sys Req";
	SymbolLookup[50] = @"Break";
	SymbolLookup[51] = @"Reset";
	SymbolLookup[52] = @"Stop";
	SymbolLookup[53] = @"Menu";
	SymbolLookup[54] = @"User";
	SymbolLookup[55] = @"System";
	SymbolLookup[56] = @"Print";
	SymbolLookup[57] = @"Clr Ln";
	SymbolLookup[58] = @"Clr Dsp";
	SymbolLookup[60] = @"Ins Ln";
	SymbolLookup[61] = @"Del Ln";
	SymbolLookup[62] = @"Previous";
	SymbolLookup[63] = @"Next";
	SymbolLookup[64] = @"Select";
	SymbolLookup[65] = @"Execute";
	SymbolLookup[66] = @"Undo";
	SymbolLookup[67] = @"Redo";
	SymbolLookup[68] = @"Find";
	SymbolLookup[69] = @"Help";
	SymbolLookup[70] = @"Mode";
	
	NSMutableCharacterSet *temp = [[NSCharacterSet alphanumericCharacterSet] mutableCopy];
	[temp formUnionWithCharacterSet:[NSCharacterSet symbolCharacterSet]];
	[temp formUnionWithCharacterSet:[NSCharacterSet punctuationCharacterSet]];
	VisibleSet = [temp copy];
	
	id keyObj = [[NSUserDefaults standardUserDefaults] objectForKey:@"CompletionKeyCode"];
	if (keyObj)
		keyCode = [keyObj intValue];
	else
		keyCode = 0x03;	//enter
}

-(void)awakeFromNib
{		
	[self replaceKeyCode:keyCode];
	enabled=[[NSUserDefaults standardUserDefaults] boolForKey:@"CalculatorIsEnabled"];
	
	/*[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDefaultsDidChange:)
												 name:NSUserDefaultsDidChangeNotification object:[NSUserDefaults standardUserDefaults]];*/
	[[NSUserDefaults standardUserDefaults] addObserver:self
											forKeyPath:@"CalculatorIsEnabled"
											   options:NSKeyValueObservingOptionNew
											   context:NULL];
	/*NSTabView *parentTabView;
	NSView *view;
	while (view = [self superview])
	{
		if ([parentTabView isKindOfClass:[NSTabView class]])
		{
			[parentTabView setDelegate:self];
			NSEnumerator *e = [[parentTabView tabViewItems] objectEnumerator];
			NSTabViewItem *item;
			while (item = [e nextObject])
			{
				if ([self isDescendantOf:[item view]])
				{
					parentTabViewItem = item;
					break;
				}
			}
		}
		break;
	}*/
}

-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[[NSUserDefaults standardUserDefaults] removeObserver:self
											   forKeyPath:@"CalculatorIsEnabled"];
}

-(void)userDefaultsDidChange:(id)note
{
	enabled=[[NSUserDefaults standardUserDefaults] boolForKey:@"CalculatorIsEnabled"];
	[self setNeedsDisplay:YES];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
					  ofObject:(id)object
						change:(NSDictionary *)change
					   context:(void *)context
{
	enabled = [[change objectForKey:NSKeyValueChangeNewKey] boolValue];
	acceptingFirstResponder=NO;
	[self setNeedsDisplay:YES];
	if ([[self window] firstResponder] == self)
		[[self window] makeFirstResponder:nil];
}

- (void)mouseDown:(NSEvent*)event
{
	if (enabled == NO)
		return;
	
	acceptingFirstResponder=YES;
	[[self window] makeFirstResponder:self];
}

- (void)keyDown:(NSEvent*)event
{
	[self setKeyCode: ConvertKeyDownToKeyCode(event)];
	acceptingFirstResponder=NO;
	[[self window] makeFirstResponder:nil];
}

-(void)viewDidHide
{
	acceptingFirstResponder=NO;
	[[self window] makeFirstResponder:nil];
	[self setNeedsDisplay:YES];
}

- (void)drawRect:(NSRect)rect
{
	BOOL firstResponder = ([[self window] firstResponder] == self);
	
	
	[super drawRect:rect];
	
    NSRect bounds = [self bounds];
	bounds = NSInsetRect(bounds, 1.0, 1.0);
	
	NSDictionary *attributes;
	NSString *string;
	
	if (firstResponder)
	{
		attributes = [NSDictionary dictionaryWithObjectsAndKeys:[NSFont labelFontOfSize:11.0],
																	NSFontAttributeName,
																	[NSColor redColor],
																	NSForegroundColorAttributeName, nil];
		string = @"Press a Key";
	}	
	else
	{
		if (enabled)
			attributes = [NSDictionary dictionaryWithObjectsAndKeys:[NSFont labelFontOfSize:12.0], NSFontAttributeName, nil];
		else
			attributes = [NSDictionary dictionaryWithObjectsAndKeys:[NSFont labelFontOfSize:12.0], NSFontAttributeName,
																	[NSColor disabledControlTextColor], NSForegroundColorAttributeName, nil];
			
		string = ConvertKeyCodeToString(keyCode);
	}
	
	NSRect drawRect;
	drawRect.size = [string sizeWithAttributes:attributes];
	drawRect.origin.x = bounds.origin.x + bounds.size.width/2 - drawRect.size.width/2;
	drawRect.origin.y = bounds.origin.y + bounds.size.height/2 - drawRect.size.height/2;
	[string drawInRect:drawRect withAttributes:attributes];
}

- (BOOL)acceptsFirstResponder
{
	return acceptingFirstResponder;
}

- (BOOL)becomeFirstResponder
{
	[self setNeedsDisplay:YES];
	return YES;
}

- (BOOL)resignFirstResponder
{
	[self setNeedsDisplay:YES];
	return YES;
}

- (int)keyCode
{
    return keyCode;
}

- (void)setKeyCode:(int)value
{
    if (keyCode != value)
	{
        keyCode = value;
		[[NSUserDefaults standardUserDefaults] setInteger:keyCode forKey:@"CompletionKeyCode"];
    }
}


- (void)replaceKeyCode:(int)value
{
	//[displayString release];
	
	keyCode=value;
	//displayString = [ConvertKeyCodeToString(value) copy];
	[self setNeedsDisplay:YES];
}

- (void)setEnabled:(BOOL)yn
{
	enabled = yn;
	[self setNeedsDisplay:YES];
}

+ (BOOL)isKeyDownEqualToKeyCode:(NSEvent*)event
{
	NSString *characters = [event charactersIgnoringModifiers];
	
	if ([characters length] == 0)
		return NO;
	
	unichar character = [characters characterAtIndex:0];
	
	if ((character & 0xFF00) == 0xF700)
	{
		return (character & 0x00FF)+128 == keyCode;
	}
	else if (character >= 0x61 && character <= 0x7A) //convert to uppercase
	{
		return (character - 0x20) == keyCode;
	}
	else
	{
		return character == keyCode;
	}	
}

+ (void)reset
{
	keyCode = 0x03;
	
}

#pragma mark NSTabViewDelegate
- (void)tabView:(NSTabView *)tabView willSelectTabViewItem:(NSTabViewItem *)tabViewItem
{
	if ([[self window] firstResponder] == self && ![self isDescendantOf:[tabViewItem view]])
	{
		[[self window] makeFirstResponder:nil];
	}
	acceptingFirstResponder=NO;
}

@end

NSString *ConvertKeyCodeToString(int keyCode)
{
	if (keyCode == INT_MAX)
		return nil;
	else if (keyCode <= 127)
	{
		switch (keyCode)
		{
			case 0x03:
				return @"Enter";
				break;
			case 0x0D:
				return @"Return";
				break;
			case 0x7F:
				return @"Backspace";
				break;
			case 0x09:
				return @"Tab";
				break;
			case 0x1B:
				return @"Escape";
				break;
			case 0x20:
				return @"Space";
				break;
			default:
				return [[NSString stringWithFormat:@"%C", keyCode] uppercaseString];
				break;
		}
	}
	else if (keyCode <= 128+70)
		return SymbolLookup[keyCode-128];
	else
	{
		switch (keyCode)
		{
			case 128+70+1:
				return @"Shift";
				break;
			case 128+70+2:
				return @"Control";
				break;
			case 128+70+3:
				return @"Option";
				break;
			case 128+70+4:
				return @"Command";
				break;
			case 128+70+5:
				return @"Function";
				break;
			default:
				[NSException raise:@"MGInvalidKeyCode" format:@"KeyCode given = %i", keyCode];
				return nil;
		}
	}
}

int ConvertKeyDownToKeyCode(NSEvent *event)
{
	NSString *characters = [event charactersIgnoringModifiers];
	
	if ([characters length] == 0)
		return INT_MAX;
	
	unichar character = [characters characterAtIndex:0];
	
	if ((character & 0xFF00) == 0xF700)
	{
		return (character & 0x00FF)+128;
	}
	else if (character >= 0x61 && character <= 0x7A) //convert to uppercase
	{
		return character - 0x20;
	}
	else
	{
		return character;
	}	
}

int ConvertFlagsChangedToKeyCode(NSEvent *event)
{
	int modFlags = [event modifierFlags];
	
	int index;
	
	//[displayString release];
	
	
	if (modFlags & NSShiftKeyMask)
	{
		index = 128+70+1;
	}
	else if (modFlags & NSControlKeyMask)
	{
		index = 128+70+2;
	}
	else if (modFlags & NSAlternateKeyMask)
	{
		index = 128+70+3;
	}
	else if (modFlags & NSCommandKeyMask)
	{
		index = 128+70+4;
	}
	else if (modFlags & NSFunctionKeyMask)
	{
		index = 128+70+5;
	}
	else
	{
		index = INT_MAX;
	}
	return index;
}


