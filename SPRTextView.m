//
//  SPRTextView.m
//  SpiralBound
//
//  Created by Philip White on 2/12/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//


#import "SPRTextView.h"
#import "SBController.h"
/*#import "SBController+Delegate.h"
#import "SBController+Security.h"
#import "PrintingView.h"*/
#import "SBConstants.h"
#import "SBProtoFrameView.h"
#import "NSAttributedString+Additions.h"
#import "SPRLockableTextStorage.h"
#import "SPRCalculatorHandler.h"

@implementation SPRTextView

//static NSImage *LockedImage, *UnlockedImage;

/*+ (void)initialize
{
	LockedImage = [[NSImage imageNamed:@"LockedLock.tiff"] retain];
	UnlockedImage = [[NSImage imageNamed:@"UnlockedLock.tiff"] retain];
}*/

-(id)initWithFrame:(NSRect)frame textContainer:(NSTextContainer*)textContainer
{
	self = [super initWithFrame:frame textContainer:textContainer];
	if (self)
	{
		calculatorHandler = [[SPRCalculatorHandler alloc] initWithTextView:self];
		[self setDelegate:self];
		[self setAllowsUndo:YES];
		[self setRichText:YES];
		[self setImportsGraphics:YES];
		
		if ([[textContainer layoutManager] textStorage] != [SPRLockableTextStorage sharedEmptyStorage])
		{
			[[NSNotificationCenter defaultCenter] addObserver:self
													 selector:@selector(textStorageLockStatusDidChange:)
														 name:SPRLockableTextStorageLockStatusDidChange
													   object:[[textContainer layoutManager] textStorage]];
		}
		
		BOOL l = [(SPRLockableTextStorage *)[self textStorage] locked] || SPRLockableTextStorageAllAreLocked();
		[self setEditable:!l];
		if ([[[self textStorage] string] hasPrefix:SPRInvisibleAttributeHolder])
		{
			[self setSelectedRange:NSMakeRange(1,0)];
		}
		else
		{
			[self setSelectedRange:NSMakeRange(0,0)];
		}
	}
	return self;
}

-(id)initWithFrame:(NSRect)frame
{
	SPRLockableTextStorage *ts = [SPRLockableTextStorage sharedEmptyStorage];
	
	NSLayoutManager *layoutManager;
	layoutManager = [[NSLayoutManager alloc] init];
	[ts addLayoutManager:layoutManager];
	
	NSTextContainer *container;
	NSSize cSize = frame.size;
	cSize.height = FLT_MAX;
	
	container = [[NSTextContainer alloc]
				 initWithContainerSize:cSize];
	[container setWidthTracksTextView:YES];
	[layoutManager addTextContainer:container];
	
	self = [self initWithFrame:frame textContainer:container];
	
	return self;
}


/*- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	BOOL l = [(SPRLockableTextStorage *)[self textStorage] locked] || SPRLockableTextStorageAllAreLocked();
	[self setEditable:!l];
}*/

-(void)textStorageLockStatusDidChange:(NSNotification*)note
{
	BOOL l = [(SPRLockableTextStorage *)[self textStorage] locked] || SPRLockableTextStorageAllAreLocked();
	[self setEditable:!l];
}

-(void)replaceTextStorage:(SPRLockableTextStorage*)newTS
{
	//if there is an equation on this page, clear it
	[calculatorHandler erasePotentialEquation];
	
	//erase undo actions for this text view
	[[self undoManager] removeAllActions];
	
	//[[self textStorage] removeObserver:self forKeyPath:@"locked"];
	
	[self setSelectedRange:NSMakeRange(0,0)];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:SPRLockableTextStorageLockStatusDidChange
												  object:nil];
	
	if (newTS == nil)
		newTS = [SPRLockableTextStorage sharedEmptyStorage];
	
	NSLayoutManager *lm = [self layoutManager];
	
	//[lm replaceTextStorage:nil];
	NSTextStorage *oldTS = [self textStorage];
	
	@try
	{
		[oldTS removeLayoutManager:lm];
		
		[newTS addLayoutManager:lm];
	}
	@catch (NSException * e)
	{
		NSLog(@"Ignoring exception thrown by [TextStorage addLayoutManager].");
	}
	
	if (newTS != [SPRLockableTextStorage sharedEmptyStorage])
	{
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(textStorageLockStatusDidChange:)
													 name:SPRLockableTextStorageLockStatusDidChange
												   object:newTS];
	}
	NSRect f = [self frame];
	f.size.height = FLT_MAX;
	[self setFrame:f];
	[self sizeToFit];
	
	BOOL l = [(SPRLockableTextStorage *)[self textStorage] locked] || SPRLockableTextStorageAllAreLocked();
	[self setEditable:!l];
	
	if ([[newTS string] hasPrefix:SPRInvisibleAttributeHolder])
	{
		[self setSelectedRange:NSMakeRange(1,0)];
	}
	else
	{
		[self setSelectedRange:NSMakeRange(0,0)];
	}
}


- (BOOL)acceptsFirstMouse:(NSEvent *)theEvent
{
	return YES;
}

-(void)viewDidMoveToWindow
{
	[calculatorHandler textViewDidMoveToWindow];
	[super viewDidMoveToWindow];
}



#pragma mark EVENTS AND ACTIONS

- (void)keyDownSkipCalculator:(NSEvent *)theEvent
{
	[super keyDown: theEvent];
}

- (void)keyDown:(NSEvent *)theEvent
{
	if ([calculatorHandler keyDown:theEvent inView:self] == NO)
		[super keyDown: theEvent];
}


- (void)undo:(id)sender
{
	hasPotentialEquation = NO;
	
	[[self nextResponder] tryToPerform:@selector(undo:) with:sender];
}

- (void)paste:(id)sender
{
	[calculatorHandler erasePotentialEquation];
	[super paste:sender];
}

- (void)pasteFont:(id)sender
{
	[calculatorHandler erasePotentialEquation];
	[super pasteFont:sender];
}

- (void)pasteRuler:(id)sender
{
	[calculatorHandler erasePotentialEquation];
	[super pasteRuler:sender];
}

//strikes out the whole line (or unstrikes)
- (void)strikeout:(id)sender
{
	//first find the range of the line
	NSTextStorage *storage = [self textStorage];
	NSRange selection = [self selectedRange];
	
	NSString *plainText = [storage string];
	
	NSRange lineRange = [plainText lineRangeForRange:selection];
	
	
	//determine if there is any strikethrough in the relevant text
	BOOL unstrike = [storage attribute:NSStrikethroughStyleAttributeName existsInRange: lineRange andReturnsTrueForSelector:@selector(intValue)];
	if ( unstrike )
	{
		//unstrike
		[storage addAttribute:NSStrikethroughStyleAttributeName value: [NSNumber numberWithInt:NSUnderlineStyleNone] range:lineRange];
	}
	else
	{
		[storage addAttribute:NSStrikethroughStyleAttributeName value: [NSNumber numberWithInt:NSUnderlineStyleThick] range:lineRange];
	}
	
	
	
}

#pragma mark PRINTING


- (IBAction)print:(id)sender
{
	//pass this on to the frame view, where it is more easily handled
	[[[self window] contentView] print:sender];
	
}

- (void)mouseDown:(NSEvent*)event
{
	/*if ([gSBController allPagesAreScrambled] == YES)
	{
		[gSBController applicationBecameActive];
	}*/
	
	[super mouseDown:event];
}

#pragma mark PASTEBOARD

- (NSArray *)writablePasteboardTypes
{
	NSArray *types = [super writablePasteboardTypes];
	NSRange selection = [self selectedRange];
	
	if ([self selectedRange].length == 1)	//see if an attachment by itself is being dragged
	{
		NSTextAttachment *attachment = [[self textStorage] attribute:NSAttachmentAttributeName atIndex: selection.location effectiveRange:NULL];
		if (attachment != nil)
		{
			NSFileWrapper *fileWrapper = [attachment fileWrapper];
			if ([fileWrapper isRegularFile])
			{
				if ((imageClipping = [[NSImage alloc] initWithData:[fileWrapper regularFileContents]]))
					types = [types arrayByAddingObject:NSTIFFPboardType];
			}
		}
	}
	
	return types;
}

- (BOOL)writeSelectionToPasteboard:(NSPasteboard *)pboard type:(NSString *)type
{
	if ([type isEqualToString:NSTIFFPboardType])
	{
		BOOL result = [pboard setData: [imageClipping TIFFRepresentation]
							  forType:NSTIFFPboardType];
		imageClipping=nil;
		if (!result)
			NSBeep();
		return result;
	}
	else
		return [super writeSelectionToPasteboard:pboard type:type];
}


-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:SPRLockableTextStorageLockStatusDidChange
												  object:nil];
	//[[self textStorage] removeObserver:self forKeyPath:@"locked"];
}

- (void)setSelectedRanges:(NSArray*)ranges affinity:(NSSelectionAffinity)affinity stillSelecting:(BOOL)flag
{
	NSRange charRange = [[ranges objectAtIndex:0] rangeValue];
	if (flag == NO)
	{
		NSTextStorage *ts = [self textStorage];
		if ([[ts string] hasPrefix:SPRInvisibleAttributeHolder])
		{
			if (charRange.location == 0)
			{
				charRange.location = 1;
				if (charRange.length > 0)
					charRange.length--;
				
				NSMutableArray *r = [ranges mutableCopy];
				[r replaceObjectAtIndex:0 withObject:[NSValue valueWithRange:charRange]];
				ranges = (id)r;
			}
		}
	}
	[super setSelectedRanges:ranges affinity:affinity stillSelecting:flag];
}

#pragma mark Calculator


- (BOOL)textView:(NSTextView *)aTextView shouldChangeTextInRange:(NSRange)affectedCharRange replacementString:(NSString *)replacementString
{
	return [calculatorHandler textView:aTextView shouldChangeTextInRange:affectedCharRange replacementString:replacementString];
}

- (void)textDidChange:(NSNotification *)aNotification
{
	[calculatorHandler textDidChange:aNotification];
	/*if ([[self textStorage] length] == 0)
	{
		id obj = [[NSUserDefaults standardUserDefaults] objectForKey:DefaultsPreferencesTextAttributes];
		[self setTypingAttributes:[NSUnarchiver unarchiveObjectWithData:obj]];
	}*/
}

- (NSRange)textView:(NSTextView *)aTextView willChangeSelectionFromCharacterRange:(NSRange)oldSelectedCharRange toCharacterRange:(NSRange)newSelectedCharRange
{
	return [calculatorHandler textView:aTextView willChangeSelectionFromCharacterRange: oldSelectedCharRange
					  toCharacterRange:newSelectedCharRange];
}

- (BOOL)becomeFirstResponder
{
	/*if ([[self textStorage] length] == 0)
	{
		id obj = [[NSUserDefaults standardUserDefaults] objectForKey:DefaultsPreferencesTextAttributes];
		[self setTypingAttributes:[NSUnarchiver unarchiveObjectWithData:obj]];
	}*/
	return [super becomeFirstResponder];
}

@end
