//
//  NSMutableArray+PushPop.h
//  SpiralBound
//
//  Created by Philip White on 2/11/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface NSMutableArray (PushPop)

- (id)pop;
- (void)push:(id)object;

@end
