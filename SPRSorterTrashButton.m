//
//  SPRSorterTrashButton.m
//  CommonSpiralbound
//
//  Created by Philip on 7/6/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import "SPRSorterTrashButton.h"
#import "PasteboardTypes.h"
#import "PageSorter_SBPro.h"
#import "SPRTextView.h"
#import "OrganizerIncludes.h"
#import "SPRNotebook.h"

@implementation SPRSorterTrashButton

- (void)awakeFromNib
{
    //register to receive drag data types
	[self registerForDraggedTypes:[NSArray arrayWithObjects:SPRHashArrayPasteboardType, SPRTitleArrayPasteboardType, nil]];
}


#pragma mark NSDraggingDestination Protocol

- (NSDragOperation)draggingEntered:(id < NSDraggingInfo >)sender
{
	[self highlight:YES];
	return NSDragOperationDelete;
}

- (void)draggingExited:(id < NSDraggingInfo >)sender
{
	[self highlight:NO];
}

- (BOOL)performDragOperation:(id < NSDraggingInfo >)sender
{
	NSPasteboard *pboard = [sender draggingPasteboard];
	
	NSArray *types = [NSArray arrayWithObjects:SPRTitleArrayPasteboardType,SPRHashArrayPasteboardType,nil];
	NSString *t = [pboard availableTypeFromArray:types];
	NSArray *data = [NSKeyedUnarchiver unarchiveObjectWithData:[pboard dataForType:t]];
	
	if ([t isEqualToString:SPRTitleArrayPasteboardType])
	{
		[self deleteNotebooks:data];
	}
	else if ([t isEqualToString:SPRHashArrayPasteboardType])
	{
		[self deletePages:data];
	}
	else
	{
		[NSException raise:@"SPRSorterTrashButtonException" format:@"Pasteboard data of incorrect type"];
	}
	
	[self highlight:NO];
	return YES;
}

-(void)deleteNotebooks:(NSArray*)titles
{	
	if ([[SPROrganizer organizers] count] == [titles count])
	{
		NSRunAlertPanel(@"You may not delete all of your notepads!", @"", nil, nil, nil);
		return;
	}
	
	BOOL multiple = [titles count] > 1;
	
	int r;
	if (multiple)
	{
		r = NSRunAlertPanel(@"Delete Notepads?",
							@"Are you sure you would like to permanently delete the selected notepads?",
							@"Cancel", @"Delete", nil);
	}
	else
	{
		r = NSRunAlertPanel(@"Delete Notepad?",
							@"Are you sure you would like to permanently delete the notepad \"%@\"?",
							@"Cancel", @"Delete", nil, [titles objectAtIndex:0]);
	}
	if (r == NSAlertDefaultReturn)
		return;
	
	for (id t in titles)
		[SPROrganizer deleteNotebookWithOrganizer:[SPROrganizer organizerForTitle:t]]; 
}

-(void)deletePages:(NSArray*)hashes
{
	NSArray *pages = [SPRNotebook pagesWithHashes:hashes];
	
	BOOL multiple = [pages count] > 1;
	
	int r;
	if (multiple)
	{
		r = NSRunAlertPanel(@"Delete Pages?",
							@"Are you sure you would like to permanently delete the selected pages?",
							@"Cancel", @"Delete", nil);
	}
	else
	{
		r = NSRunAlertPanel(@"Delete Page?",
							@"Are you sure you would like to permanently delete the page \"%@\"?",
							@"Cancel", @"Delete", nil, [[pages objectAtIndex:0] title]);
	}
	if (r == NSAlertDefaultReturn)
		return;
	
	//it's not actually possible to select multiple pages which are not in the same notebook
	//but assume that it is
	for (id p in pages)
	{
		SPROrganizer *nbo = [SPROrganizer organizerForPage:p];
		[nbo removePage: p];
	}
}

@end
