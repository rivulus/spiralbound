//
//  NSMutableArray+PushPop.m
//  SpiralBound
//
//  Created by Philip White on 2/11/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "NSMutableArray+PushPop.h"


@implementation NSMutableArray (PushPop)

- (id)pop
{
	id object = [self lastObject];
	if (object)
		[self removeLastObject];
	return object;
}

- (void)push:(id)object
{
	[self addObject: object];
}

@end
