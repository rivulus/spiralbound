//
//  SPRLockableTextStorage.m
//  LockableTextStorageTest
//
//  Created by Philip on 5/27/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//


extern void SetUnsavedChanges(BOOL);

#import "SPRLockableTextStorage.h"

static BOOL AllLocked=NO;

// An array of blocks with weak references to SPRLockableTextStorage.
// e.g. SPRLockableTextStorage *storage = [All firstObject]();
typedef SPRLockableTextStorage *(^WeakHolderBlock)(void);
static NSMutableArray<WeakHolderBlock> *All=nil;

BOOL SPRLockableTextStorageAllAreLocked(void)
{
	return AllLocked;
}

void SPRLockableTextStorageLockAll(void)
{
	AllLocked=YES;
    
    for (WeakHolderBlock block in All)
    {
        SPRLockableTextStorage *storage = block();
        if (storage != nil)
        {
            [storage edited: NSTextStorageEditedCharacters
                      range: NSMakeRange(0,[storage length])
             changeInLength: 0];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:SPRLockableTextStorageLockStatusDidChange
                                                                object:storage];
        }
    }
}

void SPRLockableTextStorageUnlockAll(void)
{
	AllLocked=NO;
    
    for (WeakHolderBlock block in All)
    {
        SPRLockableTextStorage *storage = block();
        if (storage != nil)
        {
            [storage edited: NSTextStorageEditedCharacters
                      range: NSMakeRange(0,[storage length])
             changeInLength: 0];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:SPRLockableTextStorageLockStatusDidChange
                                                                object:storage];
        }
    }
}

static void RegisterStorage(SPRLockableTextStorage *newStorage)
{
    @synchronized ([SPRLockableTextStorage class])
    {
        __weak SPRLockableTextStorage *weakNewStorage = newStorage;
        WeakHolderBlock holder = ^{
            return weakNewStorage;
        };
        
        if (All==nil)
            All = [NSMutableArray new];
        
        [All addObject:[holder copy]];
    }
}

static void UnregisterStorage(SPRLockableTextStorage *s)
{
    @synchronized ([SPRLockableTextStorage class])
    {
        for (WeakHolderBlock holder in All)
        {
            if (holder() == s)
            {
                [All removeObject:holder];
                break;
            }
        }
    }
}

NSString *SPRLockableTextStorageLockStatusDidChange = @"SPRLockableTextStorageLockStatusDidChange";

@interface SPRLockableTextStorage ()

-(NSMutableAttributedString*)storage;

@end

@implementation SPRLockableTextStorage

+(SPRLockableTextStorage*)sharedEmptyStorage
{
	static id s=nil;
	if (s == nil)
		s = [[self alloc] init];
	return s;
}

-(id)init
{
	self = [super init];
	if (self)
	{
		storage = [[NSMutableAttributedString alloc] init];
		locked = NO;
		RegisterStorage(self);
	}
	return self;
}

-(id)initWithString:(NSString*)str
{
	self = [self init];
	if (self)
	{
		[self replaceCharactersInRange:NSMakeRange(0,0) withString:str];
	}
	return self;
}

- (id)initWithAttributedString:(NSAttributedString *)attributedString
{
	self = [self init];
	if (self)
	{
		[self setAttributedString:attributedString];
	}
	return self;
}

- (id)initWithString:(NSString *)aString attributes:(NSDictionary *)attributes
{
	self = [self init];
	if (self)
	{
		[self replaceCharactersInRange:NSMakeRange(0,0) withString:aString];
		[self setAttributes:attributes range:NSMakeRange(0,[aString length])];
	}
	return self;
}

//no need for a decoder because this method simply encodes as an NSAttributedString
//security is ignored
- (void)encodeWithCoder:(NSCoder *)encoder
{
	return [storage encodeWithCoder:encoder];
}

-(id)mutableCopyWithZone:(NSZone *)zone
{
	SPRLockableTextStorage *newTS = [[[self class] allocWithZone:zone] init];
	
	[newTS setAttributedString:self];
	[newTS setLocked:locked];
	
	return newTS;
}

-(BOOL)locked {return locked;}
-(void)setLocked:(BOOL)yn
{
	if (locked != yn)
	{
		locked=yn;
		[self edited: NSTextStorageEditedCharacters
			   range: NSMakeRange(0,[storage length])
	  changeInLength: 0];
		
		[[NSNotificationCenter defaultCenter] postNotificationName:SPRLockableTextStorageLockStatusDidChange
															object:self];
		
		SetUnsavedChanges(YES);
	}
}

-(NSMutableAttributedString*)storage
{
	return storage;
}

- (NSDictionary *)attributesAtIndex:(NSUInteger)index effectiveRange:(NSRangePointer)aRange
{
	return [[self storage] attributesAtIndex: index effectiveRange: aRange];
}

- (void)replaceCharactersInRange:(NSRange)aRange withString:(NSString *)aString
{
	if (bufferedDummy)
	{
		bufferedDummy = nil;
	}
	
	[[self storage] replaceCharactersInRange: aRange withString: aString];
	
	NSInteger changeInLength = [aString length] - aRange.length;
	
	[self edited: NSTextStorageEditedCharacters range: aRange 
  changeInLength: changeInLength];
  
	SetUnsavedChanges(YES);
}

- (void)setAttributes:(NSDictionary *)attributes range:(NSRange)aRange
{
	/*NSMutableDictionary *modAttributes = [NSMutableDictionary dictionaryWithDictionary: attributes];
	
	if ([attributes objectForKey:NSBackgroundColorAttributeName] != nil)
	{
		[modAttributes removeObjectForKey:NSBackgroundColorAttributeName];
	}*/
	
	[[self storage] setAttributes: attributes range: aRange];
	
	[self edited: NSTextStorageEditedAttributes range: aRange 
  changeInLength: 0];
  
	SetUnsavedChanges(YES);
}

- (NSString*)stringIgnoringSecurity
{
	return [[self storage] string];
}

- (NSString *)string
{
	if (!locked && !AllLocked)
	{
		return [[self storage] string];
	}
	else
	{
		if (!bufferedDummy)
		{
			NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
			NSString *contentStr = [[self storage] string];
			NSUInteger length = [contentStr length];
			NSMutableString *dummy = [NSMutableString stringWithCapacity:length];
			
			NSUInteger i;
			
			for (i=0; i<length;i++)
			{
				unichar c = [contentStr characterAtIndex:i];
				if (![whiteSpace characterIsMember:c])
					c = 0x2022;
				[dummy appendFormat:@"%C",c];
			}
			bufferedDummy = [dummy copy];
		}
		
		return bufferedDummy;
	}
}

-(void)dealloc
{
	UnregisterStorage(self);
}


@end
