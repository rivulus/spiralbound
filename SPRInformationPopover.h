//
//  SPRInformationPopover.h
//  SpiralBound Pro
//
//  Created by Philip White on 4/10/18.
//

#import <Cocoa/Cocoa.h>

@interface SPRInformationPopover : NSViewController

@property (nonatomic, copy) NSString *text;

+ (NSPopover *)popoverWithText:(NSString *)text;

@end
