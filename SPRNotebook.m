//
//  SPRNotebook.m
//  SpiralBound
//
//  Created by Philip White on 2/5/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "SPRNotebook.h"
#import "SPRPage.h"
#import "SPRPlainPage.h"
#import "SPRPreferencesController.h"
#import "NotificationNames.h"
#import "SPRTodoPage.h"

#import "EMKeychainItem.h"
#import "NSMutableData+AES256.h"
#import "DefaultsKeys.h"

extern NSString *ServiceName;
//these are expanded in +initialize
//NSString *DataFolderPath = @"~/Library/Application Support/SpiralBound Pro/";
NSString *OriginalSBDataPath = @"~/Library/Application Support/SpiralBound/Data";
//#else
//NSString *DataFolderPath = @"~/Library/Application Support/SpiralBound/";

NSString *StickiesDataPath = @"~/Library/StickiesDatabase";
static NSArray *Notebooks=nil;

@implementation SPRNotebook

+(void)initialize
{
	//DataFolderPath = [[DataFolderPath stringByExpandingTildeInPath] retain];
	OriginalSBDataPath = [OriginalSBDataPath stringByExpandingTildeInPath];
	StickiesDataPath = [StickiesDataPath stringByExpandingTildeInPath];
	
	//call sharedNotebook to get it to allocate the instance
	//if ([self notebooks] == nil)
	//	NSLog(@"Failed to allocate Notebooks");
}

+(NSArray*)notebooks
{
    @synchronized (self)
    {
        if (Notebooks == nil)
        {
            Notebooks = [[NSArray alloc] init];
		}
    }
	
	return Notebooks;
}

+(BOOL)notebookDataExists
{
	NSFileManager *fm = [NSFileManager defaultManager];
	NSError *outError=nil;
	NSEnumerator *enumerator = [[fm contentsOfDirectoryAtPath:[[SPRPreferencesController sharedPreferencesController] dataFolderPath]
														error:&outError] objectEnumerator];
	if (enumerator == nil)
	{
		NSLog(@"+notebookDataExists error from contentsOfDirectoryAtPath: %@",outError);
		return NO;
	}
	
	NSString *path;
	while (path = [enumerator nextObject])
	{
		BOOL dir=NO;
		[fm fileExistsAtPath:path isDirectory:&dir];
		if (dir)
			continue;
		
		if ([path rangeOfString:@"Data for "].location == 0)
			return YES;
		
	}
	return NO;
}

+(SPRNotebook*)notebookWithTitle:(NSString*)title
{
	NSEnumerator *enumerator = [[self notebooks] objectEnumerator];
	SPRNotebook *notebook;
	
	while (notebook = [enumerator nextObject])
	{
		if ([[notebook title] isEqualToString: title])
			return notebook;
	}
	
	//didn't find it if we got here
	//try to load it
	{
		NSString *fileName = [NSString stringWithFormat: @"Data for %@", title];
		SPRNotebook *notebook = [[self alloc] initWithFileName:fileName];
		
		if (notebook == nil)
		{
			NSLog(@"Failed to load notebook \"%@\"",fileName);
		}
		else
		{
			[self addNotebook:notebook];
			
			[[NSNotificationQueue defaultQueue] enqueueNotification:
			 [NSNotification notificationWithName:NotebookDidChangeLengthNotification																																				object:notebook]
													   postingStyle:NSPostASAP];
		}
		return notebook;
	}
}

+(NSArray*)loadAllNotebooksWithUniqueHashes
{
	NSMutableArray *newlyLoadedNotebooks = [NSMutableArray array];
	NSError *err=nil;
    
	NSString *dataFolderPath = [[SPRPreferencesController sharedPreferencesController] dataFolderPath];
	NSArray *allDataFiles = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataFolderPath
                                                                                error:&err];
	if (allDataFiles == nil)
    {
		[NSException raise:@"Data Folder Path Invalid" format:@"NSFileManager's directoryContentsAtPath: error= \"%@\"", err];
    }
	
	NSEnumerator *enumerator = [allDataFiles objectEnumerator];
	NSString *path;
	while (path = [enumerator nextObject])
	{
		if ([path hasPrefix:@"Data for "])
		{
			NSString *dataName = [path substringFromIndex:9];
			
			[self beginDecodingWithUniqueHashes];
			{
				id newNotebookPages = [NSKeyedUnarchiver unarchiveObjectWithFile:[[dataFolderPath stringByAppendingFormat:@"/%@", path] stringByStandardizingPath]];
				if (newNotebookPages != nil)
				{
					SPRNotebook *nb = [self createNotebookWithObject:newNotebookPages title:dataName];
					
					[newlyLoadedNotebooks addObject:nb];
				}
			}
			[self endDecodingWithUniqueHashes];
			
		}
	}
	return newlyLoadedNotebooks;
}

//returns an array of the notebooks;
//this is used to get notebooks that aren't recorded in the organizer
+(NSArray*)loadUnloadedNotebooksWithUniqueHashes
{
	NSMutableArray *newlyLoadedNotebooks = [NSMutableArray array];
    NSError *err = nil;
	
    NSString *dataFolderPath = [[SPRPreferencesController sharedPreferencesController] dataFolderPath];
	__block NSArray *allDataFiles = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataFolderPath error:&err];
	if (allDataFiles == nil)
    {
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
		[NSException raise:@"Data Folder Path Invalid" format:@"NSFileManager's directoryContentsAtPath: error= \"%@\"", err];
        
        // Show the user an open panel.
        NSOpenPanel *panel = [NSOpenPanel openPanel];
        [panel setCanChooseDirectories:YES];
        [panel setCanChooseFiles:NO];
        
        [panel setDirectoryURL:[NSURL fileURLWithPath:dataFolderPath]];
        [panel setPrompt:@"SpiralBound Pro was unable to access the files containing your data. Please select the location of the SpiralBound Pro data. Note: If you have set SpiralBound Pro to store its data files in other than the default location, you will be prompted for the location of the files every time you run the app."];
        [panel beginWithCompletionHandler:^(NSInteger result) {
            if (result == NSFileHandlingPanelOKButton)
            {
                allDataFiles = [[NSFileManager defaultManager] contentsOfDirectoryAtPath: [[panel directoryURL] path]
                                                                    error: NULL];
                
            }
            dispatch_semaphore_signal(semaphore);}];
        
        // wait for the open panel to finish
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    }
		
	NSEnumerator *enumerator = [allDataFiles objectEnumerator];
	NSString *path;
	while (path = [enumerator nextObject])
	{
		if ([path hasPrefix:@"Data for "])
		{
			NSString *dataName = [path substringFromIndex:[@"Data for " length]];
			
			if ([self isTitleAvailable:dataName]) //if yes, then this wasn't loaded
			{
				[self beginDecodingWithUniqueHashes];
				{
					id newNotebookPages = [NSKeyedUnarchiver unarchiveObjectWithFile:[[dataFolderPath stringByAppendingFormat:@"/%@", path] stringByStandardizingPath]];
					if (newNotebookPages != nil)
					{
						SPRNotebook *nb = [self createNotebookWithObject:newNotebookPages title:dataName];
						
						[newlyLoadedNotebooks addObject:nb];
					}
				}
				[self endDecodingWithUniqueHashes];
			}
			
		}
	}
	return newlyLoadedNotebooks;
}

+(SPRNotebook*)notebookContainingPage:(SPRPage*)page
{
	NSEnumerator *enumerator = [[self notebooks] objectEnumerator];
	SPRNotebook *notebook;
	
	while (notebook = [enumerator nextObject])
	{
		if ([[notebook pages] containsObject:page])
			return notebook;
	}
	
	return nil;
}

+(void)beginEncodingWithoutSMarker
{
	[SPRPage beginEncodingWithoutSMarker];
}

+(void)endEncodingWithoutSMarker
{
	[SPRPage endEncodingWithoutSMarker];
}

+(void)beginDecodingWithUniqueHashes
{
	[SPRPage beginDecodingWithUniqueHashes];
}

+(void)endDecodingWithUniqueHashes
{
	[SPRPage endDecodingWithUniqueHashes];
}

+(BOOL)isTitleAvailable:(NSString*)title
{	
	NSEnumerator *enumerator = [[self notebooks] objectEnumerator];
	SPRNotebook *notebook;
	
	while (notebook = [enumerator nextObject])
	{
		if ([[notebook title] isEqualToString: title])
			return NO;
	}
	
	//didn't find it if we got here
	return YES;
	
}

//this is the only method that adds to the array
//it returns the new array
+(NSArray*)addNotebook:(SPRNotebook*)newNotebook
{
	Notebooks = [Notebooks arrayByAddingObject:newNotebook];
	return Notebooks;
}

+(SPRPage*)pageWithHash:(NSUInteger)hash
{
	NSEnumerator *enumerator = [[self notebooks] objectEnumerator];
	SPRNotebook *notebook;
	SPRPage *page=nil;
	while (notebook = [enumerator nextObject])
	{
		page = [notebook pageWithHash: hash];
		
		if (page != nil)
			break;
	}
	
	return page;
}

+(NSArray*)pagesWithHashes:(NSArray*)hashes
{
	NSMutableArray *pages = [[NSMutableArray alloc] initWithCapacity: [hashes count]];
	
	NSEnumerator *enumerator = [hashes objectEnumerator];
	NSNumber *hash;
	
	while (hash = [enumerator nextObject])
	{
		SPRPage *page;
		page = [self pageWithHash:[hash intValue]];
		
		if (page == nil)
			[NSException raise:@"Unknown Page Hash Exception" format:@"Page with hash %u could not be found in any notebook", [hash intValue]];
		
		[pages addObject:page];
	}
	return pages;
}

+(SPRNotebook*)createNotebookWithTitle:(NSString*)title
{
	SPRNotebook *newNotebook = [[self alloc] initWithTitle:title];
	
	if (newNotebook != nil)
		[SPRNotebook addNotebook:newNotebook];
	
	
	return newNotebook;
}

+(SPRNotebook*)createNotebookWithIntroText
{
	if ([[self notebooks] count] > 0)
		@throw([NSException exceptionWithName:@"+createNotebookWithIntroText error"
									   reason:@"Attempted to create intro notebook at inappropriate time"
									 userInfo:nil]);
	

	NSString *titles[3] = {@"Quick Tips", @"Questions?", @"Let's Get Acquainted!"};

	SPRNotebook *newNotebook = [[self alloc] initWithTitle:[NSString stringWithFormat:@"%@'s Notepad", NSUserName()]];
	SPRPage *blankPage = [[newNotebook pages] objectAtIndex:0];
	int x;
	for (x=0; x<3; x++)
	{
		NSAttributedString *pageText = nil;
		NSString *path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"IntroText%i", x+1]
														 ofType:@"rtf"];
		if (path != nil)
			pageText = [[NSAttributedString alloc] initWithPath:path
											 documentAttributes:NULL];
		if (pageText == nil)
			pageText = [[NSAttributedString alloc] init];
		
		if (x==2)
			[newNotebook _newTodoPageWithIntroText:pageText];
		else
			[newNotebook _newPlainPageWithIntroText:pageText];
		[[[newNotebook pages] objectAtIndex:x+1] setTitle:titles[x]];
		
	}
	[newNotebook deletePage:blankPage];
	
	if (newNotebook != nil)
		[SPRNotebook addNotebook:newNotebook];
	
	return newNotebook;
}

+(SPRNotebook*)createNotebookWithOriginalSBDataAndTitle:(NSString*)title
{
	SPRNotebook *newNotebook = [[self alloc] initWithOriginalSBDataAndTitle:title];
	
	if (newNotebook != nil)
		[SPRNotebook addNotebook:newNotebook];
	
	SPRPage *firstPage = [newNotebook firstPage];
	
	
	if ([[newNotebook pages] count] > 1)
		[newNotebook deletePage:firstPage];
	
	return newNotebook;
}

+(SPRNotebook*)createNotebookWithStickiesDataAndTitle:(NSString*)title
{
	SPRNotebook *newNotebook = [[self alloc] initWithTitle:title];
	
	if (newNotebook != nil)
		[SPRNotebook addNotebook:newNotebook];
	
	SPRPage *firstPage = [newNotebook firstPage];
	
	[newNotebook importFromStickies];
	
	if ([[newNotebook pages] count] > 1)
		[newNotebook deletePage:firstPage];
	
	return newNotebook;
}

// Unused
+(SPRNotebook*)createNotebookWithPages:(NSArray*)pages title:(NSString*)title
{
	SPRNotebook *newNotebook = [[self alloc] initWithTitle:title];
	if (newNotebook != nil)
		[SPRNotebook addNotebook:newNotebook];
	
	SPRPage *firstPage = [newNotebook firstPage];
	[newNotebook appendPages:pages];
	
	if ([[newNotebook pages] count] > 1)
		[newNotebook deletePage:firstPage];
	
	return newNotebook;	
}

/// +createNotebookWithObject
///
/// obj must be an NSArray of pages or an NSDictionary with an entry under @"Pages"
/// of such an array
+(SPRNotebook*)createNotebookWithObject:(id)obj title:(NSString*)title
{
    SPRNotebook *newNotebook = [[self alloc] initWithTitle:title];
	if (newNotebook != nil)
		[SPRNotebook addNotebook:newNotebook];
	
	NSArray *pages;
    if ([obj isKindOfClass:[NSArray class]])
    {
        pages = obj;
    }
    else
    {
        NSAssert([obj isKindOfClass:[NSDictionary class]],@"Decoded unknown object type!");
        pages = [obj objectForKey:@"Pages"];
        
        //override the title with a stored value, if any
        title = [obj objectForKey:@"Title"] ?: title;
    }
    
	SPRPage *firstPage = [newNotebook firstPage];
	[newNotebook appendPages:pages];
	
	if ([[newNotebook pages] count] > 1)
		[newNotebook deletePage:firstPage];
	
	return newNotebook;
}


+(NSArray*)deleteNotebook:(SPRNotebook*)notebook
{
	//move the data file to the trash
	NSString *filePath = [[[SPRPreferencesController sharedPreferencesController] dataFolderPath] stringByAppendingFormat:@"/Data for %@", [notebook title]];
	
    NSURL *fileURL = [NSURL fileURLWithPath:filePath];
    [[NSWorkspace sharedWorkspace] recycleURLs:[NSArray arrayWithObject:fileURL]
                             completionHandler:^(NSDictionary *urls,NSError *err)
     {
         if (err)
         {
             NSLog(@"Error Deleting Notepad: SpiralBound Pro was unable to move the deleted notepad to the trash. Error = %@", err);
         }
     }];
    
	NSMutableArray *oldNotebooks = [Notebooks mutableCopy];
	[oldNotebooks removeObject:notebook];
	Notebooks = [[NSArray alloc] initWithArray:oldNotebooks];
	
	return Notebooks;
}

+(NSArray*)deleteNotebooks:(NSArray*)notebooks
{
	NSEnumerator *enumerator = [notebooks objectEnumerator];
	SPRNotebook *nb;
	NSArray *remaining=nil;
	
	while (nb = [enumerator nextObject])
		remaining = [self deleteNotebook:nb];

	return remaining;
}

+(void)saveData
{
	[[self notebooks] makeObjectsPerformSelector:@selector(saveData)];
}

+(BOOL)originalSBDataExists
{
	NSFileManager *fm = [NSFileManager defaultManager];
	return [fm fileExistsAtPath:OriginalSBDataPath];
}


+(NSArray*)notebooksWithTitles:(NSArray*)titles
{
	NSMutableArray *nbs = [NSMutableArray array];
	
	for (id nb in Notebooks)
	{
		if ([titles containsObject:[nb title]])
			[nbs addObject:nb];
	}
	
	NSAssert([titles count] == [nbs count],@"Some titles were not valid");
	return nbs;
}

+(BOOL)stickiesDataExists
{
	NSFileManager *fm = [NSFileManager defaultManager];
	return [fm fileExistsAtPath:StickiesDataPath];
}

//helper functions
id TryToDecodeData(NSData* data)
{
	id obj;
	@try
	{
		obj = [NSKeyedUnarchiver unarchiveObjectWithData: data];
	}
	@catch (NSException *e)
	{
		//this exception should be an NSInvalidArchiveOperationException
#ifdef DEBUG
		NSLog(@"%@",e);
#endif
		return nil;
	}
	return obj;
}

id TryToDecryptAndDecodeData(NSData* data)
{
	NSString *key = [[EMGenericKeychainItem genericKeychainItemForService:ServiceName withUsername:NSUserName()] password];
	if (key == nil)
		return nil;
	
	NSData *unencryptedData = [[data mutableCopy] AES256DecryptWithKey:key];
	if (unencryptedData == nil)
		return nil;
	
	return TryToDecodeData(unencryptedData);
}

//this initializer is called by +notebooks the first time it is called
//that methods handles adding it to the Notebooks array
-(id)initWithFileName: (NSString*)name
{
	self = [super init];
	
	if (self != nil)
	{
		NSString *path = [[[SPRPreferencesController sharedPreferencesController] dataFolderPath] stringByAppendingFormat:@"/%@", name];
		NSData *data = [NSData dataWithContentsOfFile:path];
		
		if (data != nil)
		{
			//now, try to decrypt the data if appropriate. Use the user preferences to determine whether we should
			//start with decrypting or use that as a fallback
			BOOL tryDecryptingFirst = [[NSUserDefaults standardUserDefaults] boolForKey:DefaultsEncryptData];
			id obj;
			if (tryDecryptingFirst)
			{
				obj = TryToDecryptAndDecodeData(data);
				
				if (obj == nil)
					obj = TryToDecodeData(data);
			}
			else
			{
				obj = TryToDecodeData(data);
				
				if (obj == nil)
					obj = TryToDecryptAndDecodeData(data);
			}
            
            if ([obj isKindOfClass:[NSDictionary class]])
            {
                pages = [obj objectForKey:@"Pages"];
            }
            else
            {
                NSAssert([obj isKindOfClass: [NSArray class]], @"Decoded unknown object type!");
                pages = obj;
            }
            
            if ([obj isKindOfClass:[NSDictionary class]] && [obj valueForKey:@"Title"])
            {
                title = [obj valueForKey:@"Title"];
            }
            else if ([name hasPrefix:@"Data for "])
            {
                title = [name substringFromIndex:[@"Data for " length]];
            }
            else
            {
                title = [@"Unknown" copy];
            }
			
			if (pages != nil)
				return self;
		}
	}
	
	//failure if we got here (pages could not be decoded)
    NSRunCriticalAlertPanel(@"Failure While Loading Notepad!",
                            @"An unknown error occurred while attempting to load a notepad.",
                            nil, nil, nil);
	return nil;
}


-(id)initWithOriginalSBDataAndTitle:(NSString*)newTitle
{
	/*Original SB Data is never encrypted*/
	self = [super init];
	
	if (self != nil)
	{
		NSData *data = [NSData dataWithContentsOfFile:OriginalSBDataPath];
		
		if (data != nil)
		{
			[SPRPage beginDecodingWithUniqueHashes];
			pages = [NSKeyedUnarchiver unarchiveObjectWithData: data];
			[SPRPage endDecodingWithUniqueHashes];
			
			title = [newTitle copy];
			
			if (pages != nil)
				return self;
		}
		
		return nil;
	}
	
	return self;
}

//this is for creating new notebooks internally
//normally to create a new notebook call +createNotebookWithTitle
-(id)initWithTitle:(NSString*)newTitle
{
	if ([SPRNotebook isTitleAvailable: newTitle] == NO)
		return nil;
	
	self = [super init];
	
	if (self != nil)
	{
		pages = [[NSMutableArray alloc] initWithObjects:[[SPRPage alloc] init], nil];
		title = [newTitle copy];
	}
	
	return self;
}


//Only called when making the intro pages
-(SPRPage*)_newPlainPageWithIntroText:(NSAttributedString*)c
{
	SPRPage *newPage = [[SPRPlainPage alloc] initWithIntroText:c];
	
	[pages addObject:newPage];
	
	return newPage;
}

-(SPRPage*)_newTodoPageWithIntroText:(NSAttributedString*)c
{
	SPRPage *newPage = [[SPRTodoPage alloc] initWithIntroText:c];
	
	[pages addObject:newPage];
	
	return newPage;
}


-(SPRPage*)newPage
{
	return [self newPageOfClass:[SPRPlainPage class]];	
}

-(SPRPage*)newTodoPage
{
	return [self newPageOfClass:[SPRTodoPage class]];	
}

-(SPRPage*)newPageOfClass:(Class) c
{
	SPRPage *newPage = [[c alloc] init];
	
	[pages addObject:newPage];
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:NotebookDidChangeLengthNotification																									    object:self]
											   postingStyle:NSPostASAP];
	return newPage;
}

-(SPRPage*)newPageOfClass:(Class)c atIndex:(NSUInteger)i
{
	SPRPage *newPage = [[c alloc] init];
	
	[pages insertObject:newPage atIndex:i];
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:NotebookDidChangeLengthNotification																									    object:self]
											   postingStyle:NSPostASAP];
	return newPage;
}

-(void)deletePage:(SPRPage*)page
{
	[pages removeObject: page];
	/*[[NSNotificationCenter defaultCenter] postNotificationName:@"NotebookDidChangeLength" object:self];*/
	[[NSNotificationQueue defaultQueue] enqueueNotification:
										[NSNotification notificationWithName:NotebookDidChangeLengthNotification																									  object:self]
											   postingStyle:NSPostASAP];
}

-(void)appendPages:(NSArray*) newPages
{
	[pages addObjectsFromArray:newPages];

	/*[[NSNotificationCenter defaultCenter] postNotificationName:@"NotebookDidChangeLength" object:self];*/
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:NotebookDidChangeLengthNotification																									  object:self]
											   postingStyle:NSPostASAP];
}

-(void)insertPages:(NSArray*) newPages atIndex:(NSUInteger)index
{
	if (index == UINT32_MAX)
	{
		index = [[self pages] count];
	}
	
	NSRange indexRange = {index, [newPages count]};
	NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:indexRange];
	[pages insertObjects: newPages atIndexes: indexSet];
	
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:NotebookDidChangeLengthNotification																									  object:self]
											   postingStyle:NSPostASAP];
	
}

-(NSArray*)pages
{
	return pages;
}

-(SPRPage*)firstPage
{
	return [pages objectAtIndex:0];
}

-(NSUInteger)pageCount
{
	return [pages count];
}

-(SPRPage *)pageWithHash:(NSUInteger)hash
{
	NSEnumerator *enumerator = [[self pages] objectEnumerator];
	SPRPage *page;
	
	while (page = [enumerator nextObject])
	{
		if ([page hash] == hash)
			return page;
	}
	
	return nil;
}

-(void)saveData
{
    // save a dictionary for future expansion
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:pages, @"Pages",
                                                                    title, @"Title",
                                                                    nil];
	NSData *data = [NSKeyedArchiver archivedDataWithRootObject: dict];
	NSError *err = nil;

	/*Check to see if we are encrypting data and if there is a password set.*/
	BOOL encrypt = [[NSUserDefaults standardUserDefaults] boolForKey:DefaultsEncryptData];
	
	if (encrypt)
	{
		//get the password to use as the encryption key
		NSString *key = [[EMGenericKeychainItem genericKeychainItemForService:ServiceName withUsername:NSUserName()] password];
	
		if (key)
		{
			//note sure if the encryption category really must be on NSMutableData or not
			NSData *encryptedData = [[data mutableCopy] AES256EncryptWithKey:key];
			
			if (encryptedData)
				data = encryptedData;
		}
	}
	
	NSString *fileName = [[[SPRPreferencesController sharedPreferencesController] dataFolderPath] stringByAppendingFormat:@"/Data for %@", [self title]];
	
	if ([[NSFileManager defaultManager] fileExistsAtPath:[[SPRPreferencesController sharedPreferencesController] dataFolderPath]] == NO)
    {
		BOOL success = [[NSFileManager defaultManager] createDirectoryAtPath:[[SPRPreferencesController sharedPreferencesController] dataFolderPath]
                                                 withIntermediateDirectories:YES
                                                                  attributes:nil
                                                                       error:&err];
        if (!success)
        {
            NSRunCriticalAlertPanel(@"Error Saving Data!",
                                    @"SpiralBound Pro was unable to save the data for notepad %@ due to an error. Creating the folder %@ failed.",
                                    nil,
                                    nil,
                                    nil,
                                    [self title],
                                    [[SPRPreferencesController sharedPreferencesController] dataFolderPath]);
        }
	}
    
    BOOL success = [data writeToFile:fileName options:NSDataWritingAtomic error:&err];
    
    if (!success)
    {
        NSRunCriticalAlertPanel(@"Error Saving Data!",
                                @"SpiralBound Pro was unable to save the data for notepad %@ due to an error. Writing to file %@ failed. ",
                                nil,
                                nil,
                                nil,
                                [self title],
                                fileName);
    }
}

-(SPRPage*)pageBeforePage:(SPRPage*)page
{
	NSUInteger index = [pages indexOfObject:page];
	
	if (index == 0)
		return nil;
	
	return [pages objectAtIndex: index-1];
}

-(SPRPage*)pageAfterPage:(SPRPage*)page
{
	NSUInteger index = [pages indexOfObject:page];
	
	if (index == ([pages count]-1))
		return nil;
	
	return [pages objectAtIndex: index+1];
}

-(void)movePages:(NSArray*)pagesToMove toIndex:(NSUInteger)destIndex
{
	if (destIndex == UINT32_MAX)
	{
		destIndex = [[self pages] count];
	}

	NSUInteger startingIndex = [pages indexOfObject: [pagesToMove objectAtIndex:0]],
			count = [pagesToMove count];
	
	if (destIndex >= startingIndex && destIndex <= startingIndex+count)
		return; //moving wouldn't do anything
	
	NSUInteger adjustedIndex;
	
	if (destIndex > startingIndex+count)
		adjustedIndex = destIndex-count;
	else
		adjustedIndex = destIndex;
	
	NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(adjustedIndex,count)];
	
	[pages removeObjectsInArray:pagesToMove];
	[pages insertObjects:pagesToMove atIndexes:indexSet];


	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:PageArrangementDidChangeNotification																																				object:self]
											   postingStyle:NSPostASAP];
}

-(void)movePage:(SPRPage*)page toBeforePage:(SPRPage*)referencePage
{
	if (page == referencePage)
		return;
	
	[pages removeObject: page];
	[pages insertObject: page atIndex:[pages indexOfObject: referencePage]];
	
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:PageArrangementDidChangeNotification																																				object:self]
											   postingStyle:NSPostASAP];
}

-(void)movePageToEnd:(SPRPage*)page
{
	[pages removeObject: page];
	[pages addObject:page];
	
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:PageArrangementDidChangeNotification																																				object:self]
											   postingStyle:NSPostASAP];
}

-(void)movePageToBeginning:(SPRPage*)page
{
	[pages removeObject: page];
	[pages insertObject:page atIndex:0];
	
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:PageArrangementDidChangeNotification																																				object:self]
											   postingStyle:NSPostASAP];
}

//returns an array of the new pages
-(NSArray*)importFromStickies
{
	NSData *data = [NSData dataWithContentsOfFile:StickiesDataPath];
	
	if (data == nil)
		return nil;
	
	unsigned char *bytes = (unsigned char*)[data bytes];
	NSUInteger length = [data length], loc, len;
	NSMutableArray *dataArray = [NSMutableArray array];
	
	
	for (loc = 0; loc < length-1; loc++)
	{
		//do a little extra checking here i.e. check for '{\rt'
		if (bytes[loc]=='{' && bytes[loc+1]=='\\' && bytes[loc+2]=='r' && bytes[loc+3]=='t')
		{
			//NSLog(@"%c", bytes[loc]);
			//another check
			if (loc>0)
				if (bytes[loc-1] == '\\')
					continue;
			
			for (len = 1; loc+len<length; len++)
			{
				//NSLog(@"    %c", bytes[loc+len]);
				if (bytes[loc+len]=='}' && bytes[loc+len-1] != '\\')
				{
					
					NSData *subdata = [data subdataWithRange:NSMakeRange(loc,len+1)];
					NSLog(@"%@", [[NSString alloc] initWithBytes:[subdata bytes] length:[subdata length] encoding:NSUTF8StringEncoding]);
					NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithRTF:subdata documentAttributes:nil];
					if (string != nil && ([[string string] isEqualToString: @""] == NO))
					{
						//remove the background color
						[string removeAttribute:NSBackgroundColorAttributeName range:NSMakeRange(0,[string length])];
					
						[dataArray addObject:string];	
						break;
					}
				}
			}
		}
	}
	
	
	//now make all of the new pages
	NSEnumerator *enumerator = [dataArray objectEnumerator];
	NSAttributedString *string;
	
	NSMutableArray *newPages = [NSMutableArray arrayWithCapacity:[dataArray count]];
	NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceCharacterSet];
	
	while (string = [enumerator nextObject])
	{
		SPRPage *newPage = [[SPRPage alloc] init];
		[pages addObject:newPage];
		[newPages addObject:newPage];
		[newPage setContentText:string];
		
		//now get the first line as the title
		NSString *plainString = [string string];
		NSArray *lines = [plainString componentsSeparatedByString:@"\n"];
		NSEnumerator *lineEnumerator = [lines objectEnumerator];
		NSString *line;
		
		while (line = [lineEnumerator nextObject])
		{
			line = [line stringByTrimmingCharactersInSet:whiteSpace];
			if ([line isEqualToString:@""] == NO)
				break;
		}
		
		if ([line isEqualToString:@""] || line==nil)
			line = @"Untitled";
		
		[newPage setTitle:line];
	}
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"NotebookDidChangeLength" object:self];
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:NotebookDidChangeLengthNotification																									  object:self]
											   postingStyle:NSPostASAP];
	
	
	return newPages;
}

- (NSString *)title
{
    return title;
}

- (void)setTitle:(NSString *)value
{
    if (title != value)
	{
		NSString *oldTitle = title;
		title = [value copy];
		
		//delete the old file
	
		NSFileManager *fm = [NSFileManager defaultManager];
		NSString *pathOfOldFile = [[SPRPreferencesController sharedPreferencesController] dataFolderPath];
		pathOfOldFile = [[pathOfOldFile stringByAppendingFormat:@"/Data for %@", oldTitle] stringByStandardizingPath];
		if ([fm fileExistsAtPath:pathOfOldFile])
		{
			NSError *error;
			if (![fm removeItemAtPath:pathOfOldFile error:&error])
			{
				NSRunAlertPanel(@"An Error Has Occurred", @"%@", @"OK", nil, nil, error);
			}
		}
		
		//now save the new one
		[self saveData];
    }
}


@end
