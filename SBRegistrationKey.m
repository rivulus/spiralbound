//
//  SBRegistrationKey.m
//  SBKeyMaker
//
//  Created by Philip on 8/7/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "SBRegistrationKey.h"

#define MAX_SKIP 64

const UInt8 Mask=0xFF;


NSData *GenerateKeyData (NSString *firstName, NSString *secondName, NSString *email)
{
	NSMutableData *key = [NSMutableData data];
	const UInt8 *firstNameC,*secondNameC,*emailC;
	firstNameC = (UInt8 *)[firstName UTF8String];
	secondNameC = (UInt8 *)[secondName UTF8String];
	emailC = (UInt8 *)[email UTF8String];
	
	NSUInteger i, j;
	srandomdev();
	
	for (i=0, j=0; firstNameC[i] != 0; i++)
	{
		UInt8 invertedCharacter = firstNameC[i]^Mask;
		[key appendBytes:&invertedCharacter length:1];
		j++;
		
		UInt8 skip = random()%MAX_SKIP+1;
		[key appendBytes:&skip length:1];
		j++;
		
		NSUInteger nextByte = skip+j;
		
		
		//skip some number
		for (j; j!=nextByte; j++)
		{
			NSUInteger temp = random();
			[key appendBytes:&temp length:1];
		}
	}
	
	//mark the end of the first name
	UInt8 endMark = 0xFF;
	[key appendBytes:&endMark length:1];
	j++;
	
	for (i=0, j; secondNameC[i] != 0; i++)
	{
		UInt8 invertedCharacter = secondNameC[i]^Mask;
		[key appendBytes:&invertedCharacter length:1];
		j++;
		
		UInt8 skip = random()%MAX_SKIP+1;
		[key appendBytes:&skip length:1];
		j++;
		
		NSUInteger nextByte = skip+j;
		
		
		//skip some number
		for (j; j!=nextByte; j++)
		{
			NSUInteger temp = random();
			[key appendBytes:&temp length:1];
		}
	}
	
	//mark the end of the second name
	
	[key appendBytes:&endMark length:1];
	j++;
	
	for (i=0, j; emailC[i] != 0; i++)
	{
		UInt8 invertedCharacter = emailC[i]^Mask;
		[key appendBytes:&invertedCharacter length:1];
		j++;
		
		UInt8 skip = random()%MAX_SKIP+1;
		[key appendBytes:&skip length:1];
		j++;
		
		NSUInteger nextByte = skip+j;
		
		
		//skip some number
		for (j; j!=nextByte; j++)
		{
			NSUInteger temp = random();
			[key appendBytes:&temp length:1];
		}
	}
	
	//mark the end of the email
	
	[key appendBytes:&endMark length:1];
	
	return key;
}

BOOL DecodeKeyData (NSData *key, NSString **firstName, NSString **secondName, NSString **email)
{
	UInt8 *bytes = (UInt8*)[key bytes];
	NSUInteger len = [key length];
	
	UInt8 *temp = malloc(len); //very long, but definitely long enough 
	
	//NSMutableString *temp = [NSMutableString string];
	
	NSUInteger i=0,j=0;
	
	while (bytes[i] != 0xFF)
	{
		//[temp appendFormat:@"%c", bytes[i]^Mask];
		temp[j] = bytes[i]^Mask;
		i++;
		j++;
		
		if (i >= len)
			goto failure;
		
		NSUInteger skip = bytes[i];
		
		i += skip+1;
		
		if (i >= len)
			goto failure;
		
	}
	temp[j]=0;
	*firstName = [NSString stringWithUTF8String:(char*)temp];
	
	i++;
	if (i >= len)
		goto failure;
	
	j=0;
	//temp = [NSMutableString string];
	while (bytes[i] != 0xFF)
	{
		temp[j] = bytes[i]^Mask;
		i++;
		j++;
		
		if (i >= len)
			goto failure;
		
		NSUInteger skip = bytes[i];
		
		i += skip+1;
		
		if (i >= len)
			goto failure;
		
	}
	temp[j]=0;
	*secondName = [NSString stringWithUTF8String:(char*)temp];
	
	i++;
	if (i >= len)
		goto failure;
	j=0;
	//temp = [NSMutableString string];
	while (bytes[i] != 0xFF)
	{
		//[temp appendFormat:@"%c", bytes[i]^Mask];
		temp[j] = bytes[i]^Mask;
		i++;
		j++;
		
		if (i >= len)
			goto failure;
		
		NSUInteger skip = bytes[i];
		
		i += skip+1;
		
		if (i >= len)
			goto failure;
		
	}
	temp[j]=0;
	*email = [NSString stringWithUTF8String:(char*)temp];
	
	free(temp);
	return YES;
	
failure:
	{
		free(temp);
		return NO;
	}
}

BOOL DecodeKeyFile (NSString *path, NSString **firstName, NSString **secondName, NSString **email)
{
	NSData *data = [NSData dataWithContentsOfFile:path];
	
	if (data == nil)
		return NO;
	
	return DecodeKeyData(data, firstName, secondName, email);
}
