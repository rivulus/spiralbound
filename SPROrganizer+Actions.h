//
//  SPROrganizer+Actions.h
//  SpiralBound Pro
//
//  Created by Philip White on 4/30/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SPROrganizer.h"

@class SPRTodoPage,SPRTodoListView;
@interface SPROrganizer (Actions)

-(void)raiseNotebook;

+(void)raisePage:(SPRPage*)page range:(NSRange)rangeToSelect buldgeMain:(BOOL)buldgeMain rangeInTitle:(BOOL)rangeInTitle;
+(void)raisePage:(SPRPage*)page range:(NSRange)rangeToSelect buldgeMain:(BOOL)buldgeMain;


-(IBAction)nextPage:(id)sender;
-(IBAction)previousPage:(id)sender;
-(IBAction)deletePage:(id)sender;
-(IBAction)returnPageToStack:(id)sender;

-(IBAction)exportPage:(id)sender;
-(void)setTitle:(NSString*)title window:(SpiralBoundWindow*)window;
-(void)raisePage:(SPRPage*)page range:(NSRange)rangeToSelect buldgeMain:(BOOL)buldgeMain rangeInTitle:(BOOL)rangeInTitle;
-(void)raisePage:(SPRPage*)page range:(NSRange)rangeToSelect buldgeMain:(BOOL)buldgeMain;
-(void)numberVisiblePages;

-(void)loadTodoListView:(SPRTodoListView*)tlv page:(SPRTodoPage*)page;


@end
