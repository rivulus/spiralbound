//
//  SBRegistrationKey.h
//  SBKeyMaker
//
//  Created by Philip on 8/7/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NSData *GenerateKeyData (NSString *firstName, NSString *secondName, NSString *email);
BOOL DecodeKeyData (NSData *key, NSString **firstName, NSString **secondName, NSString **email);
BOOL DecodeKeyFile (NSString *path, NSString **firstName, NSString **secondName, NSString **email);
