//
//  DefaultsKeys.m
//  CommonSpiralbound
//
//  Created by Philip on 5/12/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//


NSString *DefaultsSerialNumberKey = @"SpiralBoundSerialNumber";

/*SPRSearchEngine*/
NSString *DefaultsSearchEngineIgnoreCase = @"SPRSearchEngineIgnoreCase",
		 *DefaultsSearchEngineWholeWordsOnly = @"SPRSearchEngineWholeWordsOnly",
		 *DefaultsSearchEngineIgnoreDiacriticals = @"SPRSearchEngineIgnoreDiacriticals",
		 *DefaultsSearchEnginePageType = @"SPRSearchEnginePageType",
		 *DefaultsSearchEngineTitlesOrContents = @"SPRSearchEngineTitlesOrContents",
		 *DefaultsSearchEngineScope = @"SPRSearchEngineScope";

NSString /*STYLE PAGE*/
		 *DefaultsPreferencesUseSimpleDragbar = @"UseSimpleDragbar",
		 *DefaultsPreferencesEquationHighlightColor = @"EquationHighlightColor",
		 *DefaultsPreferencesSolutionHighlightColor = @"SolutionHighlightColor",
		 *DefaultsPreferencesTextAttributes = @"TextAttributes",
		 
		 /*MISC. PAGE*/
		 *DefaultsPreferencesDataFolderPath = @"DataFolderPath",
		 *DefaultsPreferencesServicesAlwaysUseNotebookKey = @"ServicesAlwaysUseNotebook",
		 *DefaultsPreferencesServicesDefaultNotebookNameKey = @"ServicesDefaultNotebookName",
		 
		 /*CALCULATOR PAGE*/
		 *DefaultsPreferencesCalculatorIsEnabled = @"CalculatorIsEnabled",
		 *DefaultsPreferencesPutCalculatorResultInClipboard = @"PutCalculatorResultInClipboard",
		 *DefaultsPreferencesCalculatorPrecision = @"CalculatorPrecision",
		 *DefaultsPreferencesEnterKeyCompletesEquation = @"EnterKeyCompletesEquation",
		 
		 /*SECURITY PAGE*/
		 *DefaultsPreferencesShouldScramble = @"ShouldScramble",
		 *DefaultsPreferencesScramblingDelay = @"ScramblingDelay",
		 *DefaultsPreferencesScrambleImmediately = @"ScrambleImmediately",
		 *DefaultsPreferencesNeedsPasswordToUnscramble = @"NeedsPasswordToUnscramble",
		 *DefaultsPreferencesEncryptData = @"EncryptData",
		 *DefaultsPreferencesRequirePasswordAtLaunch = @"RequirePasswordAtLaunch";

NSString *DefaultsEncryptData = @"EncryptData",
		//todo list
		*DefaultsPreferencesMarkStyle = @"MarkStyle",
		*DefaultsPreferencesMarkColor = @"MarkColor",
		*DefaultsPreferencesStrikeOutCheckedItem = @"StrikeOutCheckedItem";
