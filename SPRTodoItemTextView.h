//
//  SPRTodoItemTextView.h
//  CocoaJunk
//
//  Created by Philip on 5/20/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SPRTextView.h"

@class SPRTodoListView;
@interface SPRTodoItemTextView : SPRTextView
{
	SPRTodoListView *parentList;
}

-(void)setParentList:(SPRTodoListView*)pl;

@end
