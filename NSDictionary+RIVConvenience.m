//
//  NSDictionary+RIVConvenience.m
//  CommonSpiralbound
//
//  Created by Philip on 5/25/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import "NSDictionary+RIVConvenience.h"


@implementation NSDictionary (RIVConvenience)

-(BOOL)boolForKey:(id)k
{
	return [[self objectForKey:k] boolValue]; 
}

-(NSRect)rectForKey:(id)k
{
	return [[self objectForKey:k] rectValue];
}

-(NSRange)rangeForKey:(id)k
{
	return [[self objectForKey:k] rangeValue];
}

@end


@implementation NSMutableDictionary (RIVConvenience)

-(void)setBool:(BOOL)b forKey:(id)k
{
	[self setObject:[NSNumber numberWithBool:b] forKey:k];
}

-(void)setRect:(NSRect)r forKey:(id)k
{
	[self setObject:[NSValue valueWithRect:r] forKey:k];
}

-(void)setRange:(NSRange)r forKey:(id)k
{
	[self setObject:[NSValue valueWithRange:r] forKey:k];
}

@end