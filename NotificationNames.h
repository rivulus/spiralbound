//
//  NotificationNames.h
//  CommonSpiralbound
//
//  Created by Philip on 5/14/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//


//gets posted in SBController.m in CG callback
//postingStyle = NSPostWhenIdle
extern NSString *SPRDisplayConfigurationDidChange;


/*SPRTodoPage*/
extern NSString *SPRTodoItemsOrderDidChange;	//object -> SPRTodoPage
extern NSString *SPRTodoItemsCountDidChange;	//object -> SPRTodoPage
extern NSString *SPRTodoItemStateDidChange;		//object -> SPRTodoPage, userInfo -> {StateKey -> state, TextStorageKey -> textStorage};


/*SPROrganizer*/
extern NSString *NotebookCountDidChangeNotification,
				*NotebookDidChangeLengthNotification,
				*PageArrangementDidChangeNotification,
				*MainWindowPageDidChangeNotification,
				*PageLayoutDidChangeNotification,
				*NotebookDidBecomeKeyNotification,
				*NotebookDidHideNotification,
				*NotebookDidUnhideNotification,
				*NotebookDidResignKeyNotification,
				*PageTitleDidChangeNotification,
				*PageWillBeDeletedNotification,
				*NotebookWillBeDeletedNotification,
				*NotebookColorDidChangeNotification,
				*NotebookTitleDidChangeNotification,
				*PageSecurityDidChangeNotification;
