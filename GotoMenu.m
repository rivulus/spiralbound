//
//  GotoMenu.m
//  SpiralBound
//
//  Created by Philip White on 2/28/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "GotoMenu.h"
#import "SPRNotebook.h"
#import "SPRPage.h"
#import "SBController.h"
#import "SPROrganizer.h"
#import "NSView+Organizer.h"

@implementation GotoMenu

- (void)mouseDown:(NSEvent*)event
{
	
	NSMenu *menu = [self menu];
	
	
	//delete all of the items except the first, which holds the image
	while ([menu numberOfItems] > 1)
		[menu removeItemAtIndex:1];
	
	NSMenuItem *item;
	SPROrganizer *organizer = [self organizer];
	NSArray *pages = [[organizer notebook] pages];
	NSEnumerator *enumerator = [pages objectEnumerator];
	SPRPage *page;
	
	while (page = [enumerator nextObject])
	{
		item = [[NSMenuItem alloc] initWithTitle:[page title]
										  action:@selector(gotoPage:)
								   keyEquivalent:@""];
		[item setRepresentedObject:page];
		[menu addItem: item];
	}
	[self setTarget:organizer];
	[super mouseDown:event];
}

@end
