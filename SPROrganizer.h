//
//  NotebookInfo.h
//  SpiralBound Pro
//
//  Created by Philip White on 4/29/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SBTypes.h"

#define TORNOFF_LOCATION_FIELD_TAG 16
#define MAIN_LOCATION_FIELD_TAG 17
#define MAIN_PAGE_COUNT_FIELD_TAG 18

//this class will take the place of the various tables (PageToPlace, PageToSecurity) that are present in the original SpiralBound.
//It will implement much of what was implemented in the +Data category of SBController


typedef struct
{
	NSUInteger notebookPageHash;
	NSRect frame;
	BOOL shaded;
} WindowInfo; 

@class SPRNotebook, SPRPage, SpiralBoundWindow;
@interface SPROrganizer : NSObject <NSWindowDelegate>
{
	//notes on object retention
	//this class does not retain objects it does not own, namely Notebooks or NotebookPages
	//it owns windows, but passes ownership of them to any collection to which they are added. i.e. it releases them after adding
	
	//persistent attributes, i.e. stored when the program is quit
	NSColor *color;
	
	//nonpersistent attributes, i.e. regenerated when the program is launched
	SPRNotebook *notebook;
	SpiralBoundWindow *mainWindow;
	SPRPage *mainWindowPage;
	NSMutableArray *tornOffWindows;	//these next two arrays match up index by index	
	NSMutableArray *tornOffPages; //array of NotebookPages
	
	BOOL hidden;
	BOOL key;
}

+(NSArray*)organizers;
+(void)reloadOrganizers;
+(NSArray*)menuItemsForOrganizers;
+(NSArray*)notebookTitles;
+(NSString*)titleOfNotebookAtIndex:(NSUInteger)index;	//notebooks are ordered alphabetically
//this next method lets a view get easy access to the notebook that its window is associated with

+(SPROrganizer*)makeNewNotebookWithTitle:(NSString*)title color:(NSColor*)color;
+(SPROrganizer*)makeNewNotebookFromStickiesWithTitle:(NSString*)title color:(NSColor*)color;
+(SPROrganizer*)makeNewNotebookFromSpiralBoundWithTitle:(NSString*)title color:(NSColor*)color;
+(SPROrganizer*)makeNewNotebookWithObject:(id)object title:(NSString*)title color:(NSColor*)color;
+(SPROrganizer*)makeNewOrganizerWithNotebook:(SPRNotebook*)newNotebook color:(NSColor*)color;

+(void)saveOrganizers;
+(SPROrganizer*)organizerForWindow:(NSWindow*)window;
+(SPROrganizer*)organizerForTitle:(NSString*)title;

+(SPROrganizer*)organizerForPage:(SPRPage*)page;

+(SPROrganizer*)organizerForNotebook:(SPRNotebook*)notebook;
+(SPRNotebook*)notebookForWindow:(NSWindow*)window;
+(SpiralBoundWindow*)loadWindowWithFrame:(NSRect)frame page:(SPRPage*)page style:(SBWindowStyle)style organizer:(SPROrganizer*)organizer;
+(void)showAll;
+(void)deleteNotebookWithOrganizer:(SPROrganizer*)organizer;
+(SPROrganizer*)keyOrganizer;

+(void)hideNonkeyNotebooks;

-(id)initWithNotebook: (SPRNotebook*)newNotebook color:(NSColor*)newColor introPages:(BOOL)introPages;
-(id)initWithNotebook: (SPRNotebook*)newNotebook color:(NSColor*)newColor;
-(id)initWithCoder:(NSCoder *)decoder;
-(void)registerForNotifications;
-(void)encodeWithCoder:(NSCoder *)coder;
//in this next method the first elements of the windowInfos array is the main window info. Array must be at least one element long
-(void)makeWindowsWithWindowInfoArray:(NSArray*)windowInfos;
-(void)makeWindowsWithWindowInfos:(WindowInfo*) windowInfos count:(NSUInteger)count;
-(SpiralBoundWindow*)requestTearOffWindow: (NSRect*)frameOnReturn;
-(void)setMainWindowPage:(SPRPage*)page;
-(SPRPage*)pageForWindow:(NSWindow*)window;
-(SpiralBoundWindow*)windowForPage:(SPRPage*)page;	//returns nil if page is not visible in a window

-(NSArray*)pageTitles;
-(NSString*)titleOfPageAtIndex:(NSUInteger)index;
-(NSUInteger)pageCount;

-(IBAction)raise:(id)sender;
-(IBAction)hideAll:(id)sender;

-(void)showAll;

-(void)closeWindows;

-(void)newPageWithAttributedString:(NSAttributedString*)string;

-(SPRPage*)pageAtIndex:(unsigned int)index;

//accessors
- (SPRNotebook *)notebook;
- (NSWindow *)mainWindow;
- (SPRPage *)mainWindowPage;
- (NSMutableArray*)tornOffWindows;

- (NSString *)title;
- (void)setTitle:(NSString*)newTitle;

- (NSColor *)color;
- (void)setColor:(NSColor *)value;

- (BOOL)isHidden;
- (void)setHidden:(BOOL)value;

- (BOOL)isKey;
- (void)setKey:(BOOL)value;


@end
