//
//  SPRLockableTextStorage.h
//  LockableTextStorageTest
//
//  Created by Philip on 5/27/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

BOOL SPRLockableTextStorageAllAreLocked(void);
void SPRLockableTextStorageLockAll(void);
void SPRLockableTextStorageUnlockAll(void);

extern NSString *SPRLockableTextStorageLockStatusDidChange;

@interface SPRLockableTextStorage : NSTextStorage
{
	NSMutableAttributedString *storage;
	BOOL locked;
	NSString *bufferedDummy;
}

+(SPRLockableTextStorage*)sharedEmptyStorage;
-(NSString*)stringIgnoringSecurity;

-(BOOL)locked;
-(void)setLocked:(BOOL)yn;


@end
