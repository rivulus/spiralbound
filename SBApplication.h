//
//  SBApplication.h
//  SpiralBound
//
//  Created by Philip White on 2/17/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SBController, SPRNotebook;
@interface SpiralBoundApplication : NSApplication
{
	IBOutlet SBController *sbController;
}

-(id)sbController;
@end
