//
//  SPRPlainPage.m
//  CommonSpiralbound
//
//  Created by Philip on 5/24/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import "SPRPlainPage.h"
#import "SPRLockableTextStorage.h"
#import "SBConstants.h"

@implementation SPRPlainPage

-(id)init
{
	self = [super init];
	
	if (self != nil)
	{
		contentText = [[SPRLockableTextStorage alloc] initWithAttributedString:SPRGetAttributeHolder(nil)];
	}
	return self;
}

-(id)initWithIntroText:(NSAttributedString*)t
{
	self = [super init];
	
	if (self != nil)
	{
		contentText = [[SPRLockableTextStorage alloc] initWithAttributedString:SPRGetAttributeHolder(nil)];
		[contentText appendAttributedString:t];
	}
	return self;
}


- (id)initWithCoder:(NSCoder *)decoder
{
	self = [super initWithCoder:decoder];
	
	//treat as an NSAttributedString for version control
	NSAttributedString *attr = [decoder decodeObjectForKey:@"ContentText"];
	contentText = [SPRLockableTextStorage new];
	if (attr != nil)
	{
		[contentText setAttributedString:attr];
		[contentText setLocked:secure];
	}
	
	return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
	//the encoder encodes as a simple NSAttributedString
	[encoder encodeObject:contentText forKey:@"ContentText"];
	[super encodeWithCoder:encoder];
}

- (id)copyWithZone:(NSZone *)zone
{
	SPRPlainPage *newCopy = [super copyWithZone:zone];
	[newCopy setContentText:contentText];
	return newCopy;
}

-(SPRLockableTextStorage*)contentText
{
	return contentText;
}

//note that this method may cause problems if used when the contents are attached to a textview
-(void)setContentText:(NSAttributedString*)value
{
	[contentText setAttributedString:value];
}

-(NSString*)contentsAsString
{
	return [[self contentText] string];
}

-(NSAttributedString*)contentsAsAttributedString
{
	return [[self contentText] copy];
}

- (void)setSecure:(BOOL)value
{
	[super setSecure:value];
	
	[contentText setLocked:value];
}

-(void)autotitle
{
	if ([self isTitled])
		return;
	
	[self autotitleWithString:[[self contentText] stringIgnoringSecurity]];
}


@end
