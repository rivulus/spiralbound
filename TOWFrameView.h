//
//  TornOffFrameView.h
//  SpiralBound
//
//  Created by Philip White on 2/2/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SBProtoFrameView.h"


@interface TOWFrameView : SBProtoFrameView
{
	//this is where MVC collapses catastrophically
	NSString *formattedIndexOfCount;	//this holds a string that is basically "Page %i of %i",
	
	NSSize tornOffImageSize;
}


+ (void)initialize;

//overriden methods


- (void)drawRect:(NSRect)rect;

-(NSImage*)dragbarImage;

-(NSNib*)nib;

//other methods
- (void)drawDividerLine;
-(NSRect)titleBarRect;

- (NSString *)formattedIndexOfCount;
- (void)setFormattedIndexOfCount:(NSString *)value;

-(IBAction)makeTopPage:(id)sender;
-(IBAction)makeFirstPage:(id)sender;
-(IBAction)makeLastPage:(id)sender;

@end
