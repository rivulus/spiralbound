//
//  DragTypeNames.h
//  CommonSpiralbound
//
//  Created by Philip on 7/6/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

extern NSString *SPRHashArrayPasteboardType,
				*SPRTitleArrayPasteboardType;