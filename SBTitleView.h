//
//  SBTitleView.h
//  SpiralBound
//
//  Created by Philip White on 2/13/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface SBTitleView : NSTextField
{
	BOOL bold;
	float inset;
}

- (void)sizeAndPosition;
- (BOOL)bold;
- (void)setBold:(BOOL)value;
- (void)rename;


@end
