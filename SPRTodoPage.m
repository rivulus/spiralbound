//
//  SPRTodoPage.m
//  CommonSpiralbound
//
//  Created by Philip on 5/24/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import "SPRTodoPage.h"
#import "NotificationNames.h"
#import "RIVTools.h"
#import "SPRLockableTextStorage.h"
#import "SPRTodoItemCheckbox.h"
#import "DefaultsKeys.h"
#import "SBConstants.h"
#import "SPRPlainPage.h"

NSString *TextStorageKey = @"TextStorage",
		 *StateKey = @"TodoState";

@implementation SPRTodoPage

-(id)init
{
	self = [super init];
	NSMutableDictionary *blankEntry =
			//[NSMutableDictionary dictionaryWithObjectsAndKeys: [[[SPRLockableTextStorage alloc] init] autorelease],
			[NSMutableDictionary dictionaryWithObjectsAndKeys: [[SPRLockableTextStorage alloc] initWithAttributedString:SPRGetAttributeHolder(nil)],
			TextStorageKey,
			[NSNumber numberWithBool:NO],
			StateKey, nil];
	items = [[NSMutableArray alloc] initWithObjects:blankEntry,nil];
	
	return self;
}

#define DELIM @"~"
-(id)initWithIntroText:(NSAttributedString*)t
{
	self = [super init];
	items = [[NSMutableArray alloc] init];
	
	NSString *tStr = [t string];
	int index=0;
	for (NSString *s in [tStr componentsSeparatedByString:DELIM])
	{
		NSRange r = NSMakeRange(index,[s length]);
		SPRLockableTextStorage *ts = [[SPRLockableTextStorage alloc] initWithAttributedString:SPRGetAttributeHolder(nil)];
		[ts appendAttributedString:[t attributedSubstringFromRange:r]];
		NSMutableDictionary *entry = [NSMutableDictionary dictionaryWithObjectsAndKeys:ts, TextStorageKey,
																					   [NSNumber numberWithBool:NO], StateKey, nil];
		[items addObject:entry];
		
		index+=[s length]+1;
	}
	return self;
}
#undef DELIM

- (id)initWithCoder:(NSCoder *)decoder
{
	self = [super initWithCoder:decoder];
	
	items = [decoder decodeObjectForKey:@"TodoItems"];
	
	//now we need to take each item and convert it from an NSAttributedString to an SPRLockableTextStorage
	for (id i in items)
	{
		NSAttributedString *attr = [i objectForKey:TextStorageKey];
		SPRLockableTextStorage *contentText = [SPRLockableTextStorage new];
		if (attr != nil)
		{
			[contentText setAttributedString:attr];
			[contentText setLocked:secure];
		}
		[i setObject:contentText forKey:TextStorageKey];
	}
	
	return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
	//the encoder encodes as simple NSAttributedStrings
	[encoder encodeObject:items forKey:@"TodoItems"];
	
	[super encodeWithCoder:encoder];
}


- (id)copyWithZone:(NSZone *)zone
{
	SPRTodoPage *newCopy = [super copyWithZone:zone];
	
	//do a deep copy on the items dictionary
	NSMutableArray *newItems = (id)[newCopy items];
	[newItems removeAllObjects];
	
	for (NSMutableDictionary *dict in items)
	{
		NSMutableDictionary *newDict = [NSMutableDictionary dictionary];
		[newDict setObject:[dict objectForKey:StateKey] forKey:StateKey]; //an NSNumber, immutable
		SPRLockableTextStorage *newTS = [[dict objectForKey:TextStorageKey] mutableCopy]; //mutableCopy returns like class
		[newDict setObject:newTS forKey:TextStorageKey];
	
		[newItems addObject:newDict];
	}
	
	return newCopy;
}

-(NSArray*)items
{
	return items;
}

-(NSUInteger)itemCount
{
	return [items count];
}

//no delimiters between items
-(NSString*)contentsAsString
{
	NSMutableString *allStrings = [NSMutableString string];
	
	for (NSDictionary *item in items)
	{
		NSString *str = [[item objectForKey:TextStorageKey] string];
		[allStrings appendString:str];
	}
	return allStrings;
}

-(NSString*)contentsAsDelimitedString:(NSString *)delimiter
{
	NSMutableString *allStrings = [NSMutableString string];
	
	NSUInteger count = [items count], i;
	
	for (i=0; i<count;i++)
	{
		id item = [items objectAtIndex:i];
		
		if (i>0)
			[allStrings appendString:delimiter];
		
		[allStrings appendString:[[item objectForKey:TextStorageKey] stringIgnoringSecurity]];
	}
	
	return allStrings;
}

//items are delimited with line breaks
//checks or blanks are printed before each line
-(NSAttributedString*)contentsAsAttributedString
{
	NSMutableAttributedString *allStrings = [NSMutableAttributedString new];
	
	unichar checkChar = [SPRTodoItemCheckbox markCharacter];
	BOOL strikeOut = [[NSUserDefaults standardUserDefaults] boolForKey:DefaultsPreferencesStrikeOutCheckedItem];
	
	NSUInteger count = [items count], i;
	NSDictionary *checkAttributes = [[[NSAttributedString alloc] initWithString:@"X"]
											attributesAtIndex:0 effectiveRange:NULL];
	
	for (i=0; i<count;i++)
	{
		id item = [items objectAtIndex:i];
		
		NSAttributedString *text = [item objectForKey:TextStorageKey];
		
		if (i>0)
			[allStrings replaceCharactersInRange:NSMakeRange([allStrings length],0)
									  withString:@"\n"];
		
		BOOL checked = [[item objectForKey:StateKey] boolValue];
		NSString *checkStr = [NSString stringWithFormat:@"%C\t", checked ? checkChar : ' '];
		NSAttributedString *attrCheckStr = [[NSAttributedString alloc] initWithString:checkStr attributes:checkAttributes];
		[allStrings appendAttributedString:attrCheckStr];
		
		[allStrings appendAttributedString:text];
		
		if (strikeOut && checked)
		{
			NSRange r = NSMakeRange([allStrings length] - [text length], [text length]);
			[allStrings addAttribute:NSStrikethroughStyleAttributeName
							   value:[NSNumber numberWithUnsignedInt:NSUnderlineStyleThick]
							   range:r];
		}
	}
	
	
	return allStrings;
}


-(NSMutableDictionary*)itemAtIndex:(NSUInteger)i
{
	return [items objectAtIndex:i];
}

-(void)removeItemAtIndex:(NSUInteger)i
{
	[items removeObjectAtIndex:i];
	NSNotification *note = [NSNotification notificationWithName:SPRTodoItemsCountDidChange
														 object:self];
	[[NSNotificationCenter defaultCenter] postNotification:note];
}

-(void)moveItemFromIndex:(NSUInteger)from toIndex:(NSUInteger)to
{
	if (to == from || to == from+1) //pointless move
		return;
	
	id item = [items objectAtIndex:from];
	if (to > from)
		to--;
	
	[items removeObjectAtIndex:from];
	[items insertObject:item atIndex:to];
	
	NSNotification *note = [NSNotification notificationWithName:SPRTodoItemsOrderDidChange
														 object:self];
	[[NSNotificationCenter defaultCenter] postNotification:note];
}

-(void)addItemAtIndex:(NSUInteger)i withContent:(NSAttributedString*)c state:(BOOL)s
{
	SPRLockableTextStorage *ts = [SPRLockableTextStorage new];
	
	if (c == nil || [c length] == 0)
		c = SPRGetAttributeHolder(nil);
	
	[ts setAttributedString:c];
		
	NSMutableDictionary *entry =
		[NSMutableDictionary dictionaryWithObjectsAndKeys:ts, TextStorageKey,
														  [NSNumber numberWithBool:s], StateKey, nil];
	
	if (i > [self itemCount])
		i = [self itemCount];	//new entry at end

#ifdef DEBUG
	for (id t in items)
	{
		if ([t objectForKey:TextStorageKey] == ts)
			Debugger();
	}

#endif
	[items insertObject:entry atIndex:i];
	

	
	NSNotification *note = [NSNotification notificationWithName:SPRTodoItemsCountDidChange
														 object:self];
	[[NSNotificationCenter defaultCenter] postNotification:note];
}

-(void)toggleItemAtIndex:(NSUInteger)i
{
	NSMutableDictionary *dict = [items objectAtIndex:i];
	BOOL oldState = [[dict objectForKey:StateKey] boolValue];
	[dict setObject: [NSNumber numberWithBool:!oldState] forKey:StateKey];
	
	NSNotification *note = [NSNotification notificationWithName:SPRTodoItemStateDidChange
														 object:self
													   userInfo:dict];
	[[NSNotificationCenter defaultCenter] postNotification:note];
}

-(void)setState:(BOOL)yn forItemAtIndex:(NSUInteger)i
{
	if (yn != [[[items objectAtIndex:i] objectForKey:StateKey] boolValue])
		[self toggleItemAtIndex:i];
}

-(void)setContents:(NSAttributedString*)c forItemAtIndex:(NSUInteger)i
{
	SPRLockableTextStorage *ts = [[items objectAtIndex:i] objectForKey:TextStorageKey];
	[ts setAttributedString:c];
}

- (void)setSecure:(BOOL)value
{
	[super setSecure:value];
	
	for (NSMutableDictionary *ts in items)
	{
		[[ts objectForKey:TextStorageKey] setLocked:value];
	}
}

-(void)autotitle
{
	if ([self isTitled])
		return;
	
	[self autotitleWithString:[self contentsAsDelimitedString:@"\n"]];
}


@end
