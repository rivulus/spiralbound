//
//  PrintingView.m
//  SpiralBound
//
//  Created by Philip White on 2/13/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "PrintingView.h"


@implementation PrintingView


-(id)init
{
	NSPrintInfo *printInfo = [NSPrintInfo sharedPrintInfo];
	float scale = [[[printInfo dictionary] objectForKey:NSPrintScalingFactor]
				   floatValue];
	
	
	NSRect frame;
	frame.origin.x = [printInfo leftMargin]/scale;
	frame.origin.y = [printInfo bottomMargin]/scale;
	frame.size = [printInfo paperSize];
	frame.size.width -= ([printInfo leftMargin] + [printInfo rightMargin]);
	frame.size.width /= scale;
	pageHeight = (frame.size.height-([printInfo topMargin] + [printInfo bottomMargin]))/scale;
	frame.size.height=0;

	//NSLog(@"print page size %f %f", frame.size.width, frame.size.height);
	
	textViews = [[NSMutableArray alloc] init];
	titles = [[NSMutableArray alloc] init];
	thrifty = YES;
	
	self = [super initWithFrame:frame];
	
	return self;
}

-(void)dealloc
{
	NSEnumerator *enumerator = [textViews objectEnumerator];
	NSTextView *view;
	while (view = [enumerator nextObject])
	{
		[view textStorage];
	}
	
}

-(void)addPageContents:(NSAttributedString*)contents title:(NSString*)title
{
	//copy the title and store it
	[titles addObject: [title copy]];
	
	//create the text system to hold contents
	NSTextStorage *textStorage = [[NSTextStorage alloc] initWithAttributedString:contents];
	
	//insert the title before the textStorage
	NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
	[style setAlignment:NSCenterTextAlignment];
	NSMutableAttributedString *attrTitle = [[NSMutableAttributedString alloc] initWithString: title
											attributes: [NSDictionary dictionaryWithObjectsAndKeys:
																[NSFont boldSystemFontOfSize:14.0], NSFontNameAttribute,
																style, NSParagraphStyleAttributeName, nil]];
	[attrTitle insertAttributedString:[[NSAttributedString alloc] initWithString: @"\n\n"] atIndex:[attrTitle length]];
	
	if (thrifty == YES && [textViews count]!=0)	//add another 2 empty lines
		[attrTitle insertAttributedString:[[NSAttributedString alloc] initWithString: @"\n\n"] atIndex:0];
	
	[textStorage insertAttributedString:attrTitle atIndex:0];
	
	
	
	
	NSLayoutManager *layoutManager;
	layoutManager = [[NSLayoutManager alloc] init];
	[textStorage addLayoutManager:layoutManager];
	
	NSRect cFrame = [self frame];
	cFrame.size.height = 10000000;
	NSTextContainer *container;
	
	container = [[NSTextContainer alloc]
				 initWithContainerSize:cFrame.size];
	[container setWidthTracksTextView:YES];
	[layoutManager addTextContainer:container];
	
	NSTextView *textView = [[NSTextView alloc]
							initWithFrame:cFrame textContainer:container];
	[textView setVerticallyResizable:YES];
	[textView setMinSize:NSMakeSize(cFrame.size.width, 0)];
	[textView setMaxSize:NSMakeSize(cFrame.size.width, FLT_MAX)];
	
	
	[self minimizeTextView:textView];
	
	
	[self expandForPageOfHeight:[textView frame].size.height];
	[self addSubview:textView];
	
	if (thrifty == YES)
	{
		[textView setFrameOrigin:NSMakePoint(0, 0)];
	}
	else
	{
		float yLocation = pageHeight - [textView frame].size.height;
		
		while (yLocation < 0)
			yLocation += pageHeight;
		
		[textView setFrameOrigin:NSMakePoint(0, yLocation)];
	}
	
	[textViews addObject:textView];
		//retained as long as we don't release textStorage
	
}

-(void)finish
{
	NSRect myFrame = [self frame];
	
	//if the frame is smaller than a single page, make it as big as one to force it to layout in the upper-left corner
	if (myFrame.size.height < pageHeight)
	{
		float deltaHeight = pageHeight-myFrame.size.height;
		
		myFrame.size.height = pageHeight;
		[self setFrame:myFrame];
		
		NSArray *subviews = [self subviews];
		NSEnumerator *enumerator = [subviews objectEnumerator];
		NSView *subview;
		
		while (subview = [enumerator nextObject])
		{
			NSRect subviewFrame = [subview frame];
			[subview setFrameOrigin:NSMakePoint(subviewFrame.origin.x,subviewFrame.origin.y+deltaHeight)];
		}
	}
}

-(void)expandForPageOfHeight:(float)deltaHeight
{
	NSRect myFrame = [self frame];
	float shift;
	
	if (thrifty == YES)
	{
		myFrame.size.height+=deltaHeight;
		shift = deltaHeight;
	}
	else
	{
		shift = ((int)((deltaHeight / pageHeight) + 1))*pageHeight;
		myFrame.size.height+=shift;
	}
	[self setFrame:myFrame];
	
	NSArray *subviews = [self subviews];
	NSEnumerator *enumerator = [subviews objectEnumerator];
	NSView *subview;
	
	while (subview = [enumerator nextObject])
	{
		NSRect subviewFrame = [subview frame];
		[subview setFrameOrigin:NSMakePoint(subviewFrame.origin.x,subviewFrame.origin.y+shift)];
	}
}



//makes the textview as short as possible
//it must be too tall to begin with
-(void)minimizeTextView:(NSTextView*)view
{
	float oldWidth = [view frame].size.width;
	NSLayoutManager *layoutManager = [view layoutManager];
	NSTextContainer *textContainer = [view textContainer];
	
	[layoutManager glyphRangeForTextContainer: textContainer]; 
	//[layoutManager boundingRectForGlyphRange:NSMakeRange(0, [layoutManager numberOfGlyphs]) inTextContainer:textContainer]; // dummy call to force layout
	NSRect usedRect = [layoutManager usedRectForTextContainer:textContainer];
	
	//NSRect usedRect = [layoutManager usedRectForTextContainer:textContainer];
	NSSize inset = [view textContainerInset];
	NSRect actualRect  = NSInsetRect(usedRect, -inset.width * 2, -inset.height * 2);
	
	//now we may have to resize with width of the view
	actualRect.size.width = oldWidth;
	actualRect.origin.x = 0;
	
	[view setFrameSize:actualRect.size];
}


-(void)setThrifty:(BOOL)value
{
	thrifty = value;
}

- (NSData*)PDFData
{
	NSMutableData *thePDF = [[NSMutableData alloc] init];
    NSPrintOperation *op;
	
    op = [NSPrintOperation PDFOperationWithView:self insideRect: [self bounds] toData: thePDF];
										
    [op setShowsPrintPanel:NO];
	
    @try
    {
        if ( [op runOperation] == NO)
            return nil;
        else
            return thePDF;
    }
    @catch (id exception)
    {
        NSLog(@"Caught exception during printing: %@", exception);
        NSLog(@"Ignoring exception");
    }

}

@end
