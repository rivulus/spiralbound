//
//  Calculator+Converter.m
//  SpiralBound
//
//  Created by Philip White on 2/11/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "Calculator+Converter.h"
#import "Calculator+Operators.h"
#import "NSMutableArray+PushPop.h"

NSUInteger operatorPrecendence (NSString *operator);
SInt8 OperatorAssociativity (NSString *operator);
BOOL isOperand (NSString *token);

@implementation Calculator (Converter)


-(NSMutableArray*)infixToPostfix:(NSString *)infixString
{
	NSMutableArray *infix =	[self identifyConstants:
								[self identifyExponents:
									[self convertStringsToNumbers:
										[self getTokens: [self identifyNegationOperators: infixString]]]]],
				   *postfix = [NSMutableArray arrayWithCapacity:[infix count]],
				   *stack = [NSMutableArray arrayWithCapacity: 10];
				   
	//start the shunting algorithm
	while ([infix count] != 0)
	{
		NSString *token = [infix objectAtIndex:0];
		[infix removeObjectAtIndex:0];
		
		if ([token isKindOfClass:[NSDecimalNumber class]])
		{
			[postfix push: token];
		}
		else if ([token isEqualToString: @"("])
		{
			[stack push: token];
		}
		else if ([token isEqualToString: @")"])
		{
			NSString *topOperator;
			while ([(topOperator = [stack pop]) isEqualToString: @"("] == NO)
			{
				if (topOperator == nil)
				{
					return nil;
				}
				else
				{
					[postfix push: topOperator];
				}
			}
		}
		else //assume it's an operator
		{
			SInt8 associativity = OperatorAssociativity(token);
			NSString *topOperator;
			
			if (associativity == -1)
			{
				while ((topOperator = [stack lastObject]) &&
					   operatorPrecendence(topOperator) >= operatorPrecendence(token))
				{
					[postfix push: [stack pop]];
				}
				
			}
			else
			{
				while ((topOperator = [stack lastObject]) &&
					   operatorPrecendence(topOperator) > operatorPrecendence(token))
				{
					[postfix push: [stack pop]];
				}
				
			}
			[stack push: token];
		}
	}
	
	//pop the rest of the stack
	NSString *topOperator;
	while ((topOperator = [stack pop]))
	{
		if ([topOperator isEqualToString: @"("])
		{
			return nil;
		}
		else
		{
			[postfix push: topOperator];
		}
	}
	
	return postfix;
}

BOOL isOperand (NSString *token)
{
	static BOOL firstCall = YES;
	static NSCharacterSet *numbers;
	
	if (firstCall)
	{
		numbers = [NSCharacterSet characterSetWithCharactersInString:@"1234567890."];
		firstCall = NO;
	}
	
	if ([token isEqualToString: @"e"] || [token isEqualToString: @"pi"] || [token isEqualToString:@"π"] ||
		[numbers characterIsMember:[token characterAtIndex:0]])
		return YES;
	else
		return NO;
}


NSUInteger operatorPrecendence (NSString *operator)
{
	static NSCharacterSet *p9, *p8, *p6, *p0;
	static BOOL firstCall = YES;
	
	if (firstCall)
	{
		p9 = [NSCharacterSet characterSetWithCharactersInString:@"^~"],
		p8 = [NSCharacterSet characterSetWithCharactersInString:@"*/%"],
		p6 = [NSCharacterSet characterSetWithCharactersInString:@"+-"],
		p0 = [NSCharacterSet characterSetWithCharactersInString:@"("];
		firstCall = NO;
	}
	
	unichar character = [operator characterAtIndex: 0];
	
	if ([p9 characterIsMember:character])
		return 9;
	else if ([p8 characterIsMember: character])
		return 8;
	else if ([p6 characterIsMember: character])
		return 6;
	else if ([p0 characterIsMember: character])
		return 0;
	else
		return 9;
}

//-1 is left, +1 is right
SInt8 OperatorAssociativity (NSString *operator)
{
	static NSCharacterSet *a1, *a2;
	static BOOL firstCall = YES;
	
	if (firstCall)
	{
		a1 = [NSCharacterSet characterSetWithCharactersInString:@"^~"],
		a2 = [NSCharacterSet characterSetWithCharactersInString:@"*/%+-"];
		firstCall = NO;
	}
	
	unichar character = [operator characterAtIndex: 0];
	
	if ([a1 characterIsMember:character])
		return 1;
	else if ([a2 characterIsMember:character])
		return -1;
	else
	{
		return 0;
	}
}

-(NSMutableArray*)convertStringsToNumbers: (NSArray*)array
{
	NSMutableArray *newArray = [NSMutableArray arrayWithCapacity:[array count]];
	NSEnumerator *enumerator = [array objectEnumerator];
	NSString *token;
	id object;
	while (token = [enumerator nextObject])
	{
		if ([Numbers characterIsMember:[token characterAtIndex: 0]])
			object = [NSDecimalNumber decimalNumberWithString: token];
		else
			object = token;
		
		[newArray addObject: object];
	}
	return newArray;
}

//this replaces all unary negations with a tilde
-(NSString*)identifyNegationOperators:(NSString*)inString
{
	NSMutableString *string = [inString mutableCopy];
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"1234567890."];
    NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceCharacterSet];
	
    NSUInteger index = 0;
    NSUInteger length = [string length];
	BOOL lastNonWhitespaceWasNumber=NO;
	
	for (index=0;index<length;index++)
	{
		unichar currentCharacter = [string characterAtIndex: index];
		if (currentCharacter == '-')
		{
			if (lastNonWhitespaceWasNumber==NO)
			{
				//swap it for a tilde
				NSRange range = {index,1};
				[string replaceCharactersInRange: range withString: @"~"];
			}
			lastNonWhitespaceWasNumber=NO;
		}
		else if ([numbers characterIsMember:currentCharacter] || currentCharacter==')')
		{
			lastNonWhitespaceWasNumber=YES;
		}
		else if ([whiteSpace characterIsMember:currentCharacter] == NO)
		{
			lastNonWhitespaceWasNumber=NO;
		}
	}
	
	return string;
}

//this finds 'e', checks that it is bounded by numbers (to distinguish from euler's constant), merges those numbers into a single one
-(NSMutableArray*)identifyExponents:(NSArray*)inTokens
{
	Class numberClass = [NSDecimalNumber class];
	NSUInteger inCount = [inTokens count];
	NSMutableArray *outTokens = [NSMutableArray arrayWithCapacity: inCount];
	NSUInteger index;
	
	for (index=0; index<inCount; index++)
	{
		id token = [inTokens objectAtIndex:index];
		
		if ([token isKindOfClass:numberClass])
		{
			[outTokens addObject:token];
			continue;
		}
		
		if ([token isEqualToString:@"e"])
		{
			id afterToken=nil;
			if (index<inCount-1)
				afterToken = [inTokens objectAtIndex:index+1];
			
			if ([afterToken isKindOfClass:numberClass])
			{
				//replace 'e' with '*10^' also go ahead and add afterToken and increment index for a little speed boost
				[outTokens addObject: @"*"];
				[outTokens addObject: [NSDecimalNumber numberWithInt:10]];
				[outTokens addObject: @"^"];
				[outTokens addObject: afterToken];
				index++;
				continue;
			}
		}
		[outTokens addObject:token];
	}
	return outTokens;
}

-(NSMutableArray*)identifyConstants:(NSArray*)inTokens
{
	Class numberClass = [NSDecimalNumber class];
	NSUInteger inCount = [inTokens count];
	NSMutableArray *outTokens = [NSMutableArray arrayWithCapacity: inCount];
	NSDecimalNumber *eulers = (id)[NSDecimalNumber numberWithDouble:M_E];
	NSDecimalNumber *pi = (id)[NSDecimalNumber numberWithDouble:M_PI];
	NSUInteger index;
	
	for (index=0; index<inCount; index++)
	{
		id token = [inTokens objectAtIndex:index];
		
		if ([token isKindOfClass:numberClass])
		{
			[outTokens addObject:token];
			continue;
		}
		
		if ([token isEqualToString:@"e"])
			[outTokens addObject:eulers];
		else if ([token isEqualToString:@"pi"] || [token isEqualToString:@"π"])
			[outTokens addObject:pi];
		else
			[outTokens addObject:token];
	}
	return outTokens;
}

/*-(NSString*)identifyExponents:(NSString*)inString
{
	NSMutableString *string = [inString mutableCopy];
	NSUInteger length = [string length];
	NSRange searchRange = {0,length}, foundRange;

	while ((searchRange.location != length))
	{
		foundRange = [string rangeOfString: @"e" options: 0 range: searchRange];
		
		if (foundRange.location==NSNotFound)
			break;
		
		if (foundRange.location == 0)
			continue;
		
		if (foundRange.location+foundRange.length == length)
			break;
			
		//it's not at the beginning or end of the string, and it did find something, so now see if it's bound by numbers
		if ([Numbers characterIsMember:[string characterAtIndex:foundRange.location-1]] &&
			[Numbers characterIsMember:[string characterAtIndex:foundRange.location+foundRange.length]])
		{
			[string replaceCharactersInRange:foundRange withString:@"*10^"];
		}
	}
	
}*/

-(NSMutableArray*)getTokens: (NSString *)string
{
	NSMutableArray *tokens = [NSMutableArray array];
	
	NSUInteger index = 0,
		   length = [string length];
	
	NSCharacterSet *letters = [NSCharacterSet letterCharacterSet],
				   *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet],
				   *basicOperators = [NSCharacterSet characterSetWithCharactersInString:@"%~+-/*^()"],
				   *numbers = [NSCharacterSet characterSetWithCharactersInString:@"1234567890."];
	
	while (index<length)
	{
		NSMutableString *token = [NSMutableString string];
		BOOL doneReadingToken=NO;
		NSCharacterSet *currentSet;
		unichar currentCharacter = [string characterAtIndex: index];
		
		if ([basicOperators characterIsMember: currentCharacter])
		{
			//basic operators are only one character long
			[token setString: [NSString stringWithCharacters: &currentCharacter length:1]];
			doneReadingToken = YES;
		}
		else if ([whiteSpace characterIsMember: currentCharacter])
		{
			doneReadingToken = YES;
		}
		else  if ([letters characterIsMember: currentCharacter])
		{
			currentSet = letters;
			[token appendString: [NSString stringWithCharacters: &currentCharacter length:1]];
		}
		else if ([numbers characterIsMember: currentCharacter])
		{
			currentSet = numbers;
			[token appendString: [NSString stringWithCharacters: &currentCharacter length:1]];
		}
		else
			return nil;
		
		index++;
		
		while (doneReadingToken==NO && index<length)
		{
			currentCharacter = [string characterAtIndex: index];
			if ([currentSet characterIsMember: currentCharacter])
			{
				[token appendString: [NSString stringWithCharacters: &currentCharacter length:1]];
				index++;
			}
			else
			{
				doneReadingToken = YES;
			}
			
		}
		
		if ([token isEqualToString: @""] == NO)
			[tokens addObject:token];
	}
	
	return tokens;
}

@end
