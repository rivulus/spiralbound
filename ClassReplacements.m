//
//  ClassReplacements.m
//  CommonSpiralbound
//
//  Created by Philip on 5/24/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import "SPRNotebook.h"
#import "SPRPlainPage.h"
#import "SPROrganizer.h"

/*
 These are so that the Unarchiver uses the new class types when unarchiving
 
 CLASS CHANGES:
 Notebook -> SPRNotebook
 NotebookPage -> SPRPage (Abstract)
	NotebookPage now unarchives as SPRPlainPage, concrete subclass of SPRPage
*/

/*Notebook*/
@interface Notebook : NSObject {}
@end

@implementation Notebook
+ (Class)classForKeyedUnarchiver {return [SPRNotebook class];}
@end

/*NotebookPage*/
@interface NotebookPage : NSObject { }
@end

@implementation NotebookPage
+ (Class)classForKeyedUnarchiver {return [SPRPlainPage class];}
@end

/*NotebookOrganizer*/
@interface NotebookOrganizer : NSObject { }
@end

@implementation NotebookOrganizer
+ (Class)classForKeyedUnarchiver {return [SPROrganizer class];}
@end