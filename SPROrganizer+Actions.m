//
//  SPROrganizer+Actions.m
//  SpiralBound Pro
//
//  Created by Philip White on 4/30/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "SPROrganizer+Actions.h"
#import "SPROrganizer+Sorting.h"
#import "SPROrganizer+Security.h"
#import "SPRNotebook.h"
#import "SPRPage.h"
#import "SpiralBoundWindow.h"
#import "Exporter.h"
#import "SBProtoFrameView.h"
#import "Inquisitor.h"
#import "PrintingView.h"
#import "SPRLockableTextStorage.h"
#import "NotificationNames.h"
#import "SBTitleView.h"
#import "SBConstants.h"
#import "NSAttributedString+Additions.h"
#import "SPRTextView.h"

#import "RIVTools.h"
#import "SPRTodoPage.h"
#import "SPRTodoListView.h"
#import "NotebookSettingsController_SBPro.h"

extern NSMutableArray *Organizers;

@implementation SPROrganizer (Actions)

+(void)raisePage:(SPRPage*)page range:(NSRange)rangeToSelect buldgeMain:(BOOL)buldgeMain rangeInTitle:(BOOL)rangeInTitle
{
	SPROrganizer *organizer = [self organizerForPage:page];
	[organizer raisePage:page range:rangeToSelect buldgeMain:buldgeMain rangeInTitle:rangeInTitle];
}

+(void)raisePage:(SPRPage*)page range:(NSRange)rangeToSelect buldgeMain:(BOOL)buldgeMain
{
	SPROrganizer *organizer = [self organizerForPage:page];
	[organizer raisePage:page range:rangeToSelect buldgeMain:buldgeMain];
}

-(IBAction)newPage:(id)sender
{
	SPRPage *page = [notebook newPage];
	
	[self setMainWindowPage: page];
	[mainWindow makeKeyAndOrderFront:nil];
	if ([(SBProtoFrameView*)[mainWindow contentView] isShaded])
		[(SBProtoFrameView*)[mainWindow contentView] setIsShaded:NO];
	[mainWindow makeFirstResponder:[mainWindow mainTextView]]; 
}

-(IBAction)newTodoPage:(id)sender
{
	SPRPage *page = [notebook newTodoPage];
	
	[self setMainWindowPage: page];
	[mainWindow makeKeyAndOrderFront:nil];
	if ([(SBProtoFrameView*)[mainWindow contentView] isShaded])
		[(SBProtoFrameView*)[mainWindow contentView] setIsShaded:NO];
	[mainWindow makeFirstResponder:[mainWindow mainTodoListView]]; 
}

-(IBAction)nextPage:(id)sender
{
	[self setMainWindowPage:[self nextPageInStack]];
	[mainWindow makeKeyAndOrderFront:nil];
	if ([(SBProtoFrameView*)[mainWindow contentView] isShaded])
		[(SBProtoFrameView*)[mainWindow contentView] setIsShaded:NO];

}

-(IBAction)previousPage:(id)sender
{
	[self setMainWindowPage:[self previousPageInStack]];
	[mainWindow makeKeyAndOrderFront:nil];
	if ([(SBProtoFrameView*)[mainWindow contentView] isShaded])
		[(SBProtoFrameView*)[mainWindow contentView] setIsShaded:NO];

}

//the general purpose method for deleting a page is removePage:
-(IBAction)deletePage:(id)sender
{
	SPRPage *page;
	SpiralBoundWindow *window;
	
	if ([sender isKindOfClass:[NSView class]])
	{
		window = (id)[sender window];
	}
	else
	{
		window = (id)[NSApp mainWindow];
	}
	
	page = [self pageForWindow: window];
	
	BOOL shouldAsk=YES;
	//determine if we need to ask the user
	if ([[page title] isEqualToString:@"Untitled"]) //TODO this could just check the [page isTitled] flag
	{
		NSUInteger length = [[page contentText] length];
		
		if (length == 0)
		{
			shouldAsk = NO;
		}
		else if (length == 1)
		{
			if ([[[page contentText] string] characterAtIndex:0] == '\a' ||
				[[[page contentText] string] isEqualToString: SPRInvisibleAttributeHolder])
				shouldAsk = NO;
		}
	}
	
	if (shouldAsk)
	{
		if ([(SBProtoFrameView*)[window contentView] isShaded])
			[(SBProtoFrameView*)[window contentView] setIsShaded:NO];
	
		//let's ask the user user first
		NSInteger result = NSRunAlertPanel(@"Delete Page?", @"Are you sure you would like to delete the page titled: \"%@\"?", @"Cancel", @"Delete", nil,
										[page title]);
		if (result == NSAlertDefaultReturn) //Cancel
			return;
	}
	
	//post the will delete notification
	NSUInteger pageIndex = [[notebook pages] indexOfObject:page];
	NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:notebook, @"Notebook", [NSNumber numberWithInt:pageIndex], @"PageIndex", nil];
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:PageWillBeDeletedNotification																																			object:page userInfo:userInfo]
											   postingStyle:NSPostNow];
	
	BOOL tornOff = (page != mainWindowPage);
	
	if (tornOff)
	{
		[window close];
		[tornOffWindows removeObject:window];
		[tornOffPages removeObject: page];
	}
	else
	{
		//get the page that will be the new top page
		SPRPage *nextPage = [self nextPageInStack];
	
		if (nextPage == mainWindowPage)	//then we need to create a new page
		{
			nextPage = [notebook newPage];
		}
		
		[self setMainWindowPage: nextPage];
	}
	
	//if 'page' was mainWindowPage it still contains the old page, which we can now delete
	[notebook deletePage: page];
}

-(IBAction)deleteNotebook:(id)sender
{
	if ([[SPROrganizer organizers] count] == 1)
	{
		NSRunAlertPanel(@"You may not delete the last notepad!", @"If you would like to delete this notepad, create a new notepad first.", nil, nil, nil);
		return;
	}
	
	NSInteger result = NSRunAlertPanel(@"Delete Notepad?", @"Are you sure you would like to delete the notepad titled: \"%@\"? The file containing this notepad's data will be moved to the trash." , @"Cancel", @"Delete", nil,
											[self title]);
	if (result == NSAlertDefaultReturn) //Cancel
		return;
	
	//unregister ourself for notifications
	//this is also done in dealloc
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	[SPRNotebook deleteNotebook: notebook];
	
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:NotebookCountDidChangeNotification																																			object:nil]
											   postingStyle:NSPostASAP];
	[Organizers removeObject:self];
	
	//close all of the windows
	[mainWindow close];
	[tornOffWindows makeObjectsPerformSelector:@selector(close)];
}

-(IBAction)returnPageToStack:(id)sender
{	
	NSWindow *window;
	if ([sender isKindOfClass: [NSView class]])
		window = [sender window];
	else
		window = [NSApp mainWindow];
	
	[self returnPageFromTornOffWindow:(id)window];
}

- (void)setTitle:(NSString*)title window:(SpiralBoundWindow*)window
{
	SPRPage *page;
	//first find the page for the window:
	if (window == mainWindow)
	{
		page = mainWindowPage;
	}
	else
	{
		page = [tornOffPages objectAtIndex:[tornOffWindows indexOfObject:window]];
	}
	
	[page setTitle:title];
	//titling the window is now handled using a notification originating
	//from the page object and picked up by this organizer
	//[window setTitle:title];
	
	
	/*[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:PageTitleDidChangeNotification																																				object:page]
											   postingStyle:NSPostASAP];*/ // added this notification to +setTitle: in SPRPage
}

- (IBAction)gotoPage:(id)sender
{
	SPRPage *page = [sender representedObject];
	[self raisePage:page range:NSMakeRange(NSNotFound,0) buldgeMain:NO];
}

-(void)raiseNotebook
{
	NSArray *allWindows = [tornOffWindows arrayByAddingObject:mainWindow];
	
	RaiseWindowsKeepingZOrder(allWindows);
	[mainWindow makeKeyWindow];
	[self setHidden:NO];
	[self setKey:YES];
}

-(void)raisePage:(SPRPage*)page range:(NSRange)rangeToSelect buldgeMain:(BOOL)buldgeMain rangeInTitle:(BOOL)rangeInTitle
{
	if ([self isHidden])
		[self raise:nil];	//this handles unhiding
	
	id textView=nil; //the "textView" may also be a SPRTodoListView which responds to some of the same messages as
				//a text view, including scrollRangeToVisible:, setSelectedRange:, and showFindIndicatorForRange:
	
	SpiralBoundWindow *window = [self windowForPage:page];
	
	if (window == nil)
	{
		[self setMainWindowPage:page];
		window = mainWindow;
	}
	
	if ([(SBProtoFrameView*)[window contentView] isShaded])
		[(SBProtoFrameView*)[window contentView] setIsShaded:NO];
	
	SBTitleView *titleView;
	if (rangeInTitle)
	{
		titleView = [window titleView];
		if ([window makeFirstResponder:window])
			textView = [window fieldEditor:YES forObject:titleView];
		else
			rangeToSelect = NSMakeRange(NSNotFound,0);
			
	}
	else
	{
		textView = [window documentView]; //may be an SPRTodoListView rather than a text view
	}
	
	[window orderFront:nil];
	[window makeMainWindow];
	
	if (NSEqualRanges(rangeToSelect, NSMakeRange(NSNotFound,0)))
	{
		if (buldgeMain == YES || window != mainWindow)
			[window buldge];
	}
	else
	{
		[textView scrollRangeToVisible:rangeToSelect]; 
		SEL selector = @selector(showFindIndicatorForRange:);
		
		//this invocation mumbo jumbo is all to delay the call of showFindIndicatorForRange:
		//which for whatever reason behaves sporadically otherwise
		if (rangeInTitle)
		{
			[titleView rename]; 
		}
		if ([textView respondsToSelector:selector])
		{
			//[textView showFindIndicatorForRange:rangeToSelect];
			NSMethodSignature *sig = [textView methodSignatureForSelector:selector];
			NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:sig];
			[invocation setArgument:&rangeToSelect atIndex:2];
			[invocation setSelector:selector];
			[invocation setTarget:textView];
			[invocation retainArguments];
			[invocation performSelector:@selector(invoke) withObject:nil afterDelay:0.0];
		}
		else
		{
			[textView setSelectedRange:rangeToSelect];
		}
	}
}

-(void)raisePage:(SPRPage*)page range:(NSRange)rangeToSelect buldgeMain:(BOOL)buldgeMain
{
	[self raisePage:page range:rangeToSelect buldgeMain:buldgeMain rangeInTitle:NO];
}

- (void)numberVisiblePages
{
	NSTextField *textField;
	NSUInteger index, count;
	NSArray *pages = [notebook pages];
	
	count = [pages count];
	
	//first do the two mainWindow ones
	textField = [[mainWindow contentView] viewWithTag: MAIN_LOCATION_FIELD_TAG];
	index = [pages indexOfObject:mainWindowPage];
	[textField setIntValue:index+1];
	
	textField = [[mainWindow contentView] viewWithTag: MAIN_PAGE_COUNT_FIELD_TAG];
	[textField setStringValue:[NSString stringWithFormat:@"of %u", count]];
	
	//now do all of the torn off windows
	NSUInteger i, tornOffCount = [tornOffWindows count];
	
	for (i=0; i < tornOffCount; i++)
	{
		textField = [[[tornOffWindows objectAtIndex:i] contentView] viewWithTag: TORNOFF_LOCATION_FIELD_TAG];
		index = [pages indexOfObject:[tornOffPages objectAtIndex:i]];
		[textField setStringValue:[NSString stringWithFormat: @"Page %u of %u", index+1, count]];
	}
}

-(IBAction)exportPage:(id)sender
{
	NSWindow *window = [NSApp mainWindow];
	SPRPage *page = [self pageForWindow:window];
	
	if ([page isSecure])
	{
		if ([[Inquisitor sharedInquisitor] challenge] == NO)
			return;
	}
	
	Exporter *exporter = [Exporter sharedExporter];
	[exporter showExportPanelForPage:page window:window];
}

-(IBAction)emailPage:(id)sender
{
	NSWindow *window = [NSApp mainWindow];
	SPRPage *page = [self pageForWindow:window];
	
	if ([page isSecure])
	{
		if ([[Inquisitor sharedInquisitor] challenge] == NO)
			return;
	}
	
	Exporter *exporter = [Exporter sharedExporter];
	[exporter emailPage:page inWindow: window];
}


-(void)emailQuickly:(id)sender
{
	NSWindow *window = [NSApp mainWindow];
	SPRPage *page = [self pageForWindow:window];
	
	if ([page isSecure])
	{
		if ([[Inquisitor sharedInquisitor] challenge] == NO)
			return;
	}
	
	Exporter *exporter = [Exporter sharedExporter];
	[exporter emailPageQuickly:page];
}

-(IBAction)saveNotebook:(id)sender
{
	if ([self areAnyPagesSecured])
	{
		if ([[Inquisitor sharedInquisitor] challenge] == NO)
			return;
	}
	
	Exporter *exporter = [Exporter sharedExporter];
	[exporter showSavePanelForNotebook: [self notebook] window: [self mainWindow]];
}

- (IBAction)printAll:(id)sender
{
	NSString *alertTitle = [NSString stringWithFormat: @"Printing Options for Notepad \"%@\"", [[self notebook] title]];
	int result = NSRunAlertPanel(alertTitle, @"Would you like to print on the fewest sheets of paper possible or have every page printed on separate sheets?",
								 @"Save Paper", @"Separate Pages", nil);
	
	PrintingView *pView = [[PrintingView alloc] init];
	
	NSArray *pages = [[self notebook] pages];
	NSEnumerator *enumerator = [pages objectEnumerator];
	SPRPage *page;
	
	if (result == NSAlertDefaultReturn)
		[pView setThrifty:YES];
	else
		[pView setThrifty:NO];
	
	while (page = [enumerator nextObject])
	{
		[pView addPageContents:[page contentsAsAttributedString] title:[page title]];
	}
	
	[pView finish];
	
	[pView print:nil];
}

-(IBAction)showNotebookSettings:(id)sender
{
	[[NotebookSettingsController sharedController] showModallyForOrganizer:self];
}

//sent by typing into the page index field at the bottom of a mainWindow
-(IBAction)gotoPageIndex:(id)sender
{
    //oddly, at least in 10.6.1, the message gets send to the wrong notebook if you click
    //out of the field to another notebook. check for that here
    if ([sender window] != mainWindow)
    {
        [[SPROrganizer organizerForWindow:[sender window]] gotoPageIndex:sender];
        return;
    }

	NSInteger desiredIndex = [sender intValue];
	NSArray *pages = [[self notebook] pages];
	NSUInteger pageCount = [pages count];
	NSUInteger mainWindowPageIndex = [pages indexOfObject:mainWindowPage]+1;
	
	if (desiredIndex < 1 || desiredIndex > pageCount)
	{
		NSBeep();
		[sender setIntValue: mainWindowPageIndex];
		[sender selectText:nil];
	}
	else if (desiredIndex != mainWindowPageIndex)
	{
		SPRPage *desiredPage = [pages objectAtIndex:desiredIndex-1];
		[self raisePage:desiredPage range:NSMakeRange(NSNotFound,0) buldgeMain:NO];
		
		if ([tornOffPages containsObject:desiredPage])
			[sender setIntValue: mainWindowPageIndex];
	}
}

#pragma mark Notifications
- (void)delayedNotebookDidChangeLength:(NSNotification*)notification
{
	[self numberVisiblePages];
}

-(void)delayedMainWindowPageChanged:(NSNotification*)notification
{
	NSTextField *textField;
	NSUInteger index;
	NSArray *pages = [notebook pages];
	textField = [[mainWindow contentView] viewWithTag: MAIN_LOCATION_FIELD_TAG];
	index = [pages indexOfObject:mainWindowPage];
	[textField setIntValue:index+1];
}

-(void)delayedPageLayoutChanged:(NSNotification*)notification
{
	[self numberVisiblePages];
}

-(void)pageArrangementDidChange:(NSNotification*)notification
{
	[self numberVisiblePages];
}

-(void)windowDidResignKey:(NSNotification*)notification
{
	NSWindow *window = [notification object];
	[[self pageForWindow:window] autotitle]; //pageForWindow will return nil if the window is not associated with the organizer
}

-(void)pageTitleDidChange:(NSNotification*)notification
{
	SpiralBoundWindow *window = [self windowForPage: [notification object]];
	//don't think can be nil as of 1.1, but may be in the future if pages can be renamed in the browser
	
	[window setTitle: [[notification object] title]];
}

//window delegate stuff
- (NSRect)window:(NSWindow *)window willPositionSheet:(NSWindow *)sheet usingRect:(NSRect)defaultSheetRect
{
	NSRect toolbarRect = [(SBProtoFrameView*)[window contentView] toolbarRect];
	NSRect dragbarRect = [(SBProtoFrameView*)[window contentView] dragbarRect];
	defaultSheetRect.origin.y -= (toolbarRect.size.height+dragbarRect.size.height);
	defaultSheetRect.size.height=0;
	
	return defaultSheetRect;
}

#pragma mark SPRTodoListView Delegate

/*actual delegate methods*/
-(void)todoListView:(SPRTodoListView*)tlv createNewItemAtIndex:(NSUInteger)index withContent:(NSAttributedString*)newText
{
	SPRTodoPage *page = (id)[self pageForWindow:[tlv window]];
	[page addItemAtIndex:index withContent:newText state:NO];
}

-(void)todoListView:(SPRTodoListView*)tlv deleteItemAtIndex:(NSUInteger)index preserveContents:(NSAttributedString*)contents
{
	SPRTodoPage *page = (id)[self pageForWindow:[tlv window]];
	
	if ([[page items] count] == 1)
	{
		[page setState:NO forItemAtIndex:index];
		[page setContents:[NSAttributedString new] forItemAtIndex:index];
		return;
	}
	
	if (index > 0 && contents != nil)
	{
		[[[[page items] objectAtIndex:index-1] objectForKey:TextStorageKey] appendAttributedString:contents];
	}
	
	[page removeItemAtIndex:index];
}

-(void)todoListView:(SPRTodoListView*)tlv toggleItemAtIndex:(NSUInteger)index
{
	SPRTodoPage *page = (id)[self pageForWindow:[tlv window]];
	[page toggleItemAtIndex:index];
}

-(NSData*)todoListView:(SPRTodoListView*)tlv dataForPBoardType:(NSString*)type forDragFromItemAtIndex:(NSUInteger)index
{
	SPRTodoPage *page = (id)[self pageForWindow:[tlv window]];
	NSData *data;
	
	if ([type isEqualToString:SPRPasteboardTypeTodoItem])
	{
		data = [NSKeyedArchiver archivedDataWithRootObject:[page itemAtIndex:index]];
	}
	else
	{
		NSString *doctype = [SPRPasteboardToDocumentTypeTable objectForKey:type];
		
		if (doctype == nil)
			[NSException raise:@"SPROrganizer+Actions" format:@"requested data for unknown drop type"];
		
		data = [[[page itemAtIndex:index] objectForKey:TextStorageKey] dataUsingDocumentType:doctype];
	}
	return data;
}

-(BOOL)todoListView:(SPRTodoListView*)tlv writeDroppedData:(NSData*)data ofType:(NSString*)type toIndex:(NSUInteger)index
{
	SPRTodoPage *page = (id)[self pageForWindow:[tlv window]];
	if ([type isEqualToString:SPRPasteboardTypeTodoItem])
	{
		NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
		NSAttributedString *c = [dict objectForKey:TextStorageKey];
		BOOL s = [[dict objectForKey:StateKey] boolValue];
		[page addItemAtIndex:index withContent:c state:s];
	}
	
	return YES;
}

-(void)todoListView:(SPRTodoListView*)tlv moveDroppedData:(NSData*)data ofType:(NSString*)type toIndex:(NSUInteger)dstIndex fromIndex:(NSUInteger)srcIndex
{
	if (![type isEqualToString:SPRPasteboardTypeTodoItem])
		[NSException raise:@"NoteBookOrganizerException"
					format:@"Tried to move item via drop but data type was incorrect. type = %@", type];
	
	SPRTodoPage *page = (id)[self pageForWindow:[tlv window]];
	[page moveItemFromIndex:srcIndex toIndex:dstIndex];
}

/*Related Methods*/
-(void)loadTodoListView:(SPRTodoListView*)tlv page:(SPRTodoPage*)page
{
	[tlv removeAllItems];
	
	for (NSDictionary *dict in [page items])
	{
		BOOL state = [[dict objectForKey:StateKey] boolValue];
		NSTextStorage *ts = [dict objectForKey:TextStorageKey];
		[tlv addTextStorage:ts state:state];
	}
}

/*Notifications*/
-(void)todoItemsReload:(NSNotification*)note
{
	SPRTodoPage *page = [note object];
	SpiralBoundWindow *window = [self windowForPage:page];
	
	if (window)
	{
		SPRTodoListView *tlv = [window mainTodoListView];
		[tlv saveSelection];
		[self loadTodoListView:tlv page:page];
		[tlv loadSelection];
	}
}

-(void)todoItemStateDidChange:(NSNotification*)note
{
	SPRTodoPage *page = [note object];
	SpiralBoundWindow *window = [self windowForPage:page];
	
	if (window)
	{
		SPRTodoListView *tlv = [window mainTodoListView];
		NSDictionary *dict = [note userInfo];
		[tlv setState:[[dict objectForKey:StateKey] boolValue] forTextStorage:[dict objectForKey:TextStorageKey]];
	}
}

#pragma mark SECURITY NOTIFICATIONS

-(void)pageSecurityDidChange:(NSNotification*)note
{
	SPRPage *page = [note object];
	
	SpiralBoundWindow *window = [self windowForPage:page];
	if (window)
	{
		[window setSecurityButtonState:([page isSecure] ? NSOnState : NSOffState)];
	}
}

@end
