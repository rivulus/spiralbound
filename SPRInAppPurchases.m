//
//  SPRInAppPurchases.m
//  SpiralBound Pro
//
//  Created by Philip White on 4/9/18.
//

#import "SPRInAppPurchases.h"
#import <StoreKit/StoreKit.h>
#import "SPRMultipleNotebookPurchaseAppeal.h"

NSString * const SPRInAppPurchaseTipJarIsAvailable = @"SPRInAppPurchaseTipJarIsAvailable";

static NSString * const ExtraBigTipProductID = @"com.rivulus.spiralboundpro.extrabigtip";
static NSString * const BigTipProductID = @"com.rivulus.spiralboundpro.bigtip";
static NSString * const TipProductID = @"com.rivulus.spiralboundpro.tip";

static NSString * const HasDonatedPreferencesKey = @"HasDonatedPreferencesKey";
static NSString * const RunCountPreferencesKey = @"RunCountPreferencesKey";

@interface SPRInAppPurchases() <SKProductsRequestDelegate, SKPaymentTransactionObserver>
{
	SKProductsRequest *_productsRequest;
	SPRMultipleNotebookPurchaseAppeal *_purchaseAppealWindowController;
	InAppPurchaseValidationCompletion _validationCompletion;
	
	SKProduct *_tipProduct;
	SKProduct *_bigTipProduct;
	SKProduct *_extraBigTipProduct;
	
	bool _hasDonated;
	NSUInteger _nthRun;
}
@end

@implementation SPRInAppPurchases

+ (instancetype)sharedInstance
{
	static SPRInAppPurchases *instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		instance = [[SPRInAppPurchases alloc] init];
	});
	
	return instance;
}

- (instancetype)init
{
	self = [super init];
	if (self != nil)
	{
		[[SKPaymentQueue defaultQueue] addTransactionObserver:self];
		NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
		_hasDonated = [userDefaults boolForKey:HasDonatedPreferencesKey];
		_nthRun = [userDefaults integerForKey:RunCountPreferencesKey];

		[userDefaults setInteger:_nthRun+1 forKey:RunCountPreferencesKey];
		
		[self fetchProducts];
	}
	return self;
}

- (void)run
{
	[self fetchProducts];
}

- (void)fetchProducts
{
	_productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithArray:@[ExtraBigTipProductID, BigTipProductID, TipProductID]]];

	[_productsRequest setDelegate:self];
	[_productsRequest start];
}

- (NSString *)priceForProduct:(SKProduct *)product
{
	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	[numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
	[numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
	[numberFormatter setLocale:[product priceLocale]];
	NSString *formattedString = [numberFormatter stringFromNumber:[product price]];
	return formattedString;
}

- (void)makePurchase:(SKProduct *)product
{
	SKPayment *payment = [SKMutablePayment paymentWithProduct:product];
	SKPaymentQueue *paymentQueue = [SKPaymentQueue defaultQueue];

	[paymentQueue addPayment:payment];
}

- (void)finishAppealWithThankYou
{
	[_purchaseAppealWindowController finish];
	[self recordDonation];
	NSRunAlertPanel(NSLocalizedString(@"TipThankYouTitle", nil), NSLocalizedString(@"TipThankYou", nil), nil, nil, nil);
}

- (BOOL)shouldShowBegUI
{
	if (_hasDonated)
	{
		return NO;
	}
	
	NSUInteger dozenRuns =  _nthRun / 12;
	
	switch (dozenRuns)
	{
		case 0:
			return NO;
		case 1:
		case 2:
			return (_nthRun - 12) % 4 == 0;
		case 3:
		case 4:
			return (_nthRun - 18) % 3 == 0;
		default:
			return (_nthRun - 30) % 2 == 0;
	}
}

- (void)showBegUIIfAppropriate
{
	if ([self shouldShowBegUI])
	{
		[self showUI];
	}
}

- (void)recordDonation
{
	_hasDonated = YES;
	[[NSUserDefaults standardUserDefaults] setBool:YES forKey:HasDonatedPreferencesKey];
}

- (void)showUI
{
	[[self purchaseAppealWindowController] showAsModalWithCompletion:^(SPRPurchaseAppealResult result) {
		
		switch (result)
		{
			case SPRPurchaseAppealTip:
				[self makePurchase:_tipProduct];
				break;
			case SPRPurchaseAppealBigTip:
				[self makePurchase:_bigTipProduct];
				break;
			case SPRPurchaseAppealExtraBigTip:
				[self makePurchase:_extraBigTipProduct];
				break;
		}
	}];
}

- (SPRMultipleNotebookPurchaseAppeal *)purchaseAppealWindowController
{
	if (_purchaseAppealWindowController == nil)
	{
		_purchaseAppealWindowController = [[SPRMultipleNotebookPurchaseAppeal alloc] init];
		[_purchaseAppealWindowController setTipPrice:[self priceForProduct:_tipProduct]];
		[_purchaseAppealWindowController setBigTipPrice:[self priceForProduct:_bigTipProduct]];
		[_purchaseAppealWindowController setExtraBigTipPrice:[self priceForProduct:_extraBigTipProduct]];
	}
	return _purchaseAppealWindowController;
}

#pragma mark -SKProductsRequestDelegate-

- (void)productsRequest:(nonnull SKProductsRequest *)request didReceiveResponse:(nonnull SKProductsResponse *)response
{
	dispatch_async(dispatch_get_main_queue(), ^{
		for (SKProduct *product in [response products])
		{
			NSString *productIdentifier = [product productIdentifier];
			if ([productIdentifier isEqualToString:ExtraBigTipProductID])
			{
				_extraBigTipProduct = product;
			}
			else if ([productIdentifier isEqualToString:BigTipProductID])
			{
				_bigTipProduct = product;
			}
			else if ([productIdentifier isEqualToString:TipProductID])
			{
				_tipProduct = product;
			}
		}
		
		if (_extraBigTipProduct != nil &&
			_bigTipProduct != nil &&
			_tipProduct != nil)
		{
			[[NSNotificationCenter defaultCenter] postNotificationName:SPRInAppPurchaseTipJarIsAvailable object:self];
			[self showBegUIIfAppropriate];
		}
	});
}

#pragma mark -SKPaymentTransactionObserver-

- (void)paymentQueue:(nonnull SKPaymentQueue *)queue updatedTransactions:(nonnull NSArray<SKPaymentTransaction *> *)transactions
{
	dispatch_async(dispatch_get_main_queue(), ^{
		for (SKPaymentTransaction *transaction in transactions)
		{
			NSString *productIdentifier = [[transaction payment] productIdentifier];
			if ([productIdentifier isEqualToString:TipProductID] ||
				[productIdentifier isEqualToString:BigTipProductID] ||
				[productIdentifier isEqualToString:ExtraBigTipProductID])
			{
				SKPaymentTransactionState transactionState = [transaction transactionState];
				if (transactionState == SKPaymentTransactionStatePurchased)
				{
					[self finishAppealWithThankYou];
					[queue finishTransaction:transaction];
				}
				else if (transactionState == SKPaymentTransactionStateFailed)
				{
					[self showTransactionError:transaction];
					[queue finishTransaction:transaction];
					[_purchaseAppealWindowController reset];
				}
			}
		}
	});
}

#pragma mark -Alerts-

- (void)showTransactionError:(SKPaymentTransaction *)transaction
{
	NSError *error = [transaction error];
	if ([error code] != SKErrorPaymentCancelled)
	{
		NSRunAlertPanel(NSLocalizedString(@"AppStoreErrorTitle", nil),
						NSLocalizedString(@"AppStoreTransactionGenericError", nil),
						nil, nil, nil, nil);
	}
}

@end
