//
//  Calculator+Operators.h
//  SpiralBound
//
//  Created by Philip White on 2/11/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Calculator.h"

@interface Calculator (Operators)

+ (void)setupLookupTables;

@end


#pragma mark UNARY

//unary operator object
@interface Negate : NSObject
{
}
+(NSDecimalNumber*)op:(NSDecimalNumber*)op;
@end

@interface Sine : NSObject
{
}
+(NSDecimalNumber*)op:(NSDecimalNumber*)op;
@end

@interface Cosine : NSObject
{
}
+(NSDecimalNumber*)op:(NSDecimalNumber*)op;
@end

@interface Tangent : NSObject
{
}
+(NSDecimalNumber*)op:(NSDecimalNumber*)op;
@end

@interface SineDegrees : NSObject
{
}
+(NSDecimalNumber*)op:(NSDecimalNumber*)op;
@end

@interface CosineDegrees : NSObject
{
}
+(NSDecimalNumber*)op:(NSDecimalNumber*)op;
@end

@interface TangentDegrees : NSObject
{
}
+(NSDecimalNumber*)op:(NSDecimalNumber*)op;
@end

@interface NaturalLog : NSObject
{
}
+(NSDecimalNumber*)op:(NSDecimalNumber*)op;
@end

@interface Base10Log : NSObject
{
}
+(NSDecimalNumber*)op:(NSDecimalNumber*)op;
@end

@interface Arcsine : NSObject
{
}
+(NSDecimalNumber*)op:(NSDecimalNumber*)op;
@end

@interface Arccosine : NSObject
{
}
+(NSDecimalNumber*)op:(NSDecimalNumber*)op;
@end

@interface Arctangent : NSObject
{
}
+(NSDecimalNumber*)op:(NSDecimalNumber*)op;
@end

@interface ArcsineDegrees : NSObject
{
}
+(NSDecimalNumber*)op:(NSDecimalNumber*)op;
@end

@interface ArccosineDegrees : NSObject
{
}
+(NSDecimalNumber*)op:(NSDecimalNumber*)op;
@end

@interface ArctangentDegrees : NSObject
{
}
+(NSDecimalNumber*)op:(NSDecimalNumber*)op;
@end

@interface AbsoluteValue : NSObject
{
}
+(NSDecimalNumber*)op:(NSDecimalNumber*)op;
@end

#pragma mark BINARY

//binary operator objects
@interface Add : NSObject
{
}
+(NSDecimalNumber*)op1:(NSDecimalNumber*)op1 op2:(NSDecimalNumber*)op2;
@end

@interface Subtract : NSObject
{
}
+(NSDecimalNumber*)op1:(NSDecimalNumber*)op1 op2:(NSDecimalNumber*)op2;
@end

@interface Multiply : NSObject
{
}
+(NSDecimalNumber*)op1:(NSDecimalNumber*)op1 op2:(NSDecimalNumber*)op2;
@end

@interface Divide : NSObject
{
}
+(NSDecimalNumber*)op1:(NSDecimalNumber*)op1 op2:(NSDecimalNumber*)op2;
@end

@interface Modulus : NSObject
{
}
+(NSDecimalNumber*)op1:(NSDecimalNumber*)op1 op2:(NSDecimalNumber*)op2;
@end

@interface Power : NSObject
{
}
+(NSDecimalNumber*)op1:(NSDecimalNumber*)op1 op2:(NSDecimalNumber*)op2;
@end

