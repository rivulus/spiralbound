//
//  NotificationNames.m
//  CommonSpiralbound
//
//  Created by Philip on 5/14/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

//see notes in header

NSString *SPRDisplayConfigurationDidChange = @"SPRDisplayConfigurationDidChange";

NSString *SPRTodoItemsOrderDidChange = @"SPRTodoItemsOrderDidChange";
NSString *SPRTodoItemsCountDidChange = @"SPRTodoItemsCountDidChange";

NSString *SPRTodoItemStateDidChange = @"SPRTodoItemStateDidChange";

NSString *MainWindowPageDidChangeNotification = @"DelayedMainWindowPageChanged",
		 *PageLayoutDidChangeNotification = @"DelayedPageLayoutChanged",
		 *NotebookDidBecomeKeyNotification = @"NotebookDidBecomeKey",
		 *NotebookDidHideNotification = @"NotebookDidHide",
		 *NotebookDidUnhideNotification = @"NotebookDidUnhide",
		 *NotebookDidResignKeyNotification = @"NotebookDidResignKey",
		 *PageWillBeDeletedNotification = @"PageWillBeDeleted",
		 *NotebookWillBeDeletedNotification = @"NotebookWillBeDeleted",
		 *NotebookColorDidChangeNotification = @"NotebookColorDidChange",
		 *NotebookTitleDidChangeNotification = @"NotebookTitleDidChange",
		 *NotebookCountDidChangeNotification = @"NotebookCountDidChange",
		 *NotebookDidChangeLengthNotification = @"DelayedNotebookDidChangeLength",
		 *PageArrangementDidChangeNotification = @"PageArrangementDidChange",
		 *PageSecurityDidChangeNotification = @"PageSecurityDidChange",
		 *PageTitleDidChangeNotification = @"PageTitleDidChange";
