//
//  SBController+Actions.h
//  SpiralBound Pro
//
//  Created by Philip White on 5/4/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SBController.h"

@interface SBController (Actions)

-(IBAction)showTipJar:(id)sender;

-(IBAction)showImportFromStickiesPanel:(id)sender;

-(IBAction)showImportFromSpiralBoundPanel:(id)sender;
-(IBAction)showNewNotebookPanel:(id)sender;
-(IBAction)showAllNotebooks:(id)sender;

-(IBAction)showPageSorter:(id)sender;

-(IBAction)importNotebook:(id)sender;

-(IBAction)showFindWindow:(id)sender;
-(IBAction)findNext:(id)sender;
-(IBAction)findPrevious:(id)sender;
-(IBAction)showPreferencesWindow:(id)sender;

-(IBAction)showSpiralBoundHelp:(id)sender;

-(IBAction)contactRivulus:(id)sender;

-(IBAction)cycleThroughWindows:(id)sender;
-(IBAction)saveImmediately:(id)sender;

-(IBAction)hideOtherNotebooks:(id)sender;

@end
