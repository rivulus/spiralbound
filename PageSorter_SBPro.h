//
//  PageSorter_SBPro.h
//  SpiralBound Pro
//
//  Created by Philip White on 5/5/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class PageBrowser, SPRTextView;
@interface PageSorter : NSWindowController
{
	IBOutlet NSSplitView *splitView;
	NSMutableArray *browsers;
	
	IBOutlet NSButton *addPaneButton;
	IBOutlet NSButton *removePaneButton;
}

+(NSWindow*)sorterWindow; //returns the sorter window but only if it is open. does not cause anything to be loaded
+(PageSorter*)sharedSorter;

-(PageBrowser*)browserWithPagesView:(NSBrowser*)pagesView; //the trash icon uses this to determine the browser from the NSBrowser pagesView
-(PageBrowser*)browserWithTextView:(SPRTextView*)textView;


-(IBAction)addPane:(id)sender;
-(IBAction)removePane:(id)sender;
-(PageBrowser*)keyBrowser;
-(IBAction)trashWasClicked:(id)sender;
-(IBAction)newNotebookButtonWasClicked:(id)sender;
-(IBAction)newRegularPageButtonWasClicked:(id)sender;
-(IBAction)newTodoPageButtonWasClicked:(id)sender;

-(void)deleteSelectionWithWarning:(BOOL)warn;

@end

@interface SPRPageBrowserSplitViewDelegate : NSObject
{
}

@end
