//
//  SBProtoFrameView.h
//  SpiralBound
//
//  Created by Philip White on 2/4/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

extern BOOL UseSimpleDragbar;

@class SPRNotebook, SPROrganizer, SBTitleView, SPRTodoListView, SPRTextView;
@interface SBProtoFrameView : NSView
{
	//SPRNotebook *notebook;

	IBOutlet NSScrollView *mainView;
	IBOutlet NSView *toolbarView;
	IBOutlet SBTitleView *titleView;
	IBOutlet NSBox *locationbarView;
	IBOutlet NSButton *securityButton;
	IBOutlet NSTextField *notebookTitleField;
	
	BOOL isShaded;
	float deltaHeight;
	
	NSArray *topLevelObjects;
	
	//NSColor *color;
	SPROrganizer *organizer;
	SPRTodoListView *mainTodoListView;
	SPRTextView *mainTextView;
	
}


- (id)initWithFrame:(NSRect)frame organizer:(SPROrganizer*)newOrganizer;
- (void)drawRect:(NSRect)rect;
- (void)drawResizer;
- (BOOL)acceptsFirstMouse:(NSEvent *)theEvent;
- (BOOL)resizerContainsPoint:(NSPoint)point;
- (void)mouseDown:(NSEvent *)event;
- (void)drawDragBar:(NSRect)clippingRect;
- (void)drawBackground;
- (BOOL)isOpaque;
- (void)positionSubviews;
//abstract or partly abstract methods


//this internally changes what needs to be changed when color is changed
- (void)setColors;
- (NSImage*)dragbarMaskImage;
- (NSImage*)dragbarImage; //must be overridden, need not call super
- (NSImage*)dragbarGrayedOutImage;
- (NSImage*)dragbarFillerImage;
- (NSRect)titleBarRect;
- (NSNib*)nib; //must be overridden, need not call super
- (NSRect)mainViewRect; //the rect of the window frame - all accoutrements in the frame's coordinates
- (NSRect)toolbarRect;
- (NSRect)dragbarRect; //used to draw the dragbar, must be in the frame view's coordinates
- (NSRect)visibleToolbarAndDragbarRect;
- (NSRect)frameRectMinusBorder;		//everything except the thick border
- (NSRect)unshadedFrameRectMinusBorder;
//accessors

- (SPROrganizer *)organizer;

- (SPRNotebook *)notebook;

- (NSScrollView *)mainView;
- (NSView*)documentView; //returns either the todolist or the main text view, whichever is present or visible
- (NSView *)toolbarView;
- (NSView *)locationbarView;
- (SPRTextView*)mainTextView;
- (void)setMainTextView: (SPRTextView*)newTextView;
- (BOOL)hasMainTextView;
- (SPRTodoListView*)mainTodoListView;
- (void)setMainTodoListView:(SPRTodoListView*)tv;
- (void)showMainTodoListView;
- (void)showMainTextView;
- (BOOL)hasMainTodoListView;

- (SBTitleView*)titleView;
- (BOOL)isShaded;
- (void)setIsShaded:(BOOL)value;
- (float)deltaHeight;
- (float)regularMinusSimpleDragbarHeight;
- (void)adjustWindowHeight;
- (void)adjustWindowHeightIfShaded;
- (void)validateWindowFrame;
- (void)setNotebookTitleFieldStringValue:(NSString*)value;

#ifdef DEBUG
+ (id)dummyWindow; //for F-Script purposes
#endif

- (void)breakBindings;
- (void)setSecurityButtonState:(int)state;
@end

NSWindow *GetDummyPositionWindow(void);
