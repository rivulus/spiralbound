//
//  NewNotebookController_SBPro.h
//  SpiralBound Pro
//
//  Created by Philip White on 5/4/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

typedef enum
{
	NewNotebookState,
	ImportFromStickiesState,
	ImportFromSpiralBoundState,
	ImportSavedNotebookState
} NewNotebookWindowState;


@interface NewNotebookController : NSWindowController <NSOpenSavePanelDelegate>
{
	IBOutlet NSColorWell *colorWell;
	IBOutlet NSTextField *nameField;
	IBOutlet NSTextField *nameNotUniqueField;
	
	IBOutlet NSPopUpButton *importFileTypeButton;
	IBOutlet NSBox *importAccessoryView;
	
	NewNotebookWindowState windowState;
	
	id relevantObject; //for the time being, this is just an NSArray for importSavedNotebook
	
}

+(NewNotebookController*)sharedController;
-(IBAction)cancelButton:(id)sender;
-(IBAction)createButton:(id)sender;
-(IBAction)setColor:(id)sender;
-(void)importSavedPagesFromFile:(NSString*)path;
-(void)importSavedNotebook;
-(void)showAsImportFromSpiralBound;
-(void)showAsImportFromStickies;
-(void)showAsImportSavedNotebookWithTitle: (NSString*)defaultTitle;
-(IBAction)showImportTypeHelp:(id)sender;
-(IBAction)setImportFileType:(id)sender;
@end
