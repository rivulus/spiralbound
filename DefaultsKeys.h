/*
 *  DefaultsKeys.h
 *  CommonSpiralbound
 *
 *  Created by Philip on 5/12/10.
 *  Copyright 2010 Rivulus Software. All rights reserved.
 *
 */

extern NSString *DefaultsSerialNumberKey;

extern NSString *DefaultsSearchEngineIgnoreCase,
				*DefaultsSearchEngineWholeWordsOnly,
				*DefaultsSearchEngineIgnoreDiacriticals,
				*DefaultsSearchEnginePageType,
				*DefaultsSearchEngineTitlesOrContents,
				*DefaultsSearchEngineScope;

extern NSString /*STYLE PAGE*/
				*DefaultsPreferencesUseSimpleDragbar,
				*DefaultsPreferencesEquationHighlightColor,
				*DefaultsPreferencesSolutionHighlightColor,
				*DefaultsPreferencesTextAttributes,

				/*MISC PAGE*/
				*DefaultsPreferencesDataFolderPath,
				*DefaultsPreferencesServicesAlwaysUseNotebookKey,
				*DefaultsPreferencesServicesDefaultNotebookNameKey,

				/*CALCULATOR PAGE*/
				*DefaultsPreferencesCalculatorIsEnabled,
				*DefaultsPreferencesPutCalculatorResultInClipboard,
				*DefaultsPreferencesCalculatorPrecision,
				*DefaultsPreferencesEnterKeyCompletesEquation,

				/*SECURITY PAGE*/
				*DefaultsPreferencesShouldScramble,
				*DefaultsPreferencesScramblingDelay,
				*DefaultsPreferencesScrambleImmediately,
				*DefaultsPreferencesNeedsPasswordToUnscramble,
				*DefaultsPreferencesEncryptData,
				*DefaultsPreferencesRequirePasswordAtLaunch;

extern NSString *DefaultsEncryptData,
				//todo list
				*DefaultsPreferencesMarkStyle,
				*DefaultsPreferencesMarkColor,
				*DefaultsPreferencesStrikeOutCheckedItem;

