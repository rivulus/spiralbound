//
//  Calculator.m
//  SpiralBound
//
//  Created by Philip White on 2/11/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "Calculator.h"
#import "Calculator+Converter.h"
#import "Calculator+Operators.h"
#import "NSMutableArray+PushPop.h"
#import "SBConstants.h"
//static NSTask *bcTask = nil;

NSCharacterSet *Numbers, *BinaryOperators;

NSDictionary *UnaryOperatorLookup;
NSDictionary *BinaryOperatorLookup;

@implementation Calculator

+(Calculator*)sharedCalculator
{
	static Calculator *sharedCalculator = nil;
	
	if (sharedCalculator == nil)
	{
		//allocate all of the globals
		//get the correct decimal point equivalent for the locale
		NSMutableString *numberString = [NSMutableString stringWithString:@"1234567890"];
		NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
		[numberString appendString:[formatter decimalSeparator]];
		
		Numbers = [NSCharacterSet characterSetWithCharactersInString:numberString];
		BinaryOperators = [NSCharacterSet characterSetWithCharactersInString:@"%/*^-+"];;
		
		
		[self setupLookupTables];
		
		sharedCalculator = [[Calculator alloc] init];
	}
	return sharedCalculator;
}


//this method will be called whenever the user types in an '='
//the string is the contents of the current page up to the '=' exclusive

-(NSString*)calculateWithString:(NSString*)string allowOneTokenEquation:(BOOL)oneTokenOK
{
	//check for the invisible character "\a" which was the legacy character used to hold attributes or the new character
	//SPRInvisibleAttributeHolder
	NSRange invisibleRange = [string rangeOfString:@"\a"];
	if (invisibleRange.location != NSNotFound)
	{
		string = (NSString*)[NSMutableString stringWithString:string];
		[(NSMutableString*)string deleteCharactersInRange:invisibleRange];
	}
	invisibleRange = [string rangeOfString:SPRInvisibleAttributeHolder];
	if (invisibleRange.location != NSNotFound)
	{
		string = (NSString*)[NSMutableString stringWithString:string];
		[(NSMutableString*)string deleteCharactersInRange:invisibleRange];
	}
		
	NSMutableArray *tokens = [self infixToPostfix:[string lowercaseString]];
	
	if (tokens == nil)
		return nil;
		
	if (oneTokenOK==NO && [tokens count]==1)
		return nil;
	
	if ([tokens count] == 0)
		return nil;
	
	NSDecimalNumber *number = [self processSimplePostfix:tokens];
	short scale = [[[NSUserDefaults standardUserDefaults] objectForKey:@"CalculatorPrecision"] intValue];
	
	id numberHandler = [[NSDecimalNumberHandler alloc] initWithRoundingMode:NSRoundPlain
																	  scale:scale
														   raiseOnExactness:NO
														    raiseOnOverflow:NO
														   raiseOnUnderflow:NO
														raiseOnDivideByZero:NO];
	
	return [[number decimalNumberByRoundingAccordingToBehavior: numberHandler] stringValue];
}

-(NSDecimalNumber*)processSimplePostfix:(NSMutableArray*)postfix
{
	NSString *token;
	NSMutableArray *stack = [NSMutableArray array]; 

	//work from the left of it
	while ([postfix count] != 0)
	{
		token = [postfix objectAtIndex:0];
		[postfix removeObjectAtIndex:0];
		
		if ([token isKindOfClass:[NSDecimalNumber class]])
		{
			[stack push:token];
		}
		else
		{
			id operator = [UnaryOperatorLookup objectForKey: token];
			if (operator != nil)
			{
				id op = [stack pop];
				if (op == nil)
					return nil;
				[stack push: [operator op:op]];
			}
            else
            {
                operator = [BinaryOperatorLookup objectForKey: token];
                if (operator != nil)
                {
                    id op1 = [stack pop];
                    id op2 = [stack pop];
                    
                    if ( (op1 == nil) || (op2 == nil))
                        return nil;
                    
                    [stack push: [operator op1:op1 op2:op2]];
                }
                else
                {
                    return nil;
                }
            }
		}
	}
	
	id returnObject = [stack pop];
	
	if ([stack count] != 0)
		return nil;
	
	return returnObject;
}


@end
