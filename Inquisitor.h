//
//  Inquisitor.h
//  SpiralBound
//
//  Created by Philip White on 2/17/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface Inquisitor : NSWindowController
{
	NSString *input;
	BOOL showTryAgain;
	BOOL showCancelButton;
	IBOutlet NSButton *cancelButton;
}

+ (id)sharedInquisitor;
- (BOOL)challenge;
- (BOOL)challengeWithoutCancel;
- (NSString *)input;
- (void)setInput:(NSString *)value;

- (BOOL)showTryAgain;
- (void)setShowTryAgain:(BOOL)value;
- (BOOL)showCancelButton;
- (void)setShowCancelButton:(BOOL)value;

- (IBAction)checkPassword:(id)sender;

- (BOOL)isChallenging;
- (void)stopChallenging;
@end
