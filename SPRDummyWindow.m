//
//  SPRDummyWindow.m
//  CommonSpiralbound
//
//  Created by Philip on 5/15/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import "SPRDummyWindow.h"


@implementation SPRDummyWindow

-(BOOL)canBecomeMainWindow
{
	return NO;
}

-(BOOL)canBecomeKeyWindow
{
	return NO;
}

@end
