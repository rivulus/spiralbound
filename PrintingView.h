//
//  PrintingView.h
//  SpiralBound
//
//  Created by Philip White on 2/13/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//an offscreen view that handles all printing

@interface PrintingView : NSView
{
	NSMutableArray *titles;
	NSMutableArray *textViews;
	BOOL thrifty;
	float pageHeight;
}


-(void)addPageContents:(NSAttributedString*)contents title:(NSString*)title;
-(void)finish;
-(void)expandForPageOfHeight:(float)deltaHeight;
-(void)minimizeTextView:(NSTextView*)view;
-(void)setThrifty:(BOOL)value;
- (NSData*)PDFData;
@end
