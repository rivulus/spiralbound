//
//  PageBrowser_SBPro.h
//  SpiralBound Pro
//
//  Created by Philip White on 5/5/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SPRTextView,SPRTodoListView, SPRPage;

@interface PageBrowser : NSObject
{
	IBOutlet NSView *browserView;
	SPRTextView *textView;
	SPRTodoListView *todoListView;
	
	IBOutlet NSScrollView *scrollView;
	
	IBOutlet NSBrowser *pagesView;
	NSArray *topLevelObjects;
	BOOL pagesColumnNeedsUpdate;
	BOOL notebooksColumnNeedsUpdate;
	//RemoteTextStorage *contentsViewStorage;
	
	SPRPage *selectedPage; //if a single page is selected
	
	//FILTERING
	IBOutlet NSSearchField *searchField;
	NSMutableString *filterValue;
	BOOL filterMatchWholeWordsOnly;
	BOOL filterMatchCase;
	BOOL filterMatchDiacriticals;
	
	
	NSMutableDictionary *filteredContents;
		//filtered contents is arranged as follows
		//keys are SPRNotebook titles. Some notebooks may be missing
		//values are arrays of page indices;
		//ergo, to get a page based on a Browser selection, get the title of the selected field from
		//column 1, use that as a key to this dictionary AND to get the correct organizer
		//use the value retrieved (an NSArray); index it with the index of the selection in column two
		//take that value, send it intValue, and use THAT index to get the correct page in the notebook.
	NSMutableDictionary *filterMatchRanges; //use the hash of the current page (as an NSNumber) to access the
											//NSArray containing the ranges
		
}

-(id)init;
-(NSView*)view;
-(BOOL)isSelected;
-(void)deleteSelectionWithWarning:(BOOL)warn;
-(NSBrowser*)pagesView;
-(SPRTextView*)textView;

//responsible for setting the textstorage and todo list and setting temporary highlights
-(void)setBackgroundColor:(NSColor*)color;
-(void)setSelectedPage:(SPRPage*)newSelection;
-(void)updateHighlights;

-(void)loadTodoListView;

-(void)createNewPageOfClass:(Class)c;
@end

/*
@interface SPRPageBrowserBrowser : NSBrowser
@end*/
