//
//  KeyRecordField.h
//  KeyStroker
//
//  Created by Philip White on 6/15/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#define KEYCODE_COUNT (128+70+5+1)

int ConvertKeyDownToKeyCode(NSEvent *event);
int ConvertFlagsChangedToKeyCode(NSEvent *event);
NSString *ConvertKeyCodeToString(int keyCode);

@interface KeyRecordField : NSBox
{
	//NSString *displayString;
	//int keyCode;
	BOOL enabled;
	BOOL acceptingFirstResponder;
	//NSTabViewItem *parentTabViewItem; 
}

- (int)keyCode;
- (void)setKeyCode:(int)value;

//this is a "quiet" non KVC version of setKeyCode
//it also sets displayString appropriately
- (void)replaceKeyCode:(int)value;
- (void)setEnabled:(BOOL)yn;
+ (BOOL)isKeyDownEqualToKeyCode:(NSEvent*)event;
+ (void)reset;
@end
