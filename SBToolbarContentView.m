//
//  SBToolbarContentView.m
//  SpiralBound
//
//  Created by Philip White on 3/15/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "SBToolbarContentView.h"


@implementation SBToolbarContentView

- (void)mouseDown:(NSEvent *)theEvent
{
	NSArray *subviews = [self subviews];
	NSEnumerator *enumerator = [subviews objectEnumerator];
	NSView *subview;
	NSPoint mouseLocation = [self convertPoint: [theEvent locationInWindow] fromView:nil];
	while (subview = [enumerator nextObject])
	{
		NSView *clickedView = [subview hitTest: mouseLocation];
		
		if (clickedView != nil)
		{
			[super mouseDown:theEvent];
			return;
		}
	}
	//otherwise
	[[[self window] contentView] mouseDown:theEvent];
}

- (BOOL)acceptsFirstMouse:(NSEvent *)theEvent
{
	return YES;
}

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)rect {
    // Drawing code here.
}

@end
