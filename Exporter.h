//
//  Exporter.h
//  SpiralBound Pro
//
//  Created by Philip White on 5/20/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SPRNotebook, SPRPage;
@interface Exporter : NSObject
{
	IBOutlet NSPopUpButton *fileTypeButton;
	IBOutlet NSBox *accessoryView;
	
	IBOutlet NSPanel *emailPanel;
	
	//NSString *to, *cc, *bcc, *from, *subject, *supplementaryText;
	NSNumber *attachmentFormat;
	NSNumber *emailType;
	NSNumber *emailClient;
	
}

+(Exporter*)sharedExporter;

-(void)showExportPanelForPage:(SPRPage*)page window:(NSWindow*)window;
-(IBAction)setFileType:(id)sender;

-(void)showSavePanelForNotebook:(SPRNotebook*)notebook window:(NSWindow*)window;

-(void)emailPage:(SPRPage*)page inWindow: (NSWindow*)window;
-(void)emailPageQuickly:(SPRPage*)page;
-(void)emailSheetDidEnd:(NSWindow*)sheet returnCode:(NSInteger)returnCode contextInfo:(void*)contextInfo;
-(IBAction)cancelEmail:(id)sender;
-(IBAction)createEmail:(id)sender;

- (NSNumber *)attachmentFormat;
- (void)setAttachmentFormat:(NSNumber *)value;

- (NSNumber *)emailType;
- (void)setEmailType:(NSNumber *)value;

- (NSNumber *)emailClient;
- (void)setEmailClient:(NSNumber *)value;

- (BOOL)appleMailExists;

- (BOOL)eudoraExists;

- (BOOL)entourageExists;

@end
