//
//  Calculator.h
//  SpiralBound
//
//  Created by Philip White on 2/11/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

extern NSDictionary *UnaryOperatorLookup;
extern NSDictionary *BinaryOperatorLookup;
extern NSCharacterSet *Numbers;
extern NSCharacterSet *BinaryOperators;

@interface Calculator : NSObject
{

}

+(Calculator*)sharedCalculator;

-(NSString*)calculateWithString:(NSString*)string allowOneTokenEquation:(BOOL)oneTokenOK; //return nil if invalid expression
-(NSDecimalNumber*)processSimplePostfix:(NSMutableArray*)postfix;

@end
