//
//  TornOffFrameView.m
//  SpiralBound
//
//  Created by Philip White on 2/2/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "TOWFrameView.h"
#import "SBConstants.h"
#import "SBController.h"
#import "SPROrganizer+Sorting.h"
#import "SPROrganizer+Actions.h"

static NSString *TornOffNibName = @"TornOff_SBPro";
static float SimpleStyleInvisibleHeight=20.0,
RegularStyleInvisibleHeight=13.0;

static NSString *DragbarImageName = @"TornOffAlpha";
static NSString *DragbarMaskImageName = @"TornOffColorMask";
static NSString *SimpleDragbarImageName = @"SimpleTOW";
static NSString *SimpleDragbarMaskImageName = @"SimpleTOWColorMask";

@implementation TOWFrameView


+ (void)initialize
{
	NSImage *image = [NSImage imageNamed:@"TornOffAlpha.tiff"];
	//register the requisite images
	[image setName:DragbarImageName];
	
	image = [NSImage imageNamed:@"TornOffColorMask.tiff"];
	[image setName:DragbarMaskImageName];
	
	image = [NSImage imageNamed:@"SimpleTOW.tiff"];
	[image setName:SimpleDragbarImageName];
	
	image = [NSImage imageNamed:@"SimpleTOWColorMask.tiff"];
	[image setName:SimpleDragbarMaskImageName];
}


- (void)drawRect:(NSRect)rect
{
	[super drawRect:rect];
	[self drawDividerLine]; //the line between the toolbar and the mainview
}


- (void)drawDividerLine
{
	
}


-(NSRect)titleBarRect
{
	NSRect rect;
	rect.origin.x=0;
	rect.origin.y=[self bounds].size.height-tornOffImageSize.height;
	rect.size.height = tornOffImageSize.height;
	rect.size.width = [self bounds].size.width;
	return rect;
}

- (NSRect)visibleToolbarAndDragbarRect
{
	NSRect unionRect = NSUnionRect([self toolbarRect], [self dragbarRect]);
	if (UseSimpleDragbar)
		unionRect.size.height -= SimpleStyleInvisibleHeight;
	else
		unionRect.size.height -= RegularStyleInvisibleHeight;
	
	unionRect.size.width = [self frame].size.width;
	
	return unionRect;
}

-(NSImage*)dragbarImage
{
	if (UseSimpleDragbar)
		return [NSImage imageNamed:SimpleDragbarImageName];
	else
		return [NSImage imageNamed:DragbarImageName];
	
}

-(NSImage*)dragbarGrayedOutImage
{
	if (UseSimpleDragbar)
		return [NSImage imageNamed:SimpleDragbarImageName];
	else
		return [NSImage imageNamed:DragbarImageName];
	
}

- (NSImage*)dragbarMaskImage
{
	if (UseSimpleDragbar)
		return [NSImage imageNamed:SimpleDragbarMaskImageName];
	else
		return [NSImage imageNamed:DragbarMaskImageName];
}

-(NSNib*)nib
{
	static NSNib *nib=nil;
	
	if (nib == nil)
		nib = [[NSNib alloc] initWithNibNamed: TornOffNibName bundle: nil];
	
	return nib;
}

- (NSRect)frameRectMinusBorder
{
	return [self bounds];
}

- (NSString *)formattedIndexOfCount
{
    return formattedIndexOfCount;
}

- (void)setFormattedIndexOfCount:(NSString *)value
{
    if (formattedIndexOfCount != value)
	{
        formattedIndexOfCount = [value copy];
    }
}

-(IBAction)makeTopPage:(id)sender
{
	[[self organizer] movePageToTop: (id)[self window]];
}

-(IBAction)makeFirstPage:(id)sender
{
	[[self organizer] movePageToBeginning: (id)[self window]];
}

-(IBAction)makeLastPage:(id)sender
{
	[[self organizer] movePageToEnd: (id)[self window]];
}

- (IBAction)returnPage:(id)sender
{
	[[self organizer] returnPageToStack:sender];
	//[[self] closeTornOffWindow: (id)[self window]];
}

- (void)breakBindings
{
	NSEnumerator *enumerator = [topLevelObjects objectEnumerator];
	id object;
	
	while (object = [enumerator nextObject])
	{
		if ([object isKindOfClass:[NSObjectController class]] == YES)
		{
			[object removeObject:self];
			return;
		}
	}
}

- (float)regularMinusSimpleDragbarHeight
{
	return [[NSImage imageNamed:DragbarMaskImageName] size].height - [[NSImage imageNamed:SimpleDragbarMaskImageName] size].height;
}



@end
