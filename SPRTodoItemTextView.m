//
//  SPRTodoItemTextView.m
//  CocoaJunk
//
//  Created by Philip on 5/20/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//


#import "SPRTodoItemTextView.h"
#import "SPRTodoListView.h"
#import "SPRTodoItemView.h"
#import "SPRTodoItemSampleView.h"
#import "SBConstants.h"
#import "SPRCalculatorHandler.h"

@implementation SPRTodoItemTextView

-(void)setParentList:(SPRTodoListView*)pl
{
	parentList = pl;
}

- (BOOL)becomeFirstResponder
{
	id parentTodoListView = [self parentTodoListView];
	
	if ([parentTodoListView isKindOfClass:[SPRTodoItemSampleView class]])
		return NO;
	
	//tell the parent list view to unhighlight all selections in other items
	[[self parentTodoListView] unselectOtherItems:[self parentTodoItemView]];
	return YES;
}

-(void)keyDown:(NSEvent*)event
{	
	if ([calculatorHandler keyDown:event inView:self])
		return;
	
	NSUInteger modifiers = [event modifierFlags];
	NSUInteger stripModifiers=0;
	
	//check for up or down arrows
	unichar character = [[event characters] characterAtIndex:0];
	switch (character)
	{
		case 25: //shift-tab
			[[self parentTodoListView] selectPrevious:self wrap:YES];
			return;
			break;
		
		case '\t': //tab
			if (modifiers&NSAlternateKeyMask)
			{
				stripModifiers|=NSAlternateKeyMask; //control continues below
			}
			else
			{
				[[self parentTodoListView] selectNext:self wrap:YES];
				return;
			}
			break;
		case '\r':
			[[self parentTodoListView] returnPressed:self];
			return;
			break;
	}
	
	//only check for up or down if the selection is appropriate
	NSArray *selections = [self selectedRanges];
	BOOL atFirst=NO, atLast=NO;
	
	if ([selections count] == 1)
	{
		NSRange r = [[selections objectAtIndex:0] rangeValue];
		if (r.length == 0)
		{
			if (r.location == 0 || (r.location == 1 && [[[self textStorage] string] hasPrefix:SPRInvisibleAttributeHolder]))
				atFirst=YES;
			if (r.location == [[self textStorage] length])
				atLast=YES;
		}
	}
	
	if (atLast || atFirst)
	{
		switch (character)
		{
			case NSUpArrowFunctionKey:
				if (atFirst)
				{
					[[self parentTodoListView] selectPrevious:self wrap:NO];
					return;
				}
				break;
				
			case NSDownArrowFunctionKey:
				if (atLast)
				{
					[[self parentTodoListView] selectNext:self wrap:NO];
					return;
				}
				break;
			
			case 127: //backspace
				if (atFirst)
				{
					if ([[self parentTodoListView] backspacePressed:self])
						return;
				}
					
		}
	}
	
	NSEvent *newEvent;
	if (stripModifiers != 0)
	{
		newEvent = [NSEvent keyEventWithType:[event type]
									location:[event locationInWindow]
							   modifierFlags:modifiers^stripModifiers
								   timestamp:[event timestamp]
								windowNumber:[event windowNumber]
									 context:[event context]
								  characters:[event characters]
				 charactersIgnoringModifiers:[event charactersIgnoringModifiers]
								   isARepeat:[event isARepeat]
									 keyCode:[event keyCode]];
	}
	else
	{
		newEvent = event;
	}
	
	[super keyDownSkipCalculator:newEvent];
}


@end
