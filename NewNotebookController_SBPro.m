//
//  NewNotebookController.m
//  SpiralBound Pro
//
//  Created by Philip White on 5/4/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "NewNotebookController_SBPro.h"
#import "ColorButtonCell.h"
#import "SPRNotebook.h"
#import "SPROrganizer.h"
#import "SBConstants.h"

#define USER_DID_CANCEL 0x0F0F
#define USER_DID_CLICK_CREATE 0x0F0A

static NewNotebookController *SharedController = nil;
static NSString *NormalTitle = @"New Notepad",
				*ImportFromStickiesTitle = @"Import from Stickies",
				*ImportFromSpiralBoundTitle = @"Import from SpiralBound",
				*ImportSavedNotebookTitle = @"Import Saved Notepad";

extern NSString *SPRLArchiveExtension;

//import types
enum
{
	SPRL_TYPE,
	OLD_STYLE_TYPE
};

@implementation NewNotebookController

+(NewNotebookController*)sharedController
{
	if (SharedController == nil)
	{
		SharedController = [[self alloc] initWithWindowNibName:@"NewNotebook"];
	}
	return SharedController;
}

-(void)windowDidLoad
{
	[[NSColorPanel sharedColorPanel] setShowsAlpha:YES];
	[colorWell setColor:SBYellow];
	[super windowDidLoad];
}

-(void)importSavedPagesFromFile:(NSString*)path
{
	NSData *data = [NSData dataWithContentsOfFile: path];
	
	/*
	Previously, the version number was stored in the last 6 bytes of the archive. This was never used
	and is now removed. This uses exceptions to try to unarchive the archive by trial and error.
	 */
	
	//first try unarchiving as is.
	[SPRNotebook beginDecodingWithUniqueHashes];
	id obj;
	BOOL tryAgain=NO, failedToUnarchive=NO;
	@try
	{
		obj = [NSKeyedUnarchiver unarchiveObjectWithData:data];
	}
	@catch (NSException * e) 
	{
		tryAgain=YES;
	}
	
	if (tryAgain)
	{
		@try
		{
			obj = [NSKeyedUnarchiver unarchiveObjectWithData:[data subdataWithRange:NSMakeRange(0,[data length]-6)]];
		}
		@catch (NSException * e)
		{
			failedToUnarchive=YES;
		}
	}
	
	[SPRNotebook endDecodingWithUniqueHashes];
	
	if (obj == nil || failedToUnarchive)
	{
		NSRunAlertPanel(@"Error opening file.", @"The file \"%@\" does not appear to contain SpiralBound data", @"OK", nil, nil, path);
		return;
	}
	
	
	relevantObject = obj;
	
	NSString *title = [[path stringByDeletingPathExtension] lastPathComponent];
	
	[self showAsImportSavedNotebookWithTitle:title];
}

- (void)importSavedNotebook
{
	[self window];//load the nib

	NSOpenPanel *openPanel = [NSOpenPanel openPanel];
	[openPanel setCanChooseDirectories:NO];
	[openPanel setAllowedFileTypes:[NSArray arrayWithObject:SPRLArchiveExtension]];
	[openPanel setMessage:@"Import a notepad saved by another copy of SpiralBound or SpiralBound Pro"];
	[openPanel setAccessoryView:importAccessoryView];
	
	[openPanel beginWithCompletionHandler:^(NSInteger result) {
        if (result == NSFileHandlingPanelOKButton)
        {
            NSAssert([[openPanel URLs] count] >= 1, @"Fewer than one item selected in NSSavePanel");
            for (NSURL *url in [openPanel URLs])
            {
                [self importSavedPagesFromFile:[url path]];
            }
        }}];
    }

-(IBAction)setImportFileType:(id)sender
{
    NSAssert([[sender window] isKindOfClass:[NSOpenPanel class]],@"setImportFileType: invalid sender");
    NSOpenPanel *openPanel = (NSOpenPanel*)[sender window];
    if ([importFileTypeButton indexOfSelectedItem] == OLD_STYLE_TYPE)
    {
        [openPanel setAllowedFileTypes:nil];
    }
    else
    {
        [openPanel setAllowedFileTypes:[NSArray arrayWithObject:SPRLArchiveExtension]];
    }
}

-(IBAction)showImportTypeHelp:(id)sender
{
	NSRunAlertPanel(@"About File Types", @"Versions 1.1.1 and earlier of SpiralBound and versions 1.1.4 and earlier of SpiralBound Pro exported notepads as files without extensions. Choose \"Old Style SpiralBound Archive\" to show files without extensions. These files may or may not be valid SpiralBound archives.", @"OK", nil, nil);
}

- (IBAction)showWindow:(id)sender
{
	NSWindow *myWindow = [self window];
	
	//if (relevantObject != nil)
	//	[relevantObject release];
	
	[myWindow setTitle:NormalTitle];
	windowState = NewNotebookState;
	
	[nameNotUniqueField setHidden:YES];
	
	if ([myWindow isVisible])
	{
		[nameField selectText:nil];
	}
	else
	{
		[nameField setStringValue:@""];
		[nameField selectText:nil];
	}
	[myWindow center];
	[super showWindow:sender];
}

-(void)showAsImportFromSpiralBound
{
	NSWindow *myWindow = [self window];
	
	//if (relevantObject != nil)
	//	[relevantObject release];
	
	[myWindow setTitle:ImportFromSpiralBoundTitle];
	windowState = ImportFromSpiralBoundState;
	
	[nameNotUniqueField setHidden:YES];
	
	if ([myWindow isVisible])
	{
		[nameField selectText:nil];
	}
	else
	{
		[nameField setStringValue:@""];
		[nameField selectText:nil];
	}
	
	[myWindow center];
	[super showWindow:nil];
}


-(void)showAsImportFromStickies
{
	NSWindow *myWindow = [self window];
	
	//if (relevantObject != nil)
	//	[relevantObject release];
	
	[myWindow setTitle:ImportFromStickiesTitle];
	windowState = ImportFromStickiesState;
	
	[nameNotUniqueField setHidden:YES];
	
	if ([myWindow isVisible])
	{
		[nameField selectText:nil];
	}
	else
	{
		[nameField setStringValue:@""];
		[nameField selectText:nil];
	}
	
	[myWindow center];
	[super showWindow:nil];
}

-(void)showAsImportSavedNotebookWithTitle: (NSString*)defaultTitle
{
	NSWindow *myWindow = [self window];
	
	//if (relevantObject != nil)
	//	[relevantObject release];
	
	[myWindow setTitle:ImportSavedNotebookTitle];
	windowState = ImportSavedNotebookState;
	
	[nameNotUniqueField setHidden:YES];
	
	[nameField setStringValue:defaultTitle];
	[nameField selectText:nil];
	
	[myWindow center];
	[super showWindow:nil];
}

-(IBAction)cancelButton:(id)sender
{
	if (windowState == ImportSavedNotebookState)
	{
		relevantObject=nil;
	}
	
	[self close];
}

-(IBAction)createButton:(id)sender
{
	NSString *title = [nameField stringValue];
	
	BOOL uniqueTitle = [SPRNotebook isTitleAvailable:title];
	
	if (uniqueTitle == NO)
	{
		[nameNotUniqueField setStringValue:@"Name Not Unique"];
		[nameNotUniqueField setHidden:NO];
		[nameField selectText:nil];
		NSBeep();
	}
	else if ([[title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0)
	{
		[nameNotUniqueField setStringValue:@"Invalid Name"];
		[nameNotUniqueField setHidden:NO];
		[nameField selectText:nil];
		NSBeep();
	}
	else
	{
		[self close];
		
		NSColor *color = [colorWell color];
		SPROrganizer *nbo=nil;
		
		if (windowState == NewNotebookState)
		{
			nbo=[SPROrganizer makeNewNotebookWithTitle:title color:color];
		}
		else if (windowState == ImportFromStickiesState)
		{
			nbo=[SPROrganizer makeNewNotebookFromStickiesWithTitle:title color:color];
		}
		else if (windowState == ImportFromSpiralBoundState)
		{
			nbo=[SPROrganizer makeNewNotebookFromSpiralBoundWithTitle:title color:color];
		}
		else if (windowState == ImportSavedNotebookState)
		{
			nbo=[SPROrganizer makeNewNotebookWithObject:relevantObject title: title color: color];
			relevantObject = nil;
		}
		
		[[nbo mainWindow] makeKeyAndOrderFront:nil];
        [SPROrganizer saveOrganizers];
	}
}

-(IBAction)setColor:(id)sender
{
	NSColor *color = [(ColorButtonCell*)[sender cell] color];
	
	[colorWell setColor:color];
}




@end
