//
//  NotebookInfo.m
//  SpiralBound Pro
//
//  Created by Philip White on 4/29/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "SPROrganizer.h"
#import "SPROrganizer+Sorting.h"
#import "SPROrganizer+Actions.h"
#import "SPRNotebook.h"
#import "SPRPage.h"
#import "SPRPlainPage.h"
#import "SPRTodoPage.h"
#import "SPRTodoListView.h"
#import "PageSorter_SBPro.h"
#import "SBConstants.h"
#import "SBController.h"
#import "SpiralBoundWindow.h"
#import "SBProtoFrameView.h"
#import "SPRTextView.h"
#import "SPRPreferencesController.h"
#import "RIVTools.h"
#import "NotificationNames.h"
#import "SPRLockableTextStorage.h"

static const int HideMenuItemTag=1;

#define USE_WINDOW_INFO_STRUCTS 0
#define MAKE_INTRO_PAGES 1

static WindowInfo DefaultWindowInfoForNotebook(SPRNotebook *notebook);
static NSArray *IntroWindowInfoArrayForNotebook(SPRNotebook *notebook);
static NSArray *DefaultWindowInfoArrayForNotebook(SPRNotebook *notebook);
//void MoveRectOntoScreen(NSRect *rect);

static NSMutableDictionary *WindowsZOrder=nil;

//extern NSString *DataFolderPath; //this is in SPRNotebook.m
NSString *DataFileName = @"/Organizer Data";

NSMutableArray *Organizers=nil;

@implementation SPROrganizer

+(void)performPreSandboxManualUpgradeIfNecessary
{
	NSString *oldDataFolderPath = [[SPRPreferencesController sharedPreferencesController] dataFolderPath];
	NSString *newDataFolderPath = [SPRPreferencesController defaultDataFolderPath];
	
	if (![oldDataFolderPath isEqualToString: newDataFolderPath])
	{
		NSRunAlertPanel(@"Permission Needed to Proceed", @"Because your SpiralBound Pro data was stored in a custom location, you will need to give SpiralBound Pro explicit permission to move the data into its new SandBoxed location (%@). In the next window, the old data folder should already be selected. Please click \"Grant Permission\" in that window.", nil, nil, nil, oldDataFolderPath, newDataFolderPath);
		
		NSOpenPanel *openPanel = [NSOpenPanel openPanel];
		[openPanel setDirectoryURL:[NSURL fileURLWithPath:oldDataFolderPath]];
		[openPanel setCanChooseFiles:NO];
		[openPanel setCanChooseDirectories:YES];
		[openPanel setMessage:[NSString stringWithFormat:@"Please click Grant Permission.", newDataFolderPath]];
		[openPanel setPrompt:@"Grant Permission"];
		
		if ([openPanel runModal] == NSModalResponseOK && [[[openPanel URL] path] isEqualToString:oldDataFolderPath])
		{
			NSURL *urlWithPermission = [openPanel URL];
			
			NSFileManager *fileManager = [NSFileManager defaultManager];
			BOOL success = [fileManager moveItemAtURL:urlWithPermission toURL:[NSURL fileURLWithPath:newDataFolderPath] error:NULL];
			
			if (success == NO)
			{
				NSRunAlertPanel(@"Error Moving Old Data", @"SpiralBound Pro was unable to move data from the old location (%@) into SpiralBound Pro's SandBox (%@). Please copy the files manually using Finder or import the notepad individually using \"Import Notepad\".", nil, nil, nil, oldDataFolderPath, newDataFolderPath);
			}
		}
		else
		{
			NSRunAlertPanel(@"Not Moving Old Data", @"You have chosen not to move data from the old location (%@) into SpiralBound Pro's SandBox (%@). Please copy the files manually using Finder or import the notepad individually using \"Import Notepad\".", nil, nil, nil, oldDataFolderPath, newDataFolderPath);
		}
		[[SPRPreferencesController sharedPreferencesController] resetDataFolderPath];
	}
}

+(NSArray*)organizers
{
	if (Organizers == nil)
	{
		// Upgrade scenario from Pre-SandBox Custom data location
		[self performPreSandboxManualUpgradeIfNecessary];
		
		//check for an organizer data file
		NSFileManager *fileManager = [NSFileManager defaultManager];
		NSString *dataFilePath = [[[SPRPreferencesController sharedPreferencesController] dataFolderPath] stringByAppendingString: DataFileName];
		
		if ([fileManager fileExistsAtPath:dataFilePath])
		{
			WindowsZOrder = [[NSMutableDictionary alloc] init];
			
			Organizers = [NSKeyedUnarchiver unarchiveObjectWithFile:dataFilePath];
			
			if (Organizers == nil)
				Organizers = [[NSMutableArray alloc] init];
			
			//the static variable WindowsZOrder should now contain a dictionary of relative window depths with window numbers  as keys
			NSArray *sortedKeys = [WindowsZOrder keysSortedByValueUsingSelector:@selector(compare:)]; 
			
			//we actually don't need the dictionary now that we have the keys
			NSEnumerator *enumerator = [sortedKeys reverseObjectEnumerator];
			NSNumber *windowNumber;
			while (windowNumber = [enumerator nextObject])
			{
				NSWindow *window = [NSApp windowWithWindowNumber:[windowNumber intValue]];
				[window orderWindow:NSWindowAbove relativeTo:0];
			}
			
			WindowsZOrder=nil;
			
			NSArray *extraNotebooks = [SPRNotebook loadUnloadedNotebooksWithUniqueHashes];
			if ([extraNotebooks count] != 0) //notify the user of extra notebooks, probably from the
											 //rename bug in 1.0
			{
				NSMutableString *titles = [NSMutableString string];
				NSEnumerator *enumerator = [extraNotebooks objectEnumerator];
				SPRNotebook *nb;
				
				while (nb = [enumerator nextObject])
				{
					[titles appendFormat:@"%@\n", [nb title]];
				}
				
				int result = NSRunAlertPanel(@"Extra Notepads found", @"Some notepads have been found that probably exist due to a bug in SpiralBound Pro 1.0. In that version, when renaming a notepad, an extra data file with the original name would remain. The following notepads have been found that may be unwanted: \n%@It is recommended that you use the notepad sorter to review these and delete them if desired. If you are certain that you do not want them you may delete them immediately.", @"Open Sorter", @"Continue", @"Delete Immediately", titles);
			
				switch (result)
				{
					case NSAlertOtherReturn:
						[SPRNotebook deleteNotebooks:extraNotebooks];
						break;	
					
					case NSAlertDefaultReturn:
						[[PageSorter sharedSorter] performSelector:@selector(showWindow:) withObject:nil afterDelay: 0.1];
						//continue to default behavior
					
					default:
						{
							//load the new organizers
							NSEnumerator *enumerator = [extraNotebooks objectEnumerator];
							SPRNotebook *nb;
							NSPoint cascadePoint = NSZeroPoint;
							while (nb = [enumerator nextObject])
							{
								SPROrganizer *org = [self makeNewOrganizerWithNotebook:nb color:SBYellow];
								NSWindow *orgWindow = [org mainWindow];
								cascadePoint = [orgWindow cascadeTopLeftFromPoint:cascadePoint];
							}
						}
				}
			}
		}
		
		if (Organizers == nil || [Organizers count] == 0)
		{
			//create anew
			if (Organizers == nil)
				Organizers = [[NSMutableArray alloc] init];
			
			//see if there are any data files
			if ([SPRNotebook notebookDataExists])
			{
				NSEnumerator *enumerator = [[SPRNotebook loadAllNotebooksWithUniqueHashes] objectEnumerator];
				SPRNotebook *notebook;
				while (notebook = [enumerator nextObject])
				{
					SPROrganizer *organizer = [[self alloc] initWithNotebook: notebook color:SBYellow];
					[Organizers addObject: organizer];
				}
				
				//now cascade the windows
				enumerator = [[NSApp windows] objectEnumerator];
				NSPoint point = NSZeroPoint;
				NSWindow *window;
				while (window = [enumerator nextObject])
					point = [window cascadeTopLeftFromPoint:point];
			}
			else //make a new notebook
			{
			#if MAKE_INTRO_PAGES
				SPRNotebook *notebook = [SPRNotebook createNotebookWithIntroText];
				SPROrganizer *organizer = [[self alloc] initWithNotebook: notebook color:SBGreen introPages:YES];
				[Organizers addObject: organizer];
				
				//the windows are not visible now because the IntroWindowInfo… function assigns zorder numbers
				//this prevents makeWindowsForWindowInfoArray from moving the windows on screen
				//instead it store their position in the WindowZOrder dictionary, which at this point was never
				//allocated, but is safe because it is set to nil
				[[organizer mainWindow] orderFront:nil];
				[[[organizer tornOffWindows] objectAtIndex:0] orderFront:nil];
				[[[organizer tornOffWindows] objectAtIndex:1] makeKeyAndOrderFront:nil];
			#else
				SPRNotebook *notebook = [SPRNotebook createNotebookWithTitle:
									  [NSString stringWithFormat: @"%@'s Notepad", NSFullUserName()]];
				SPROrganizer *organizer = [[self alloc] initWithNotebook: notebook color:SBYellow];
				[Organizers addObject: organizer];
				[organizer release];
			#endif
			}
			
		}
		[[NSNotificationQueue defaultQueue] enqueueNotification:
		 [NSNotification notificationWithName:NotebookCountDidChangeNotification																																			object:nil]
												   postingStyle:NSPostASAP];
	
		NSEnumerator *enumerator = [Organizers objectEnumerator];
		SPROrganizer *obj;
		while (obj=[enumerator nextObject])
		{
			[[NSNotificationQueue defaultQueue]
			 enqueueNotification:[NSNotification notificationWithName:PageArrangementDidChangeNotification object:[obj notebook]]
			 postingStyle:NSPostASAP];
		}
	}
	
	
	return Organizers;
}

+(void)reloadOrganizers
{
    Organizers = nil;
	[self organizers]; 
}

+(NSArray*)menuItemsForOrganizers
{
	NSMutableArray *menuItems = [NSMutableArray arrayWithCapacity:[Organizers count]];
	SPROrganizer *organizer;
	NSUInteger count = [Organizers count],i;
	
	NSRect imageRect = NSMakeRect(0,0,16.0,16.0);
	NSBezierPath *borderPath = [NSBezierPath bezierPathWithRect:imageRect];
	NSColor *borderColor = [NSColor blackColor];
	
	for (i=0; i<count; i++)
	{
		organizer=[Organizers objectAtIndex:i];
		
		NSString *title = [[organizer notebook] title];
		NSString *keyEquivalent = [NSString stringWithFormat: @"%lu", i+1];
		NSMenuItem *newItem = [[NSMenuItem alloc] initWithTitle:title action:@selector(raise:) keyEquivalent:keyEquivalent];
		[newItem setTarget: organizer];
		
		//make the color image
		NSImage *image = [[NSImage alloc] initWithSize:imageRect.size];
		[image lockFocus];
		{
			[[organizer color] set];
			NSRectFill(imageRect);
			
			[borderColor set];
			[borderPath stroke];
		}
		[image unlockFocus];
		[newItem setImage: image];
		
		NSAttributedString *attrTitle;
		
		if ([organizer isHidden])
		{
			NSDictionary *attrs = [NSDictionary dictionaryWithObject:
												[[NSFontManager sharedFontManager] convertFont: [NSFont fontWithName:@"Helvetica" size:14.0]
																				   toHaveTrait: NSItalicFontMask]
															  forKey:NSFontAttributeName];
															  
			attrTitle = [[NSAttributedString alloc] initWithString: title attributes: attrs];
			[newItem setAttributedTitle:attrTitle];
		}
		
		if ([organizer isKey])
		{
			NSDictionary *attrs = [NSDictionary dictionaryWithObject:
								   [[NSFontManager sharedFontManager] convertFont: [NSFont menuFontOfSize:14.0]
																	  toHaveTrait: NSBoldFontMask]
															  forKey:NSFontAttributeName];
			
			attrTitle = [[NSAttributedString alloc] initWithString: title attributes: attrs];
			[newItem setAttributedTitle:attrTitle];
		}
		
		[menuItems addObject:newItem];
	}
	return menuItems;
}


+(NSArray*)notebookTitles
{
	NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity: [Organizers count]];
	
	NSEnumerator *enumerator = [Organizers objectEnumerator];
	SPROrganizer *organizer;
	
	while (organizer = [enumerator nextObject])
	{
		[array addObject:[[organizer notebook] title]];
	}
	return array;
}

+(NSString*)titleOfNotebookAtIndex:(NSUInteger)index
{
	if (index >= [Organizers count])
	{
		NSLog(@"+titleOfNotebookAtIndex was passed an invalid index (%u)", index);
		return nil;
	}
	return [[[Organizers objectAtIndex:index] notebook] title];
}

+(SPROrganizer*)makeNewNotebookWithTitle:(NSString*)title color:(NSColor*)color
{
	SPRNotebook *newNotebook = [SPRNotebook createNotebookWithTitle:title];
	SPROrganizer *organizer = [[self alloc] initWithNotebook:newNotebook color:color];
	
	if (organizer != nil)
		[Organizers addObject:organizer];
	else
	{
		NSLog(@"Failed to allocate SPROrganizer");
		return nil;
	}
	
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:NotebookCountDidChangeNotification																																			object:newNotebook]
											   postingStyle:NSPostASAP];
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:NotebookDidChangeLengthNotification																																			object:newNotebook]
											   postingStyle:NSPostASAP];
	
	return organizer;
}

+(SPROrganizer*)makeNewNotebookFromStickiesWithTitle:(NSString*)title color:(NSColor*)color
{
	SPRNotebook *newNotebook = [SPRNotebook createNotebookWithStickiesDataAndTitle:title];
	SPROrganizer *organizer = [[self alloc] initWithNotebook:newNotebook color:color];
	
	if (organizer != nil)
		[Organizers addObject:organizer];
	else
	{
		NSLog(@"Failed to allocate SPROrganizer");
		return nil;
	}
	
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:NotebookCountDidChangeNotification																																			object:newNotebook]
											   postingStyle:NSPostASAP];
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:NotebookDidChangeLengthNotification																																			object:newNotebook]
											   postingStyle:NSPostASAP];
	
	return organizer;
}

+(SPROrganizer*)makeNewNotebookFromSpiralBoundWithTitle:(NSString*)title color:(NSColor*)color
{
	SPRNotebook *newNotebook = [SPRNotebook createNotebookWithOriginalSBDataAndTitle:title];
	SPROrganizer *organizer = [[self alloc] initWithNotebook:newNotebook color:color];
	
	if (organizer != nil)
		[Organizers addObject:organizer];
	else
	{
		NSLog(@"Failed to allocate SPROrganizer");
		return nil;
	}
	
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:NotebookCountDidChangeNotification																																			object:newNotebook]
											   postingStyle:NSPostASAP];
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:NotebookDidChangeLengthNotification																																			object:newNotebook]
											   postingStyle:NSPostASAP];
	
	return organizer;
}

+(SPROrganizer*)makeNewNotebookWithObject:(id)obj title:(NSString*)title color:(NSColor*)color
{
	SPRNotebook *newNotebook = [SPRNotebook createNotebookWithObject:obj title:title];
	SPROrganizer *organizer = [[self alloc] initWithNotebook:newNotebook color:color];
	
    NSAssert(newNotebook,@"Failed to create notepad");
    NSAssert(organizer,@"Failed to create organizer");
	
    [Organizers addObject:organizer];
    
	
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:NotebookCountDidChangeNotification																																			object:newNotebook]
											   postingStyle:NSPostASAP];
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:NotebookDidChangeLengthNotification																																			object:newNotebook]
											   postingStyle:NSPostASAP];
	
	return organizer;
}

+(SPROrganizer*)makeNewOrganizerWithNotebook:(SPRNotebook*)newNotebook color:(NSColor*)color
{
	SPROrganizer *organizer = [[self alloc] initWithNotebook:newNotebook color:color];
	
	if (organizer != nil)
		[Organizers addObject:organizer];
	else
	{
		NSLog(@"Failed to allocate SPROrganizer");
		return nil;
	}
	
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:NotebookCountDidChangeNotification																																			object:newNotebook]
											   postingStyle:NSPostASAP];
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:NotebookDidChangeLengthNotification																																			object:newNotebook]
											   postingStyle:NSPostASAP];
	
	return organizer;
}

+(void)saveOrganizers
{
#ifdef DEBUG
//	return;
#endif	
	
	[SPRNotebook saveData];
	
	//fill the static variable WindowsZOrder with a dictionary of relative window levels with window numbers as key
	NSInteger count;
	NSCountWindows(&count);
	NSInteger *windowNumbers = malloc(count*sizeof(NSInteger));
	NSWindowList(count,windowNumbers);
	WindowsZOrder = [[NSMutableDictionary alloc] initWithCapacity:count];
	int i;
	for (i=0;i<count;i++)
		[WindowsZOrder setObject:[NSNumber numberWithInt:i]
						  forKey:[NSNumber numberWithInt:windowNumbers[i]]];
		
	
	[NSKeyedArchiver archiveRootObject:Organizers toFile:[[[SPRPreferencesController sharedPreferencesController] dataFolderPath] stringByAppendingString: DataFileName]];
	WindowsZOrder=nil;
}



+(SPROrganizer*)organizerForWindow:(NSWindow*)window
{
	NSEnumerator *enumerator = [[self organizers] objectEnumerator];
	SPROrganizer *organizer;
	
	while (organizer = [enumerator nextObject])
	{
		if ([organizer mainWindow] == window || [[organizer tornOffWindows] containsObject:window])
			return organizer;
	}
	
	return nil;
}

+(SPROrganizer*)organizerForTitle:(NSString*)title
{
	NSEnumerator *enumerator = [[self organizers] objectEnumerator];
	SPROrganizer *organizer;
	
	while (organizer = [enumerator nextObject])
	{
		if ([[organizer title] isEqualToString: title])
			return organizer;
	}
	
	return nil;
}


+(SPROrganizer*)organizerForPage:(SPRPage*)page
{
	SPRNotebook *notebook = [SPRNotebook notebookContainingPage:page];
	
	NSEnumerator *enumerator = [[self organizers] objectEnumerator];
	SPROrganizer *organizer;
	
	while (organizer = [enumerator nextObject])
	{
		if ([organizer notebook] == notebook)
			return organizer;
	}
	return nil;
}


+(SPROrganizer*)organizerForNotebook:(SPRNotebook*)notebook
{
	NSEnumerator *enumerator = [[self organizers] objectEnumerator];
	SPROrganizer *organizer;
	
	while (organizer = [enumerator nextObject])
	{
		if ([organizer notebook] == notebook)
			return organizer;
	}
	return nil;
}

+(SPRNotebook*)notebookForWindow:(NSWindow*)window
{
	return [[self organizerForWindow:window] notebook];
}

+ (SpiralBoundWindow*)loadWindowWithFrame:(NSRect)frame page:(SPRPage*)page style:(SBWindowStyle)style  organizer:(SPROrganizer*)organizer
{
	SpiralBoundWindow *newWindow = [[SpiralBoundWindow alloc] initWithContentRect:frame
																			style:style
																		organizer:organizer];
	NSSize contentSize = [(id)[newWindow mainView] contentSize];
	
	SPRTodoListView *todoListView = nil;
	BOOL todo = [page isKindOfClass:[SPRTodoPage class]];
	
	if (style == SBSpiralBoundStyle || !todo) //the main page always has a text view; a torn off may or may not
	{
		SPRTextView *textView = [[SPRTextView alloc] initWithFrame:NSMakeRect(0,0,contentSize.width,contentSize.height)];
		if (!todo)
			[textView replaceTextStorage:[page contentText]];
		[textView setVerticallyResizable:YES];
		[textView setHorizontallyResizable:NO];
		
		[textView setAutoresizingMask:NSViewWidthSizable];
		[newWindow setMainTextView: textView];
	}
	
	if (style == SBSpiralBoundStyle || todo)
	{
		todoListView = [[SPRTodoListView alloc] initWithFrame: NSMakeRect(0,0,contentSize.width,contentSize.height)];
		[todoListView setDelegate:organizer];
		[todoListView setAutoresizingMask:NSViewWidthSizable];
		
		[newWindow setMainTodoListView:todoListView];
		
		
	}
	
	if (todo)
	{
		[newWindow showMainTodoListView];
		for (NSDictionary *listItem in [(id)page items])
		{
			[todoListView addTextStorage:[listItem objectForKey:TextStorageKey]
								   state:[[listItem objectForKey: StateKey] boolValue]];
		}
	}
	else
	{
		[newWindow showMainTextView];
	}
	//set the title
	[newWindow setTitle: [page title]];
	
	//set the security
	[newWindow setSecurityButtonState: ([page isSecure] ? NSOnState : NSOffState)];
	
	return newWindow;
}

+(void)showAll
{
	NSEnumerator *enumerator = [Organizers objectEnumerator];
	SPROrganizer *org;
	while (org = [enumerator nextObject])
	{
		if ([org isHidden])
			[Organizers makeObjectsPerformSelector:@selector(raiseNotebook)];
	}
}

+(void)deleteNotebookWithOrganizer:(SPROrganizer*)organizer
{
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:NotebookWillBeDeletedNotification																																			object:[organizer notebook]]
											   postingStyle:NSPostNow];
	
	[organizer closeWindows];
	[SPRNotebook deleteNotebook:[organizer notebook]];
	[Organizers removeObject: organizer];
	
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:NotebookCountDidChangeNotification																																			object:nil]
											   postingStyle:NSPostASAP];
}

+(SPROrganizer*)keyOrganizer
{
	NSEnumerator *enumerator = [Organizers objectEnumerator];
	SPROrganizer *organizer;
	while (organizer = [enumerator nextObject])
	{
		if ([organizer isKey])
			return organizer;
	}
	return nil;
}

+(void)hideNonkeyNotebooks
{
	SPROrganizer *keyOrg = [self keyOrganizer];
	
	for (SPROrganizer *org in [self organizers])
	{
		if (org != keyOrg)
		{
			[org hideAll:nil];
		}
	}
}

//this first initializer will be called in the event that there is no saved Organizer data
-(id)initWithNotebook: (SPRNotebook*)newNotebook color:(NSColor*)newColor
{
	return [self initWithNotebook:newNotebook color:newColor introPages:NO];
}

-(id)initWithNotebook: (SPRNotebook*)newNotebook color:(NSColor*)newColor introPages:(BOOL)introPages
{
	self = [super init];
	
	if (self != nil)
	{
		color = newColor;
		
		notebook = newNotebook;
		
		[self registerForNotifications];
		
		tornOffWindows = [[NSMutableArray alloc] init];
		tornOffPages = [[NSMutableArray alloc] init];
	
	#if !USE_WINDOW_INFO_STRUCTS
		NSArray *windowInfo;
		if (introPages)
			windowInfo = IntroWindowInfoArrayForNotebook(notebook);
		else
			windowInfo = DefaultWindowInfoArrayForNotebook(notebook);
		[self makeWindowsWithWindowInfoArray:windowInfo];
	#else
		WindowInfo windowInfo = DefaultWindowInfoForNotebook(notebook);
		
		[self makeWindowsWithWindowInfos:&windowInfo count:1];
	#endif
	
		//set the title field in all of the windows
		{
		
			NSString *title = [notebook title];
			[(SBProtoFrameView*)[mainWindow contentView] setNotebookTitleFieldStringValue: title];
			
			
			NSEnumerator *enumerator = [tornOffWindows objectEnumerator];
			SpiralBoundWindow *window;
			
			while (window = [enumerator nextObject])
			{
				[(SBProtoFrameView*)[window contentView] setNotebookTitleFieldStringValue: title];
			}
		}
	}
	return self;
}


-(id)initWithCoder:(NSCoder *)decoder
{
	self = [super init];
	
	if (self != nil)
	{
		color = [decoder decodeObjectForKey:@"Color"];
		hidden = [decoder decodeBoolForKey:@"Hidden"];
		
		NSString *title = [decoder decodeObjectForKey:@"NotebookTitle"];
		
		notebook = [SPRNotebook notebookWithTitle: title];
	
		//determine whether to use old style struct encoding or new-style NSDictionary encoding
		BOOL newStyleEncoding = [decoder containsValueForKey:@"WindowInfos"];
	
		if (newStyleEncoding)
		{
			if (notebook == nil)
			{
				NSRunAlertPanel(@"Error Loading Data", @"There appears to have been some data loss associated with the Notepad \"%@\"", nil, nil, nil, title);
				SPRNotebook *newNotebook = [SPRNotebook createNotebookWithTitle:title];
				SPROrganizer *organizer = [[SPROrganizer alloc] initWithNotebook:newNotebook color:color];
				
				if (organizer == nil)
					return nil;
				
				self = organizer;
				
				return self;
			}
			
			[self registerForNotifications];
			
			tornOffWindows = [[NSMutableArray alloc] init];
			tornOffPages = [[NSMutableArray alloc] init];
			
			
			NSArray *windowInfos = [decoder decodeObjectForKey:@"WindowInfos"];
			
			[self makeWindowsWithWindowInfoArray:windowInfos];
		}
		else
		{
			NSUInteger tornOffWindowCount;
			NSUInteger byteCount;
			WindowInfo temp; //used below if mainWindowInfo is nil
			WindowInfo *mainWindowInfo;
			WindowInfo *tornOffWindowInfos;
			
			if (notebook == nil)
			{
				//really in SB original the error should be handled in SPRNotebook
				NSRunAlertPanel(@"Error Loading Data", @"There appears to have been some data loss associated with the Notepad \"%@\"", @"OK", nil, nil, title);
				SPRNotebook *newNotebook = [SPRNotebook createNotebookWithTitle:title];
				SPROrganizer *organizer = [[SPROrganizer alloc] initWithNotebook:newNotebook color:color];
				
				if (organizer == nil)
					return nil;
				
				self = organizer;
				
				return self;
			}
			else
			{
				mainWindowInfo = (WindowInfo*)[decoder decodeBytesForKey:@"MainWindowInfo" returnedLength:&byteCount];
				tornOffWindowInfos = (WindowInfo*)[decoder decodeBytesForKey:@"TornOffWindowInfos" returnedLength:&byteCount];
			}
			
			[self registerForNotifications];
			
			tornOffWindows = [[NSMutableArray alloc] init];
			tornOffPages = [[NSMutableArray alloc] init];
			
			if (mainWindowInfo == NULL)
			{
				temp = DefaultWindowInfoForNotebook(notebook);
				mainWindowInfo = &temp;
			}
			
			if (tornOffWindowInfos == NULL)
			{
				byteCount = 0;
				tornOffWindowCount = 0;
			}
			else
				tornOffWindowCount = byteCount/sizeof(WindowInfo);
			
			//the first element of this array will be the mainWindow
			WindowInfo *allWindowInfos = malloc(sizeof(WindowInfo)*(tornOffWindowCount+1));	//+1 for the mainWindowInfo
			
			allWindowInfos[0]=*mainWindowInfo;
			
			if (tornOffWindowInfos != NULL)
				memcpy(allWindowInfos+1, tornOffWindowInfos, byteCount);
			
			[self makeWindowsWithWindowInfos:allWindowInfos count:tornOffWindowCount+1];
			free(allWindowInfos);
			
		}
		
		//set the title field in all of the windows
		{
			NSString *title = [notebook title];
			[(SBProtoFrameView*)[mainWindow contentView] setNotebookTitleFieldStringValue: title];
			
			NSEnumerator *enumerator = [tornOffWindows objectEnumerator];
			SpiralBoundWindow *window;
			
			while (window = [enumerator nextObject])
			{
				[(SBProtoFrameView*)[window contentView] setNotebookTitleFieldStringValue: title];
			}
		}
		
	}
	
	return self;
}

-(void)registerForNotifications
{
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(delayedNotebookDidChangeLength:)
												 name:NotebookDidChangeLengthNotification
											   object:notebook];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(delayedMainWindowPageChanged:)
												 name:MainWindowPageDidChangeNotification
											   object:notebook];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(delayedPageLayoutChanged:)
												 name:PageLayoutDidChangeNotification
											   object:notebook];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(pageArrangementDidChange:)
												 name:PageArrangementDidChangeNotification
											   object:notebook];
											
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(pageSecurityDidChange:)
												 name:PageSecurityDidChangeNotification
											   object:nil]; //object for this note is a page
															//the method determines whether to process this note or not
											
	//this is used to autotitle
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(windowDidResignKey:)
												 name:NSWindowDidResignKeyNotification
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(pageTitleDidChange:)
												 name:PageTitleDidChangeNotification
											   object:nil];
	
	/*To do List notification*/
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(todoItemsReload:)
												 name:SPRTodoItemsCountDidChange
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(todoItemsReload:)
												 name:SPRTodoItemsOrderDidChange
											   object:nil];
	
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(todoItemStateDidChange:)
												 name:SPRTodoItemStateDidChange
											   object:nil];
											   
}

-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
}


-(void)makeWindowsWithWindowInfoArray:(NSArray*)windowInfos
{
	BOOL windowMismatch=NO;
	
	NSEnumerator *enumerator = [windowInfos objectEnumerator];
	//skip the first element, which contains the mainWindow stuff
	[enumerator nextObject];
	NSDictionary *info;
	while (info = [enumerator nextObject])
	{
		SPRPage *page = [SPRNotebook pageWithHash:[[info objectForKey:@"NotebookPageHash"] unsignedIntValue]];
		
		if (page == nil)
		{
			windowMismatch = YES;
			break;
		}
		
		[tornOffPages addObject:page];
		
		NSRect frame = NSMakeRect(0,0,0,0);
        //frame was formally stored as an NSData containing an NSRect, then as an NSValue, but apparently
        //that is not compatible with Snow Leopard. Now stored as a string
        if ([info objectForKey:@"FrameString"])
        {
            frame = NSRectFromString([info objectForKey:@"FrameString"]);
        }
        else if ([info objectForKey:@"FrameValue"])
        {
            frame = [[info objectForKey:@"FrameValue"] rectValue];
        }
        else if ([info objectForKey:@"Frame"])
        {
            [[info objectForKey:@"Frame"] getBytes:&frame length:sizeof(NSRect)];
		}
        
        //check to see if the frame is an invalid size
        if (NSWidth(frame)<[SpiralBoundWindow minSize].width ||
            NSHeight(frame)<[SpiralBoundWindow minSize].height)
        {
            frame = NSMakeRect(0,0,0,0);
            frame.size=[SpiralBoundWindow minSize];
        }
        
		SpiralBoundWindow *tornOffWindow = [SPROrganizer loadWindowWithFrame:frame
                                                                        page:page
                                                                       style:SBTornOffStyle
                                                                   organizer:self];
		
		[tornOffWindows addObject: tornOffWindow];
		[tornOffWindow setDelegate: self];
		[(SBProtoFrameView*)[tornOffWindow contentView] setIsShaded:[[info objectForKey:@"Shaded"] boolValue]];
		
		if (!hidden)
		{
			//z order preservation stuff
			id zPosition = [info objectForKey:@"ZPosition"];
			
			//don't move onscreen yet if it's zPosition was stored
			if ([zPosition isKindOfClass:[NSNumber class]])
				[WindowsZOrder setObject:zPosition
								  forKey:[NSNumber numberWithInt:[tornOffWindow windowNumber]]];
			else
				[tornOffWindow orderFront:nil];			
		}
				
		if ([[info objectForKey:@"Floater"] boolValue])
			[tornOffWindow setFloater:YES];
	}
	
	if (!windowMismatch)
	{
		NSInteger hash = [[[windowInfos objectAtIndex:0] objectForKey:@"NotebookPageHash"] unsignedIntValue];
		mainWindowPage = [SPRNotebook pageWithHash:hash];
	}
	
	if (mainWindowPage==nil || windowMismatch)
	{
		//release all of the torn off windows
		[tornOffWindows makeObjectsPerformSelector:@selector(close)];
		[tornOffWindows removeAllObjects];
		[tornOffPages removeAllObjects];

		mainWindowPage = [[[self notebook] pages] objectAtIndex:0];
	}
	
	NSDictionary *mainWindowInfo = [windowInfos objectAtIndex:0];
	
    NSRect frame = NSMakeRect(0,0,0,0);
    //frame was formally stored as an NSData containing an NSRect, then as an NSValue, but apparently
    //that is not compatible with Snow Leopard. Now stored as a string
    if ([mainWindowInfo objectForKey:@"FrameString"])
    {
        frame = NSRectFromString([mainWindowInfo objectForKey:@"FrameString"]);
    }
    else if ([mainWindowInfo objectForKey:@"FrameValue"])
    {
        frame = [[mainWindowInfo objectForKey:@"FrameValue"] rectValue];
    }
    else if ([mainWindowInfo objectForKey:@"Frame"])
    {
        [[mainWindowInfo objectForKey:@"Frame"] getBytes:&frame length:sizeof(NSRect)];
    }
    
    //check to see if the frame is an invalid size
    if (NSWidth(frame)<[SpiralBoundWindow minSize].width ||
        NSHeight(frame)<[SpiralBoundWindow minSize].height)
    {
        frame = NSMakeRect(0,0,0,0);
        frame.size=[SpiralBoundWindow minSize];
    }
    
	mainWindow = [SPROrganizer loadWindowWithFrame:frame
												   page:mainWindowPage
												  style:SBSpiralBoundStyle
											  organizer:self];
	
	[mainWindow setDelegate: self];
	[(SBProtoFrameView*)[mainWindow contentView] setIsShaded:[[mainWindowInfo objectForKey:@"Shaded"] boolValue]];
	
	if (!hidden)
	{
		//z order preservation stuff
		id zPosition = [mainWindowInfo objectForKey:@"ZPosition"];
		
		if ([zPosition isKindOfClass:[NSNumber class]])
			[WindowsZOrder setObject:zPosition
							  forKey:[NSNumber numberWithInt:[mainWindow windowNumber]]];
		else
			[mainWindow orderFront:nil];
	}
	
	if ([[mainWindowInfo objectForKey:@"Floater"] boolValue])
		[mainWindow setFloater:YES];
	
}

//old style
-(void)makeWindowsWithWindowInfos:(WindowInfo*) windowInfos count:(NSUInteger)count
{
	BOOL windowMismatch=NO;
	NSUInteger i;
	//start at the second element
	for (i=1; i<count; i++)
	{
		SPRPage *page = [SPRNotebook pageWithHash:windowInfos[i].notebookPageHash];
		
		if (page == nil)
		{
			windowMismatch = YES;
			break;
		}
		
		[tornOffPages addObject:page];
		
		SpiralBoundWindow *tornOffWindow = [SPROrganizer loadWindowWithFrame:windowInfos[i].frame
																		 page:page
																		style:SBTornOffStyle
																		organizer:self];
		[tornOffWindows addObject: tornOffWindow];
		[tornOffWindow setDelegate: self];
		[(SBProtoFrameView*)[tornOffWindow contentView] setIsShaded:windowInfos[i].shaded];
		[tornOffWindow orderFront:nil];
		
	/*moved this functionality into SBProtoFrameView
		//make sure it's on screen
		NSRect trueFrame = [tornOffWindow frame]; //takes in account shading
		MoveRectOntoScreen(&trueFrame);
		[tornOffWindow setFrame:trueFrame display:YES];
	*/
	}
	
	if (!windowMismatch)
	{
		mainWindowPage = [SPRNotebook pageWithHash:windowInfos[0].notebookPageHash];
	}
	
	if (mainWindowPage==nil || windowMismatch)
	{
		//release all of the torn off windows
		[tornOffWindows makeObjectsPerformSelector:@selector(close)];
		[tornOffWindows removeAllObjects];
		[tornOffPages removeAllObjects];

		mainWindowPage = [[[self notebook] pages] objectAtIndex:0];
	}
	
	mainWindow = [SPROrganizer loadWindowWithFrame:windowInfos[0].frame
												   page:mainWindowPage
												  style:SBSpiralBoundStyle
											  organizer:self];
	[mainWindow setDelegate: self];
	[(SBProtoFrameView*)[mainWindow contentView] setIsShaded:windowInfos[0].shaded];
	[mainWindow orderFront:nil];
	
	/*moved this functionality into SBProtoFrameView
	//make sure it's on screen
	NSRect trueFrame = [mainWindow frame]; //takes in account shading 
	MoveRectOntoScreen(&trueFrame);
	[mainWindow setFrame:trueFrame display:YES];
	*/
	
}

-(SpiralBoundWindow*)requestTearOffWindow: (NSRect*)frameOnReturn
{
	SPRPage *pageForTornOff = mainWindowPage;
	SPRPage *pageForMainWindow = [self nextPageInStack];
	
	if (pageForMainWindow == mainWindowPage) //then we need to create a new blank page
	{
		pageForMainWindow = [notebook newPage];
	}
	
	id contentView = [mainWindow contentView];
	
	*frameOnReturn = [contentView unshadedFrameRectMinusBorder];
	NSRect mainWindowFrame = [mainWindow frame];
	frameOnReturn->origin.x = mainWindowFrame.origin.x;
	frameOnReturn->origin.y = mainWindowFrame.origin.y + (mainWindowFrame.size.width-frameOnReturn->size.width);
	
	if ([contentView isShaded] == YES)
	{
		frameOnReturn->origin.y-= [contentView deltaHeight];
	}
	
	//it's important to call this accessor
	//this sets up the text system as well as setting the instance variable
	[self setMainWindowPage:pageForMainWindow];
	SpiralBoundWindow *newWindow = [SPROrganizer loadWindowWithFrame:*frameOnReturn page:pageForTornOff style:SBTornOffStyle organizer:self];
	
	[newWindow setDelegate: self];
	[newWindow makeKeyAndOrderFront:nil];
	
	[tornOffWindows addObject: newWindow];
	[tornOffPages addObject: pageForTornOff];	//don't autorelease it, its owned by its SPRNotebook

	[(SBProtoFrameView*)[newWindow contentView] setNotebookTitleFieldStringValue:[[self notebook] title]];
	
	//[[newWindow mainTextView] setIsScrambled:[gSBController allPagesAreSecured]];
	
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:PageLayoutDidChangeNotification																																				object:notebook]
											   postingStyle:NSPostASAP];
	
	return newWindow;
}


- (void)setMainWindowPage:(SPRPage*)page
{	
	if (page == mainWindowPage)
		return;
	
	[mainWindowPage autotitle]; //this just returns if it is already titled
	
	mainWindowPage = page;
	
	if ([page isKindOfClass:[SPRTodoPage class]])
	{
		[[mainWindow mainTextView] replaceTextStorage:nil];
		
		SPRTodoListView *todoList = [mainWindow mainTodoListView];
		[todoList removeAllItems];
		
		[mainWindow showMainTodoListView];
		
		for (NSDictionary *dict in [(SPRTodoPage*)page items])
		{
			BOOL state = [[dict objectForKey:StateKey] boolValue];
			NSTextStorage *ts = [dict objectForKey:TextStorageKey];
			[todoList addTextStorage:ts state:state];
		}
		
	}
	else
	{
		[[mainWindow mainTextView] replaceTextStorage:[page contentText]];
		[mainWindow showMainTextView];
	}
	
	[mainWindow setTitle: [page title]];
	
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:MainWindowPageDidChangeNotification																																				object:notebook]
											   postingStyle:NSPostASAP];
	
	
	[mainWindow setSecurityButtonState: ([page isSecure] ? NSOnState : NSOffState)];
}

-(void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:color forKey:@"Color"];
	[encoder encodeBool:hidden forKey:@"Hidden"];
	
	//this next one doesn't actually correspond directly to an instance variable but
	//will be used to regenerate the connection to the SPRNotebook
	[encoder encodeObject: [notebook title] forKey:@"NotebookTitle"];
	
#if !USE_WINDOW_INFO_STRUCTS
	//instead of the windowInfo struct, which is difficult to expand in the future, use arrays of keys
	//let the first element of the array be the main window
	NSInteger count = [tornOffWindows count]+1;
	NSMutableArray *windowInfos = [NSMutableArray arrayWithCapacity:count];
		
	//encode the windows starting with the main window
	NSInteger i;
	
	//remember count is one greater than the number of torn offs because it includes the main window
	for (i=-1; i<count-1; i++)
	{
		SpiralBoundWindow *window;
		SPRPage *page;
		
		if (i == -1)
		{
			window = mainWindow;
			page = mainWindowPage;
		}
		else
		{
			window = [tornOffWindows objectAtIndex:i];
			page = [tornOffPages objectAtIndex:i];
		}
		
		BOOL shaded = [(SBProtoFrameView*)[window contentView] isShaded];
		NSRect frame = [window frame];
		if (shaded)
		{
			float deltaHeight = [(SBProtoFrameView*)[window contentView] deltaHeight];
			frame.size.height+=deltaHeight;
			frame.origin.y-=deltaHeight;
		}
		
		id zPosition = [WindowsZOrder objectForKey:[NSNumber numberWithInt:[window windowNumber]]];
		if (zPosition == nil) //hidden
			zPosition = [NSNull null];
		
		NSDictionary *windowInfo = [NSDictionary dictionaryWithObjectsAndKeys:
									[NSNumber numberWithUnsignedInt:[page hash]], @"NotebookPageHash",
									[NSNumber numberWithBool:shaded], @"Shaded",
									NSStringFromRect(frame), @"FrameString",
                                    //[NSValue valueWithRect:frame], @"FrameValue",
                                    //[NSData dataWithBytes:&frame length:sizeof(NSRect)], @"Frame",
									zPosition, @"ZPosition",
									[NSNumber numberWithBool:[window isFloater]], @"Floater",
									nil];
		[windowInfos addObject:windowInfo];
	}
	//save an immutable copy
	[encoder encodeObject:[NSArray arrayWithArray:windowInfos] forKey:@"WindowInfos"];
				
#else
	//make the windowInfo structs and encode them
	//these will be used when recreating the windows and the pages they are connected to
	WindowInfo windowInfo;
	
	//first do the main window
	windowInfo.notebookPageHash = [mainWindowPage hash];
	windowInfo.shaded = [(SBProtoFrameView*)[mainWindow contentView] isShaded];
	windowInfo.frame = [mainWindow frame];
	if (windowInfo.shaded)
	{
		float deltaHeight = [(SBProtoFrameView*)[mainWindow contentView] deltaHeight];
		windowInfo.frame.size.height+=deltaHeight;
		windowInfo.frame.origin.y-=deltaHeight;
	}
	
	[encoder encodeBytes:(void*)&windowInfo length:sizeof(WindowInfo) forKey:@"MainWindowInfo"];
	
	NSUInteger i, count = [tornOffWindows count];
	
	WindowInfo *windowInfos = malloc(sizeof(WindowInfo)*count);
	
	for (i=0; i<count; i++)
	{
		NSWindow *window = [tornOffWindows objectAtIndex:i];
		SPRPage *page = [tornOffPages objectAtIndex:i];
		
		windowInfos[i].notebookPageHash = [page hash];
		windowInfos[i].shaded = [(SBProtoFrameView*)[window contentView] isShaded];
		windowInfos[i].frame = [window frame];
		if (windowInfos[i].shaded)
		{
			float deltaHeight = [(SBProtoFrameView*)[window contentView] deltaHeight];
			windowInfos[i].frame.size.height+=deltaHeight;
			windowInfos[i].frame.origin.y-=deltaHeight;
		}
	}
	
	[encoder encodeBytes:(void*)windowInfos length:count*sizeof(WindowInfo) forKey:@"TornOffWindowInfos"];
	
	free(windowInfos);
#endif
}

-(SPRPage*)pageForWindow:(NSWindow*)window
{
	if ([window isKindOfClass: [SpiralBoundWindow class]] == NO)
		return nil;
		
	if (window == mainWindow)
	{
		return mainWindowPage;
	}
	else
	{
		NSUInteger index = [tornOffWindows indexOfObject:window];
		
		if (index == NSNotFound)
			return nil;
		else
			return [tornOffPages objectAtIndex: index];
	}
}

//return nil if page is not visible in a window
-(SpiralBoundWindow*)windowForPage:(SPRPage*)page
{
	if ([tornOffPages containsObject:page])
	{
		return [tornOffWindows objectAtIndex:[tornOffPages indexOfObject:page]];
	}
	else if (page == mainWindowPage)
	{
		return mainWindow;
	}
	else
	{
		return nil;
	}
}

-(NSArray*)pageTitles
{
	NSArray *pages = [notebook pages];
	
	NSMutableArray *titles = [[NSMutableArray alloc] initWithCapacity:[pages count]];
	NSEnumerator *enumerator = [pages objectEnumerator];
	SPRPage *page;
	
	while (page = [enumerator nextObject])
	{
		[titles addObject: [page title]];
	}
	return titles;
}

-(NSString*)titleOfPageAtIndex:(NSUInteger)index
{
	NSArray *pages = [notebook pages];
	
	if (index >= [pages count])
	{
		NSLog(@"-titleOfPageAtIndex: was passed invalid index (%u)",index);
		return nil;
	}
	
	return [[pages objectAtIndex:index] title];
}

-(NSUInteger)pageCount
{
	return [[notebook pages] count];
}

-(IBAction)raise:(id)sender
{
	[self raiseNotebook];
}

-(IBAction)hideAll:(id)sender
{
	[tornOffWindows makeObjectsPerformSelector: @selector(orderOut:) withObject: nil];
	[mainWindow orderOut:nil];
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:NotebookDidHideNotification																																			object:self]
											   postingStyle:NSPostASAP];
	
	[self setHidden:YES];
}

-(void)showAll
{
	[mainWindow orderBack:nil];
	[tornOffWindows makeObjectsPerformSelector:@selector(orderBack:) withObject:nil];
	[self setHidden:NO];
}

- (void)windowDidBecomeKey:(NSNotification *)notificationWithName
{
	if (![self isKey])
	{
		[self setKey:YES];
	}
	
	if (hidden)
	{
		[self setHidden:NO];
	}
}

-(void)closeWindows
{
	[mainWindow close];
	[tornOffWindows makeObjectsPerformSelector:@selector(close)];
}

-(void)newPageWithAttributedString:(NSAttributedString*)string
{
	SPRPage *page = [notebook newPage];
	[[page contentText] setAttributedString:string];
	
	[self setMainWindowPage: page];
	[mainWindow makeKeyAndOrderFront:nil];
	if ([(SBProtoFrameView*)[mainWindow contentView] isShaded])
		[(SBProtoFrameView*)[mainWindow contentView] setIsShaded:NO];
}

-(SPRPage*)pageAtIndex:(unsigned int)index
{
	return [[notebook pages] objectAtIndex:index];
}

#pragma mark ACCESSORS

- (SPRNotebook *)notebook
{
	return notebook;
}
- (NSWindow *)mainWindow
{
	return mainWindow;
}

- (SPRPage *)mainWindowPage
{
	return mainWindowPage;
}

- (NSMutableArray*)tornOffWindows
{
	return tornOffWindows;
}

- (NSColor *)color
{
    return color;
}

- (void)setColor:(NSColor *)value
{
    if (color != value)
	{
        color = [value copy];
		
		//update all windows
		NSArray *windows = [NSApp windows];
		NSUInteger count = [windows count],i;
		
		for (i=0; i<count; i++)
		{
			NSWindow *window = [windows objectAtIndex:i];
			if ([window isKindOfClass:[SpiralBoundWindow class]])
			{
				SBProtoFrameView *view = (SBProtoFrameView *)[window contentView];
				[view setColors];
				[view setNeedsDisplay:YES];
			}
		}
		
		//post the notification
		[[NSNotificationQueue defaultQueue] enqueueNotification:
		 [NSNotification notificationWithName:NotebookColorDidChangeNotification																																				object:notebook]
												   postingStyle:NSPostNow];
    }
}

- (NSString *)title
{
	return [notebook title];
}

- (void)setTitle:(NSString*)newTitle
{
	NSString *oldTitle = [notebook title];
	[notebook setTitle:newTitle];
	
	[(SBProtoFrameView*)[mainWindow contentView] setNotebookTitleFieldStringValue: newTitle];
	
	NSEnumerator *enumerator = [tornOffWindows objectEnumerator];
	SpiralBoundWindow *window;
	
	while (window = [enumerator nextObject])
	{
		[(SBProtoFrameView*)[window contentView] setNotebookTitleFieldStringValue: newTitle];
	}
	
	//post the notification
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:NotebookTitleDidChangeNotification																																				object:notebook
												  userInfo:[NSDictionary dictionaryWithObjectsAndKeys:oldTitle,@"OldTitle",
																									 [notebook title],@"NewTitle", nil]]
											   postingStyle:NSPostASAP];
	
	//update the title of the hide notebook menu item
	NSMenuItem *hideItem = [[[[NSApp mainMenu] itemWithTitle:@"Notepad"] submenu] itemWithTag:HideMenuItemTag];
	[hideItem setTitle:[NSString stringWithFormat:@"Hide \"%@\"", [self title]]];
}

- (BOOL)isHidden
{
    return hidden;
}

- (void)setHidden:(BOOL)value
{
    if (hidden != value)
	{
        hidden = value;
		[[NSNotificationQueue defaultQueue] enqueueNotification: [NSNotification notificationWithName:NotebookDidUnhideNotification
																								 object:self]
													 postingStyle:NSPostASAP];
    }
}

- (BOOL)isKey
{
    return key;
}

- (void)setKey:(BOOL)value
{
    if (key != value)
	{
        key = value;
		
		if (key == YES)
		{
			//set all other windows to not be key
			NSEnumerator *enumerator = [Organizers objectEnumerator];
			SPROrganizer *organizer;
			
			while (organizer = [enumerator nextObject])
			{
				if (organizer == self)
					continue;
				
				[organizer setKey:NO];
			}
			//set the Hide menu item's title to reflect the new notebook
			NSMenuItem *hideItem = [[[[NSApp mainMenu] itemWithTitle:@"Notepad"] submenu] itemWithTag:HideMenuItemTag];
			[hideItem setTitle:[NSString stringWithFormat:@"Hide \"%@\"", [self title]]];
			
			
			[[NSNotificationQueue defaultQueue] enqueueNotification:
			 [NSNotification notificationWithName:NotebookDidBecomeKeyNotification																																			object:self]
													   postingStyle:NSPostASAP];
		}
		else
		{
			[[NSNotificationQueue defaultQueue] enqueueNotification:
			 [NSNotification notificationWithName:NotebookDidResignKeyNotification																																			object:self]
													   postingStyle:NSPostASAP];
		}
    }
}


@end

//TODO maybe implement this to randomly place the window within the main screen
//that might eliminate problems with the windows completely obscuring others
//in the event that the user has to reset the organizer data
static WindowInfo DefaultWindowInfoForNotebook(SPRNotebook *notebook)
{
	WindowInfo info;
	NSRect screenRect = [[NSScreen mainScreen] visibleFrame];
	
	
	info.frame.size.width = 200;
	info.frame.size.height = 200;
	
	info.frame.origin.x = screenRect.origin.x + (screenRect.size.width/2 - info.frame.size.width/2);
	info.frame.origin.y = screenRect.origin.y + (screenRect.size.height/2 - info.frame.size.height/2);
	
	info.notebookPageHash = [[[notebook pages] objectAtIndex:0] hash];
	
	info.shaded=NO;
	
	return info;
}

static NSArray *DefaultWindowInfoArrayForNotebook(SPRNotebook *notebook)
{
	NSUInteger hash = [[[notebook pages] objectAtIndex:0] hash];
	NSRect screenRect = [[NSScreen mainScreen] visibleFrame];
	NSRect frame = {screenRect.origin.x + (screenRect.size.width/2 - 100.0),
					screenRect.origin.y + (screenRect.size.height/2 - 100.0),
					200.0,200.0};
	
	NSArray *array = [NSArray arrayWithObject: [NSDictionary dictionaryWithObjectsAndKeys:
												[NSNumber numberWithUnsignedInt:hash], @"NotebookPageHash",
												[NSNumber numberWithBool:NO], @"Shaded",
												[NSData dataWithBytes:&frame length:sizeof(NSRect)], @"Frame",
												//[NSNumber numberWithInt:0], @"ZPosition",
												[NSNumber numberWithBool:NO], @"Floater", nil]];
	
	return array;
}

static NSArray *IntroWindowInfoArrayForNotebook(SPRNotebook *notebook)
{
	NSArray *pages = [notebook pages];
	if ([pages count] != 3)
		@throw([NSException exceptionWithName:@"IntroWindowInfoArray error"
									   reason:@"passed notebook page count != 3"
									 userInfo:nil]);
	
	int hash1 = [[pages objectAtIndex:0] hash],
		hash2 = [[pages objectAtIndex:1] hash],
		hash3 = [[pages objectAtIndex:2] hash];
	
	NSRect frame1 = {354, 205, 373, 502},
		   frame2 = {757, 505, 327, 195},
		   frame3 = {603, 134, 377, 455},
		   //frame3 = {642, 92, 380, 259},
		   encompassingFrame, newEncompassingFrame;
	
	encompassingFrame = NSUnionRect(NSUnionRect(frame1, frame2), frame3);
	newEncompassingFrame = RIVCenterRect(encompassingFrame, [[NSScreen mainScreen] visibleFrame]);
	float xShift, yShift;
	xShift = newEncompassingFrame.origin.x-encompassingFrame.origin.x;
	yShift = newEncompassingFrame.origin.y-encompassingFrame.origin.y;
	
	frame1.origin.x += xShift; frame1.origin.y += yShift;
	frame2.origin.x += xShift; frame2.origin.y += yShift;
	frame3.origin.x += xShift; frame3.origin.y += yShift;
	
	NSArray *array = [NSArray arrayWithObjects: [NSDictionary dictionaryWithObjectsAndKeys:
												[NSNumber numberWithUnsignedInt:hash1], @"NotebookPageHash",
												[NSNumber numberWithBool:NO], @"Shaded",
												[NSData dataWithBytes:&frame1 length:sizeof(NSRect)], @"Frame",
												[NSNumber numberWithInt:3], @"ZPosition",
												[NSNumber numberWithBool:NO], @"Floater", nil],
					   
												[NSDictionary dictionaryWithObjectsAndKeys:
												[NSNumber numberWithUnsignedInt:hash2], @"NotebookPageHash",
												[NSNumber numberWithBool:NO], @"Shaded",
												[NSData dataWithBytes:&frame2 length:sizeof(NSRect)], @"Frame",
												[NSNumber numberWithInt:2], @"ZPosition",
												[NSNumber numberWithBool:NO], @"Floater", nil],
												
												[NSDictionary dictionaryWithObjectsAndKeys:
												[NSNumber numberWithUnsignedInt:hash3], @"NotebookPageHash",
												[NSNumber numberWithBool:NO], @"Shaded",
												[NSData dataWithBytes:&frame3 length:sizeof(NSRect)], @"Frame",
												[NSNumber numberWithInt:1], @"ZPosition",
												[NSNumber numberWithBool:NO], @"Floater", nil], nil];
	return array;
}

//does nothing if the rect is already visible
//this only handles x placement, the window itself handles y placement
//this does however makes sure the window isn't taller than the tallest screen
/*
void MoveRectOntoScreen(NSRect *rect)
{
	float x1 = rect->origin.x+10,
		  x2 = rect->origin.x+rect->size.width-10;
	
	NSArray *screens = [NSScreen screens];
	NSEnumerator *enumerator = [screens objectEnumerator];
	NSScreen *screen;
	
	BOOL needsToBeCentered=YES;
	while (screen = [enumerator nextObject])
	{
		NSRect r = [screen frame];
		
		if ((x1 > r.origin.x && x1 < r.origin.x+r.size.width) ||
			(x2 > r.origin.x && x2 < r.origin.x+r.size.width))
		{
			needsToBeCentered=NO;
			break;
		}
	}
	
	if (needsToBeCentered)
	{
		//just center it on the main screen
		NSRect mainScreenRect = [[NSScreen mainScreen] visibleFrame];
		rect->origin.x = mainScreenRect.origin.x+(mainScreenRect.size.width/2 - rect->size.width/2);
	}
	
	//now check if it is taller than the tallest screen
	enumerator = [screens objectEnumerator];
	NSRect tallestRect = NSZeroRect;
	
	//get the tallestRect
	while (screen = [enumerator nextObject])
		if ([screen visibleFrame].size.height > tallestRect.size.height)
			tallestRect = [screen visibleFrame];
	
	if (tallestRect.size.height < rect->size.height)
	{
		rect->size.height = tallestRect.size.height*0.95;
		*rect = RIVCenterRect(*rect, tallestRect);
	}
}*/
