//
//  PageBrowser+Filtering_SBPro.m
//  CommonSpiralbound
//
//  Created by Philip on 5/18/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import "PageBrowser+Filtering_SBPro.h"
#import "RIVTools.h"
#import "SPROrganizer.h"
#import "SPRPage.h"
#import "NSString+RIVSearch.h"

@implementation PageBrowser (Filtering)

-(NSUInteger)pageIndexForCellIndex:(NSUInteger)indexIn
{
	NSAssert1([[[pagesView matrixInColumn:0] selectedCells] count] == 1,
			  @"Invalid number of notebooks selected = %lu",
			  [[[pagesView matrixInColumn:0] selectedCells] count]);
	
	NSString *notebookTitle = [[pagesView selectedCellInColumn:0] title];
	
	return [[[filteredContents objectForKey:notebookTitle] objectAtIndex:indexIn] unsignedIntValue];
}

-(void)loadUnfiltered
{
	//simply load all pages without filtering
	[filteredContents removeAllObjects];
	
	for (SPROrganizer *org in [SPROrganizer organizers])
	{
		NSMutableArray *pageIndexes = [NSMutableArray array];
		int pageCount = [org pageCount];
		int i;
		for (i=0; i<pageCount;i++)
			[pageIndexes addObject:[NSNumber numberWithInt:i]];
		
		[filteredContents setObject:pageIndexes forKey:[org title]];
	}
}

/*refiltering the selected page will not caus it to be removed from filteredContents
because the user may actively be using it*/
-(void)refilterSelectedPage
{
	if (selectedPage == nil)
		return;
	
	NSNumber *hashObj = [NSNumber numberWithInt:[selectedPage hash]];
	
	NSString *pageText = [selectedPage contentsAsString];
	
	CFStringCompareFlags opts = (filterMatchCase ? 0 : kCFCompareCaseInsensitive) |
	(filterMatchDiacriticals ? 0 : kCFCompareDiacriticInsensitive) |
	kCFCompareNonliteral | kCFCompareWidthInsensitive;
	
	NSArray *matchingRanges = [pageText rangesForSearchString:filterValue
													  options:opts
												   wholeWords:filterMatchWholeWordsOnly];
	
	[filterMatchRanges setObject:matchingRanges forKey:hashObj];
	
	[self updateHighlights];
}

-(void)filter
{
	[filterMatchRanges removeAllObjects];
	
	if (!filterValue || [filterValue isEqualToString:@""])
	{
		[self updateHighlights];
		return;
	}
	
	for(NSString *notebookTitle in filteredContents)
	{
		NSMutableArray *pageIndexes = [filteredContents objectForKey:notebookTitle];
		NSMutableArray *pagesToRemove = [NSMutableArray array];
		SPROrganizer *org = [SPROrganizer organizerForTitle:notebookTitle];
		
		for (NSNumber *pageIndex in pageIndexes)
		{
			//now do the actual filtering
			int index = [pageIndex unsignedIntValue];
			SPRPage *page = [org pageAtIndex:index];
			NSString *pageText = [page contentsAsString];
			
			CFStringCompareFlags opts = (filterMatchCase ? 0 : kCFCompareCaseInsensitive) |
											(filterMatchDiacriticals ? 0 : kCFCompareDiacriticInsensitive) |
												kCFCompareNonliteral | kCFCompareWidthInsensitive;
			
			NSArray *matchingRanges = [pageText rangesForSearchString:filterValue
															  options:opts
														   wholeWords:filterMatchWholeWordsOnly];
			
			NSArray *titleRanges = [[page title] rangesForSearchString:filterValue
															   options:opts
															wholeWords:filterMatchWholeWordsOnly];
			
			if ([matchingRanges count] == 0 && [titleRanges count] == 0)
			{
				[pagesToRemove addObject:pageIndex];
				
				//check to see if the page that was just searched and turned up empty was the selected page
				if (page == selectedPage)
				{
					[self setSelectedPage:nil];
				}
			}
			else if ([matchingRanges count] > 0)
			{
				[filterMatchRanges setObject:matchingRanges forKey:[NSNumber numberWithInt:[page hash]]];
			}
		}
		
		[pageIndexes removeObjectsInArray:pagesToRemove];
	}
	[self updateHighlights];
}

-(IBAction)filterAction:(id)sender //assume sender responds to -stringValue only
{
	NSString *lastFilterValue = filterValue;
	
	filterValue = [[sender stringValue] copy];
	
	//if the results will be a subset of the current results, no need to load
	if (filterMatchWholeWordsOnly || ([filterValue rangeOfString:lastFilterValue].location == NSNotFound))
		[self loadUnfiltered];
	
	[self filter];
	
	pagesColumnNeedsUpdate=YES;
	notebooksColumnNeedsUpdate=YES;
	
	[pagesView validateVisibleColumns];
	
}

-(IBAction)toggleFilterSetting:(id)sender
{
	int newState = [sender state]==NSOnState ? NSOffState : NSOnState;
	[sender setState:newState];
	BOOL needToReload=NO; //if the search has become less specific, we need to reload
	
	switch ([sender tag])
	{
		case 10:
			filterMatchCase=newState;
			needToReload = (filterMatchCase == NO); 
			break;
			
		case 11:
			filterMatchWholeWordsOnly=newState;
			needToReload = (filterMatchWholeWordsOnly == NO);
			break;
			
		case 12:
			filterMatchDiacriticals=newState;
			needToReload = (filterMatchDiacriticals == NO);
			break;
	}
	if (needToReload)
		[self loadUnfiltered];
	
	[self filter];
	
	pagesColumnNeedsUpdate=YES;
	notebooksColumnNeedsUpdate=YES;
	
	[pagesView validateVisibleColumns];
}

@end
