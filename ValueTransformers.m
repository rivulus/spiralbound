//
//  ValueTransformers.m
//  NewCommonSpiralBound
//
//  Created by Philip on 1/3/11.
//  Copyright 2011 Rivulus Software. All rights reserved.
//

#import "ValueTransformers.h"


@implementation CheckIntervalTransformer

+ (Class)transformedValueClass
{
	return [NSString class];
}

+ (BOOL)allowsReverseTransformation { return NO; }

- (id)transformedValue:(id)value
{
    NSTimeInterval lastCheck = [value doubleValue];
	if (lastCheck == 0)
		return @"Last Checked: Never";
	
	NSTimeInterval interval = [NSDate timeIntervalSinceReferenceDate]-lastCheck;
	
	unsigned days = interval / (60*60*24);
	
	switch (days)
	{
		case 0:
			return @"Last Checked: Less than 24 hours ago.";
		case 1:
			return @"Last Checked: 1 day ago.";
		default:
			return [NSString stringWithFormat:@"Last Checked: %u days ago.", days];
	}
}

@end
