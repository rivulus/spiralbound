//
//  SBProtoFrameView.m
//  SpiralBound
//
//  Created by Philip White on 2/4/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "SpiralBoundWindow.h"
#import "SBProtoFrameView.h"
#import "SBConstants.h"
#import "SPRNotebook.h"
#import "SBController.h"
#import "SPROrganizer.h"
#import "SPROrganizer+Actions.h"
#import "PrintingView.h"
#import "SBTitleView.h"
#import "RIVTools.h"
#import "SPRTextView.h"
#import "SPRPage.h"

#import <ExceptionHandling/ExceptionHandlingDefines.h>
#import <ExceptionHandling/NSExceptionHandler.h>
#import "ErrorController.h"
#import "NotificationNames.h"
#import "SPRDummyWindow.h"
#import "SPRTodoListView.h"

static NSString *DragbarFillerImageName = @"TitleBarFillerColorMask";

BOOL UseSimpleDragbar;


//always called on main thread
NSWindow *GetDummyPositionWindow(void)
{
	static NSWindow *window=nil;
	
	if (window == nil)
	{
		window = [[SPRDummyWindow alloc] initWithContentRect: NSMakeRect(0.0, 0.0, 100.0, 100.0)
												   styleMask: NSTitledWindowMask|NSResizableWindowMask
													 backing: NSBackingStoreBuffered
													   defer: NO];
		[window setAlphaValue:0.0];
		//[window orderFront:nil];
	}
	[window orderBack:nil];
	return window;
}

@implementation SBProtoFrameView
 
#ifdef DEBUG 
+ (id)dummyWindow
{
	return GetDummyPositionWindow();
}
#endif

+ (void)initialize
{
	NSImage *image;
	
	image = [NSImage imageNamed:@"TitleBarFillerColorMask.tiff"];
	[image setName:DragbarFillerImageName];
	
	UseSimpleDragbar = [[NSUserDefaults standardUserDefaults] boolForKey:@"UseSimpleDragbar"];
	
	[[NSUserDefaults standardUserDefaults] addObserver:self
											forKeyPath:DefaultsPreferencesUseSimpleDragbar
											   options:NSKeyValueObservingOptionNew
											   context:NULL];
}

+(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	//only observes DefaultsPreferencesUseSimpleDragbar
	
	UseSimpleDragbar = [[change valueForKey:NSKeyValueChangeNewKey] boolValue];
	NSArray *windows = [NSApp windows];
	NSUInteger count = [windows count],i;
	
	for (i=0; i<count; i++)
	{
		NSWindow *window = [windows objectAtIndex:i];
		if ([window isKindOfClass:[SpiralBoundWindow class]])
		{
			SBProtoFrameView *view = (SBProtoFrameView*)[window contentView];
			
			[view positionSubviews];
			[view adjustWindowHeight];
		}
	}
}

- (id)initWithFrame:(NSRect)frame organizer:(SPROrganizer*)newOrganizer
{
	self = [super initWithFrame: frame];
	
	if (self != nil)
	{
        NSArray *localTopLevelObjects = nil;
		if ([[self nib] instantiateNibWithOwner:self topLevelObjects:&localTopLevelObjects] == NO)
		{
			NSLog(@"Warning! Could not instantiate nib.\n");
			return nil;
		}
		topLevelObjects = localTopLevelObjects;
		
		organizer = newOrganizer;
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(displayConfigurationDidChange:)
													 name:SPRDisplayConfigurationDidChange
												   object:nil];
		
		//[self setColors];
		[self positionSubviews];
		
		[self performSelector:@selector(validateWindowFrame) withObject:nil afterDelay: 0.0];
	}
	
	return self;
}


-(void)displayConfigurationDidChange:(NSNotification*)notification
{
	[self validateWindowFrame];
}

-(void)validateWindowFrame
{
	BOOL shade = [self isShaded];
	NSRect workingRect = [[self window] frame];
	if (shade)
	{
		float dh = [self deltaHeight];
		workingRect.origin.y-=dh;
		workingRect.size.height+=dh;
	}
	
	//get the dummy window
	NSWindow *dummy = GetDummyPositionWindow();
	
	[dummy setFrame:workingRect display:NO];
	//now see if it actually has that frame
	//if not use whatever frame it does have
	NSRect validatedFrame = [dummy frame];
	if (!NSEqualRects(validatedFrame, workingRect))
	{
		if (shade)
		{
			//just change the deltaHeight;
			deltaHeight += validatedFrame.size.height-workingRect.size.height;
		}
		else
		{
			[[self window] setFrame:validatedFrame display:YES];
		}
	}
    [dummy orderOut:nil];
}

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	//release all of the toplevel objects
	//[organizer release];
	
	[mainTextView replaceTextStorage:nil]; //the textstorage owns it, so it will never get released otherwise
}

- (void)drawRect:(NSRect)rect
{
	
	[self drawDragBar:rect];
	[self drawBackground];
	if ([self isShaded] == NO)
		[self drawResizer];
}

- (void)drawResizer
{
	//we'll draw three lines like in a regular window at the bottom right corner
	//we'll draw them from the bottom left to the upper right
	
	NSPoint startingPoint, endingPoint;
	NSRect borderlessRect = [self frameRectMinusBorder];
	
	NSBezierPath *bezierPath = [NSBezierPath bezierPath];
	
	//first set the color and linewidth
	[[NSColor blackColor] set];
	//[bezierPath setLineWidth: 0];
	
	//big one first
	startingPoint.y = borderlessRect.origin.y + kResizerDistanceFromEdge;
	startingPoint.x = borderlessRect.origin.x + (borderlessRect.size.width-kBigResizerLineInset);
	
	endingPoint.y = borderlessRect.origin.y + kBigResizerLineInset;
	endingPoint.x = borderlessRect.origin.x + (borderlessRect.size.width - kResizerDistanceFromEdge);
	
	[bezierPath moveToPoint:startingPoint];
	[bezierPath lineToPoint:endingPoint];
	
	
	//medium
	//y is the same
	startingPoint.x = borderlessRect.origin.x + (borderlessRect.size.width-kMediumResizerLineInset);
	
	endingPoint.y = borderlessRect.origin.y + kMediumResizerLineInset;
	//x is the same
	
	[bezierPath moveToPoint:startingPoint];
	[bezierPath lineToPoint:endingPoint];
	
	
	//small
	//y is the same
	startingPoint.x = borderlessRect.origin.x + (borderlessRect.size.width-kSmallResizerLineInset);
	
	endingPoint.y = borderlessRect.origin.y + kSmallResizerLineInset;
	//x is the same
	
	[bezierPath moveToPoint:startingPoint];
	[bezierPath lineToPoint:endingPoint];
	
	
	//draw 'em
	[bezierPath stroke];
}

- (BOOL)resizerContainsPoint:(NSPoint)point
{
	if (point.x >= [self bounds].size.width-kBigResizerLineInset &&
		point.y <= kBigResizerLineInset)
		return YES;
	else
		return NO;
}

- (void)mouseDown:(NSEvent *)event
{
	//make it so that one click outside of the titleView
	//will cause it to become uneditable
	if ((id)[[self window] firstResponder] != (id)[(id)mainView documentView])
		[[self window] makeFirstResponder:self];
	
	NSPoint pointInView = [self convertPoint:[event locationInWindow] fromView:nil];
	
	//first check if it was a double click
	if ([event clickCount] == 2)
	{
		if (NSPointInRect(pointInView, [self toolbarRect]) || NSPointInRect(pointInView, [self dragbarRect]))
		{
			[self setIsShaded:![self isShaded]];
			return;
		}
	}
	
	
	//treat it as a drag
	
	
	BOOL resize = NO;
	if ([self resizerContainsPoint:pointInView] && ![self isShaded])
	{
		resize = YES;
	}
	
	NSWindow *window = [self window];
	NSPoint originalMouseLocation = [window convertBaseToScreen:[event locationInWindow]];
	NSRect originalFrame = [window frame];
	NSRect newFrame;
	BOOL checkFrame=NO;
    while (YES)
	{
		//
		// Lock focus and take all the dragged and mouse up events until we
		// receive a mouse up.
		//
        NSEvent *newEvent = [window
							 nextEventMatchingMask:(NSLeftMouseDraggedMask | NSLeftMouseUpMask)];
		
        if ([newEvent type] == NSLeftMouseUp)
		{
			break;
		}
		
		//
		// Work out how much the mouse has moved
		//
		NSPoint newMouseLocation = [window convertBaseToScreen:[newEvent locationInWindow]];
		NSPoint delta = NSMakePoint(
									newMouseLocation.x - originalMouseLocation.x,
									newMouseLocation.y - originalMouseLocation.y);
		
		newFrame = originalFrame;
		
		if (!resize)
		{
			//
			// Alter the frame for a drag
			//
			newFrame.origin.x += delta.x;
			newFrame.origin.y += delta.y;
			checkFrame=YES;
		}
		else
		{
			//
			// Alter the frame for a resize
			//
			newFrame.size.width += delta.x;
			newFrame.size.height -= delta.y;
			newFrame.origin.y += delta.y;
			
			//
			// Constrain to the window's min and max size
			//
			NSRect newContentRect = [window contentRectForFrameRect:newFrame];
			NSSize maxSize = [window maxSize];
			NSSize minSize = [window minSize];
			if (newContentRect.size.width > maxSize.width)
			{
				newFrame.size.width -= newContentRect.size.width - maxSize.width;
			}
			else if (newContentRect.size.width < minSize.width)
			{
				newFrame.size.width += minSize.width - newContentRect.size.width;
			}
			if (newContentRect.size.height > maxSize.height)
			{
				newFrame.size.height -= newContentRect.size.height - maxSize.height;
				newFrame.origin.y += newContentRect.size.height - maxSize.height;
			}
			else if (newContentRect.size.height < minSize.height)
			{
				newFrame.size.height += minSize.height - newContentRect.size.height;
				newFrame.origin.y -= minSize.height - newContentRect.size.height;
			}
		}
		
		[window setFrame:newFrame display:YES animate:NO];
	}
	
	[self validateWindowFrame];
	
	[[self nextResponder] tryToPerform:@selector(mouseDown:) with:event];
}

- (BOOL)isOpaque
{
	return NO;
}


- (BOOL)acceptsFirstMouse:(NSEvent *)theEvent
{
	return YES;
}

- (BOOL)acceptsFirstResponder
{
	return YES;
}

- (NSImage*)dragbarMaskImage
{
	return nil;
}

-(NSImage*)dragbarImage
{
	return nil;
}

-(NSImage*)dragbarGrayedOutImage
{
	return nil;
}

-(NSImage*)dragbarFillerImage
{
	return [NSImage imageNamed:DragbarFillerImageName];
}

-(void)drawDragBar: (NSRect)clippingRect
{
	NSRect dragbarRect = [self dragbarRect];
	
	//modifiy the clipping path to reflect portions of the dragbar where we will not draw
	[NSGraphicsContext saveGraphicsState];
	NSRectClip(dragbarRect);
	
	NSImage *dragbarMaskImage = [self dragbarMaskImage];
	
	NSImage *dragbarImage;
	if ([[self window] isMainWindow])
		dragbarImage = [self dragbarImage];
	else
		dragbarImage = [self dragbarGrayedOutImage];

	if (dragbarImage == nil)
		@throw([NSException exceptionWithName:@"SB Image Exception" reason:@"Unable to obtain drag bar image" userInfo:nil]);
	
	NSImage *dragbarFillerImage = [self dragbarFillerImage];
	
	NSAssert(dragbarImage && dragbarFillerImage, @"SBProtoFrameView not properly subclassed");
	
	NSSize dragbarImageSize = [dragbarImage size];
	NSSize dragbarFillerImageSize = [dragbarFillerImage size];
	
	//draw the background color first
	//this will be cropped appropriately by drawing the dragbarMaskImage over it with the appropriate mask
	if (ShowPattern)
		[GetUnregisteredPattern([[self organizer] color]) set];
	else
		[[[self organizer] color] set];
	
	NSRect colorRect = NSMakeRect(dragbarRect.origin.x,dragbarRect.origin.y,dragbarRect.size.width,20.0);
	clippingRect = NSIntersectionRect(clippingRect, colorRect);

	NSRectFill(clippingRect);
	
	
	//assumptions are that the window doesn't get smaller than it needs to be to draw
	//a complete title bar
	
	//first determine how many coils we can fit in window
	NSUInteger coilCount = dragbarRect.size.width/dragbarImageSize.width; //implicit casting
	
	//this is the amount we have to fill in on either side of the coils to finish
	//the title bar
	float leftOverMargin = (dragbarRect.size.width-coilCount*dragbarImageSize.width)/2.0;
	
	//leftOverMargin is also the inset from the left where we should start drawing
	//the coils
	NSPoint destinationPoint; //bottom left corner of the image
	
	destinationPoint.y = dragbarRect.origin.y;
	
	if (UseSimpleDragbar)
	{
		destinationPoint.x = 0;
		coilCount++;
	}
	else
	{
		destinationPoint.x = leftOverMargin;
	}
	
	//draw 'em
	NSUInteger counter;
	for (counter=0; counter<coilCount; counter++)
	{
		NSRect destinationRect = NSMakeRect(destinationPoint.x, destinationPoint.y,
											dragbarImageSize.width, dragbarImageSize.height);
		//ask super if we need to draw this
		if ([super needsToDrawRect: destinationRect])
		{
			//NSRectFill(destinationRect);
			
			[dragbarMaskImage drawAtPoint: destinationPoint fromRect: NSZeroRect
								operation: NSCompositeDestinationIn fraction: 1.0];
			
			
			[dragbarImage drawAtPoint: destinationPoint fromRect: NSZeroRect
							operation: NSCompositeSourceOver fraction: 1.0];
		}
		
		destinationPoint.x += dragbarImageSize.width;
	}
	
	if (UseSimpleDragbar)
	{
		[NSGraphicsContext restoreGraphicsState];
		return;
	}
	
	//figure out how many fillers we need to draw
	NSUInteger fillerCount = leftOverMargin/dragbarFillerImageSize.width; //implicit cast
	fillerCount++; //just to fill in the partial bits, could do checking to see if it's needed
	
	
	//draw the ones to the right
	//x and y should be good from where the loop ended
	for (counter=0; counter<fillerCount; counter++)
	{
		NSRect destinationRect = NSMakeRect(destinationPoint.x, destinationPoint.y,
											dragbarFillerImageSize.width,
											dragbarFillerImageSize.height);
		//ask super if we need to draw this
		if ([super needsToDrawRect: destinationRect])
		{
			//ZeroRect == full source image
			//NSRectFill(destinationRect);
			[dragbarFillerImage drawAtPoint: destinationPoint fromRect: NSZeroRect
								  operation: NSCompositeDestinationIn fraction: 1.0];
		}
		
		destinationPoint.x += dragbarFillerImageSize.width;
	}
	
	//and the ones to the left
	destinationPoint.x = leftOverMargin-dragbarFillerImageSize.width; //draw from right to left
	//y stays the same
	//NSRect sourceRect = NSZeroRect;
	//sourceRect.size.height = dragbarImageSize.height;
	
	for (counter=0; counter<fillerCount; counter++)
	{
		NSRect destinationRect = NSMakeRect(destinationPoint.x, destinationPoint.y,
											dragbarFillerImageSize.width,
											dragbarFillerImageSize.height);
		//ask super if we need to draw this
		if ([super needsToDrawRect: destinationRect])
		{
			//ZeroRect == full source image
			//1.0 == fully opaque
			//NSRectFill(destinationRect);
			[dragbarFillerImage drawAtPoint: destinationPoint fromRect: NSZeroRect
								  operation: NSCompositeDestinationIn fraction: 1.0];
		}
		
		destinationPoint.x -= dragbarFillerImageSize.width;
	}
	[NSGraphicsContext restoreGraphicsState];
}

-(void)drawBackground
{
	//else
	//	color = NotKeyMainColor;

	NSRect backgroundRect, borderlessRect = [self frameRectMinusBorder], dragbarRect = [self dragbarRect];
	
	backgroundRect.origin.x = borderlessRect.origin.x;
	backgroundRect.origin.y = borderlessRect.origin.y;
	backgroundRect.size.width = borderlessRect.size.width;
	backgroundRect.size.height = dragbarRect.origin.y-borderlessRect.origin.y;
	
	if (ShowPattern)
		[GetUnregisteredPattern([[self organizer] color]) set];
	else
		[[[self organizer] color] set];
	NSRectFill(backgroundRect);
}


-(void)positionSubviews
{
	[[self toolbarView] setFrame:[self toolbarRect]];
	[super addSubview: [self toolbarView]];
	
	if ([self isShaded])
		return;
	
	[[self mainView] setFrame:[self mainViewRect]];
	[super addSubview: [self mainView]];
	
	NSRect frame = [[self locationbarView] frame];
	frame.origin = [self frameRectMinusBorder].origin;
	frame.size.width = [self frameRectMinusBorder].size.width;
	[[self locationbarView] setFrame: frame];
	
	/*
	[[self locationbarView] setFrameOrigin:[self frameRectMinusBorder].origin];
	[[self locationbarView] s*/
	[super addSubview: [self locationbarView]];
}

-(NSNib*)nib
{
	return nil;
}


-(void)setColors
{
	NSColor *color = [[self organizer] color];
	
	if (ShowPattern)
		color = GetUnregisteredPattern(color);
	
	[(NSScrollView*)[self mainView] setBackgroundColor: color];
	
	
	[mainTextView setBackgroundColor: color];
	
	[mainTodoListView setPrimaryBackgroundColor: color];
	NSColor *sColor = [SPRTodoListView calculateSecondaryColor:[[self organizer] color]];
	if (ShowPattern)
		sColor = GetUnregisteredPattern(sColor);
	[mainTodoListView setSecondaryBackgroundColor:sColor];
}

-(NSRect)titleBarRect
{
	return NSZeroRect;
}

/****Accessors***/

- (SPROrganizer *)organizer
{
    return organizer;
}

- (SPRNotebook *)notebook
{
    return [organizer notebook];
}

- (NSScrollView *)mainView
{
	return mainView;
}

- (NSView*)documentView
{
	return [[self mainView] documentView];
}

- (NSView *)toolbarView
{
	return toolbarView;
}

- (NSView *)locationbarView
{
	return locationbarView;
}

-(SPRTextView*)mainTextView
{
	if (!mainTextView)
		[NSException raise:@"SBProtoFramViewException" format:@"Called -mainTextView but mainTextView is not set"];
		
	return mainTextView;
}

- (void)setMainTextView: (SPRTextView*)newTextView
{
	//these should only be set once)
	if (mainTextView)
		[NSException raise:@"SBProtoFramViewException" format:@"Called -setMainTextView: but mainTextView is already set"];
	
	mainTextView = newTextView;
}

- (BOOL)hasMainTextView
{
	return mainTextView != nil;
}

- (SPRTodoListView*)mainTodoListView
{
	if (!mainTodoListView)
		[NSException raise:@"SBProtoFramViewException" format:@"Called -mainTodoListView but mainTodoListView is not set"];
	
	return mainTodoListView;
}

- (void)setMainTodoListView:(SPRTodoListView*)tv
{
	if (mainTodoListView)
		[NSException raise:@"SBProtoFramViewException" format:@"Called -setMainTodoListView: but mainTodoListView is already set"];
		
	mainTodoListView = tv;
}

- (void)showMainTodoListView
{
	NSRect frame = {{0,0},[mainView contentSize]};
	
	[[self mainTodoListView] setFrame:frame];
	
	[[self mainView] setDocumentView:[self mainTodoListView]];
	[self setColors];
}

- (void)showMainTextView
{
	NSRect frame = {{0,0},[mainView contentSize]};
	
	[[self mainTextView] setFrame:frame];
	
	[[self mainView] setDocumentView:[self mainTextView]];
	[[self mainTextView] sizeToFit];
	[self setColors];
}

- (BOOL)hasMainTodoListView
{
	return mainTodoListView != nil;
}

- (NSRect)mainViewRect
{
	NSRect rect, toolbarRect = [self toolbarRect], borderlessRect = [self frameRectMinusBorder];
	
	rect.origin.x = borderlessRect.origin.x;
	rect.origin.y = borderlessRect.origin.y+([[self locationbarView] frame].size.height*1.1);
	rect.size.width = borderlessRect.size.width;//-kBigResizerLineInset;
	rect.size.height = toolbarRect.origin.y-rect.origin.y;
	return rect;
}

- (NSRect)toolbarRect
{
	NSRect rect, dragbarRect = [self dragbarRect];
	
	rect.size.width = dragbarRect.size.width;
	rect.size.height = [[self toolbarView] frame].size.height;
	rect.origin.x = dragbarRect.origin.x;
	rect.origin.y = dragbarRect.origin.y-rect.size.height;
	
	return rect;
}

- (NSRect)dragbarRect
{
	NSRect rect, borderlessRect = [self frameRectMinusBorder];
	rect.origin.x = borderlessRect.origin.x;
	rect.size.width = borderlessRect.size.width;
	NSImage *dragbarImage = [self dragbarImage];
	
	if (dragbarImage != nil)
		rect.size.height = [dragbarImage size].height;
	else
		rect.size.height = 0;
		
	rect.origin.y = (borderlessRect.size.height-rect.size.height)+borderlessRect.origin.y;
	
	return rect;
}

- (NSRect)visibleToolbarAndDragbarRect
{
	return NSUnionRect([self toolbarRect], [self dragbarRect]);
}

- (NSRect)frameRectMinusBorder		//everything except the thick border
{
	return NSZeroRect;
}

- (NSRect)unshadedFrameRectMinusBorder
{
	NSRect rect = [self frameRectMinusBorder];
	
	if ([self isShaded] == YES)
	{
		rect.origin.y -= deltaHeight;
		rect.size.height += deltaHeight;
	}
	return rect;
}

- (SBTitleView*)titleView
{
	return titleView;
}

- (BOOL)isShaded
{
    return isShaded;
}

- (void)setIsShaded:(BOOL)value
{
    if (isShaded != value)
	{
        isShaded = value;
		
		if (value == YES)
		{
			[[self mainView] removeFromSuperview];
			[[self locationbarView] removeFromSuperview];
		
			//always a positive value
			deltaHeight = [[self window] frame].size.height -
							([self dragbarRect].size.height+[self toolbarRect].size.height +
							 [self frameRectMinusBorder].origin.y);
			
			NSRect newFrame = [[self window] frame];
			newFrame.size.height-=deltaHeight;
			newFrame.origin.y += deltaHeight;
			[[self window] setFrame: newFrame display:YES];
		}
		else
		{
			[self addSubview: [self mainView]];
			[self addSubview: [self locationbarView]];
			
			[self positionSubviews];
			NSRect newFrame = [[self window] frame];
			newFrame.size.height +=deltaHeight;
			newFrame.origin.y -= deltaHeight;
			[[self window] setFrame: newFrame display:YES];
			//[self setNeedsDisplay:YES];
		}
		[organizer numberVisiblePages];		//Possible TODO, replace this with a notification
    }
}

- (float)deltaHeight
{
	return deltaHeight;
}

//call this after a change in dragbar style
-(void)adjustWindowHeight
{
	float heightChange = [self regularMinusSimpleDragbarHeight];
	if (UseSimpleDragbar)
		heightChange = -heightChange;
	
	NSRect frame = [[self window] frame];
	frame.size.height+=heightChange;
	frame.origin.y-=heightChange;
	[[self window] setFrame: frame display: YES];
	deltaHeight -= heightChange;
}


-(void)adjustWindowHeightIfShaded
{
	if (![self isShaded])
		return;

	float heightChange = [self regularMinusSimpleDragbarHeight];
	if (UseSimpleDragbar)
		heightChange = -heightChange;
	
	NSRect frame = [[self window] frame];
	frame.size.height+=heightChange;
	frame.origin.y-=heightChange;
	[[self window] setFrame: frame display: YES];
	deltaHeight -= heightChange;
}

-(float)regularMinusSimpleDragbarHeight
{
	//virtual
	return 0.0;
}

- (void)setNotebookTitleFieldStringValue:(NSString*)value
{
	[notebookTitleField setStringValue:value];
}

#pragma mark ACTIONS

- (IBAction)print:(id)sender
{
#if EXCEPTIONS_ON
	@try
	{
		SPRPage *page = [organizer pageForWindow:[self window]];
		[[NSExceptionHandler defaultExceptionHandler] setDelegate:nil];
		PrintingView *printingView = [[PrintingView alloc] init];
		[printingView addPageContents:[page contentsAsAttributedString] title:[page title]];
		[printingView finish];
		[printingView print:sender];
	}
	@catch (id exception)
	{
		NSLog(@"Caught exception during printing: %@", exception);
		NSLog(@"Ignoring exception");
	}
	@finally
	{
		[[NSExceptionHandler defaultExceptionHandler] setDelegate:[ErrorController sharedController]];
	}
#else
    SPRPage *page = [organizer pageForWindow:[self window]];
    PrintingView *printingView = [[PrintingView alloc] init];
    [printingView addPageContents:[page contentsAsAttributedString] title:[page title]];
    [printingView finish];
    [printingView print:sender];
#endif
}

- (IBAction)rename:(id)sender
{
	[titleView rename];
}

- (void)breakBindings
{
}

- (void)setSecurityButtonState:(int)state
{
	NSImage *i;
	if (state == NSOnState)
		i = [NSImage imageNamed:@"LockedLock"];
	else
		i = [NSImage imageNamed:@"UnlockedLock"];
	
	[securityButton setImage:i];
}
@end
