//
//  Calculator+Converter.h
//  SpiralBound
//
//  Created by Philip White on 2/11/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Calculator.h"

@interface Calculator (Converter)

-(NSMutableArray*)infixToPostfix:(NSString *)infixString;
-(NSString*)identifyNegationOperators:(NSString*)inString;
-(NSMutableArray*)getTokens: (NSString *)string;
-(NSMutableArray*)convertStringsToNumbers: (NSArray*)array;
-(NSMutableArray*)identifyExponents:(NSArray*)inTokens;
-(NSMutableArray*)identifyConstants:(NSArray*)inTokens;

@end
