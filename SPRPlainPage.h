//
//  SPRPlainPage.h
//  CommonSpiralbound
//
//  Created by Philip on 5/24/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SPRPage.h"

@class SPRLockableTextStorage;
@interface SPRPlainPage : SPRPage
{
	SPRLockableTextStorage *contentText;
}

@end
