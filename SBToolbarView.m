//
//  SBToolbarView.m
//  SpiralBound
//
//  Created by Philip White on 2/18/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "SBToolbarView.h"
#import "SBToolbarContentView.h"


@implementation SBToolbarView

- (void)awakeFromNib
{
	SBToolbarContentView *contentView = [[SBToolbarContentView alloc] initWithFrame:[[self contentView] frame]];
	
	NSArray *subviews = [[[self contentView] subviews] copy];
	NSEnumerator *enumerator = [subviews objectEnumerator];
	NSView *subview;
	
	while (subview = [enumerator nextObject])
	{
		[contentView addSubview:subview];
	}
	
	[self setContentView: contentView];
}


@end
