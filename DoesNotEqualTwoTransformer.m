//
//  DoesNotEqualTwoTransformer.m
//  SpiralBound Pro
//
//  Created by Philip White on 6/2/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "DoesNotEqualTwoTransformer.h"


@implementation DoesNotEqualTwoTransformer


+ (Class)transformedValueClass
{
	return [NSNumber class];
}

+ (BOOL)allowsReverseTransformation { return NO; }

- (id)transformedValue:(id)value
{
    if ([value intValue] == 2)
		return [NSNumber numberWithBool:NO];
	else
		return [NSNumber numberWithBool:YES];
}

@end
