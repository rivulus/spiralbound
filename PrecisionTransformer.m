//
//  PrecisionTransformer.m
//  SpiralBound
//
//  Created by Philip White on 2/16/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "PrecisionTransformer.h"

static NSDecimalNumber *Number;

@implementation PrecisionTransformer

+ (void) initialize
{
	Number = [[NSDecimalNumber alloc] initWithString:@"0.8888888888888888888888888888888888888888"];
}

+ (Class)transformedValueClass
{
	return [NSDecimalNumber class];
}

+ (BOOL)allowsReverseTransformation { return NO; }

- (id)transformedValue:(id)value
{
    id NumberHandler = [[NSDecimalNumberHandler alloc] initWithRoundingMode:NSRoundPlain
																	   scale:[value intValue]
															raiseOnExactness:NO
															 raiseOnOverflow:NO
															raiseOnUnderflow:NO
														 raiseOnDivideByZero:NO];
	
	return [Number decimalNumberByRoundingAccordingToBehavior:NumberHandler];
	
}

@end
