//
//  SPRProgressWindow.m
//  SpiralBound Pro
//
//  Created by Philip White on 4/11/18.
//

#import "SPRProgressWindow.h"

@interface SPRProgressWindow ()
{
	IBOutlet NSTextField *_textField;
	IBOutlet NSProgressIndicator *_progressIndicator;
}
@end

@implementation SPRProgressWindow

- (instancetype)init
{
	return [self initWithWindow:nil];
}

- (NSString *)windowNibName
{
	return @"SPRProgressWindow";
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
	[_progressIndicator startAnimation:nil];
}


@end
