//
//  SBApplication.m
//  SpiralBound
//
//  Created by Philip White on 2/17/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "SBApplication.h"
#import "SBController.h"
/*#import "SBController+Data.h"
#import "SBController+Delegate.h"
#import "SBController+Security.h"*/

@implementation SpiralBoundApplication

- (void)sendEvent:(NSEvent *)event
{
	LastEventTime = time(NULL);
	
	[super sendEvent:event];
	
	if (Inactive)
	{
		[gSBController applicationDidBecomeActive:nil];
	}
}


-(id)sbController
{
	return sbController;
}

@end
