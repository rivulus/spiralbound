//
//  SpiralWindow.h
//  SpiralBound
//
//  Created by Philip White on 2/1/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SBTypes.h"


@class SPRTextView,SPROrganizer,SPRTodoListView, SBTitleView;
@interface SpiralBoundWindow : NSWindow
{
	
}

+ (NSSize)minSize;
- (id)initWithContentRect:(NSRect)contentRect style: (SBWindowStyle)style organizer: (SPROrganizer*)organizer;

//overridden methods


- (BOOL)canBecomeKeyWindow;
- (BOOL)canBecomeMainWindow;

- (NSRect)contentRectForFrameRect:(NSRect)windowFrame;

+ (NSRect)frameRectForContentRect:(NSRect)windowContentRect
						styleMask:(NSUInteger)windowStyle;

-(SBTitleView*)titleView;
-(NSView*)mainView; //just calls the same method in the frame instance
-(NSView*)documentView; //calls the same method in the frame instance
- (SPRTodoListView*)mainTodoListView;
- (void)setMainTodoListView:(SPRTodoListView*)tv;
- (SPRTextView*)mainTextView;
- (void)setMainTextView: (SPRTextView*)newTextView;
- (void)showMainTextView;
- (void)showMainTodoListView;

- (void)setTitle: (NSString*)title;
- (void)setIsTitleBold: (BOOL)isBold;

- (void)setFrame:(NSRect)windowFrame display:(BOOL)displayViews animate:(BOOL)performAnimation rate:(NSTimeInterval)rate;

- (void)buldge;

- (void)setFloater:(BOOL)floater;
- (BOOL)isFloater;
- (IBAction)toggleFloater:(id)sender;
- (IBAction)dummy:(id)sender;
- (void)setSecurityButtonState:(int)state;
@end
