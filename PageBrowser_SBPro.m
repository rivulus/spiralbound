//
//  PageBrowser.m
//  SpiralBound Pro
//
//  Created by Philip White on 5/5/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "PageBrowser_SBPro.h"
#import "OrganizerIncludes.h"
#import "DataLayerIncludes.h"
#import "SPRTextView.h"
#import "SPRTodoListView.h"
#import "SPRLockableTextStorage.h"
#import "PageBrowser+Filtering_SBPro.h"
#import "RIVTools.h"
#import "SBController.h"
#import "SPRPage.h"
#import "SBConstants.h"
#import "NSAttributedString+Additions.h"


@implementation PageBrowser

-(id)init
{
	self = [super init];
	
	if (self)
	{
		static NSNib *nib=nil;
		
		if (nib == nil)
			nib = [[NSNib alloc] initWithNibNamed: @"PageBrowser" bundle: nil];
		
        NSArray *localTopLevelObjects = nil;
		BOOL success = [nib instantiateNibWithOwner:self topLevelObjects:&localTopLevelObjects];
		
		if (!success)
		{
			[NSException raise:@"PageBrowserException" format:@"Could not instantiate PageBrowser nib."];
			return nil;
		}
		
		topLevelObjects = localTopLevelObjects;
		
		NSRect contentFrame = {{0,0},[scrollView contentSize]};
		todoListView = [[SPRTodoListView alloc] initWithFrame:contentFrame];
		textView = [[SPRTextView alloc] initWithFrame:contentFrame];
		
		
		[todoListView setDelegate:self];
		[todoListView setAutoresizingMask:NSViewWidthSizable];
		
		[textView setVerticallyResizable:YES];
		[textView setHorizontallyResizable:NO];
		[textView setAutoresizingMask:NSViewWidthSizable];
		
		[pagesView registerForDraggedTypes:[NSArray arrayWithObject:SPRHashArrayPasteboardType]];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(notebookDidChangeLength:)
													 name:NotebookDidChangeLengthNotification
												   object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(notebookCountDidChange:)
													 name:NotebookCountDidChangeNotification
												   object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(pageTitleDidChange:)
													 name:PageTitleDidChangeNotification
												   object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(pageArrangementDidChange:)
													 name:PageArrangementDidChangeNotification
												   object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(pageWillBeDeleted:)
													 name:PageWillBeDeletedNotification
												   object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(notebookWillBeDeleted:)
													 name:NotebookWillBeDeletedNotification
												   object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(notebookColorDidChange:)
													 name:NotebookColorDidChangeNotification
												   object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(notebookTitleDidChange:)
													 name:NotebookTitleDidChangeNotification
												   object:nil];							   
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(windowResignedKey:)
													 name:NSWindowDidResignKeyNotification
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(windowBecameKey:)
													 name:NSWindowDidBecomeKeyNotification
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(selectedPageWasEdited:)
													 name:NSTextDidChangeNotification
												   object:textView];
		
		[pagesView setAction:@selector(cellWasSelected:)];
		[pagesView setDoubleAction: @selector(cellWasDoubleClicked:)];
		[pagesView setTarget:self];
		[pagesView setReusesColumns:NO];
		[pagesView setMaxVisibleColumns:2];
		[pagesView setMinColumnWidth:0];
		
		//setup filter related things
		filteredContents = [NSMutableDictionary new];
		filterValue = [NSMutableString new];
		
		filterMatchRanges = [NSMutableDictionary new];
		
		[self loadUnfiltered];
	}
	
	return self;
}

-(NSView*)view
{
	return browserView;
}


-(void)dealloc
{
	//[contentsViewStorage release];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(IBAction)cellWasSelected:(id)sender
{
	NSArray *cells = [pagesView selectedCells];
	NSBrowserCell *cell=nil;
	if ([cells count] == 1)
	{
		cell = [cells objectAtIndex:0];
	}
	
	if (cell == nil)
	{
		[self setSelectedPage:nil];
		
		if ([pagesView selectedColumn] != 1)
			[textView setBackgroundColor:[NSColor whiteColor]];
		
		return;
	}
	
	NSString *notebookTitle = [[pagesView selectedCellInColumn:0] title];
	SPROrganizer *organizer = [SPROrganizer organizerForTitle:notebookTitle];
	
	if ([cell isLeaf])
	{
		NSUInteger filteredIndex = [pagesView selectedRowInColumn:1];
		NSUInteger pageIndex = [[[filteredContents objectForKey:notebookTitle] objectAtIndex:filteredIndex] unsignedIntValue];
		
		[self setSelectedPage:[[[organizer notebook] pages] objectAtIndex:pageIndex]];
	}
	else
	{
		[self setBackgroundColor:[organizer color]];
		
		[self setSelectedPage:nil];
	}
}

-(void)setBackgroundColor:(NSColor*)color
{
	if (ShowPattern)
		color = GetUnregisteredPattern(color);
	
	[scrollView setBackgroundColor: color];
	
	[textView setBackgroundColor: color];
	
	[todoListView setPrimaryBackgroundColor: color];
	NSColor *sColor = [SPRTodoListView calculateSecondaryColor:color];
	if (ShowPattern)
		sColor = GetUnregisteredPattern(sColor);
	[todoListView setSecondaryBackgroundColor:sColor];
}

-(IBAction)cellWasDoubleClicked:(id)sender
{
	NSBrowserCell *cell = [pagesView selectedCell];
	
	if ([cell isLeaf])
	{
		NSString *notebookTitle = [[pagesView selectedCellInColumn:0] title];
		SPROrganizer *organizer = [SPROrganizer organizerForTitle:notebookTitle];
		
		NSUInteger filteredIndex = [pagesView selectedRowInColumn:1];
		NSUInteger pageIndex = [[[filteredContents objectForKey:notebookTitle] objectAtIndex:filteredIndex] unsignedIntValue];
		
		SPRPage *page = [[[organizer notebook] pages] objectAtIndex:pageIndex];
		
		[SPROrganizer raisePage:page range:NSMakeRange(NSNotFound,0) buldgeMain:YES];
	}
	else
	{
		NSString *notebookTitle = [[pagesView selectedCellInColumn:0] title];
		SPROrganizer *organizer = [SPROrganizer organizerForTitle:notebookTitle];
		[organizer raiseNotebook];
	}
}

-(NSBrowser*)pagesView
{
	return pagesView;
}

-(SPRTextView*)textView
{
	return textView;
}

-(void)createNewPageOfClass:(Class)c
{
	NSString *notebookTitle = [[pagesView selectedCellInColumn:0] title];
	
	if (notebookTitle == nil)
	{
		NSRunAlertPanel(@"No Notepad Selected", @"Please select a notepad in which to make the new page.",nil,nil,nil);
		return;
	}
	
	SPRNotebook *notebook = [[SPROrganizer organizerForTitle:notebookTitle] notebook];
	
	//determine the correct index based on selection
	//either choose the index after the last selection or just place at the end
	id newPage;
	if ([[pagesView selectedCell] isLeaf])
	{
		NSUInteger cellIndex = [[pagesView matrixInColumn:1] selectedRow];
		NSUInteger pageIndex = [self pageIndexForCellIndex:cellIndex];
		newPage = [notebook newPageOfClass:c atIndex:pageIndex+1];
	}
	else
	{
		newPage = [notebook newPageOfClass:c];
	}
	
	[searchField setStringValue:@""];
	[searchField sendAction:[searchField action] to:[searchField target]];
	
	
	[self performSelector:@selector(selectCell:)
		  withObject:newPage
		  afterDelay:0.0];
}

//this method is called delayedly to select a new created notebook or page
//the object is either an NSNumber with a cell index or an NSString with the title of a notebook
-(void)selectCell:(id)object
{
	if ([object isKindOfClass:[SPRPage class]])
	{
		NSArray *cells = [[pagesView matrixInColumn:1] cells];
		NSUInteger i;
		SPRNotebook *notebook = [[SPROrganizer organizerForTitle:[[pagesView selectedCellInColumn:0] title]] notebook];
		NSArray *pages = [notebook pages];
		for (i=0; i < [cells count]; i++)
		{
			//NSLog(@"%@",[[cells objectAtIndex:i] title]);
			NSUInteger pageIndex = [self pageIndexForCellIndex:i];
			if ([pages objectAtIndex:pageIndex] == object)
			{
				[pagesView selectRow:i inColumn:1];
				break;
			}
		}
		if (i==[cells count])
			//if we got here the cell must be hidden (because of filtering)
			return;
	}
	else if ([object isKindOfClass:[NSString class]])
	{
		NSArray *cells = [[pagesView matrixInColumn:0] cells];
		NSUInteger i;
		for (i=0; i < [cells count]; i++)
		{
			id cell = [cells objectAtIndex:i];
			if ([[cell title] isEqualToString:object])
			{
				[pagesView selectRow:i inColumn:0];
				break;
			}
		}
	}
	
	[self cellWasSelected:nil];
}

#pragma mark NSTEXT_NOTIFICATION

-(void)selectedPageWasEdited:(NSNotification*)note
{
	[self refilterSelectedPage];
}

#pragma mark NSBROWSER_DELEGATE

- (void)browser:(NSBrowser *)sender createRowsForColumn:(NSInteger)column inMatrix:(NSMatrix *)matrix
{
	if (column == 0)	//the SPRNotebook column
	{
		NSArray *filteredTitles = [filteredContents allKeys];
		NSUInteger count = [filteredContents count];
		
		[matrix renewRows:count columns:1];
		NSInteger i;
		
		for (i=0; i<count; i++)
		{
			id cell = [matrix cellAtRow:i column:0];
			
			NSString *notebookTitle = [filteredTitles objectAtIndex:i];
			//gray if notebook contents have all been filtered out
			NSColor *textColor = ([[filteredContents objectForKey:notebookTitle] count] == 0) ? [NSColor grayColor] :
																								[NSColor blackColor];
			NSAttributedString *attrTitle = [[NSAttributedString alloc] initWithString:notebookTitle
																		attributes:
													[NSDictionary dictionaryWithObject:textColor forKey:NSForegroundColorAttributeName]];
			
			SPROrganizer *organizer = [SPROrganizer organizerForTitle:notebookTitle];
			
			//[cell setTitle: notebookTitle];
			[cell setAttributedStringValue:attrTitle];
			[cell setLeaf:NO];
			
			NSRect imageRect = {0,0,12.0,12.0};
			
			NSImage *image = [[NSImage alloc] initWithSize:imageRect.size];
			[image lockFocus];
			{
				NSBezierPath *borderPath = [NSBezierPath bezierPathWithRect:imageRect];
				NSColor *borderColor = [NSColor blackColor];
				
				[[organizer color] set];
				NSRectFill(imageRect);
				
				[borderColor set];
				[borderPath stroke];
			}
			[image unlockFocus];
			
			[cell setImage:image];
		}
			
	}
	else if (column == 1)	//the pages column
	{
		NSString *notebookTitle = [[pagesView selectedCellInColumn:0] title];
		SPROrganizer *organizer = [SPROrganizer organizerForTitle:notebookTitle];
		NSArray *pageIndexArray = [filteredContents objectForKey:notebookTitle];
		NSUInteger count = [pageIndexArray count];
		
		[matrix renewRows:count columns:1];
		NSInteger i;
		for (i=0; i<count; i++)
		{
			id cell = [matrix cellAtRow:i column:0];
			NSInteger index = [[pageIndexArray objectAtIndex:i] intValue];
			NSString  *title = [organizer titleOfPageAtIndex:index];
			[cell setTitle: title];
			[cell setLeaf:YES];
		}
			
	}
	else
	{
		[NSException raise:@"Invalid Column in Notepad Sorter" format:@"Invalid column #%i created in the Notepad Sorter", column];
	}

}


- (BOOL)browser:(NSBrowser *)sender isColumnValid:(NSInteger)column
{
	if (column == 0 && notebooksColumnNeedsUpdate)
	{
		notebooksColumnNeedsUpdate=NO;
		return NO;
	}
	else if (column == 1 && pagesColumnNeedsUpdate)
	{
		pagesColumnNeedsUpdate=NO;
		return NO;
	}
	return YES;
}


- (BOOL)browser:(NSBrowser *)sender canDragRowsWithIndexes:(NSIndexSet *)rowIndexes inColumn:(NSInteger)columnIndex withEvent:(NSEvent *)dragEvent
{
	return YES;
	/*if (columnIndex == 1)
		return YES;
	else
		return NO;
	 */
}



- (BOOL)browser:(NSBrowser *)sender writeRowsWithIndexes:(NSIndexSet *)rowIndexes inColumn:(NSInteger)columnIndex toPasteboard:(NSPasteboard *)pasteboard
{
	NSUInteger count = [rowIndexes count];
	NSMutableArray *dataArray = [NSMutableArray arrayWithCapacity: count];
	NSString *type;
	
	if (columnIndex == 0)
	{
		//write an array of NSString containing notebook titles
		//as of 7/2010, only one notebook can be selected and therefore dragged at a time
		//however, for expandability assume multiple can be dragged
		
		NSUInteger index;
		for (index = [rowIndexes firstIndex]; index != NSNotFound; index = [rowIndexes indexGreaterThanIndex:index])
		{
			NSString *title = [[[pagesView matrixInColumn:0] cellAtRow:index column:0] title];
			[dataArray addObject:title];
		}
		type = SPRTitleArrayPasteboardType;
	}
	else
	{
		//write an array of NSNumbers containing page hashes
		NSArray *pages;
		
		NSString *notebookTitle = [[pagesView selectedCellInColumn:0] title];
		SPROrganizer *organizer = [SPROrganizer organizerForTitle:notebookTitle];
		
		pages = [[organizer notebook] pages];
		
		NSArray *pageIndexes = [filteredContents objectForKey:notebookTitle];
		
		NSUInteger index = [rowIndexes firstIndex];
		
		//convert the row index into a page index
		NSUInteger pageIndex = [[pageIndexes objectAtIndex:index] intValue];
		[dataArray addObject:[NSNumber numberWithInt:[[pages objectAtIndex:pageIndex] hash]]];
		
		while ((index = [rowIndexes indexGreaterThanIndex:index]) != NSNotFound)
		{
			//convert the row index into a page index
			NSUInteger pageIndex = [[pageIndexes objectAtIndex:index] intValue];
			
			[dataArray addObject:[NSNumber numberWithInt:[[pages objectAtIndex:pageIndex] hash]]];
		}
		type = SPRHashArrayPasteboardType;
	}
	
	NSData *dataArrayData = [NSKeyedArchiver archivedDataWithRootObject:dataArray];
	
	//declare all types here but only provide data for the internal types (SPRTitleArrayPasteboardType or
	//																	   SPRHashArrayPasteboardType)
	//if another type is requested, provide it in -pasteboard:provideDataForType: below
	//which uses one of the internal types to generate the data
	[pasteboard declareTypes:[NSArray arrayWithObjects:type,NSRTFDPboardType,NSRTFPboardType, NSStringPboardType,nil]
					   owner:self];
	[pasteboard setData: dataArrayData forType: type];
	
	{		
		[pagesView setDraggingSourceOperationMask:([[NSApp currentEvent] modifierFlags] & NSAlternateKeyMask) ?
												  NSDragOperationCopy : NSDragOperationMove
										 forLocal:YES];
		[pagesView setDraggingSourceOperationMask:NSDragOperationCopy
										 forLocal:NO];
	}
	
	return YES;
}

NSAttributedString* CombinePageContents(NSArray *pages)
{
	NSMutableAttributedString *attr = [NSMutableAttributedString new];
	NSUInteger i;
	for (i=0; i<[pages count]; i++)
	{
		if (i>0)
			[attr replaceCharactersInRange:NSMakeRange([attr length],0)
								withString:@"\f"];
		[attr appendAttributedString:[[pages objectAtIndex:i] contentsAsAttributedString]];
	}
	return attr;
}

- (void)pasteboard:(NSPasteboard *)sender provideDataForType:(NSString *)type
{
	//start by generating one attributed string for the pboard contents. Separate pages with page breaks
	NSMutableAttributedString *attrStr = [NSMutableAttributedString new];
	
	if (![NSApp isRunning])
	{
		[attrStr setAttributedString:[NSAttributedString new]];
	}
    else
    {
	
        NSString *baseType;
        for (baseType in [sender types]) //apparently cocoa inserts some of it's own types in the array
                                         //so we need to skip some other types
        {
            if ([baseType isEqualToString:SPRHashArrayPasteboardType] ||
                [baseType isEqualToString:SPRTitleArrayPasteboardType])
                break; 
        }
        
        NSAssert(baseType,@"Pasteboard did not contain base data type");
        
        if ([baseType isEqualToString:SPRHashArrayPasteboardType])
        {
            NSArray *hashes = [NSKeyedUnarchiver unarchiveObjectWithData:[sender dataForType:SPRHashArrayPasteboardType]];
            NSArray *pages = [SPRNotebook pagesWithHashes:hashes];
            
            [attrStr setAttributedString:CombinePageContents(pages)];
        }
        else if ([baseType isEqualToString:SPRTitleArrayPasteboardType])
        {
            //here, start each notebook with the underlines notebook title and two line breaks
            NSArray *titles = [NSKeyedUnarchiver unarchiveObjectWithData:[sender dataForType:SPRTitleArrayPasteboardType]];
            NSUInteger i;
            
            for (i=0; i<[titles count]; i++)
            {
                id title = [titles objectAtIndex:i];
                if (i>0)
                    [attrStr replaceCharactersInRange:NSMakeRange([attrStr length],0)
                                           withString:@"\f"];
                
                NSDictionary *ul = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:NSUnderlineStyleSingle]
                                                               forKey:NSUnderlineStyleAttributeName];

                NSAttributedString *attrTitle = [[NSAttributedString alloc] initWithString:title
                                                                                attributes:ul];
                [attrStr appendAttributedString:attrTitle];
                [attrStr replaceCharactersInRange:NSMakeRange([attrStr length],0)
                                       withString:@"\n\n"];
                
                SPRNotebook *nb = [SPRNotebook notebookWithTitle:title];
                [attrStr appendAttributedString:CombinePageContents([nb pages])];
            }
        }
        else
        {
            [NSException raise:@"PageBrowserException" format:@"Unrecognized pboard type: %@", [[sender types] objectAtIndex:0]];
        }
    }
    
    //now turn that attributed string into an RTFD, RTF or plain text data object
    NSData *data = [attrStr dataUsingDocumentType:[SPRPasteboardToDocumentTypeTable objectForKey:type]];
    [sender setData:data forType:type];
}

- (NSDragOperation)browser:(NSBrowser *)sender validateDrop:(id <NSDraggingInfo>)dragInfo proposedRow:(NSInteger *)inout_dropReferenceRowIndex column:(NSInteger *)inout_dropColumnIndex dropOperation:(NSBrowserDropOperation *) inout_dropRowRelativeLocation
{
	
	if (*inout_dropColumnIndex == 0)
	{
		if (*inout_dropReferenceRowIndex >= [[pagesView matrixInColumn:0] numberOfRows])
			return NSDragOperationNone;
		
		//reject the drop if dragging to the currently selected notepad
		if (*inout_dropReferenceRowIndex == [[dragInfo draggingSource] selectedRowInColumn:0])
			return NSDragOperationNone;
		
		*inout_dropRowRelativeLocation = NSBrowserDropOn;
		
		return [dragInfo draggingSourceOperationMask];
	}
	else if (*inout_dropColumnIndex == 1)
	{
		*inout_dropRowRelativeLocation = NSBrowserDropAbove;
		return [dragInfo draggingSourceOperationMask];
	}

	return NSDragOperationNone;
}


- (BOOL)browser:(NSBrowser *)sender acceptDrop:(id <NSDraggingInfo>)dragInfo atRow:(NSInteger)dropReferenceRowIndex column:(NSInteger)dropColumnIndex dropOperation:(NSBrowserDropOperation)dropRowRelativeLocation
{
	SPROrganizer *destination;
	NSString *notebookTitle;
	NSUInteger firstPageIndex;
	
	if (dropColumnIndex == 0)
	{
		notebookTitle = [[[pagesView matrixInColumn:0] cellAtRow:dropReferenceRowIndex column:0] title];
		firstPageIndex = UINT32_MAX;
	}
	else
	{
		notebookTitle = [[pagesView selectedCellInColumn:0] title];
		//drop the pages either at the beginning or after the previous shown page
		if (dropReferenceRowIndex == 0)
		{
			firstPageIndex=0;
		}
		else
		{
			firstPageIndex = [[[filteredContents objectForKey:notebookTitle] objectAtIndex:dropReferenceRowIndex-1] intValue];
			firstPageIndex++;
		}

	}
	
	if (notebookTitle == nil)
		return NO;
	
	destination = [SPROrganizer organizerForTitle:notebookTitle];
	
	NSPasteboard *pasteboard = [dragInfo draggingPasteboard];
	NSData *data = [pasteboard dataForType:SPRHashArrayPasteboardType];
	NSArray *hashArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
	
	if ([dragInfo draggingSourceOperationMask] & NSDragOperationCopy)
		[SPROrganizer copyPagesWithHashes:hashArray destinationOrganizer:destination firstPageIndex:firstPageIndex];
	else
		[SPROrganizer movePagesWithHashes:hashArray destinationOrganizer:destination firstPageIndex:firstPageIndex];
	
	return YES;
}


//not a delegate method, but a notification from SPRNotebook
-(void)notebookDidChangeLength:(NSNotification*)notification
{
	SPRNotebook *notebook = [notification object];
	
	if ([[notebook title] isEqualToString: [[pagesView selectedCellInColumn: 0] title]])
	{
		pagesColumnNeedsUpdate = YES;
	}
	
	[self loadUnfiltered];
	[self filter];
	[pagesView validateVisibleColumns];
}

-(void)pageWillBeDeleted:(NSNotification*)notification
{
	if ([pagesView selectedColumn] == 0)
		return;
	
	SPRPage *page = [notification object];
	
	//check if the page being deleted is currently being shown
	if (page == selectedPage)
	{
		[self setSelectedPage:nil];
	}
}

-(void)notebookWillBeDeleted:(NSNotification*)notification
{
	SPRNotebook *notebook = [notification object];
	
	if ([[notebook title] isEqualToString: [[pagesView selectedCellInColumn: 0] title]])
	{
		[textView setBackgroundColor:[NSColor whiteColor]];
	}
	
	//[self loadUnfiltered];
	//[self filter];
}

-(void)notebookCountDidChange:(NSNotification*)notification
{
	notebooksColumnNeedsUpdate = YES;
	
	[self loadUnfiltered];
	[self filter];
	
	[pagesView validateVisibleColumns];
}

-(void)notebookColorDidChange:(NSNotification*)notification
{
	notebooksColumnNeedsUpdate = YES;
	SPRNotebook *notebook = [notification object];
	
	if (selectedPage && [[notebook pages] containsObject:selectedPage])
	{
		SPROrganizer *org = [SPROrganizer organizerForNotebook:notebook];
		[self setBackgroundColor:[org color]];
	}
	
	[pagesView validateVisibleColumns];
}

-(void)notebookTitleDidChange:(NSNotification*)notification
{
	notebooksColumnNeedsUpdate = YES;
	
	[self loadUnfiltered];
	[self filter];
	
	[pagesView validateVisibleColumns];
}

-(void)pageTitleDidChange:(NSNotification*)notification
{
	SPRPage *page = [notification object];
	SPROrganizer *organizer = [SPROrganizer organizerForPage:page];
	
	if ([[organizer title] isEqualToString: [[pagesView selectedCellInColumn: 0] title]])
	{
		pagesColumnNeedsUpdate = YES;
	}
	
	[self loadUnfiltered];
	[self filter];
	
	[pagesView validateVisibleColumns];
}

-(void)pageArrangementDidChange:(NSNotification*)notification
{
	SPRNotebook *notebook = [notification object];
	SPROrganizer *organizer = [SPROrganizer organizerForNotebook:notebook];
	
	if ([[organizer title] isEqualToString: [[pagesView selectedCellInColumn: 0] title]])
	{
		pagesColumnNeedsUpdate = YES;
	}
	
	[self loadUnfiltered];
	[self filter];
	
	[pagesView validateVisibleColumns];
}

/*-(void)pageSecurityDidChange:(NSNotification*)notification
{
	NSBrowserCell *cell = [pagesView selectedCell];
	
	NSString *notebookTitle = [[pagesView selectedCellInColumn:0] title];
	SPROrganizer *organizer = [SPROrganizer organizerForTitle:notebookTitle];
	
	if ([cell isLeaf])
	{
		NSUInteger pageIndex = [pagesView selectedRowInColumn:1];
		SPRPage *page = [[[organizer notebook] pages] objectAtIndex:pageIndex];
		if (page == [notification object])
		{
			[textView setLocked: [page isSecure]];
		}
	}
}*/

-(BOOL)isSelected
{
	NSResponder *responder = [[pagesView window] firstResponder];
	
	if ([responder isKindOfClass:[NSView class]])
		return [(NSView*)responder isDescendantOf:pagesView];
	
	return NO;
}


-(void)deleteSelectionWithWarning:(BOOL)warn
{
	NSArray *cells = [pagesView selectedCells];
	NSUInteger cellCount = [cells count];
	if (cellCount == 0)
		return;
		
	//find out what column they are in
	if ([pagesView selectedColumn] == 0) //notebooks are selected
	{
		if (cellCount == 1)
		{
			NSString *title = [[cells objectAtIndex:0] title];
			
			if ([[SPROrganizer organizers] count] == 1)
			{
				NSRunAlertPanel(@"You may not delete the last notepad!", @"If you would like to delete this notepad, create a new notepad first.", nil, nil, nil);
				return;
			}
			
			BOOL shouldDelete;
			if (warn)
				shouldDelete = NSRunAlertPanel(@"Delete Notepad?", @"Are you sure you would like to delete the notepad \"%@\"? The file containg this notepad's data will be moved to the trash.", @"Cancel", @"Delete", nil, title) == NSAlertAlternateReturn;
			else
				shouldDelete = YES;
			
			if (shouldDelete)
			{
				[SPROrganizer deleteNotebookWithOrganizer:[SPROrganizer organizerForTitle:title]];
			}
		}
		else
		{
			/*Currently selecting multiple notepads is not supported so this code won't be called*/
			BOOL shouldDelete;
			if (warn)
				shouldDelete = NSRunAlertPanel(@"Delete Notepads?", @"Are you sure you would like to delete the selected notepads? All pages within those notepads will be permanently erased.", @"Cancel", @"Delete", nil) == NSAlertAlternateReturn;
			else
				shouldDelete = YES;
			
			if (shouldDelete)
			{
				NSMutableArray *organizers = [NSMutableArray arrayWithCapacity:cellCount];
				
				NSEnumerator *enumerator = [cells objectEnumerator];
				NSCell *cell;
				while (cell = [enumerator nextObject])
				{
					[organizers addObject:[SPROrganizer organizerForTitle:[cell title]]];
				}
				NSUInteger i;
				for (i=0; i<cellCount; i++)
				{
					[SPROrganizer deleteNotebookWithOrganizer:[organizers objectAtIndex:i]];
				}
			}
		}
	}
	else	//pages are selected
	{
		NSString *notebookTitle = [[pagesView selectedCellInColumn:0] title];
		SPROrganizer *organizer = [SPROrganizer organizerForTitle:notebookTitle];
		
		if (cellCount == 1)
		{
			NSString *title = [[cells objectAtIndex:0] title];
			
			BOOL shouldDelete;
			if (warn)
				shouldDelete = NSRunAlertPanel(@"Delete Page?", @"Are you sure you would like to permanently delete the page \"%@\"? ", @"Cancel", @"Delete", nil, title) == NSAlertAlternateReturn;
			else
				shouldDelete = YES;
			
			if (shouldDelete)
			{
				NSUInteger filteredIndex = [pagesView selectedRowInColumn:1];
				NSUInteger pageIndex = [[[filteredContents objectForKey:notebookTitle] objectAtIndex:filteredIndex] unsignedIntValue];
				
				[organizer removePageAtIndex: pageIndex];
				
				[self setSelectedPage:nil];
			}
		}
		else
		{
			
			BOOL shouldDelete;
			if (warn)
				shouldDelete = NSRunAlertPanel(@"Delete Pages?", @"Are you sure you would like to permanently delete the selected pages?", @"Cancel", @"Delete", nil) == NSAlertAlternateReturn;
			else
				shouldDelete = YES;
			
			
			if (shouldDelete)
			{
				NSArray *allPages = [[organizer notebook] pages];
				NSMutableArray *selectedPages = [NSMutableArray arrayWithCapacity:cellCount];
				
				NSEnumerator *enumerator = [cells objectEnumerator];
				NSCell *cell;
				while (cell = [enumerator nextObject])
				{
					NSInteger filteredIndex, dummy;
					[[pagesView matrixInColumn:1] getRow:&filteredIndex column:&dummy ofCell:cell];
					NSUInteger pageIndex = [[[filteredContents objectForKey:notebookTitle] objectAtIndex:filteredIndex] unsignedIntValue];

					[selectedPages addObject: [allPages objectAtIndex:pageIndex]];
				}
				NSUInteger i;
				for (i=0; i<cellCount; i++)
				{
					[organizer removePage:[selectedPages objectAtIndex:i]];
				}
			}
		}
	}
}

-(void)setSelectedPage:(SPRPage*)newSelection
{	
	NSLayoutManager *lm = [textView layoutManager];
	if (selectedPage) //if there was a selection
	{
		[selectedPage autotitle];
		if ([selectedPage isKindOfClass:[SPRPlainPage class]])
		{
			[lm removeTemporaryAttribute:NSBackgroundColorAttributeName
					   forCharacterRange:NSMakeRange(0,[[textView textStorage] length])];
		}
		else
		{
			//remove previous notificaiton registrations
			[[NSNotificationCenter defaultCenter] removeObserver:self
															name:SPRTodoItemsCountDidChange
														  object:nil];
			[[NSNotificationCenter defaultCenter] removeObserver:self
															name:SPRTodoItemsOrderDidChange
														  object:nil];
			[[NSNotificationCenter defaultCenter] removeObserver:self
															name:SPRTodoItemStateDidChange
														  object:nil];
			[todoListView removeTemporaryHighlights];		
		}
	}
	
	selectedPage = newSelection;
	
	if (selectedPage) //if there is now a selection
	{
		NSRect frame = [textView frame];
		frame.size.width = [scrollView contentSize].width;
		
		if ([selectedPage isKindOfClass:[SPRPlainPage class]])
		{
			[textView replaceTextStorage:[selectedPage contentText]];
			NSArray *ranges = [filterMatchRanges objectForKey:[NSNumber numberWithInt:[selectedPage hash]]];
			if (ranges)
			{
				NSColor *hc = [NSColor selectedTextBackgroundColor];
				for (NSValue *robj in ranges)
				{
					NSRange r = [robj rangeValue];
					[lm addTemporaryAttribute:NSBackgroundColorAttributeName value:hc forCharacterRange:r];
				}
			}
			
			[textView setFrame:frame];
			[scrollView setDocumentView:textView];
			[textView sizeToFit];
		}
		else //Todo Page
		{
			[[NSNotificationCenter defaultCenter] addObserver:self
													 selector:@selector(todoItemsReload:)
														 name:SPRTodoItemsCountDidChange
													   object:nil];
			
			[[NSNotificationCenter defaultCenter] addObserver:self
													 selector:@selector(todoItemsReload:)
														 name:SPRTodoItemsOrderDidChange
													   object:nil];
			
			
			[[NSNotificationCenter defaultCenter] addObserver:self
													 selector:@selector(todoItemStateDidChange:)
														 name:SPRTodoItemStateDidChange
													   object:nil];
			
			[todoListView setFrame:frame];
			[scrollView setDocumentView:todoListView];
			[self loadTodoListView];
			
			NSArray *ranges = [filterMatchRanges objectForKey:[NSNumber numberWithInt:[selectedPage hash]]];
			if (ranges)
			{
				for (NSValue *robj in ranges)
				{
					NSRange r = [robj rangeValue];
					[todoListView addTemporaryHighlightForRange:r];
				}
			}
		}
	}
	else //no new selection
	{
		[textView replaceTextStorage:nil]; //this causes the shared empty storage to be used
		//[textView replaceTextStorage:[[SPRLockableTextStorage new] autorelease]];
		[scrollView setDocumentView:nil];
	}
}

-(void)updateHighlights
{
	if (selectedPage)
	{
		//one of these will be nil, we'll send messages to both anyway
		NSLayoutManager *lm=nil;
		SPRTodoListView *tlv=nil;
		
		if ([selectedPage isKindOfClass:[SPRTodoPage class]])
			tlv = todoListView;
		else
			lm = [textView layoutManager];
		
		[lm removeTemporaryAttribute:NSBackgroundColorAttributeName
				   forCharacterRange:NSMakeRange(0,[[textView textStorage] length])];
		[tlv removeTemporaryHighlights];
				   		   
		NSArray *ranges = [filterMatchRanges objectForKey:[NSNumber numberWithInt:[selectedPage hash]]];
		if (ranges)
		{
			NSColor *hc = [NSColor selectedTextBackgroundColor];
			for (NSValue *robj in ranges)
			{
				NSRange r = [robj rangeValue];
				[lm addTemporaryAttribute:NSBackgroundColorAttributeName value:hc forCharacterRange:r];
				[tlv addTemporaryHighlightForRange:r];
			}
		}
	}
}

-(void)windowResignedKey:(NSNotification*)note
{
	if ([note object] == [browserView window])
	{
		[filterMatchRanges removeAllObjects];
		[self updateHighlights];
	}
}

-(void)windowBecameKey:(NSNotification*)note
{
	if ([note object] == [browserView window])
	{
		[self loadUnfiltered];
		[self filter];
		pagesColumnNeedsUpdate=YES;
		notebooksColumnNeedsUpdate=YES;
		
		[pagesView validateVisibleColumns];
		
	}
}

#pragma mark Todo List

-(void)todoListView:(SPRTodoListView*)tlv createNewItemAtIndex:(NSUInteger)index withContent:(NSAttributedString*)newText
{
#ifdef DEBUG
	if (![selectedPage isKindOfClass:[SPRTodoPage class]])
		[NSException raise:@"PageBrowserException" format:@"TodoListView delegate method called when no todo page was selected"];
#endif
	SPRTodoPage *page = (id)selectedPage;
	[page addItemAtIndex:index withContent:newText state:NO];
}

-(void)todoListView:(SPRTodoListView*)tlv deleteItemAtIndex:(NSUInteger)index preserveContents:(NSAttributedString*)contents
{
#ifdef DEBUG
	if (![selectedPage isKindOfClass:[SPRTodoPage class]])
		[NSException raise:@"PageBrowserException" format:@"TodoListView delegate method called when no todo page was selected"];
#endif
	SPRTodoPage *page = (id)selectedPage;
	
	if ([[page items] count] == 1)
	{
		[page setState:NO forItemAtIndex:index];
		[page setContents:[NSAttributedString new] forItemAtIndex:index];
		return;
	}
	
	if (index > 0 && contents != nil)
	{
		[[[[page items] objectAtIndex:index-1] objectForKey:TextStorageKey] appendAttributedString:contents];
	}
	
	[page removeItemAtIndex:index];
}

-(void)todoListView:(SPRTodoListView*)tlv toggleItemAtIndex:(NSUInteger)index
{
#ifdef DEBUG
	if (![selectedPage isKindOfClass:[SPRTodoPage class]])
		[NSException raise:@"PageBrowserException" format:@"TodoListView delegate method called when no todo page was selected"];
#endif
	SPRTodoPage *page = (id)selectedPage;
	[page toggleItemAtIndex:index];
}

-(NSData*)todoListView:(SPRTodoListView*)tlv dataForPBoardType:(NSString*)type forDragFromItemAtIndex:(NSUInteger)index
{
#ifdef DEBUG
	if (![selectedPage isKindOfClass:[SPRTodoPage class]])
		[NSException raise:@"PageBrowserException" format:@"TodoListView delegate method called when no todo page was selected"];
#endif
	SPRTodoPage *page = (id)selectedPage;
	NSData *data;
	
	if ([type isEqualToString:SPRPasteboardTypeTodoItem])
	{
		data = [NSKeyedArchiver archivedDataWithRootObject:[page itemAtIndex:index]];
	}
	else
	{
		NSString *doctype = [SPRPasteboardToDocumentTypeTable objectForKey:type];
		
		if (doctype == nil)
			[NSException raise:@"SPROrganizer+Actions" format:@"requested data for unknown drop type"];
		
		data = [[[page itemAtIndex:index] objectForKey:TextStorageKey] dataUsingDocumentType:doctype];
	}
	return data;
}

-(BOOL)todoListView:(SPRTodoListView*)tlv writeDroppedData:(NSData*)data ofType:(NSString*)type toIndex:(NSUInteger)index
{
#ifdef DEBUG
	if (![selectedPage isKindOfClass:[SPRTodoPage class]])
		[NSException raise:@"PageBrowserException" format:@"TodoListView delegate method called when no todo page was selected"];
#endif
	SPRTodoPage *page = (id)selectedPage;
	if ([type isEqualToString:SPRPasteboardTypeTodoItem])
	{
		NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
		NSAttributedString *c = [dict objectForKey:TextStorageKey];
		BOOL s = [[dict objectForKey:StateKey] boolValue];
		[page addItemAtIndex:index withContent:c state:s];
	}
	
	return YES;
}

-(void)todoListView:(SPRTodoListView*)tlv moveDroppedData:(NSData*)data ofType:(NSString*)type toIndex:(NSUInteger)dstIndex fromIndex:(NSUInteger)srcIndex
{
#ifdef DEBUG
	if (![selectedPage isKindOfClass:[SPRTodoPage class]])
		[NSException raise:@"PageBrowserException" format:@"TodoListView delegate method called when no todo page was selected"];
#endif
	if (![type isEqualToString:SPRPasteboardTypeTodoItem])
		[NSException raise:@"NoteBookOrganizerException"
					format:@"Tried to move item via drop but data type was incorrect. type = %@", type];
	
	SPRTodoPage *page = (id)selectedPage;
	[page moveItemFromIndex:srcIndex toIndex:dstIndex];
}

/*Related Methods*/
-(void)loadTodoListView
{
#ifdef DEBUG
	if (![selectedPage isKindOfClass:[SPRTodoPage class]])
		[NSException raise:@"PageBrowserException" format:@"TodoListView delegate method called when no todo page was selected"];
#endif
	[todoListView removeAllItems];
	SPRTodoPage *page = (id)selectedPage;
	
	for (NSDictionary *dict in [page items])
	{
		BOOL state = [[dict objectForKey:StateKey] boolValue];
		NSTextStorage *ts = [dict objectForKey:TextStorageKey];
		[todoListView addTextStorage:ts state:state];
	}
}

/*Notifications*/
-(void)todoItemsReload:(NSNotification*)note
{
	if ([note object] == selectedPage)
	{
		[todoListView saveSelection];
		[self loadTodoListView];
		[todoListView loadSelection];
	}
}



-(void)todoItemStateDidChange:(NSNotification*)note
{
	if ([note object] == selectedPage)
	{
		NSDictionary *dict = [note userInfo];
		[todoListView setState:[[dict objectForKey:StateKey] boolValue] forTextStorage:[dict objectForKey:TextStorageKey]];
	}
}


@end

/*
@implementation SPRPageBrowserBrowser

- (NSDragOperation)draggingSourceOperationMaskForLocal:(BOOL)isLocal
{
	if (isLocal)
	{
		if ([[NSApp currentEvent] modifierFlags] & NSAlternateKeyMask)
			return NSDragOperationCopy;
		else
			return NSDragOperationMove;
	}
	else
		return NSDragOperationCopy;
}

@end*/
