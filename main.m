//
//  main.m
//  SpiralBound Pro
//
//  Created by Philip White on 4/29/09.
//  Copyright Rivulus Software 2009. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#ifdef DEBUG

@interface NSObject (DebugTimeNu)
+(void)modalNuTerminal;
@end

@implementation NSObject (DebugTimeNu)
+(void)modalNuTerminal
{
	id NCC_Class = NSClassFromString(@"NuConsoleWindowController");
	id NCC = [NCC_Class new];
	NSWindow *window = [NCC window];
	[NSApp runModalForWindow:window];
	[window close];}
@end

#endif

int main(int argc, char *argv[])
{
	return NSApplicationMain(argc,  (const char **) argv);
	
}
