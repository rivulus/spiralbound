//
//  SBWFrameView.m
//  SpiralBound
//
//  Created by Philip White on 2/1/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "SBWFrameView.h"
#import "SBConstants.h"
#import "SBController.h"
#import "SPROrganizer.h"
#import "SPRTodoListView.h"

static NSString *SpiralBoundNibName = @"SpiralBound_SBPro";

static float SimpleStyleInvisibleHeight=14.0,
			 RegularStyleInvisibleHeight=0.0;

@implementation SBWFrameView

static NSString *DragbarImageName = @"CoilAlpha";
static NSString *DragbarMaskImageName = @"CoilColorMask";
static NSString *SimpleDragbarImageName = @"SimpleBar";
static NSString *SimpleDragbarMaskImageName = @"SimpleBarColorMask";
static NSString *GrayedOutDragbarImageName = @"CoilAlphaGrayedOut";
//static NSString *SimpleGrayedOutDragbarImageName = @"SimpleBarGrayedOut";

+ (void)initialize
{
	NSImage *image = [NSImage imageNamed:@"CoilAlpha.tiff"];
	[image setName: DragbarImageName];
	
	image = [NSImage imageNamed:@"CoilColorMask.tiff"];
	[image setName:DragbarMaskImageName];
	
	image = [NSImage imageNamed:@"SimpleBar.tiff"];
	[image setName:SimpleDragbarImageName];
	
	image = [NSImage imageNamed:@"SimpleBarColorMask.tiff"];
	[image setName:SimpleDragbarMaskImageName];
	
	image = [NSImage imageNamed:@"CoilAlphaGrayedOut.tiff"];
	[image setName:GrayedOutDragbarImageName];
	
	//image = [NSImage imageNamed:@"SimpleBarGrayedOut.tiff"];
	//[image setName:SimpleGrayedOutDragbarImageName];
	
}

- (void)drawRect:(NSRect)rect
{
	[super drawRect:rect];
	[self drawBorder];
}

- (void)drawBorder
{
	//else
	//	color = NotKeyMainColor;
	//let's try drawing alternate grey and main color lines, see if that looks ok
	
	float lineWidth = kStackEffectEdgeWidth/kPagesInStack;
	
	
	NSRect borderlessRect = [self frameRectMinusBorder];
	
	NSPoint blPoint = borderlessRect.origin;
	NSPoint trPoint = {NSMaxX(borderlessRect),NSMaxY(borderlessRect)-14};
	NSPoint brPoint = {trPoint.x, blPoint.y};
	
	NSUInteger counter = kPagesInStack;
	//NSBezierPath *bezierPath = [NSBezierPath bezierPath];
	//[bezierPath setLineWidth:1.0];//4*lineWidth];
	//first draw the gray color lines
	NSColor *mainColor = [[self organizer] color];
	NSColor *shadowColor = [[NSColor blackColor] colorWithAlphaComponent:[mainColor alphaComponent]];
	do
	{
		NSBezierPath *bezierPath1 = [NSBezierPath bezierPath];
		[bezierPath1 setLineWidth:1.0];
		
		//move all of the points right and down\
		blPoint.x += lineWidth;
		blPoint.y -= lineWidth;
		trPoint.x += lineWidth;
		trPoint.y -= lineWidth;
		brPoint.x += lineWidth;
		brPoint.y -= lineWidth;
		
		[bezierPath1 moveToPoint: blPoint];
		[bezierPath1 lineToPoint: brPoint];
		[bezierPath1 lineToPoint: trPoint];
		
		[shadowColor set];
		[bezierPath1 stroke];
		
		
		NSBezierPath *bezierPath2 = [NSBezierPath bezierPath];
		[bezierPath2 setLineWidth:1.4];
		
		blPoint.x += lineWidth;
		blPoint.y -= lineWidth;
		trPoint.x += lineWidth;
		trPoint.y -= lineWidth;
		brPoint.x += lineWidth;
		brPoint.y -= lineWidth;
		
		[bezierPath2 moveToPoint: blPoint];
		[bezierPath2 lineToPoint: brPoint];
		[bezierPath2 lineToPoint: trPoint];
		
		//[[NSColor lightGrayColor] set];
		[mainColor set];
		[bezierPath2 stroke];
		
	} while (--counter);
	
	
}

-(NSRect)titleBarRect
{
	NSSize coilImageSize = [[self dragbarImage] size];
	NSRect rect;
	rect.origin.x=0;
	rect.origin.y=[self bounds].size.height-coilImageSize.height;
	rect.size.height = coilImageSize.height;
	rect.size.width = [self bounds].size.width;
	return rect;
}

- (NSRect)visibleToolbarAndDragbarRect
{
	NSRect unionRect = NSUnionRect([self toolbarRect], [self dragbarRect]);
	if (UseSimpleDragbar)
		unionRect.size.height -= SimpleStyleInvisibleHeight;
	else
		unionRect.size.height -= RegularStyleInvisibleHeight;
	
	unionRect.size.width = [self frame].size.width;
	return unionRect;
}

- (NSImage*)dragbarImage
{
	if (UseSimpleDragbar)
		return [NSImage imageNamed:SimpleDragbarImageName];
	else
		return [NSImage imageNamed:DragbarImageName];
}

- (NSImage*)dragbarGrayedOutImage
{
	if (UseSimpleDragbar)
		return [NSImage imageNamed:SimpleDragbarImageName];
	else
		return [NSImage imageNamed:GrayedOutDragbarImageName];
}

- (NSImage*)dragbarMaskImage
{
	if (UseSimpleDragbar)
		return [NSImage imageNamed:SimpleDragbarMaskImageName];
	else
		return [NSImage imageNamed:DragbarMaskImageName];
}


-(NSNib*)nib
{
	static NSNib *nib=nil;
	
	if (nib == nil)
		nib = [[NSNib alloc] initWithNibNamed: SpiralBoundNibName bundle: nil];

	return nib;
}


- (NSRect)frameRectMinusBorder		//everything except the thick border
{
	NSRect rect;
	rect = [self bounds];
	rect.size.width -= kStackEffectEdgeWidth;
	rect.size.height -= kStackEffectEdgeWidth;
	rect.origin.y += kStackEffectEdgeWidth;
	
	return rect;
}

- (float)regularMinusSimpleDragbarHeight
{
	return [[NSImage imageNamed:DragbarMaskImageName] size].height - [[NSImage imageNamed:SimpleDragbarMaskImageName] size].height;
}



#pragma mark FORWARDERS

- (IBAction)nextPageFromMenu:(id)sender;
{
	//[gSBController nextPageInStack:sender];
	//[self setNeedsDisplay:YES];
}
	
- (IBAction)previousPageFromMenu:(id)sender
{
	//[gSBController previousPageInStack:sender];
	//[self setNeedsDisplay:YES];
}

@end
