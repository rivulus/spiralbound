//
//  RivTools.h
//  MathGame
//
//  Created by Philip on 6/25/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>


// NSRect RIVCenterRect(NSRect rectToCenter, NSRect rectInWhichToCenter);

#pragma mark MACROS
#define THROW_IF_NULL(x) if (x==NULL) @throw([NSException exceptionWithName:@"Malloc Error" reason:[NSString stringWithFormat: @"errno = %i; strerror give \"%s\"",errno,strerror(errno)] userInfo:nil] )

#pragma mark FUNCTIONS

NSDate *RIVOneDayFrom(NSDate *date);
NSDate *RIVOneWeekFrom(NSDate *date);
NSDate *RIVTwoWeeksFrom(NSDate *date);
NSDate *RIVOneMonthFrom(NSDate *date);

static inline void Cross (const float v1[3], const float v2[3], float p[3])
{
	p[0]=v1[1]*v2[2]-v1[2]*v2[1];
	p[1]=-(v1[0]*v2[2]-v1[2]*v2[0]);
	p[2]=v1[0]*v2[1]-v1[1]*v2[0];
}

static inline void Vector (const float c1[3], const float c2[3], float v[3])
{
	v[0]=c2[0]-c1[0];
	v[1]=c2[1]-c1[1];
	v[2]=c2[2]-c1[2];
}

static inline void Normal(const float c1[3],const float c2[3],const float c3[3], float n[3])
{
	float v13[3],v32[3];
	Vector(c1,c3,v13);
	Vector(c3,c2,v32);
	
	Cross(v13,v32,n);
}

static inline int RIVRandomBetweenInclusive(int min, int max)
{
	if (min > max)
	{
		int t = min;
		min = max;
		max = t;
	}
	
	int range = 1 + (max-min);
	return (random()%range) + min;
}

static inline NSRect RIVCenterRect(NSRect rectToCenter, NSRect rectInWhichToCenter)
{
	NSRect centeredRect;
	centeredRect.size = rectToCenter.size;
	centeredRect.origin.x = rectInWhichToCenter.origin.x + (rectInWhichToCenter.size.width/2 - rectToCenter.size.width/2);
	centeredRect.origin.y = rectInWhichToCenter.origin.y + (rectInWhichToCenter.size.height/2 - rectToCenter.size.height/2);
	
	return centeredRect;
}

static inline NSPoint RIVAddPoints(NSPoint point1, NSPoint point2)
{
	NSPoint newPoint = {point1.x+point2.x, point1.y+point2.y};
	return newPoint;
}

static inline NSPoint RIVSubtractPoints(NSPoint point1, NSPoint point2)
{
	NSPoint newPoint = {point1.x-point2.x, point1.y-point2.y};
	return newPoint;
}

//the center of the rect remains the same
static inline NSRect RIVScaleRect(NSRect inRect, double percent)
{
	NSRect outRect=inRect;
	outRect.size.width *= percent;
	outRect.size.height *= percent;
	
	return RIVCenterRect(outRect, inRect);
}

NSInteger RIVSimpleRound(double unrounded);

BOOL RIVDecimalIsWholeNumber(NSDecimal number);

NSDictionary *RIVAttributesToFitStringInRect(NSString *string, NSRect rect, NSFont *font, float minPointSize, float maxPointSize, NSSize *outSize); 

//returns [NSScreen mainScreen] is point is not within any screen
NSScreen* RIVScreenContainingPoint (NSPoint point);
NSScreen* RIVScreenAsLargeAs(NSSize size);
NSRect RIVUnionOfScreenFrames(void);
void RaiseWindowsKeepingZOrder(NSArray *windows);
float RIVYOffsetToConfineRectVerticallyWithinRects(NSRect rect, NSRectArray rects, int count);
float RIVYOffsetToConfineRectVerticallyWithinScreens(NSRect rect);
NSUInteger RIVCountWindowsOfKind(NSUInteger *outCount, Class kind);
void RIVWindowListOfKind(NSUInteger maxCount, NSUInteger list[], Class kind, NSUInteger *actualCount);

extern NSString *AutomaticallyCheckForUpdatesKey,
				*UpdateCheckIntervalKey,
				*UpdateLastCheckKey	; 

void RIVCheckForUpdate(BOOL quietly); //pass quietly=YES if not responding to a user asking to check for updates explicitly
#pragma mark CATEGORIES

@interface NSAffineTransform (RIVAdditions)
	
-(NSRect)rivTransformRect:(NSRect)rectIn;
	
@end

@interface NSDecimalNumber (RIVAdditions)

-(BOOL)isWholeNumber;
-(NSDecimalNumber*)integralPart;
-(NSDecimalNumber*)fractionalPart;

@end

#pragma mark CLASSES

@interface NSObject (GetObject)

@end
