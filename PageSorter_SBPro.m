//
//  PageSorter.m
//  SpiralBound Pro
//
//  Created by Philip White on 5/5/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "PageSorter_SBPro.h"
#import "PageBrowser_SBPro.h"
#import "RIVTools.h"
#import "SBController+Actions.h"
#import "SPRTextView.h"
#import "SPRPlainPage.h"
#import "SPRTodoPage.h"

@implementation PageSorter

static PageSorter *SharedSorter=nil;

+(NSWindow*)sorterWindow
{
    if (SharedSorter==nil)
        return nil;
    
    if ([[[self sharedSorter] window] isVisible])
        return [[self sharedSorter] window];
    else
        return nil;
}

+(PageSorter*)sharedSorter
{
	if (SharedSorter == nil)
	{
		SharedSorter = [[self alloc] init];
	}
	return SharedSorter;
}

-(id)init
{
	self = [super initWithWindowNibName:@"PageSorter"];
	
	if (self)
	{
		NSWindow *w = [self window];
		
        
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(windowWillClose:)
													 name:NSWindowWillCloseNotification
												   object:w];
												    
		browsers = [[NSMutableArray alloc] init];
		
		//instatiate one PageBrowser
		PageBrowser *browser = [[PageBrowser alloc] init];
		[browsers addObject:browser];
		
		[splitView addSubview:[browser view]];
		
		[splitView adjustSubviews];
		[removePaneButton setEnabled:NO];
	}
	
	return self;
}

-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)windowWillClose:(NSNotification*)note
{
	SharedSorter=nil;
}

-(IBAction)addPane:(id)sender
{	
	PageBrowser *browser = [[PageBrowser alloc] init];
	[browsers addObject:browser];
	
	[splitView addSubview:[browser view]];
	//set each subview to an arbitrary but equal size so adjustSubview sizes them equally
	for (NSView *pane in [splitView subviews])
	{
		[pane setFrame:NSMakeRect(0, 0, 100, 100)];
	}
	
	[splitView adjustSubviews];
	
	//restrict the number of panes to between 1 and 5 inclusive
	int count = [browsers count];
	if (count > 1)
		[removePaneButton setEnabled:YES];
	
	if (count >= 5)
		[addPaneButton setEnabled:NO];
}

-(IBAction)removePane:(id)sender
{
	PageBrowser *browser=[self keyBrowser];
	
	if (browser==nil)
	{
		browser = [browsers lastObject];
	}
	
	[[browser view] removeFromSuperview];
	//set each subview to an arbitrary but equal size so adjustSubview sizes them equally
	for (NSView *pane in [splitView subviews])
	{
		[pane setFrame:NSMakeRect(0, 0, 100, 100)];
	}
	[splitView adjustSubviews];
	[browsers removeObject: browser];

	//disable the remove pane button is there is only one panel
	if ([browsers count] == 1)
		[removePaneButton setEnabled:NO];
	
	//no need to check here, if you were able to remove a pane then
	//there are fewer than the maximum
	[addPaneButton setEnabled:YES];
}

-(void)keyDown:(NSEvent*)theEvent
{
	NSString *characters = [theEvent characters];
	
	if ([characters length] == 0)
		return;
	
	unichar key = [characters characterAtIndex:0];
	
	if (key == NSDeleteFunctionKey || key == NSDeleteCharacter)
	{
		NSUInteger result1 = [theEvent modifierFlags]&NSControlKeyMask;
		NSUInteger result2 = [theEvent modifierFlags]&NSAlternateKeyMask;
		BOOL warn = result1>0 && result2>0;//[theEvent modifierFlags] & (NSControlKeyMask|NSAlternateKeyMask);
		[self deleteSelectionWithWarning:!warn];
	}
}

-(PageBrowser*)browserWithPagesView:(NSBrowser*)pagesView
{
	for (PageBrowser *b in browsers)
	{
		if ([b pagesView] == pagesView)
			return b;
	}
	return nil;
}

-(PageBrowser*)browserWithTextView:(SPRTextView*)textView
{
	for (PageBrowser *b in browsers)
	{
		if ([b textView] == textView)
			return b;
	}
	return nil;
}

#pragma mark ACTIONS

-(void)deleteSelectionWithWarning:(BOOL)warn
{
	//find the correct PageBrowser and send the message to it
	
	NSEnumerator *enumerator = [browsers objectEnumerator];
	PageBrowser *pageBrowser;
	
	while (pageBrowser = [enumerator nextObject])
	{
		if ([pageBrowser isSelected])
		{
			[pageBrowser deleteSelectionWithWarning:warn];
			return;
		}
	}
}

-(PageBrowser*)keyBrowser //may return nil
{
	//see if there is an NSBrowser or SPRTextView in the responder chain
	id r;
	for (r = [[self window] firstResponder]; r != nil; r = [r nextResponder])
	{
		if ([r isKindOfClass:[NSBrowser class]])
		{
			return [[PageSorter sharedSorter] browserWithPagesView:r];
		}
		else if ([r isKindOfClass:[SPRTextView class]])
		{
			return [[PageSorter sharedSorter] browserWithTextView:r];
		}
	}
	return nil;
}

-(IBAction)trashWasClicked:(id)sender
{
	[[self keyBrowser] deleteSelectionWithWarning:YES];
}

-(IBAction)newNotebookButtonWasClicked:(id)sender
{
	[gSBController showNewNotebookPanel:nil];
}

-(void)createNewPageOfClass:(Class)c
{
	PageBrowser *browser = [self keyBrowser];
	
	if (browser == nil)
	{
		NSRunAlertPanel(@"No Notepad Selected", @"Please select a notepad in which to create the new page.",nil,nil,nil);
		return;
	}
	
	[browser createNewPageOfClass:c];
}

-(IBAction)newRegularPageButtonWasClicked:(id)sender
{
	[self createNewPageOfClass:[SPRPlainPage class]];
}

-(IBAction)newTodoPageButtonWasClicked:(id)sender
{
	[self createNewPageOfClass:[SPRTodoPage class]];
}

#pragma mark SPLITVIEW DELEGATE
//This is for the vertical delegate
//see the SPRPageBrowserSplitViewDelegate class for the horizontal delegate

#define MIN_HEIGHT 60.0

- (CGFloat)splitView:(NSSplitView *)sv constrainMaxCoordinate:(CGFloat)proposedMax ofSubviewAt:(NSInteger)di
{
	return [sv frame].size.height - (MIN_HEIGHT + [sv dividerThickness]);
}

- (CGFloat)splitView:(NSSplitView *)sv constrainMinCoordinate:(CGFloat)proposedMin ofSubviewAt:(NSInteger)di
{
	return MIN_HEIGHT;
}

- (void)splitView:(NSSplitView *)sv resizeSubviewsWithOldSize:(NSSize)oldSize
{
	NSSize newSize = [splitView frame].size;
	{
		NSMutableArray *minimizedSubviews = [NSMutableArray array];
		int viewCount = [[splitView subviews] count];
		//all of these deltas are always negative (or zero)
		float deltaSize = [splitView frame].size.height - oldSize.height;
		//float perViewDelta = (deltaSize - (float)(viewCount-1)*[splitView dividerThickness]) / (float)viewCount;
		float perViewDelta = deltaSize / (float)viewCount;
		float amountOfDeltaUsedToResizeMinimizedSubviews=0.0; //positive
		
		//now gather the subviews that want to be resized beyond their minimum.
		for (id view in [splitView subviews])
		{
			NSRect oldViewFrame = [view frame];
			if (oldViewFrame.size.height + perViewDelta < MIN_HEIGHT)
			{
				//excessFromMinimizedSubviews += -perViewDelta - (oldViewFrame.size.height - MIN_HEIGHT);
				amountOfDeltaUsedToResizeMinimizedSubviews += -(MIN_HEIGHT - oldViewFrame.size.height);
				[minimizedSubviews addObject:view];
			}
		}
		
		//now calculate a new perViewDelta taking into account excessFromMinimizedSubviews
		if ([minimizedSubviews count] > 0)
		{
			int minCount = [minimizedSubviews count];
			deltaSize = [splitView frame].size.height - oldSize.height + amountOfDeltaUsedToResizeMinimizedSubviews;
			//perViewDelta = (deltaSize - (float)(viewCount-1)*[splitView dividerThickness]) / (float)(viewCount-minCount);
			perViewDelta = deltaSize / (float)(viewCount-minCount);
		}
		
		
		//now go through and resize all of the subviews (any subviews that are are in the minimizedSubviews array
		//should be just repositioned since they were already resized
		float yOrigin = 0.0;
		for (id view in [splitView subviews])
		{
			NSRect frame = [view frame];
			if ([minimizedSubviews containsObject:view])
				frame.size.height = MIN_HEIGHT;
			else
				frame.size.height += perViewDelta;
		
			frame.size.width = newSize.width;
			frame.origin.y = yOrigin;
			[view setFrame:frame];
			yOrigin += frame.size.height + [splitView dividerThickness];
		}
	}
}

@end

@implementation SPRPageBrowserSplitViewDelegate
#define MIN_WIDTH 100.0
- (CGFloat)splitView:(NSSplitView *)sv constrainMaxCoordinate:(CGFloat)proposedMax ofSubviewAt:(NSInteger)di
{
	return [sv frame].size.width - (MIN_WIDTH + [sv dividerThickness]);
}

- (CGFloat)splitView:(NSSplitView *)sv constrainMinCoordinate:(CGFloat)proposedMin ofSubviewAt:(NSInteger)di
{
	return MIN_WIDTH;
}


- (void)splitView:(NSSplitView *)splitView resizeSubviewsWithOldSize:(NSSize)oldSize
{
	NSSize newSize = [splitView frame].size;
	{
		//these two are negative
		float deltaSize = newSize.width - oldSize.width;
		float perViewDelta = deltaSize / 2.0;
		
		NSView *leftView = [[splitView subviews] objectAtIndex:0];
		NSView *rightView = [[splitView subviews] objectAtIndex:1];
		
		NSRect leftFrame={0,0,0,newSize.height}, rightFrame={0,0,0,newSize.height};
		
		if ([leftView frame].size.width + perViewDelta < MIN_WIDTH)
		{
			leftFrame.size.width = MIN_WIDTH;
			rightFrame.size.width = newSize.width - (MIN_WIDTH + [splitView dividerThickness]);
		}
		else if ([rightView frame].size.width + perViewDelta < MIN_WIDTH)
		{
			rightFrame.size.width = MIN_WIDTH;
			leftFrame.size.width = newSize.width - (MIN_WIDTH + [splitView dividerThickness]);
		}
		else
		{
			leftFrame.size.width = [leftView frame].size.width + perViewDelta;
			rightFrame.size.width = [rightView frame].size.width + perViewDelta;
		}
		
		rightFrame.origin.x = leftFrame.size.width + [splitView dividerThickness];
		
		[leftView setFrame:leftFrame];
		[rightView setFrame:rightFrame];
	}
}

#pragma mark MENU ACTIONS
-(void)deletePage:(id)sender
{
    NSLog(@"CACA");
}

@end
