//
//  SPRInAppPurchases.h
//  SpiralBound Pro
//
//  Created by Philip White on 4/9/18.
//

#import <Foundation/Foundation.h>

extern NSString * const SPRInAppPurchaseTipJarIsAvailable;

typedef void(^InAppPurchaseValidationCompletion)(BOOL);

@interface SPRInAppPurchases : NSObject

+ (instancetype)sharedInstance;
- (void)showUI;
- (void)run;

@property (nonatomic, readonly, getter=isMultipleNotebooksPurchased) BOOL multipleNotebooksPurchased;
@property (nonatomic, readonly, getter=isTipJarAvailable) BOOL tipJarAvailable;

@end
