/*
 *  SBTypes.h
 *  SpiralBound
 *
 *  Created by Philip White on 2/10/09.
 *  Copyright 2009 Rivulus Software. All rights reserved.
 *
 */

#ifndef SBTYPES

#define SBTYPES 1

typedef enum
{
	SBSpiralBoundStyle,
	SBTornOffStyle
} SBWindowStyle;


#endif