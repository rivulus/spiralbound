//
//  NSMutableData+Encrypt.h
//  CommonSpiralbound
//
//  Created by Philip on 5/10/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface NSMutableData (AES256)
- (NSData *)AES256EncryptWithKey:(NSString *)key;
- (NSData *)AES256DecryptWithKey:(NSString *)key;
@end
