//
//  SBConstants.m
//  SpiralBound
//
//  Created by Philip White on 2/3/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "SBConstants.h"
#import "DefaultsKeys.h"

//regular SB uses a number for major version, SBPro uses a letter
NSString *kVersion=@"1.1";
NSString *OriginalSpiralBoundBundleID = @"com.rivulus.SpiralBound";

//NSImage *TrashButtonImage, *LeftArrowImage, *RightArrowImage, *ReturnToNotebookButtonImage,
//		*DisabledTrashButtonImage, *DisabledLeftArrowImage, *DisabledRightArrowImage,
//		*DisabledReturnToNotebookButtonImage;

NSColor *SBYellow, *SBGreen, *SBBlue, *SBWhite, *SBLavender, *SBPeach, *SBBoldRed, *SBBoldPink,
		*SBBoldYellow, *SBBoldGreen, *SBBoldPurple, *SBBoldBlue;

NSDictionary *SPRPasteboardToDocumentTypeTable;

NSString *SPRInvisibleAttributeHolder;
unichar SPRInvisibleAttributeHolderCharacter = 0x200c; //zero width non-joiner

NSAttributedString *SPRGetAttributeHolder(NSDictionary *attrs)
{
	if (attrs == nil)
		attrs = [NSUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"TextAttributes"]];
	
	return [[NSAttributedString alloc] initWithString:SPRInvisibleAttributeHolder attributes:attrs];
}

@implementation SBConstants

+ (void)load
{
	@autoreleasepool {
		SBYellow = [NSColor colorWithCalibratedRed: 0.996 green: 0.980 blue: 0.620 alpha: 1.0];
		SBBlue = [NSColor colorWithCalibratedRed: 0.686 green: 0.976 blue: 0.980 alpha: 1.0];
		SBGreen = [NSColor colorWithCalibratedRed: 0.655 green: 0.996 blue: 0.620 alpha: 1.0];
		SBLavender = [NSColor colorWithCalibratedRed: 0.988235 green: 0.909804 blue: 0.988235 alpha: 1.0];
		SBPeach = [NSColor colorWithCalibratedRed: 1.0 green: 0.847059 blue: 0.72549 alpha: 1.0];
		SBWhite = [NSColor whiteColor];
		SBBoldRed = [NSColor colorWithCalibratedRed: 1.0 green: 0 blue: 0.055 alpha: 1.0];
		SBBoldPink = [NSColor colorWithCalibratedRed: 1.0 green: 0.110 blue: 0.38 alpha: 1.0];
		SBBoldYellow = [NSColor colorWithCalibratedRed: 1.0 green: 0.996 blue: 0.133 alpha: 1.0];
		SBBoldGreen = [NSColor colorWithCalibratedRed: 0.251 green: 0.667 blue: 0.224 alpha: 1.0];
		SBBoldPurple = [NSColor colorWithCalibratedRed: 0.373 green: 0.09 blue: 0.729 alpha: 1.0];
		SBBoldBlue = [NSColor colorWithCalibratedRed: 0.518 green: 0.486 blue: 0.992 alpha: 1.0];
		
		
		SPRPasteboardToDocumentTypeTable = [[NSDictionary alloc] initWithObjectsAndKeys:NSRTFDTextDocumentType, NSRTFDPboardType,
																						NSRTFTextDocumentType, NSRTFPboardType,
																						NSPlainTextDocumentType, NSStringPboardType,
																						nil];
		
		SPRInvisibleAttributeHolder = [[NSString alloc] initWithFormat:@"%C",SPRInvisibleAttributeHolderCharacter];
	
	}
}

@end
