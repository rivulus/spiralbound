//
//  SPRInformationPopover.m
//  SpiralBound Pro
//
//  Created by Philip White on 4/10/18.
//

#import "SPRInformationPopover.h"

@interface SPRInformationPopover ()
{
	IBOutlet NSTextField *_textField;
}

@end

@implementation SPRInformationPopover

+ (NSPopover *)popoverWithText:(NSString *)text
{
	NSPopover *popover = [[NSPopover alloc] init];
	[popover setBehavior:NSPopoverBehaviorTransient];
	SPRInformationPopover *informationPopover = [[self alloc] initWithNibName:@"SPRInformationPopover" bundle:nil];
	[informationPopover setText:text];
	[popover setContentViewController:informationPopover];
	return popover;
}

- (void)setText:(NSString *)text
{
	[self view];
	[_textField setStringValue:text];
}
 - (NSString *)text
{
	[self view];
	return [_textField stringValue];
}

@end
