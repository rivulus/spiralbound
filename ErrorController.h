//
//  ErrorController.h
//  SpiralBound 1.1
//
//  Created by Philip on 8/5/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <ExceptionHandling/ExceptionHandlingDefines.h>
#import <ExceptionHandling/NSExceptionHandler.h>

@interface ErrorController : NSWindowController
{
	IBOutlet NSTextView *userField, *reportField;
}

- (BOOL)shouldDisplayException:(NSException *)exception;
-(IBAction)sendReport:(id)sender;
-(IBAction)dontSend:(id)sender;

-(void)showReportForException:(NSException*)exception;

+(ErrorController*)sharedController;

@end
