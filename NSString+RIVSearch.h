//
//  NSString+RIVSearch.h
//  DeleteMe
//
//  Created by Philip on 5/19/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>



@interface NSString (RIVSearch)

-(NSArray*)rangesForSearchString:(NSString*)str options:(CFStringCompareFlags)options wholeWords:(BOOL)ww;
-(NSArray*)wholeWordRangesFromRanges:(NSArray*)inRanges;

@end
