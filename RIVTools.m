//
//  RivTools.m
//  MathGame
//
//  Created by Philip on 6/25/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "RIVTools.h"

typedef enum
{
	RIVOneDay=0,
	RIVOneWeek,
	RIVTwoWeeks,
	RIVOneMonth
} RIVInterval;

static NSDate *RIVDayPreciseDate(NSDate *start, RIVInterval interval);

static NSDate *RIVDayPreciseDate(NSDate *start, RIVInterval interval)
{
	NSTimeInterval seconds[4] = {60*60*24,
								 60*60*24*7,
								 60*60*24*7*2,
								 60*60*24*30};
	
	NSDate *inOneWeek = [[NSDate alloc] initWithTimeInterval:seconds[interval] sinceDate:start];
	
	
	NSDateComponents *futureComponents;// = [[NSDateComponents alloc] init];
	
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	
	futureComponents = [gregorian components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:inOneWeek];
	NSDate *roughlyDate = [gregorian dateFromComponents:futureComponents];
	return roughlyDate;
}

NSDate *RIVOneDayFrom(NSDate *date)
{ return RIVDayPreciseDate(date, RIVOneDay); }

NSDate *RIVOneWeekFrom(NSDate *date)
{ return RIVDayPreciseDate(date, RIVOneWeek); }


NSDate *RIVTwoWeeksFrom(NSDate *date)
{ return RIVDayPreciseDate(date, RIVTwoWeeks); }


NSDate *RIVOneMonthFrom(NSDate *date)
{ return RIVDayPreciseDate(date, RIVOneMonth); }



NSInteger RIVSimpleRound(double unrounded)
{
	NSInteger integral = (NSInteger)unrounded;
	double fractional = fabs(unrounded - (double)integral);
	SInt8 shift;
	
	if (fractional >= 0.5)
		shift = 1;
	else
		shift = 0;
	
	if (integral < 0)
		shift = -shift;
	
	return integral+shift;
}


NSDictionary *RIVAttributesToFitStringInRect(NSString *string, NSRect rect, NSFont *font, float minPointSize, float maxPointSize, NSSize *outSize)
{
	NSFontManager *fm = [NSFontManager sharedFontManager];
	NSMutableDictionary *dict = [NSMutableDictionary dictionary];
	
	float top=maxPointSize, bottom=minPointSize;
	float oldPointSize=FLT_MAX;
	
	BOOL perfectFit = NO;
	while (perfectFit == NO)
	{
		float newPointSize =  bottom + (top-bottom)/2.0;
		NSFont *newFont = [fm convertFont:font toSize: newPointSize];
		[dict setObject:newFont forKey: NSFontAttributeName];
		
		if (newPointSize == oldPointSize)
		{
			perfectFit=YES;
			continue;
		}
		
		NSSize size = [string sizeWithAttributes:dict];
		
		if (size.width > rect.size.width || size.height > rect.size.height)
		{
			top = newPointSize;
		}
		else
		{
			bottom = newPointSize;
			if (top-bottom <= 1.0)
			{
				perfectFit = YES;
				if (outSize != NULL)
					*outSize = size;
			}	
		}
		oldPointSize=newPointSize;
	}
	return dict;
}

NSScreen* RIVScreenContainingPoint (NSPoint point)
{
	NSEnumerator *screenEnumerator = [[NSScreen screens] objectEnumerator];
	
	NSScreen *screen;
	
	while (screen = [screenEnumerator nextObject])
	{
		if (NSPointInRect(point, [screen frame]))
			return screen;
	}
	
	return [NSScreen mainScreen];
}

NSScreen* RIVScreenAsLargeAs(NSSize size)
{
	NSEnumerator *enumerator = [[NSScreen screens] objectEnumerator];
	NSScreen *screen;
	while ( screen = [enumerator nextObject] )
	{
		NSSize screenSize = [screen frame].size;
		if (screenSize.width >= size.width && screenSize.height >= size.height)
		{
			return screen;
		}
	}
	return nil;
}

NSRect RIVUnionOfScreenFrames(void)
{
	NSEnumerator *enumerator = [[NSScreen screens] objectEnumerator];
	NSScreen *screen;
	
	screen = [enumerator nextObject];
	NSRect unionRect = [screen frame];
	
	while ( screen = [enumerator nextObject] )
	{
		unionRect = NSUnionRect( unionRect, [screen frame]);
	}
	return unionRect;
}

NSUInteger RIVCountWindowsOfKind(NSUInteger *outCount, Class kind)
{
	NSInteger count;
    NSInteger *windowNumbers;
	NSCountWindows(&count);
	windowNumbers = malloc(sizeof(NSInteger)*count);
	if (windowNumbers == NULL)
		@throw([NSException exceptionWithName:@"Malloc Error"
									   reason:[NSString stringWithFormat:
											   @"errno = %i; strerror give \"%s\"",errno,strerror(errno)]
									 userInfo:nil]);
	NSWindowList(count, windowNumbers);
	
	NSInteger i,kindCount=0;
	
	for (i=0; i<count; i++)
	{
		if ([[NSApp windowWithWindowNumber:windowNumbers[i]] isKindOfClass:kind])
			kindCount++;
	}
	
	if (outCount != NULL)
		*outCount = kindCount;
	
	return kindCount;
}

void RIVWindowListOfKind(NSUInteger maxCount, NSUInteger list[], Class kind, NSUInteger *actualCount)
{
	NSInteger count, *windowNumbers;
	NSCountWindows(&count);
	windowNumbers = malloc(sizeof(NSInteger)*count);
	if (windowNumbers == NULL)
		@throw([NSException exceptionWithName:@"Malloc Error"
									   reason:[NSString stringWithFormat:
											   @"errno = %i; strerror give \"%s\"",errno,strerror(errno)]
									 userInfo:nil]);
	NSWindowList(count, windowNumbers);
	
	int i,j;
	for (i=0, j=0; i<count && j<maxCount; i++)
	{
		if ([[NSApp windowWithWindowNumber:windowNumbers[i]] isKindOfClass:kind])
			list[j++]=windowNumbers[i];
	}
	
	*actualCount=j;
	
	free(windowNumbers);
}

void RaiseWindowsKeepingZOrder(NSArray *w)
{
	NSMutableArray *windows = [w mutableCopy];
	NSInteger count, *windowNumbers;
	NSCountWindows(&count);
	windowNumbers = malloc(sizeof(NSInteger)*count);
	if (windowNumbers == NULL)
		@throw([NSException exceptionWithName:@"Malloc Error"
									   reason:[NSString stringWithFormat:
											   @"errno = %i; strerror give \"%s\"",errno,strerror(errno)]
									 userInfo:nil]);
	NSWindowList(count, windowNumbers);
	
	int i, lastWindowNumber=0;
	for (i=count-1; i>=0; i--)
	{
		//check to see if the window with the number windowNumbers[i] is in the Obj C array.
		BOOL windowMatches=NO;
		
		NSEnumerator *enumerator = [windows objectEnumerator];
		NSWindow *window;
		while (window = [enumerator nextObject])
		{
			if ([window windowNumber] == windowNumbers[i])
			{
				windowMatches = YES;
				[windows removeObject:window];
				break;
			}
		}
		
		if (windowMatches)
		{
			[window orderWindow: NSWindowAbove relativeTo:lastWindowNumber];
			lastWindowNumber = windowNumbers[i];
		}
	}
	
	//raise the remaining windows (these were hidden)
	[windows makeObjectsPerformSelector:@selector(orderFront:) withObject:nil];
	
	free(windowNumbers);
}

float RIVYOffsetToConfineRectVerticallyWithinScreens(NSRect rect)
{
	NSRectArray rects;
	int count;
	NSArray *screens = [NSScreen screens];
	count = [screens count];
	rects = malloc(sizeof(NSRect)*count);
	
	int i;
	
	for (i=0; i<count; i++)
	{
		rects[i] = [[screens objectAtIndex:i] visibleFrame];
	}
	
	float smallestYOffset = FLT_MAX;
	
	float x1,x2,y1,y2;
	x1=rect.origin.x;
	x2=rect.origin.x+rect.size.width;
	y1=rect.origin.y;
	y2=rect.origin.y+rect.size.height;
	
	for (i=0; i<count; i++)
	{
		float X1,X2,Y1,Y2;
		X1=rects[i].origin.x;
		X2=rects[i].origin.x+rects[i].size.width;
		Y1=rects[i].origin.y;
		Y2=rects[i].origin.y+rects[i].size.height;
		
		if ((x1 >= X1 && x1 < X2) ||
			(x2 > X1 && x2 <= X2))
		{
			float yoffset;
			//it overlaps in the x direction with rect[i]. now see if all of it is confined vertically
			if (y2 > Y2)
			{
				yoffset=Y2-y2;
			}
			else if (y1 < Y1)
			{
				yoffset=Y1-y1;
			}
			else
			{
				free(rects);
				return 0;
			}
			
			if (fabsf(yoffset) < fabsf(smallestYOffset))
				smallestYOffset=yoffset;
		}
	}
	free(rects);
	
	if (smallestYOffset == FLT_MAX)
		@throw([NSException exceptionWithName:@"Frame Offset = FLT_MAX"
									   reason:@"Frame Offset = FLT_MAX"
									 userInfo:nil]);
	return smallestYOffset;
}

float RIVYOffsetToConfineRectVerticallyWithinRects(NSRect rect, NSRectArray rects, int count)
{
	int i;
	
	float smallestYOffset = FLT_MAX;
	
	float x1,x2,y1,y2;
	x1=rect.origin.x;
	x2=rect.origin.x+rect.size.width;
	y1=rect.origin.y;
	y2=rect.origin.y+rect.size.height;
	
	for (i=0; i<count; i++)
	{
		float X1,X2,Y1,Y2;
		X1=rects[i].origin.x;
		X2=rects[i].origin.x+rects[i].size.width;
		Y1=rects[i].origin.y;
		Y2=rects[i].origin.y+rects[i].size.height;
		
		if ((x1 >= X1 && x1 < X2) ||
			(x2 > X1 && x2 <= X2))
		{
			float yoffset;
			//it overlaps in the x direction with rect[i]. now see if all of it is confined vertically
			if (y2 > Y2)
			{
				yoffset=Y2-y2;
			}
			else if (y1 < Y1)
			{
				yoffset=Y1-y1;
			}
			else
			{
				return 0;
			}
			
			if (fabsf(yoffset) < fabsf(smallestYOffset))
				smallestYOffset=yoffset;
		}
	}
	
	return smallestYOffset;
}

NSString *AutomaticallyCheckForUpdatesKey = @"AutomaticallyCheckForUpdates",
		 *UpdateCheckIntervalKey = @"UpdateCheckInterval",
		 *UpdateLastCheckKey = @"UpdateLastCheck"; 

static void DoCheckForUpdateError(bool quietly)
{
    if (!quietly)
        NSRunAlertPanel(@"Unable to Check for Update!", @"An error occurred while trying to check for update. Please check your internet connection and try again. If the problem persists, please notify Rivulus Software.", nil, nil, nil);
}

void RIVCheckForUpdate(BOOL quietly)
{
	//if quietly, then check to see if the user's preferences really want us to check without explicit permission and if enough time has passed since the last check.
	NSUserDefaults *stud = [NSUserDefaults standardUserDefaults];
	BOOL checkAuto = [stud boolForKey:AutomaticallyCheckForUpdatesKey];
	
	if (quietly && !checkAuto)
		return;
	
	if (quietly)
	{
		//see when the last check was
		NSTimeInterval lastCheckTime = [stud doubleForKey:UpdateLastCheckKey];
		NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];
		NSTimeInterval minimumDeltaTime = [stud integerForKey:UpdateCheckIntervalKey];
		
		
		if (currentTime-lastCheckTime < minimumDeltaTime)
			return;
	}
	
	//if we got here, then the user has either explicity asked to check for an update, or the update is automatic and it has been long enough since the last update to warrant a new check
	
	NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
	NSString *urlStr = [info valueForKey:@"RIVUpdateURL"];		//the url of the remote file containing the latest version number
	
	if (urlStr == nil)
    {
        DoCheckForUpdateError(quietly);
    }
	
	NSURL *url = [NSURL URLWithString:urlStr];
	
    if (url == nil)
    {
        DoCheckForUpdateError(quietly);
    }
	
	NSError *err=NULL;
	
	NSString *latestVer = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&err];
	
    if (latestVer == nil)
    {
        DoCheckForUpdateError(quietly);
    }
	
	latestVer = [latestVer stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	NSString *localVer = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleVersion"];
	
	BOOL newerAvailable = ([localVer compare:latestVer] == NSOrderedAscending);
	
	if (newerAvailable)
	{
		NSString *urlStr = [info valueForKey:@"RIVProductURL"];
		if (urlStr == nil)
			urlStr = @"http://www.rivulus-sw.com";
		NSURL *url = [NSURL URLWithString:urlStr];
		
		int result = NSRunAlertPanel(@"Update Available!", @"Version %@ is now available. You are currently running version %@. Click \"Visit Website\" to open the Rivulus website where you can learn more about the update and download it.", @"Not Now",@"Visit Website", nil, latestVer, localVer);
		
		if (result == NSAlertAlternateReturn)
		{
			[[NSWorkspace sharedWorkspace] openURL:url];
		}
	}
	else if (!quietly)
	{
		NSRunAlertPanel(@"No Update Available.", @"You are running the latest version of this application.",nil,nil,nil);
	}
	
	//if we got here, we successfully checked for updates
	[stud setDouble:[NSDate timeIntervalSinceReferenceDate] forKey:UpdateLastCheckKey];
	
	return;
}

#pragma mark CATEGORIES

@implementation NSAffineTransform (RIVAdditions)

-(NSRect)rivTransformRect:(NSRect)rectIn
{
	NSRect rectOut;
	rectOut.size = [self transformSize:rectIn.size];
	rectOut.origin = [self transformPoint:rectIn.origin];
	return rectOut;
}

@end

@implementation NSDecimalNumber (RIVAdditions)

-(BOOL)isWholeNumber
{
	NSDecimal zero = [[NSDecimalNumber zero] decimalValue];
	NSDecimal number = [self decimalValue];
	NSRoundingMode roundingMode;
	NSDecimal wholeNumber;
	
	NSComparisonResult comp = NSDecimalCompare(&number, &zero);
	if (comp == NSOrderedAscending)	//number is negative
		roundingMode = NSRoundUp;
	else if (comp == NSOrderedDescending)
		roundingMode = NSRoundDown;
	else //it must be NSOrderedSame so number equals zero
		return YES;
	
	NSDecimalRound(&wholeNumber,&number,0,roundingMode);
	
	return (NSDecimalCompare(&wholeNumber, &number) == NSOrderedSame);
}

-(NSDecimalNumber*)integralPart
{
	NSDecimal zero = [[NSDecimalNumber zero] decimalValue];
	NSDecimal number = [self decimalValue];
	NSRoundingMode roundingMode;
	NSDecimal wholeNumber;
	
	NSComparisonResult comp = NSDecimalCompare(&number, &zero);
	if (comp == NSOrderedAscending)	//number is negative
		roundingMode = NSRoundUp;
	else if (comp == NSOrderedDescending)
		roundingMode = NSRoundDown;
	else //it must be NSOrderedSame so number equals zero
		return [NSDecimalNumber zero];
	
	NSDecimalRound(&wholeNumber,&number,0,roundingMode);
	
	return [NSDecimalNumber decimalNumberWithDecimal:wholeNumber];
}

-(NSDecimalNumber*)fractionalPart
{
	NSDecimal zero = [[NSDecimalNumber zero] decimalValue];
	NSDecimal number = [self decimalValue];
	NSRoundingMode roundingMode;
	NSDecimal wholeNumber, fractional;
	
	NSComparisonResult comp = NSDecimalCompare(&number, &zero);
	if (comp == NSOrderedAscending)	//number is negative
		roundingMode = NSRoundUp;
	else if (comp == NSOrderedDescending)
		roundingMode = NSRoundDown;
	else //it must be NSOrderedSame so number equals zero
		return [NSDecimalNumber zero];
	
	NSDecimalRound(&wholeNumber,&number,0,roundingMode);
	NSDecimalSubtract(&fractional, &number, &wholeNumber, NSRoundPlain);
	
	return [NSDecimalNumber decimalNumberWithDecimal:fractional];
}

@end
