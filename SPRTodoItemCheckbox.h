//
//  SPRTodoItemCheckbox.h
//  CocoaJunk
//
//  Created by Philip on 5/21/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SPRTodoItemView;
@interface SPRTodoItemCheckbox : NSView
{
	SPRTodoItemView *itemView;
	BOOL checked;
	BOOL beingClicked; //check is drawn with less than full opacity to show that the mouse is being clicked on it
	int markStyle;
}

+ (void)GenerateGlyph;
+ (NSBezierPath*)markGlyph;

- (id)initWithFrame:(NSRect)frame itemView:(SPRTodoItemView*)iv;
- (void)setChecked:(BOOL)yn;
- (BOOL)isChecked;
+ (unichar)markCharacter;

@end
