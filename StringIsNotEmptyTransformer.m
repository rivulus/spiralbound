//
//  StringIsNotEmptyTransformer.m
//  SpiralBound
//
//  Created by Philip White on 2/25/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "StringIsNotEmptyTransformer.h"


@implementation StringIsNotEmptyTransformer

+ (Class)transformedValueClass
{
	return [NSNumber class];
}

+ (BOOL)allowsReverseTransformation { return NO; }

- (id)transformedValue:(id)value
{
    if (value == nil)
		return [NSNumber numberWithBool:NO];
	else if ([value isEqualToString: @""])
		return [NSNumber numberWithBool:NO];
	else
		return [NSNumber numberWithBool:YES];
}

@end
