//
//  SPRTodoPage.h
//  CommonSpiralbound
//
//  Created by Philip on 5/24/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SPRPage.h"

extern NSString *TextStorageKey, *StateKey;


@interface SPRTodoPage : SPRPage
{
	NSMutableArray *items; //NSMutableDictionaries
}

-(NSUInteger)itemCount;
-(NSMutableDictionary*)itemAtIndex:(NSUInteger)i;
-(NSArray*)items; //consider the returned array to be immutable
-(void)removeItemAtIndex:(NSUInteger)i;
-(void)moveItemFromIndex:(NSUInteger)from toIndex:(NSUInteger)to; //to is the unadjusted index
																  //example 3 items, moving second to send
																  //from:1 to:4
-(void)addItemAtIndex:(NSUInteger)i withContent:(NSAttributedString*)c state:(BOOL)s; //pass value equal to or large than count to place at end (e.g. NSUIntegerMax)
-(void)toggleItemAtIndex:(NSUInteger)i;
-(void)setState:(BOOL)yn forItemAtIndex:(NSUInteger)i;
-(void)setContents:(NSAttributedString*)c forItemAtIndex:(NSUInteger)i;
-(NSString*)contentsAsDelimitedString:(NSString *)delimiter;

@end
