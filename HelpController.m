//
//  HelpController.m
//  SpiralBound
//
//  Created by Philip White on 2/27/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "HelpController.h"

@interface HelpController() <WebPolicyDelegate>
@end

@implementation HelpController

+(id)sharedHelpController
{
	static HelpController *sharedController = nil;
	
	if (sharedController==nil)
	{
		sharedController = [[HelpController alloc] init];
	}
	return sharedController;
}

-(id)init
{
	self = [super initWithWindowNibName:@"Help"];
	
	if (self)
	{
		
	}
	return self;
}

- (void)windowDidLoad
{
	[[webView mainFrame] loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:
																[[NSBundle mainBundle] pathForResource:@"Help" ofType:@"html"]]]];
	[webView setPolicyDelegate:self];
}

- (void)webView:(WebView *)sender decidePolicyForNewWindowAction:(NSDictionary *)actionInformation request:(NSURLRequest *)request newFrameName:(NSString *)frameName decisionListener:(id)listener {
	[[NSWorkspace sharedWorkspace] openURL:[request URL]];
}


-(NSAttributedString*)help
{
	return help;
}

- (IBAction)showWindow:(id)sender
{
	if ([[self window] isVisible] == NO)
		[[self window] center];
	
	[super showWindow:sender];
}

@end
