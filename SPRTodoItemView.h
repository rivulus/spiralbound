//
//  SPRTodoItem.h
//  CocoaJunk
//
//  Created by Philip on 5/20/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SPRTodoItemCheckbox,SPRTodoListView,SPRTodoItemTextView;
@interface SPRTodoItemView : NSView <NSLayoutManagerDelegate>
{
	NSArray *topLevelObjects;
	SPRTodoItemCheckbox *checkBox;
	SPRTodoItemTextView *textView;
	SPRTodoListView *parentList;
    BOOL inFrameChange;
}

- (SPRTodoItemTextView *)textView;
- (void)setState:(BOOL)s;
- (void)setTextView:(SPRTodoItemTextView *)value;
-(id)initWithFrame:(NSRect)frame textContainer:(NSTextContainer*)tc state:(BOOL)state parentList:(SPRTodoListView*)p;
-(SPRTodoListView*)parentList;
-(CGFloat)necessaryHeight;

/*Layoutmanager delegate*/
- (NSDictionary *)layoutManager:(NSLayoutManager *)layoutManager shouldUseTemporaryAttributes:(NSDictionary *)attrs forDrawingToScreen:(BOOL)toScreen atCharacterIndex:(NSUInteger)charIndex effectiveRange:(NSRangePointer)effectiveCharRange;

@end

@interface NSView (SPRTodoItemView)

-(SPRTodoItemView*)parentTodoItemView;

@end
