//
//  NoteBook.h
//  SpiralBound
//
//  Created by Philip White on 2/5/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

/*
Let this class be in charge of saving data periodically
*/


@class SPRPage;
@interface SPRNotebook : NSObject
{
	NSString *title;
	NSMutableArray *pages;
}

+(void)initialize;
+(NSArray*)notebooks;
+(BOOL)notebookDataExists;

+(NSArray*)loadAllNotebooksWithUniqueHashes;
+(NSArray*)loadUnloadedNotebooksWithUniqueHashes;

+(SPRNotebook*)notebookWithTitle:(NSString*)title;
+(SPRNotebook*)notebookContainingPage:(SPRPage*)page;

+(void)beginEncodingWithoutSMarker;
+(void)endEncodingWithoutSMarker;
+(void)beginDecodingWithUniqueHashes;
+(void)endDecodingWithUniqueHashes;
+(BOOL)isTitleAvailable:(NSString*)title;

//this method is called automatically by -initWithTitle:
//this is the only method that modifies the array
//it returns the new array
+(NSArray*)addNotebook:(SPRNotebook*)newNotebook;
+(SPRPage*)pageWithHash:(NSUInteger)hash;
+(NSArray*)pagesWithHashes:(NSArray*)hashes;
+(SPRNotebook*)createNotebookWithTitle:(NSString*)title;
+(SPRNotebook*)createNotebookWithIntroText;
+(SPRNotebook*)createNotebookWithOriginalSBDataAndTitle:(NSString*)title;
+(SPRNotebook*)createNotebookWithStickiesDataAndTitle:(NSString*)title;
+(SPRNotebook*)createNotebookWithPages:(NSArray*)pages title:(NSString*)title;
+(SPRNotebook*)createNotebookWithObject:(id)obj title:(NSString*)title;
+(NSArray*)deleteNotebook:(SPRNotebook*)notebook;
+(NSArray*)deleteNotebooks:(NSArray*)notebooks;
+(NSArray*)notebooksWithTitles:(NSArray*)titles;

+(void)saveData;

+(BOOL)originalSBDataExists;
+(BOOL)stickiesDataExists;

//this first initializer should not be called except by +sharedNotebooks;
-(id)initWithFileName:(NSString*)name;

-(id)initWithOriginalSBDataAndTitle:(NSString*)newTitle;
-(id)initWithTitle:(NSString*)newTitle;
-(void)deletePage:(SPRPage*)page;
-(SPRPage*)_newPlainPageWithIntroText:(NSAttributedString*)c;
-(SPRPage*)_newTodoPageWithIntroText:(NSAttributedString*)c;
-(SPRPage*)newPage;
-(SPRPage*)newTodoPage;
-(SPRPage*)newPageOfClass:(Class)c;
-(SPRPage*)newPageOfClass:(Class)c atIndex:(NSUInteger)i;
-(NSArray*)pages;

-(SPRPage*)firstPage;
-(NSUInteger)pageCount;

-(SPRPage*)pageWithHash:(NSUInteger)hash;

-(void)saveData;

-(SPRPage*)pageBeforePage:(SPRPage*)page; //return nil if it's the first page
-(SPRPage*)pageAfterPage:(SPRPage*)page; //return nil if it's the last page

-(void)movePages:(NSArray*)pagesToMove toIndex:(NSUInteger)destIndex;

-(void)movePage:(SPRPage*)page toBeforePage:(SPRPage*)referencePage;
-(void)movePageToEnd:(SPRPage*)page;
-(void)movePageToBeginning:(SPRPage*)page;
-(void)appendPages:(NSArray*) pages;

-(void)insertPages:(NSArray*) newPages atIndex:(NSUInteger)index;

//returns an array of the new pages
-(NSArray*)importFromStickies;

- (NSString *)title;
- (void)setTitle:(NSString *)value;




@end
