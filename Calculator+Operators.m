//
//  Calculator+Operators.m
//  SpiralBound
//
//  Created by Philip White on 2/11/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "Calculator+Operators.h"

static double RadiansToDegreesMultiplier = 57.295779513082310781487325535855875606;

@implementation Calculator (Operators)

+ (void)setupLookupTables
{
	BinaryOperatorLookup = [[NSDictionary alloc] initWithObjectsAndKeys:
											[Add class], @"+",
											[Subtract class], @"-",
											[Multiply class], @"*",
											[Divide class], @"/",
											[Modulus class],@"%",
											[Power class],@"^",
																						nil];
	
	
	UnaryOperatorLookup = [[NSDictionary alloc] initWithObjectsAndKeys:
											[Negate class], @"~",
											[Sine class], @"sin",
											[Sine class], @"sine",
											[Cosine class], @"cos",
											[Cosine class], @"cosine",
											[Tangent class], @"tan",
											[Tangent class], @"tangent",
											[SineDegrees class], @"sind",
											[CosineDegrees class], @"cosd",
											[TangentDegrees class], @"tand",
											[NaturalLog class], @"ln",
											[Base10Log class], @"log",
											[Arcsine class], @"asin",
											[Arccosine class], @"acos",
											[Arctangent class], @"atan",
											[ArcsineDegrees class], @"asind",
											[ArccosineDegrees class], @"acosd",
											[ArctangentDegrees class], @"atand",
											[AbsoluteValue class], @"abs",		nil];	
}




#pragma mark UNARY

@end

@implementation Negate : NSObject
+(NSDecimalNumber*)op:(NSDecimalNumber*)op
{
	return (NSDecimalNumber*)[op decimalNumberByMultiplyingBy: (NSDecimalNumber*)[NSDecimalNumber numberWithInt:-1]];
}
@end

@implementation Sine : NSObject
+(NSDecimalNumber*)op:(NSDecimalNumber*)op
{
	return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: sin([op doubleValue])];
}
@end

@implementation Cosine : NSObject
+(NSDecimalNumber*)op:(NSDecimalNumber*)op
{
	return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: cos([op doubleValue])];
}
@end

@implementation Tangent : NSObject
+(NSDecimalNumber*)op:(NSDecimalNumber*)op
{
	return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: tan([op doubleValue])];
}

@end

@implementation SineDegrees : NSObject

+(NSDecimalNumber*)op:(NSDecimalNumber*)op
{
	return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: sin([op doubleValue]/RadiansToDegreesMultiplier)];
}
@end

@implementation CosineDegrees : NSObject
+(NSDecimalNumber*)op:(NSDecimalNumber*)op
{
	return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: cos([op doubleValue]/RadiansToDegreesMultiplier)];
}
@end

@implementation TangentDegrees : NSObject
+(NSDecimalNumber*)op:(NSDecimalNumber*)op
{
	return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: tan([op doubleValue]/RadiansToDegreesMultiplier)];
}
@end

@implementation NaturalLog : NSObject
+(NSDecimalNumber*)op:(NSDecimalNumber*)op
{
	return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: log([op doubleValue])];
}
@end

@implementation Base10Log : NSObject
+(NSDecimalNumber*)op:(NSDecimalNumber*)op
{
	return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: log10([op doubleValue])];
}
@end

@implementation Arcsine : NSObject
+(NSDecimalNumber*)op:(NSDecimalNumber*)op
{
	return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: asin([op doubleValue])];
}
@end

@implementation Arccosine : NSObject
+(NSDecimalNumber*)op:(NSDecimalNumber*)op
{
	return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: acos([op doubleValue])];
}
@end

@implementation Arctangent : NSObject
+(NSDecimalNumber*)op:(NSDecimalNumber*)op
{
	return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: atan([op doubleValue])];
}

@end

@implementation ArcsineDegrees : NSObject

+(NSDecimalNumber*)op:(NSDecimalNumber*)op
{
	return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: asin([op doubleValue])*RadiansToDegreesMultiplier];
}
@end

@implementation ArccosineDegrees : NSObject
+(NSDecimalNumber*)op:(NSDecimalNumber*)op
{
	return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: acos([op doubleValue])*RadiansToDegreesMultiplier];
}
@end

@implementation ArctangentDegrees : NSObject
+(NSDecimalNumber*)op:(NSDecimalNumber*)op
{
	return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: atan([op doubleValue])*RadiansToDegreesMultiplier];
}
@end

@implementation AbsoluteValue : NSObject
+(NSDecimalNumber*)op:(NSDecimalNumber*)op
{
	return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble: fabs([op doubleValue])];
}
@end

#pragma mark BINARY


@implementation Add : NSObject
+(NSDecimalNumber*)op1:(NSDecimalNumber*)op1 op2:(NSDecimalNumber*)op2
{
	return [op1 decimalNumberByAdding:op2];
}
@end

@implementation Subtract : NSObject
+(NSDecimalNumber*)op1:(NSDecimalNumber*)op1 op2:(NSDecimalNumber*)op2
{
	return [op2 decimalNumberBySubtracting: op1];
}
@end

@implementation Multiply : NSObject
+(NSDecimalNumber*)op1:(NSDecimalNumber*)op1 op2:(NSDecimalNumber*)op2
{
	return [op1 decimalNumberByMultiplyingBy:op2];
}
@end

@implementation Divide : NSObject
+(NSDecimalNumber*)op1:(NSDecimalNumber*)op1 op2:(NSDecimalNumber*)op2
{
    if ([op1 doubleValue] == 0)
        return [NSDecimalNumber zero];
    else
        return [op2 decimalNumberByDividingBy:op1];
}
@end

@implementation Modulus : NSObject
+(NSDecimalNumber*)op1:(NSDecimalNumber*)op1 op2:(NSDecimalNumber*)op2
{
	if ([op1 doubleValue] == 0.0)
        return [NSDecimalNumber zero];
    else
        return (NSDecimalNumber*)[NSDecimalNumber numberWithLongLong:
			[op2 longLongValue] % [op1 longLongValue]];
}
@end

@implementation Power : NSObject
+(NSDecimalNumber*)op1:(NSDecimalNumber*)op1 op2:(NSDecimalNumber*)op2
{
	double op1d = [op1 doubleValue], op2d = [op2 doubleValue];
	
	if (op2d < 0 && (op1d > -1 && op1d < 1) && op1d != 0)
		return nil;
	
	return (NSDecimalNumber*)[NSDecimalNumber numberWithDouble:
			pow(op2d, op1d)];
}
@end

