//
//  SPROrganizer+Sorting.m
//  SpiralBound Pro
//
//  Created by Philip White on 5/1/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "SPROrganizer+Sorting.h"
#import "SPRNotebook.h"
#import "SPRPage.h"
#import "SpiralBoundWindow.h"
#import "NotificationNames.h"
#import "PageSorter_SBPro.h"
extern BOOL ShouldAnimate;

@implementation SPROrganizer (Sorting)

+(void)movePagesWithHashes:(NSArray*)hashArray destinationOrganizer:(SPROrganizer*)destination firstPageIndex:(NSUInteger)firstPageIndex
{
	//first get the pages and retain them (implicitly via the array)
	NSArray *pages = [SPRNotebook pagesWithHashes:hashArray];
	NSEnumerator *enumerator = [pages objectEnumerator];
	SPRPage *page;
	SPROrganizer *organizer = nil;
	
	page = [enumerator nextObject];
	organizer = [self organizerForPage:page];
	
	if (organizer == destination) //moving within one notebook
	{
		[organizer movePages:pages toIndex:firstPageIndex];
	}
	else
	{
		do
		{
			[organizer removePage: page];
		} while (page = [enumerator nextObject]);
		
		[destination addPages: pages atIndex:firstPageIndex];
		 
	}
}

+(void)copyPagesWithHashes:(NSArray*)hashArray destinationOrganizer:(SPROrganizer*)destination firstPageIndex:(NSUInteger)firstPageIndex
{
	NSArray *pages = [SPRNotebook pagesWithHashes:hashArray];
	
	NSEnumerator *enumerator = [pages objectEnumerator];
	SPRPage *page;
	
	NSMutableArray *copiedPages = [NSMutableArray arrayWithCapacity:[pages count]];
	
	while (page = [enumerator nextObject])
	{
		[copiedPages addObject: [page copy]];
	}
	
	[destination addPages: copiedPages atIndex:firstPageIndex];
}

-(void)movePages:(NSArray*)pages toIndex:(NSUInteger)index
{
	[notebook movePages:pages toIndex:index];
}

-(void)addPages:(NSArray*)pages atIndex:(NSUInteger)index
{
	[notebook insertPages:pages atIndex:index];
}

+(NSArray*)orderedArrayOfWindows
{
	NSMutableArray *windows = [NSMutableArray array];
	NSEnumerator *enumerator = [[self organizers] objectEnumerator];
	SPROrganizer *org;
	while (org = [enumerator nextObject])
	{
		[windows addObject:[org mainWindow]];
		[windows addObjectsFromArray:[org tornOffWindows]];
	}

    //now find if the sorter window is open and add it to the end
    NSWindow *sw = [PageSorter sorterWindow];
    if (sw)
        [windows addObject:sw];
    
	return windows;
}

-(void)removePage:(SPRPage*)page
{
	NSUInteger pageIndex = [[notebook pages] indexOfObject:page];
	NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:notebook, @"Notebook", [NSNumber numberWithUnsignedInteger:pageIndex], @"PageIndex", nil];
	[[NSNotificationQueue defaultQueue] enqueueNotification:
	 [NSNotification notificationWithName:PageWillBeDeletedNotification	object:page userInfo:userInfo]
											   postingStyle:NSPostNow];

	SpiralBoundWindow *window;
	
	
	if (page == mainWindowPage)
	{
		//get the page that will be the new top page
		SPRPage *nextPage = [self nextPageInStack];
		
		if (nextPage == mainWindowPage)	//then we need to create a new page
		{
			nextPage = [notebook newPage];
		}
		
		[self setMainWindowPage: nextPage];
	}
	else if ((window = [self windowForPage:page]))
	{
		[window close];
		[tornOffWindows removeObject:window];
		[tornOffPages removeObject: page];
	}
	
	//if 'page' was mainWindowPage it still contains the old page, which we can now delete
	[notebook deletePage: page];
}

-(void)removePageAtIndex:(NSUInteger)index
{
	NSArray *pages = [notebook pages];
	
	SPRPage *page = [pages objectAtIndex:index];
	
	[self removePage:page];
}

-(SPRPage*)nextPageInStack
{
	NSArray *pages = [[self notebook] pages];
    NSUInteger i = 0;
    NSUInteger pageCount = [pages count];
    NSUInteger topPageIndex = [pages indexOfObject:mainWindowPage];
	
	for (i=topPageIndex+1; i < pageCount; i++)
	{
		SPRPage *page = [pages objectAtIndex:i];
		
		if ([tornOffPages containsObject:page] == NO)
			return page;
	}
	
	//wrap around
	for (i=0; i<topPageIndex; i++)
	{
		SPRPage *page = [pages objectAtIndex:i];
		
		if ([tornOffPages containsObject:page] == NO)
			return page;
	}
	
	//if we got here then there are no other pages in the stack so just return the top page
	return mainWindowPage;
}

-(SPRPage*)previousPageInStack
{
	NSArray *pages = [[self notebook] pages];
    NSInteger i = 0;
    NSUInteger pageCount = [pages count];
	NSUInteger topPageIndex = [pages indexOfObject:mainWindowPage];
	
	
	for (i=topPageIndex-1; i >= 0; i--)
	{
		SPRPage *page = [pages objectAtIndex:i];
		
		if ([tornOffPages containsObject:page] == NO)
			return page;
	}
	
	for (i=pageCount-1; i > topPageIndex; i--)
	{
		SPRPage *page = [pages objectAtIndex:i];
		
		if ([tornOffPages containsObject:page] == NO)
			return page;		
	}
	
	//if we got here then there are no other pages in the stack so just return the top page
	return mainWindowPage;
}


- (void)returnPageFromTornOffWindow:(SpiralBoundWindow*)window
{
	[[self pageForWindow:window] autotitle]; //this just returns if it is already titled
	
	NSUInteger index = [tornOffWindows indexOfObject:window];
	SPRPage *pageBeingReturned = [tornOffPages objectAtIndex:index];
	
	if (pageBeingReturned == nil)
	{
		NSLog(@"ERROR: tornOffPages and tornOffWindows arrays not in sync");
		return;
	}
	
	if (ShouldAnimate)
	{
		NSRect disappearanceFrame = [mainWindow frame];
		disappearanceFrame.origin.x+=disappearanceFrame.size.width/2;
		disappearanceFrame.origin.y+=disappearanceFrame.size.height/2;
		disappearanceFrame.size.width = 10;
		disappearanceFrame.size.height = 10;
		[window setFrame:disappearanceFrame display: YES animate: YES rate:0.25];
	}
	
	[window close];
	[tornOffWindows removeObject:window];
	[tornOffPages removeObject:pageBeingReturned];
}

//page in
-(void)movePageToTop:(SpiralBoundWindow*)window
{
	[[self pageForWindow:window] autotitle]; //this just returns if it is already titled
	
	if (ShouldAnimate)
	{
		NSRect disappearanceFrame = [mainWindow frame];
		[window setFrame:disappearanceFrame display: YES animate: YES rate:0.25];
	}
	
	SPRPage *pageBeingReturned = [tornOffPages objectAtIndex:[tornOffWindows indexOfObject:window]];
	
	
	[notebook movePage:pageBeingReturned toBeforePage:mainWindowPage];
	[self setMainWindowPage:pageBeingReturned];
	
	[window setReleasedWhenClosed:YES];
	[window performSelector:@selector(close) withObject:nil afterDelay: 0.0];
	[tornOffWindows removeObject:window];
	[tornOffPages removeObject:pageBeingReturned];
}

-(void)movePageToEnd:(SpiralBoundWindow*)window
{
	[[self pageForWindow:window] autotitle]; //this just returns if it is already titled
	
	NSUInteger index = [tornOffWindows indexOfObject:window];
	SPRPage *pageBeingReturned = [tornOffPages objectAtIndex:index];
	
	if (pageBeingReturned == nil)
	{
		[NSException raise:@"NotebookOrganizerException" format:@"ERROR: tornOffPages and tornOffWindows arrays not in sync"];
		return;
	}
	
	if (ShouldAnimate)
	{
		NSRect disappearanceFrame = [mainWindow frame];
		disappearanceFrame.origin.x+=disappearanceFrame.size.width/2;
		disappearanceFrame.origin.y+=disappearanceFrame.size.height/2;
		disappearanceFrame.size.width = 10;
		disappearanceFrame.size.height = 10;
		[window setFrame:disappearanceFrame display: YES animate: YES rate:0.25];
	}
	
	[window close];
	[tornOffWindows removeObject:window];
	[tornOffPages removeObject:pageBeingReturned];

	[notebook movePageToEnd:pageBeingReturned];
}

-(void)movePageToBeginning:(SpiralBoundWindow*)window
{
	[[self pageForWindow:window] autotitle]; //this just returns if it is already titled
	
	NSUInteger index = [tornOffWindows indexOfObject:window];
	SPRPage *pageBeingReturned = [tornOffPages objectAtIndex:index];
	
	if (pageBeingReturned == nil)
	{
		NSLog(@"ERROR: tornOffPages and tornOffWindows arrays not in sync");
		return;
	}
	
	if (ShouldAnimate)
	{
		NSRect disappearanceFrame = [mainWindow frame];
		disappearanceFrame.origin.x+=disappearanceFrame.size.width/2;
		disappearanceFrame.origin.y+=disappearanceFrame.size.height/2;
		disappearanceFrame.size.width = 10;
		disappearanceFrame.size.height = 10;
		[window setFrame:disappearanceFrame display: YES animate: YES rate:0.25];
	}
	
	[window close];
	[tornOffWindows removeObject:window];
	[tornOffPages removeObject:pageBeingReturned];
	
	[notebook movePageToBeginning:pageBeingReturned];
}

@end
