//
//  SPROrganizer+Security.m
//  SpiralBound Pro
//
//  Created by Philip White on 5/9/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "SPROrganizer+Security.h"
#import "SPRPreferencesController.h"
#import "Inquisitor.h"
#import "SpiralBoundWindow.h"
#import "SPRTextView.h"
#import "SPRPage.h"
#import "SPRNotebook.h"
#import "SPRLockableTextStorage.h"
#import "SBProtoFrameView.h"
#import "SPRTodoListView.h"
extern NSMutableArray *Organizers;

@implementation SPROrganizer (Security)

+(void)lockAllPages
{
	SPRLockableTextStorageLockAll();
	//todo lists need to be resized to fit the bullets
	
	for (SPROrganizer *org in [self organizers])
	{
		NSEnumerator *e = [[org tornOffWindows] objectEnumerator];
		SpiralBoundWindow *win = (id)[org mainWindow];
		
		do {
			if ([(id)[win contentView] hasMainTodoListView])
			{
				[[win mainTodoListView] sizeAndPlaceItems];
			}
			
			
		}while (win = [e nextObject]);
	}
	//[Organizers makeObjectsPerformSelector:@selector(lockAllPages)];
}

+(void)unlockAllPages
{
	SPRLockableTextStorageUnlockAll();
	
	//todo lists need to be resized to fit the unlocked text
	for (SPROrganizer *org in [self organizers])
	{
		NSEnumerator *e = [[org tornOffWindows] objectEnumerator];
		SpiralBoundWindow *win = (id)[org mainWindow];
		
		do {
			if ([(id)[win contentView] hasMainTodoListView])
			{
				[[win mainTodoListView] sizeAndPlaceItems];
			}
		}while (win = [e nextObject]);
	}
	//[Organizers makeObjectsPerformSelector:@selector(unlockAllPages)];
}

-(void)lockAllPages
{
	[NSException raise:@"NotebookOrganizerException" format:@"Called discontinued method -lockAllPages"];
	/*[[mainWindow mainTextView] setIsScrambled:YES];
	
	NSUInteger i, count = [tornOffPages count];
	
	for (i=0; i<count; i++)
	{
		[[[tornOffWindows objectAtIndex:i] mainTextView] setIsScrambled:YES];
	}*/
}

-(void)unlockAllPages
{
	[NSException raise:@"NotebookOrganizerException" format:@"Called discontinued method -unlockAllPages"];
	/*[[mainWindow mainTextView] setIsScrambled:NO];
	
	NSUInteger i, count = [tornOffPages count];
	
	for (i=0; i<count; i++)
	{
		[[[tornOffWindows objectAtIndex:i] mainTextView] setIsScrambled:NO];
	}*/
}

-(IBAction)lockPage:(id)sender
{
	NSWindow *window;
	SPRPage *page;
	
	if ([sender isKindOfClass:[NSButton class]])
	{
		window = [sender window];
	}
	else
	{
		window = [NSApp mainWindow];
	}
	
	page = [self pageForWindow:window];
	
	if ([page isSecure])
	{
		if ([[SPRPreferencesController sharedPreferencesController] hasPassword])
		{
			if ([[Inquisitor sharedInquisitor] challenge] == NO)
				return;
		}
		[page setSecure:NO];
	}
	else
	{
		[page setSecure:YES];
	}
}


-(BOOL)areAnyPagesSecured
{
	NSArray *pages = [[self notebook] pages];
	NSEnumerator *enumerator = [pages objectEnumerator];
	SPRPage *page;
	
	while (page = [enumerator nextObject])
	{
		if ([page isSecure])
			return YES;
	}
	
	return NO;
}

@end
