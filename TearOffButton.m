//
//  TearOffButton.m
//  SpiralBound
//
//  Created by Philip White on 2/5/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "TearOffButton.h"
#import "SBController.h"
#import "SPROrganizer.h"
#import "RIVTools.h"
#import "SBProtoFrameView.h"

static NSImage *sButtonImage;

@implementation TearOffButton

+ (void)initialize
{
	sButtonImage = [NSImage imageNamed:@"Sheet.tiff"];
}

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
	{
        // Initialization code here.
    }
    return self;
}


static BOOL sHasExitedCriticalArea = NO;
//static NSPoint sDrawOffset;
static NSPoint sWindowOffset;
static BOOL sHasCreatedWindow = NO;
static NSWindow *sNewWindow = nil;

- (void)mouseDragged:(NSEvent *)theEvent
{
	static NSPoint originalMouseLocation;
	static NSRect originalFrame;
    
	
	if (sHasCreatedWindow == NO)
	{
		originalMouseLocation = [[self window] convertBaseToScreen:[theEvent locationInWindow]];
		sNewWindow = (id)[[SPROrganizer organizerForWindow:[self window]] requestTearOffWindow: (&originalFrame)];
		
		sHasCreatedWindow = YES;
	}
	else
	{
		//move the window
		NSRect newFrame = originalFrame;
		NSPoint newMouseLocation = [[self window] convertBaseToScreen:[theEvent locationInWindow]];
		
		float deltaX = newMouseLocation.x-originalMouseLocation.x;
		float deltaY = newMouseLocation.y-originalMouseLocation.y;
		
		newFrame.origin.x+=deltaX;
		newFrame.origin.y+=deltaY;
		[sNewWindow setFrame: newFrame display: YES];
	}
}

- (void)mouseUp:(NSEvent *)theEvent
{
	NSRect newFrame;
	NSTimer *movementTimer=nil;
	if (sHasCreatedWindow == NO)
	{
		NSRect originalFrame;
		sNewWindow = (id)[[SPROrganizer organizerForWindow:[self window]] requestTearOffWindow: (&originalFrame)];
		originalFrame = NSOffsetRect([[self window] frame], 30, -30);
		
		{
			//animate the new page. But later, so the page contents can update
			NSMethodSignature *sig = [sNewWindow methodSignatureForSelector:@selector(setFrame:display:animate:)];
			NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:sig];
			[invocation setTarget:sNewWindow];
			[invocation setSelector:@selector(setFrame:display:animate:)];
			BOOL yep = YES;
			[invocation setArgument:&originalFrame atIndex:2];
			[invocation setArgument:&yep atIndex:3];
			[invocation setArgument:&yep atIndex:4];
			movementTimer = [NSTimer scheduledTimerWithTimeInterval:0.0
														 invocation:invocation
															repeats:NO];
		}
		newFrame = originalFrame;
	}
	else
	{
		newFrame = [sNewWindow frame];
	}
	
	[self setImage:[NSImage imageNamed:@"Sheet"]];

	//constrain the windows bar to the screen
	{
		SBProtoFrameView *view = [sNewWindow contentView];
		NSRect dragbarRect = [view visibleToolbarAndDragbarRect];
		
		dragbarRect.origin.x += newFrame.origin.x;
		dragbarRect.origin.y += newFrame.origin.y;
		float offset = RIVYOffsetToConfineRectVerticallyWithinScreens(dragbarRect);
		
		if (offset != 0.0)
		{
			newFrame.origin.y+=offset;
			[sNewWindow setFrame:newFrame display:YES animate:NO];
			if (movementTimer)
				[movementTimer invalidate];
		}
	}
	sNewWindow = nil;
	sHasCreatedWindow=NO;
}

- (void)mouseDown:(NSEvent *)theEvent
{
	sHasExitedCriticalArea = NO;

	sWindowOffset = [theEvent locationInWindow];
	sHasCreatedWindow = NO;
	
	[self setImage:[NSImage imageNamed:@"SheetAlt"]];
}


- (BOOL)acceptsFirstMouse:(NSEvent *)theEvent
{
	return YES;
}

@end
