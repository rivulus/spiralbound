//
//  SPRSearchEngine.m
//  CommonSpiralbound
//
//  Created by Philip on 5/31/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import "SPRSearchEngine.h"
#import "SpiralBoundWindow.h"
#import "OrganizerIncludes.h"
#import "DataLayerIncludes.h"
#import "NotificationNames.h"
#import "RIVTools.h"
#import "NSString+RIVSearch.h"
#import "DefaultsKeys.h"

//these enumerated values correspond to indices in the menus/radio buttons in the search dialog
//the values can be accessed from userdefaults as integers (binding uses index value)


typedef enum
{
	SearchTitlesOnly,
	SearchContentsOnly,
	SearchTitlesAndContents
} SearchTitlesOrContents;

typedef enum
{
	SearchPlainPagesOnly,
	SearchTodoListsOnly,
	SearchAllTypesOfPages
} SearchPageType;

typedef enum
{
	SearchCurrentPage,
	SearchCurrentNotebook,
	SearchAllNotebooks
} SearchScope;

@class SPRPage;
//typedef struct
//{
//	SPRPage *page;
//	BOOL titleResult; //the result was found in the title
//	NSRange	range;
//} SearchResult;

@interface SPRSearchResult : NSObject
@property (nonatomic, strong) SPRPage *page;
@property (nonatomic, assign, getter = isTitleResult) BOOL titleResult;
@property (nonatomic, assign) NSRange range;
@end

@implementation SPRSearchResult
@end

SPRSearchEngine *Instance=nil;

@interface SPRSearchEngine ()

@end

@implementation SPRSearchEngine

+(SPRSearchEngine*)soleInstance
{
	if (Instance == nil)
	{
		@synchronized (self)
		{
			Instance = [[SPRSearchEngine alloc] init];
		}
	}
	return Instance;
}

-(id)init
{
	self = [super initWithWindowNibName:@"Search"];
	
	if (self)
	{
		[self determineScopeTitles:nil];
		
		NSWindow *window = [self window];
												   
		//we need to know which page is frontmost to correctly set the scope menu title
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(windowDidBecomeMain:)
													 name:NSWindowDidBecomeMainNotification
												   object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(mainWindowPageDidChange:)
													 name:MainWindowPageDidChangeNotification
												   object:nil];										   
		
		results = [NSMutableArray new];
	}
	return self;
}

- (NSArray *)scopeTitles
{
    return scopeTitles;
}

- (void)setScopeTitles:(NSArray *)value
{
    if (scopeTitles != value)
	{
        scopeTitles = [value copy];
    }
}

#pragma mark Notifications

-(void)windowDidBecomeMain:(NSNotification*)note
{
	SpiralBoundWindow *newMain = [note object];
	
	if ([newMain isKindOfClass:[SpiralBoundWindow class]]) //FScript is the only reason it wouldn't be that I can think of
	{
		[self determineScopeTitles:newMain];
	}
}

-(void)mainWindowPageDidChange:(NSNotification*)note
{
	//remember that main window here refers to the spirally window of an organizer. this window should be the main one also
	//doesn't matter here though, just call -determineScopeTitles which uses NSApp's -mainWindow if passed nil
	[self determineScopeTitles:nil];
}

-(void)determineScopeTitles:(SpiralBoundWindow*)newMain
{
	if (newMain == nil)
		newMain = (SpiralBoundWindow*)[NSApp mainWindow];
		
	SPROrganizer *org;

	org = [SPROrganizer organizerForWindow:newMain];

	SPRPage *page = [org pageForWindow:newMain];
	
	NSArray *newTitles;
	
	NSString *item0 = [NSString stringWithFormat:@"Page \"%@\"",[page title]];
	NSString *item1 = [NSString stringWithFormat:@"Notepad \"%@\"", [org title]];
	newTitles = [NSArray arrayWithObjects:item0,item1,@"All Notepads",nil];
	[self setScopeTitles:newTitles];
}

-(NSArray*)pagesForSearchSettings
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	SearchScope scope = (SearchScope)[defaults integerForKey:DefaultsSearchEngineScope];
	
	SearchPageType pageType = (SearchPageType)[defaults integerForKey:DefaultsSearchEnginePageType];
	
	//construct the correct pages array based on the scope
	NSMutableArray *temp = [NSMutableArray array];
	
	NSWindow *mainWindow = [NSApp mainWindow];
	if (![mainWindow isKindOfClass:[SpiralBoundWindow class]])
		[NSException raise:@"SPRSearchEngineException"
					format:@"Failure in -pagesForSearchSettings. NSApp's -mainWindow returned window with title:\"%@\", class:\"%@\"",
		 [mainWindow title], NSStringFromClass([mainWindow class])];
	
	SPROrganizer *organizer = [SPROrganizer organizerForWindow:mainWindow];
	SPRPage *page = [organizer pageForWindow:mainWindow];
	
	if (scope == SearchCurrentPage)
	{
		[temp addObject:page];
	}
	else if (scope == SearchCurrentNotebook)
	{
		[temp setArray:[[organizer notebook] pages]];
	}
	else if (scope == SearchAllNotebooks)
	{
		NSEnumerator *enumerator = [[SPROrganizer organizers] objectEnumerator];
		SPROrganizer *organizer;
		while (organizer = [enumerator nextObject])
		{
			[temp addObjectsFromArray:[[organizer notebook] pages]];
		}
	}
	else
	{
		[NSException raise:@"SPRSearchEngineException"
					format:@"Failure in -pagesForSearchSettings. scope = %i", scope];
	}
	
	//now reorder the array so that the main window page is first
	//important to do this before filter below, because the main window page might be removed there
	if ([temp count] > 1)
	{
		NSUInteger mainWindowPageIndex = [temp indexOfObject:page];
		NSRange moveToEndRange = NSMakeRange(0,mainWindowPageIndex);
		NSArray *pagesToMoveToEnd = [temp subarrayWithRange:moveToEndRange];
		[temp removeObjectsInRange:moveToEndRange];
		[temp addObjectsFromArray:pagesToMoveToEnd];
	}
	
	//now remove locked pages and pages of the wrong class
	Class dontSearchClass=nil;
	if (pageType == SearchTodoListsOnly)
		dontSearchClass = [SPRPlainPage class];
	else if (pageType == SearchPlainPagesOnly)
		dontSearchClass = [SPRTodoPage class];
	
	NSMutableArray *final = [NSMutableArray array];
	NSEnumerator *enumerator = [temp objectEnumerator];
	SPRPage *p;
	while (p=[enumerator nextObject])
	{
		if (![p isSecure] && (dontSearchClass==nil || ![p isKindOfClass:dontSearchClass]))
			[final addObject:p];
	}
	
	return final;
}

-(IBAction)find:(id)sender
{
	[results removeAllObjects];
	selectedResult = 0;
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	SearchTitlesOrContents titleOrContents = (SearchTitlesOrContents)[defaults integerForKey:DefaultsSearchEngineTitlesOrContents];
	NSArray *pages = [self pagesForSearchSettings];
	
	BOOL ignoreCase = [defaults boolForKey:DefaultsSearchEngineIgnoreCase],
		 wholeWordsOnly = [defaults boolForKey:DefaultsSearchEngineWholeWordsOnly];
	BOOL ignoreDiacriticals = [defaults boolForKey:DefaultsSearchEngineIgnoreDiacriticals];
	
	//note, adding kCFCompareNonLiteral 
	CFStringCompareFlags compareFlags = (ignoreCase ? kCFCompareCaseInsensitive : 0);
	compareFlags |= kCFCompareWidthInsensitive;
	compareFlags |= (ignoreDiacriticals ? kCFCompareDiacriticInsensitive : 0);
	
	[self setSearchString:[searchField stringValue]];
	if (![searchString isEqualToString:@""])
    {
        for(SPRPage *page in pages)
        {
            if (titleOrContents == SearchTitlesOnly || titleOrContents == SearchTitlesAndContents)
            {
                NSString *text = [page title];
                NSArray *ranges = [text rangesForSearchString:searchString
                                                      options:compareFlags
                                                   wholeWords:wholeWordsOnly];
                
                for (NSValue *range in ranges)
                {
                    NSRange r = [range rangeValue];
                    SPRSearchResult *result = [[SPRSearchResult alloc] init];
                    [result setPage:page];
                    [result setTitleResult:YES];
                    [result setRange:r];
                    [results addObject:result];
                }
            }
            
            if (titleOrContents == SearchContentsOnly || titleOrContents == SearchTitlesAndContents)
            {
                NSString *text = [page contentsAsString];
                NSArray *ranges = [text rangesForSearchString:searchString
                                                      options:compareFlags
                                                   wholeWords:wholeWordsOnly];
                
                for (NSValue *range in ranges)
                {
                    NSRange r = [range rangeValue];
                    SPRSearchResult *result = [[SPRSearchResult alloc] init];
                    [result setPage:page];
                    [result setTitleResult:NO];
                    [result setRange:r];
                    [results addObject:result];
                }
            }
        }
        if ([results count] > 0)
            [self selectResult:0];
        else
            NSBeep();
    }
	
    //Now set the message text field
    
    if ([searchString length] == 0)
        [resultsMessageField setStringValue:@"No Search Performed"];
    else if ([results count] == 0)
        [resultsMessageField setStringValue:[NSString stringWithFormat:@"No results for \"%@\"",searchString]];
    else
        [resultsMessageField setStringValue:[NSString stringWithFormat:@"Result 1 of %u for \"%@\"",
                                             [results count],
                                             searchString]];
}

-(IBAction)findNext:(id)sender
{
	if ([results count] == 0)
	{
		NSBeep();
		return;
	}
	
	
	NSUInteger index;
	if (selectedResult == [results count]-1)
	{
		NSBeep();
		index = 0;
	}
	/*else if (selectedResult == UINT32_MAX)
	{
		index=0;
	}*/
	else
	{
		index = selectedResult+1;
	}
	[self selectResult:index];
}

-(IBAction)findPrevious:(id)sender
{
	if ([results count] == 0)
	{
		NSBeep();
		return;
	}
	
	NSUInteger index;
	if (selectedResult == 0)
	{
		NSBeep();
		index = [results count]-1;
	}
	/*else if (selectedResult == UINT32_MAX)
	{
		index=[results count]-1;
	}*/
	else
	{
		index = selectedResult-1;
	}
	[self selectResult:index];
}

-(void)selectResult:(NSUInteger)i
{
    SPRSearchResult *result = [results objectAtIndex:i];
	selectedResult = i;
	[SPROrganizer raisePage:result.page
						   range:result.range
					  buldgeMain:YES
					rangeInTitle:result.titleResult];
	
	[resultsMessageField setStringValue:[NSString stringWithFormat:@"Result %u of %u for \"%@\"",
																   selectedResult+1,
																   [results count],
																   searchString]];
}

- (NSString *)searchString
{
    return searchString;
}

- (void)setSearchString:(NSString *)value
{
    if (searchString != value)
	{
        searchString = [value copy];
    }
}



-(void)dealloc
{
	@synchronized ([SPRSearchEngine class])
	{
		[[NSNotificationCenter defaultCenter] removeObserver:self];
		Instance=nil;
	}
}
@end
