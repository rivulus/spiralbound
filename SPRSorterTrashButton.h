//
//  SPRSorterTrashButton.h
//  CommonSpiralbound
//
//  Created by Philip on 7/6/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface SPRSorterTrashButton : NSButton
{

}
-(void)deleteNotebooks:(NSArray*)titles;
-(void)deletePages:(NSArray*)hashes;
@end
