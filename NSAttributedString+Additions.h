//
//  NSAttributedString+Additions.h
//  SpiralBound Pro
//
//  Created by Philip White on 5/27/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface NSAttributedString (RIVAdditions)

-(BOOL)attribute:(NSString*)attribute existsInRange:(NSRange)range andReturnsTrueForSelector:(SEL)selector;
-(NSData*)dataUsingDocumentType:(NSString*)doctype;

@end
