//
//  SPRCalculatorHandler.h
//  CommonSpiralbound
//
//  Created by Philip on 5/28/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

/*
One instance of this class exists for every SPRTextView.
It is no longer the text view delegate
*/

@class SPRTextView;
@interface SPRCalculatorHandler : NSObject
{
	SPRTextView *textView;
	
	BOOL calculatorShouldBeCalled;
	BOOL hasPotentialEquation;
	NSRange potentialEquationRange;
	NSUInteger potentialEquationRHSLength;
}

-(id)initWithTextView:(SPRTextView*)tv;
-(void)textViewDidMoveToWindow;
-(void)markPotentialEquationWithRange:(NSRange)range rhsLength:(NSUInteger)rhsLength;
-(BOOL)hasPotentialEquation;
-(NSRange)potentialEquationRange;
-(NSUInteger)potentialEquationRHSLength;
-(void)erasePotentialEquation;
- (void)textDidChange:(NSNotification *)aNotification;
-(void)finalizePotentialEquation;
- (BOOL)keyDown:(NSEvent*)event inView:(SPRTextView*)view;
- (NSRange)textView:(NSTextView *)aTextView willChangeSelectionFromCharacterRange:(NSRange)oldSelectedCharRange toCharacterRange:(NSRange)newSelectedCharRange;
- (BOOL)textView:(NSTextView *)aTextView shouldChangeTextInRange:(NSRange)affectedCharRange replacementString:(NSString *)replacementString;
@end
