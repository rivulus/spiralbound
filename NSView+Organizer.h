//
//  NSView+Organizer.h
//  SpiralBound Pro
//
//  Created by Philip White on 5/1/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SPROrganizer;
@interface NSView (Organizer)

-(SPROrganizer*)organizer;

@end
