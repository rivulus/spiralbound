//
//  ColorButton.h
//  SpiralBound Pro
//
//  Created by Philip White on 5/4/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface ColorButtonCell : NSButtonCell
{

}

- (NSColor*)color;

@end
