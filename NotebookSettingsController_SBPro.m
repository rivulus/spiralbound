//
//  NotebookSettingsController.m
//  SpiralBound Pro
//
//  Created by Philip White on 5/22/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "SBController.h"
#import "NotebookSettingsController_SBPro.h"
#import "ColorButtonCell.h"
#import "SPROrganizer.h"
#import "SPRNotebook.h"


static NotebookSettingsController *sharedController = nil;

#define USER_DID_CANCEL 0x0F0F
#define USER_DID_SAVE 0x0F0A

@implementation NotebookSettingsController

+(NotebookSettingsController*)sharedController
{
	if (sharedController == nil)
	{
		sharedController = [[self alloc] init];
	}
	
	return sharedController;
}

-(id)init
{
	self = [super initWithWindowNibName:@"NotebookSettings"];
	
	if (self)
	{
		
	}
	
	return self;
}

-(void)showModallyForOrganizer:(SPROrganizer*)currentOrganizer
{
	NSWindow *myWindow = [self window];
	organizer = currentOrganizer;
	originalColor = [[organizer color] copy];
	[self setColor: originalColor];
	[descriptionField setStringValue:[NSString stringWithFormat:@"\"%@\" Settings", [[organizer notebook] title]]];
	[self setTitle:[[organizer notebook] title]];
	
	NSInteger result = [NSApp runModalForWindow:myWindow];
	[myWindow orderOut:nil];
	
	if (result == USER_DID_CANCEL)
		return;
}

-(IBAction)cancelButton:(id)sender
{
	[NSApp stopModalWithCode:USER_DID_CANCEL];
	[organizer setColor:originalColor];
	originalColor=nil;
	organizer = nil;
}

-(IBAction)saveButton:(id)sender
{
	[NSApp stopModalWithCode:USER_DID_SAVE];
	
	originalColor=nil;		//color gets set automatically via bindings
	[organizer setTitle:title];
	organizer = nil;
	[gSBController updateNotebookMenu];
}

-(IBAction)colorButton:(id)sender
{
	static NSColor *oldColor = nil;
	static UInt8 colorMatchCount = 0;
	
	NSColor *newColor = [(ColorButtonCell*)[sender cell] color]; 
	
	if (oldColor != nil)
	{
		if ([oldColor isEqualTo: newColor])
			colorMatchCount++;
		else
			colorMatchCount=0;
		
	
		if (colorMatchCount == 5)
		{
			ShowEasterEggForColor(newColor);
			colorMatchCount = 0;
		}
	}
	oldColor = newColor;
	
	[self setColor:newColor];
}

- (NSColor *)color
{
	return color;
}
	
- (void)setColor:(NSColor *)value
{
	if (color != value)
	{
		color = [value copy];
		
		[organizer setColor: color];
	}
}

- (NSString *)title
{
    return title;
}

- (void)setTitle:(NSString *)value
{
    if (title != value)
	{
        title = [value copy];
		
		if ([title isEqualToString:[[organizer notebook] title]])
			[self setTitleUnique:YES]; //its ok if it's the original title
		else
			[self setTitleUnique:[SPRNotebook isTitleAvailable:title]];
		
		[self setTitleValid:([self isTitleUnique] && [[title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]>0)];
    }
}

- (void)setTitleUnique:(BOOL)value
{
	if (titleUnique != value)
		titleUnique = value;
}

- (void)setTitleValid:(BOOL)value
{
	if (titleValid != value)
		titleValid = value;
}

- (BOOL)isTitleUnique
{
	return titleUnique;
}

- (BOOL)isTitleValid
{
	return titleValid;
}

-(void)windowDidLoad
{
	[[NSColorPanel sharedColorPanel] setShowsAlpha:YES];
	[super windowDidLoad];
}	
	

@end
