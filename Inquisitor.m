//
//  Inquisitor.m
//  SpiralBound
//
//  Created by Philip White on 2/17/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "Inquisitor.h"
#import "SPRPreferencesController.h"

static BOOL Challenging=NO;

@implementation Inquisitor

+ (id)sharedInquisitor
{
	static Inquisitor *sharedInquisitor=nil;
	
	if (sharedInquisitor==nil)
	{
		sharedInquisitor = [[Inquisitor alloc] init];
	}
	
	return sharedInquisitor;
}

- (id)init
{
	self = [super initWithWindowNibName: @"Inquisitor"];
	
	if (self)
	{
		
	}
	
	return self;
}

#define VALID_CODE 0

- (BOOL)challenge
{
	if (Challenging)
		return NO;
	
	if ([[SPRPreferencesController sharedPreferencesController] hasPassword] == NO)
		return YES;
	
	Challenging = YES;

	[self setShowCancelButton:YES];
	[self setShowTryAgain:NO];
	[self setInput: @""];
	NSInteger result = [NSApp runModalForWindow:[self window]];
	
	[[self window] orderOut:nil];
	
	[self setInput: @""];
	
	Challenging = NO;
	
	if (result == VALID_CODE)
		return YES;
	else
		return NO;
}

- (BOOL)challengeWithoutCancel
{
	if (Challenging)
	{
		NSLog(@"Already challenging");
		return NO;
	}
	
	if ([[SPRPreferencesController sharedPreferencesController] hasPassword] == NO)
		return YES;
	
	Challenging = YES;

	[self setShowCancelButton:NO];
	[self setShowTryAgain:NO];
	[self setInput: @""];
	NSInteger result = [NSApp runModalForWindow:[self window]];
	
	[[self window] orderOut:nil];
	
	[self setInput: @""];
	
	Challenging = NO;
	
	if (result == VALID_CODE)
		return YES;
	else
		return NO;
}


- (IBAction)checkPassword:(id)sender
{
	BOOL valid = [[SPRPreferencesController sharedPreferencesController] isPasswordValid:[self input]];
	
	if (valid == NO)
	{
		[self setShowTryAgain:YES];
		[self setInput: @""];
		NSBeep();
		return;
	}
	else
	{
		[NSApp stopModalWithCode:VALID_CODE];
	}
}

- (NSString *)input
{
    return input;
}

- (void)setInput:(NSString *)value
{
    if (input != value)
	{
        input = [value copy];
    }
}

- (BOOL)showTryAgain
{
    return showTryAgain;
}

- (void)setShowTryAgain:(BOOL)value
{
    if (showTryAgain != value)
	{
        showTryAgain = value;
    }
}

- (BOOL)showCancelButton
{
    return showCancelButton;
}

- (void)setShowCancelButton:(BOOL)value
{
    if (showCancelButton != value)
	{
        showCancelButton = value;
    }
}

- (BOOL)isChallenging
{
	return Challenging;
}

- (void)stopChallenging
{
	[NSApp abortModal];
}

@end
