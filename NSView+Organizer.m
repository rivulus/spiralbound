//
//  NSView+Organizer.m
//  SpiralBound Pro
//
//  Created by Philip White on 5/1/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "NSView+Organizer.h"
#import "SBProtoFrameView.h"

@implementation NSView (Organizer)

-(SPROrganizer*)organizer
{
	return [(id)[[self window] contentView] organizer];
}

@end
