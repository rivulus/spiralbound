//
//  ColorButton.m
//  SpiralBound Pro
//
//  Created by Philip White on 5/4/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "ColorButtonCell.h"
#import "SBConstants.h"

NSArray *StandardColors;

@implementation ColorButtonCell

+ (void)initialize
{
	//SBYellow = [[NSColor colorWithCalibratedRed: 0.996 green: 0.980 blue: 0.620 alpha: 1.0] retain];
	//SBBlue = [[NSColor colorWithCalibratedRed: 0.686 green: 0.976 blue: 0.980 alpha: 1.0] retain];
	//SBGreen = [[NSColor colorWithCalibratedRed: 0.655 green: 0.996 blue: 0.620 alpha: 1.0] retain];
	
	StandardColors = [[NSArray  alloc] initWithObjects: SBLavender, SBPeach,
					  SBYellow, SBGreen, SBWhite, SBBlue, SBBoldRed, SBBoldPink,
					  SBBoldYellow, SBBoldGreen, SBBoldPurple, SBBoldBlue, nil];
	
}

#ifdef MAC_OS_X_VERSION_10_3

- (void)drawWithFrame:(NSRect)frame inView:(NSView *)controlView
{
	[super drawWithFrame:frame inView:controlView];
	
	[NSGraphicsContext saveGraphicsState];
	NSRectClip(NSInsetRect(frame, 8, 8));
	
	NSColor *color = [StandardColors objectAtIndex: [controlView tag]-1];
	
	[color set];
	
	NSRect colorRect = NSInsetRect(frame, 5, 5);
	
	NSBezierPath *path = [NSBezierPath bezierPathWithOvalInRect:colorRect];
	[path fill];
	
	[NSGraphicsContext restoreGraphicsState];
}

#else
- (void)drawBezelWithFrame:(NSRect)frame inView:(NSView *)controlView
{
	[super drawBezelWithFrame:frame inView:controlView];
	
	[NSGraphicsContext saveGraphicsState];
	NSRectClip(NSInsetRect(frame, 8, 8));
	
	NSColor *color = [StandardColors objectAtIndex: [controlView tag]-1];
	
	[color set];
	
	NSRect colorRect = NSInsetRect(frame, 5, 5);
	
	NSBezierPath *path = [NSBezierPath bezierPathWithOvalInRect:colorRect];
	[path fill];
	
	[NSGraphicsContext restoreGraphicsState];
	//NSRectFill(colorRect);
}
#endif

/*- (void)mouseDown:(NSEvent)*event
{
	if ([event clickCount] == 5)
		NSBeep();
	else
		[super mouseDown:event];
}*/

- (NSColor*)color
{
	return [StandardColors objectAtIndex: [[self controlView] tag]-1];
}

@end
