//
//  SPRPreferencesController.m
//  SpiralBound
//
//  Created by Philip White on 2/12/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Security/Security.h>
#import "SPRPreferencesController.h"
#import "SpiralBoundWindow.h"
#import "SBProtoFrameView.h"
#import "SBConstants.h"
#import "EMKeychainItem.h"
//#import "SBController+Update.h"
#import "KeyRecordField.h"

#import "DefaultsKeys.h"
#import "SPROrganizer.h"
#import "SBServices.h"
//#import <Sparkle/Sparkle.h>

#import "RIVTools.h"

#define USE_SPARKLE 0

NSString *ServiceName = @"SpiralBound Pro";

@implementation SPRPreferencesController


+ (SPRPreferencesController*)sharedPreferencesController
{
	static SPRPreferencesController *sharedController = nil;
	
	if (sharedController == nil)
	{
		sharedController = [[self alloc] init];
	}
	
	return sharedController;
}

+ (NSString*)defaultDataFolderPath
{
    NSError *err=NULL;
    NSURL *baseURL = [[NSFileManager defaultManager] URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:NULL create:YES error:&err];
    
    if (!baseURL)
    {
        NSRunAlertPanel(@"Unable to get access to the default data directory.", @"Please contact Rivulus Software for assistance.", nil, nil, nil, nil);
        return nil;
    }
    
	NSString *dataFolderPath = [[baseURL path] stringByAppendingPathComponent: @"SpiralBound Pro"];
	
    return dataFolderPath;
}

//called by +initialize in app delegate
+ (void)setupDefaults
{
	//save defaults colors here
	NSData *equationHighlightColor = [NSArchiver archivedDataWithRootObject: [NSColor darkGrayColor]];
	NSData *solutionHighlightColor = [NSArchiver archivedDataWithRootObject: [NSColor lightGrayColor]];
	NSData *markColor = [NSArchiver archivedDataWithRootObject: [NSColor redColor]];
	
	NSString *dataFolderPath = [SPRPreferencesController defaultDataFolderPath];
	
	//make the defaults text attributes dictionary
	NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
									[NSFont userFontOfSize: 12.0], NSFontAttributeName,
									[NSColor blackColor], NSForegroundColorAttributeName, nil];
	//of course it must be stored as data
	NSData *textAttributesData = [NSArchiver archivedDataWithRootObject:textAttributes];
	
	NSMutableDictionary *defaultsValues = [NSMutableDictionary dictionaryWithContentsOfFile:
												[[NSBundle mainBundle] pathForResource:@"UserDefaults" ofType:@"plist"]];
	
	//add a few values that can't be easily set in plists
	[defaultsValues addEntriesFromDictionary: [NSDictionary dictionaryWithObjectsAndKeys: equationHighlightColor, @"EquationHighlightColor",
																							solutionHighlightColor, @"SolutionHighlightColor",
																							dataFolderPath, @"DataFolderPath",
																							textAttributesData, DefaultsPreferencesTextAttributes,
																							markColor, DefaultsPreferencesMarkColor,
																							//[NSNull null], DefaultsPreferencesServicesDefaultNotebookNameKey,
																							
																							nil]];
	 
	NSUserDefaultsController *defaultsController = [NSUserDefaultsController sharedUserDefaultsController];
	[defaultsController setInitialValues: defaultsValues];
	[defaultsController setAppliesImmediately:YES];
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults registerDefaults:defaultsValues];
}


- (id)init
{
	self = [super initWithWindowNibName: @"Preferences_SBPro"];
	
	if (self)
	{
        //password stuff
		
		[self clearSecureInstances]; //this just initializes them to empty strings
		//check whether to use keychain or not
		
		//if a keychain password exists, use that and
		
		[self setUseKeychain:[[NSUserDefaults standardUserDefaults] boolForKey:@"UseKeychain"]];
		
		if ([self useKeychain])
		{
			if ([EMGenericKeychainItem genericKeychainItemForService:ServiceName withUsername:NSUserName()] != nil)
				//we have a key chain password
				[self setHasPassword:YES];
			else
				[self setHasPassword:NO];
				
			[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Flightline"];
		}
		else
		{
			if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Flightline"] != nil)
				[self setHasPassword:YES];
			else
				[self setHasPassword:NO];
		}
		
		//setup font stuff
		NSDictionary *textAttributes = [NSUnarchiver unarchiveObjectWithData:
												[[NSUserDefaults standardUserDefaults] objectForKey:@"TextAttributes"]];
		NSFont *font = [textAttributes objectForKey:NSFontAttributeName];
		[self willChangeValueForKey:@"fontSize"];
		[self willChangeValueForKey:@"fontName"];
		[self willChangeValueForKey:@"textColor"];
			fontSize = [NSNumber numberWithFloat:[font pointSize]];
			fontName = [font fontName];
			textColor = [textAttributes objectForKey:NSForegroundColorAttributeName];
		[self didChangeValueForKey:@"textColor"];
		[self didChangeValueForKey:@"fontName"];
		[self didChangeValueForKey:@"fontSize"];
		
		[[NSUserDefaults standardUserDefaults] addObserver:self
												forKeyPath:DefaultsPreferencesEncryptData
												   options:NSKeyValueObservingOptionNew
												   context:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(updateNotebookTitles:)
													 name:NotebookCountDidChangeNotification
												   object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(updateNotebookTitles:)
													 name:NotebookTitleDidChangeNotification
												   object:nil];
	}
	
	return self;
}

- (void)dealloc
{
	[[NSUserDefaults standardUserDefaults] removeObserver:self forKeyPath:DefaultsPreferencesEncryptData];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)windowDidLoad
{
    //make bindings connection between tabview and toolbar
    [tabView bind:@"selectedIdentifier" toObject:self withKeyPath:@"window.toolbar.selectedItemIdentifier" options:nil];
    
	[self makeTextAttributes];
	[[[self window] toolbar] setSelectedItemIdentifier:@"Style"];
}

- (void)applicationWillTerminate
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSUInteger launchCount = [defaults integerForKey:@"LaunchCount"];
	[defaults setInteger:launchCount+1 forKey:@"LaunchCount"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([keyPath isEqualToString:DefaultsPreferencesEncryptData] &&
		[[change valueForKey:NSKeyValueChangeNewKey] boolValue])
	{
		[[NSUserDefaults standardUserDefaults] setBool:YES forKey:DefaultsPreferencesRequirePasswordAtLaunch];
	}
}

#pragma mark DATA PATH

- (IBAction)chooseDataFolderPath:(id)sender
{
	NSOpenPanel *openPanel = [NSOpenPanel openPanel];
	[openPanel setCanChooseFiles:NO];
	[openPanel setCanChooseDirectories:YES];
	//[[openPanel defaultButtonCell] setTitle:@"Choose"];
	[openPanel setPrompt:@"Choose"];
	[openPanel setTitle: @"Choose Data Folder"];
	[openPanel setMessage:@"Please select a folder in which to store your SpiralBound Pro data.\nNote that the data will be stored in a subfolder named \"SpiralBound Pro\"."];
	int result = [openPanel runModal];
	[openPanel orderOut:nil];
	
	if (result == NSFileHandlingPanelCancelButton)
		return;
	
	NSFileManager *fm = [NSFileManager defaultManager];
	NSString *path = [openPanel filename];
	if (![fm isWritableFileAtPath:path])
	{
		NSRunAlertPanel(@"Privileges Error", @"You are not allowed to write in the folder: \"%@\". Please choose a different location.", @"OK", nil, nil, path);
		return;
	}
	
	path = [path stringByAppendingString:@"/SpiralBound Pro"];
	path = [path stringByStandardizingPath];
	
	
	NSUserDefaults *stdUD = [NSUserDefaults standardUserDefaults];
	
	NSString *oldPath = [stdUD objectForKey:@"DataFolderPath"];
	
	[stdUD setObject:path forKey:@"DataFolderPath"]; 
	
	[SPROrganizer saveOrganizers];
	
	[self deleteOldDataFolder:oldPath];
	
}

- (void)resetDataFolderPath
{
    NSError *err=nil;
    NSURL *appSupportUrl = [[NSFileManager defaultManager] URLForDirectory:NSApplicationSupportDirectory
                                                                  inDomain:NSUserDomainMask
                                                         appropriateForURL:nil
                                                                    create:YES
                                                                     error:&err];
    if (!appSupportUrl)
    {
        NSRunCriticalAlertPanel(@"Unable to Reset Data Folder path!",
                                @"An unknown error occurred while trying to reset the data folder path.\nError:\n%@",
                                nil,
                                nil,
                                nil,
                                err ?: @"");
        return;
    }
    
    NSString *dataFolderPath = [[appSupportUrl path] stringByAppendingPathComponent: @"SpiralBound Pro/"];
	
    [[NSUserDefaults standardUserDefaults] setObject:dataFolderPath forKey:@"DataFolderPath"];
}

- (IBAction)resetDataFolderPath:(id)sender
{
	NSUserDefaults *stdUD = [NSUserDefaults standardUserDefaults];
	NSString *oldPath = [stdUD objectForKey:@"DataFolderPath"];
    
    [self resetDataFolderPath];
    
	[self deleteOldDataFolder:oldPath];
}

//prompts user first
- (void)deleteOldDataFolder: (NSString*)oldPath
{
	BOOL pathDiffers;
	
	//note both path and oldPath should be standardized by now
	pathDiffers = ![oldPath isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"DataFolderPath"]];
	
	if (pathDiffers)
	{
		int result = NSRunAlertPanel(@"Delete Old Data?", @"Would you like to delete the old data located at: \"%@\"? Please note that the entire folder will be moved to the trash. You can always do this manually using the Finder.", @"Delete", @"Don't Delete", nil, oldPath);
	
		if (result == NSAlertDefaultReturn)
		{
			//move the old data to the trash
			[[NSWorkspace sharedWorkspace] recycleURLs:[NSArray arrayWithObject:[NSURL fileURLWithPath: oldPath]]
                                     completionHandler:^(NSDictionary *urls,NSError *err)
            {
                 if (err)
                 {
                     NSLog(@"Error Deleting Old Data: SpiralBound Pro was unable to move the old data to the trash.n\Error:\n%@", err);
                 }
             }];
		}
	}
}

//these next two are fileManager call backs for the removeFileAtPath: call in the method above
- (BOOL)fileManager:(NSFileManager *)manager shouldProceedAfterError:(NSDictionary *)errorInfo
{
	NSRunCriticalAlertPanel(@"Error Deleting Old Data Folder", @"The following error occurred: \n%@", @"OK", nil, nil, [errorInfo objectForKey:@"Error"]);
	return NO;
}

- (void)fileManager:(NSFileManager *)manager willProcessPath:(NSString *)path
{
	//do nothing
	//documentation is unclear whether this must be implemented
}

- (NSString*)dataFolderPath
{
	NSString *path = [[NSUserDefaults standardUserDefaults] objectForKey:@"DataFolderPath"];

	NSFileManager *fm = [NSFileManager defaultManager];
	
	BOOL isDirectory;
	
	tryAgain:
	
	if ([fm fileExistsAtPath:[path stringByDeletingLastPathComponent] isDirectory:&isDirectory] && isDirectory)
	{
		return path;
	}
	else
	{
		NSString *productName;
		productName = @"SpiralBound Pro";
	
		int result = NSRunCriticalAlertPanel(@"Data Folder Does Not Exist!", @"It appears that the folder in which %@ has been configured to save its data has disappeared. If you know how to fix this problem, do so using the Finder and then click \"Try Again\". Otherwise click \"Use Default Location\" to use the standard location.", @"Try Again", @"Use Default Location", nil, productName);
		
		if (result != NSAlertDefaultReturn)
		{
			path = [SPRPreferencesController defaultDataFolderPath];
			[[NSUserDefaults standardUserDefaults] setObject:path forKey:@"DataFolderPath"];
		}
			
		goto tryAgain;
	}
}

#pragma mark FONTS

- (void)makeTextAttributes
{
	NSFont *font = [NSFont fontWithName:[self fontName] size:[[self fontSize] intValue]];
	NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
									font, NSFontAttributeName,
									[self textColor], NSForegroundColorAttributeName, nil];
																							
	[[NSUserDefaults standardUserDefaults] setObject:[NSArchiver archivedDataWithRootObject:dict] forKey:@"TextAttributes"];
	[[fontSampleView textStorage] setAttributes:dict range:NSMakeRange(0,[[fontSampleView textStorage] length])];
}

- (NSString *)fontName
{
    return fontName;
}

- (void)setFontName:(NSString *)value
{
    if (fontName != value)
	{
        fontName = [value copy];
		[self makeTextAttributes];
    }
}

- (id)fontSize
{
    return fontSize;
}

- (void)setFontSize:(id)value
{
    if (fontSize != value)
	{
        fontSize = [value copy];
		[self makeTextAttributes];
    }
}

- (NSColor *)textColor
{
    return textColor;
}

- (void)setTextColor:(NSColor *)value
{
    if (textColor != value)
	{
        textColor = [value copy];
		[self makeTextAttributes];
    }
}


#pragma mark PASSWORD


-(IBAction)setPasswordFromButton:(id)sender
{
	//both 1 and 2 should be verified as being equal by now
	[self storePassword:[self newPassword1]];
	
	[self setHasPassword:YES];
	
	[self closeSheet:sender];
}

-(IBAction)changePasswordFromButton:(id)sender
{
	if ([self isPasswordValid:[self oldPassword]] == NO)
	{
		NSBeep();
		return;
	} 
	
	[self storePassword:[self newPassword1]];
	
	[self closeSheet:sender];
}

-(IBAction)removePasswordFromButton:(id)sender
{
	if ([self isPasswordValid:[self oldPassword]] == NO)
	{
		NSBeep();
		return;
	} 
	
	EMGenericKeychainItem *keychainItem = [EMGenericKeychainItem genericKeychainItemForService:ServiceName
																				  withUsername:NSUserName()];
	
	[keychainItem removeFromKeychain];
	
	[self setHasPassword:NO];
			
	[self closeSheet:sender];
}

//these just show windows
-(IBAction)setPassword:(id)sender
{
	[self setNewPasswordsAreEqual:NO];
	
	[NSApp runModalForWindow:setPasswordPanel];
	
#if 0
	[NSApp beginSheet: setPasswordPanel modalForWindow:[self window] modalDelegate: nil didEndSelector: nil
			contextInfo: nil];
#endif
}

-(IBAction)changePassword:(id)sender
{
	[self setOldPasswordIsValid:NO];
	[self setNewPasswordsAreEqual:NO];
	[self setNewPasswordsAreEqualAndOldPasswordIsValid:NO];
	
	[NSApp runModalForWindow:changePasswordPanel];
	
#if 0
	[NSApp beginSheet: changePasswordPanel modalForWindow:[self window] modalDelegate: nil didEndSelector: nil
		  contextInfo: nil];
#endif
}

-(IBAction)removePassword:(id)sender
{
	
	[self setOldPasswordIsValid:NO];
	
	[NSApp runModalForWindow:removePasswordPanel];
#if 0
	[NSApp beginSheet: removePasswordPanel modalForWindow:[self window] modalDelegate: nil didEndSelector: nil
		  contextInfo: nil];
#endif
}

-(IBAction)closeSheet:(id)sender
{
	[NSApp stopModal];
	#if 0
		[NSApp endSheet:[sender window]];
	#endif
	[[sender window] close];
	[self clearSecureInstances];
}

/***ACCESSORS****/

- (void)clearSecureInstances
{
	[self willChangeValueForKey:@"oldPassword"];
	oldPassword = @"";
	[self didChangeValueForKey:@"oldPassword"];
	
	[self willChangeValueForKey:@"newPassword1"];
	newPassword1 = @"";
	[self didChangeValueForKey:@"newPassword1"];
	
	[self willChangeValueForKey:@"newPassword2"];
	newPassword2 = @"";
	[self didChangeValueForKey:@"newPassword2"];
	
	NSArray *s1 = [[setPasswordPanel contentView] subviews],
	*s2 = [[changePasswordPanel contentView] subviews],
	*s3 = [[removePasswordPanel contentView] subviews];
	NSMutableArray *subbies = [NSMutableArray arrayWithCapacity:[s1 count]+[s2 count]+[s3 count]];
	if (s1)
		[subbies addObjectsFromArray:s1];
	if (s2)
		[subbies addObjectsFromArray:s2];
	if (s3)
		[subbies addObjectsFromArray:s3];
	
	NSEnumerator *enumerator = [subbies objectEnumerator];
	id subview;
	while (subview = [enumerator nextObject])
	{
		if ([subview isKindOfClass:[NSSecureTextField class]])
			[subview setStringValue:@""];
	}
}

- (NSString *)oldPassword
{
    return oldPassword;
}

- (void)setOldPassword:(NSString *)value
{
    if (oldPassword != value)
	{
        oldPassword = [value copy];
		
		//check to see if it's valid
		
		[self setOldPasswordIsValid: [self isPasswordValid:oldPassword]];
		
		[self setNewPasswordsAreEqualAndOldPasswordIsValid: newPasswordsAreEqual&&oldPasswordIsValid];
    }
}

- (NSString *)newPassword1
{
    return newPassword1;
}

- (void)setNewPassword1:(NSString *)value
{
    if (newPassword1 != value)
	{
        newPassword1 = [value copy];
		
		if ([newPassword1 isEqualToString: newPassword2] && ![newPassword1 isEqualToString:@""])
			[self setNewPasswordsAreEqual:YES];
		else
			[self setNewPasswordsAreEqual:NO];
		
		[self setNewPasswordsAreEqualAndOldPasswordIsValid: newPasswordsAreEqual&&oldPasswordIsValid];
    }
}

- (NSString *)newPassword2
{
    return newPassword2;
}

- (void)setNewPassword2:(NSString *)value
{
    if (newPassword2 != value)
	{
        newPassword2 = [value copy];
		
		if ([newPassword2 isEqualToString: newPassword1] && ![newPassword2 isEqualToString:@""])
			[self setNewPasswordsAreEqual:YES];
		else
			[self setNewPasswordsAreEqual:NO];
		
		[self setNewPasswordsAreEqualAndOldPasswordIsValid: newPasswordsAreEqual&&oldPasswordIsValid];
    }
}

- (BOOL)newPasswordsAreEqual
{
    return newPasswordsAreEqual;
}

- (void)setNewPasswordsAreEqual:(BOOL)value
{
    if (newPasswordsAreEqual != value)
	{
        newPasswordsAreEqual = value;
    }
}

- (BOOL)oldPasswordIsValid
{
    return oldPasswordIsValid;
}

- (void)setOldPasswordIsValid:(BOOL)value
{
    if (oldPasswordIsValid != value)
	{
        oldPasswordIsValid = value;
    }
}

- (BOOL)newPasswordsAreEqualAndOldPasswordIsValid
{
    return newPasswordsAreEqualAndOldPasswordIsValid;
}

- (void)setNewPasswordsAreEqualAndOldPasswordIsValid:(BOOL)value
{
    if (newPasswordsAreEqualAndOldPasswordIsValid != value)
	{
        newPasswordsAreEqualAndOldPasswordIsValid = value;
    }
}
- (BOOL)hasPassword
{
    return hasPassword;
}

- (void)setHasPassword:(BOOL)value
{
    if (hasPassword != value)
	{
        hasPassword = value;
		
		if (value == NO)
		{
			NSUserDefaults *stud = [NSUserDefaults standardUserDefaults];
			
			[stud setBool:NO forKey:DefaultsPreferencesNeedsPasswordToUnscramble];
			[stud setBool:NO forKey:DefaultsPreferencesEncryptData];
			[stud setBool:NO forKey:DefaultsPreferencesRequirePasswordAtLaunch];
		}
	}
}

- (BOOL)useKeychain
{
    return useKeychain;
}

- (void)setUseKeychain:(BOOL)value
{
    if (useKeychain != value)
	{
		useKeychain = value;
		
		//also record in user defaults
		[[NSUserDefaults standardUserDefaults] setBool:value forKey:@"UseKeychain"];
    }
}

- (BOOL)keychainIsLocked
{
	return keychainIsLocked;
}
							
- (void)setKeychainIsLocked:(BOOL)value
{
	if (keychainIsLocked != value)
	{
		keychainIsLocked = value;
	}
}

#pragma mark PASSWORD TOOLS

- (void)storePassword:(NSString*)password
{
	/*Now we always use key chain*/
	EMGenericKeychainItem *keyChainItem;
	if ([self hasPassword])
	{
		keyChainItem = [EMGenericKeychainItem genericKeychainItemForService:ServiceName withUsername:NSUserName()];
		[keyChainItem setPassword: password];
	}
	else
	{
		keyChainItem = [EMGenericKeychainItem addGenericKeychainItemForService:ServiceName
																		  withUsername:NSUserName() password:password];
	}
}

- (BOOL)isPasswordValid:(NSString*)password
{
	if ([self hasPassword] == NO)
	{
		NSLog(@"Asked to verify password with no password set");
		return YES;
	}
	
	NSString *actualPassword;
	
	//Always use keychain now
	
	
	if ([self useKeychain])
	{
		actualPassword = [[EMGenericKeychainItem genericKeychainItemForService:ServiceName withUsername:NSUserName()] password];
	}
	else
	{
		NSData *pwdData = [[NSUserDefaults standardUserDefaults] objectForKey:@"Flightline"];
		actualPassword  = [NSUnarchiver unarchiveObjectWithData:[self roll: pwdData]];
	}
	return [password isEqualToString: actualPassword];
}

-(NSData*)roll:(NSData*)inData
{
	NSData *outData;
	NSUInteger length = [inData length], index;
	unsigned char *buffer = NSZoneMalloc(NSDefaultMallocZone(), length);
	
	[inData getBytes:buffer length:length];
	
	for (index = 0; index<length; index++)
	{
		buffer[index] = 255-buffer[index];				//pretty schmancy
	}
	
	outData = [NSData dataWithBytes:buffer length:length];
	
	NSZoneFree(NSDefaultMallocZone(), buffer);
	return outData;
}

-(void)switchPasswordToKeychain
{
	if ([[NSUserDefaults standardUserDefaults] boolForKey:@"UseKeychain"])
		return;
	
	NSData *pwdData = [[NSUserDefaults standardUserDefaults] objectForKey:@"Flightline"];
	
	if (pwdData)
	{
		NSRunAlertPanel(@"Keychain Usage", @"Storing your password without the use of Keychain is no longer supported. Your password will be moved to your user Keychain.", nil, nil, nil);
		
		NSString *pw = [NSUnarchiver unarchiveObjectWithData:[self roll: pwdData]];
		
		[EMGenericKeychainItem addGenericKeychainItemForService:ServiceName
														   withUsername:NSUserName() password:pw];
	}
	[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Flightline"];
	[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"UseKeychain"];
}

#pragma mark Topical Help

enum
{
	FastCompletionHelp=10,
	EncryptDataHelp,
	SystemProfileHelp,
	DataLocationHelp,
	ServiceHelp
};

#define HELP_OFFSET 10

static NSString *TopicalHelpText[] = {@"Normally one enters an equal sign to solve the equation and then presses the completion key to accept the result, but to make the number pad faster and easier to use, one may choose to have the completion key perform both tasks at once.",
									  @"If the \"Encrypt Data\" option is enabled, your password will be used to encrypt your data when it is stored on your hard drive. Please note that if you forget your password, you may retrieve it using Apple's Keychain Access utility.",
									  @"Selecting this supplies Rivulus with information about your system to help us better tailor our products to our customers' systems. The information included includes the following and nothing else: the version of SpiralBound you are running, the model and speed of the Mac you are using, the version of OS X you are running and how much RAM your computer has. It does NOT include your name, any serial numbers, or anything that can connect this information to you. The system profile will be sent when checking for updates.",
									  @"PLEASE NOTE: It is recommended that you only change this location if you have a compelling reason for doing so, such as if you are using a sync tool.\nAlso, attempting to sync the preferences file is not recommended.\n\nIf there is already SpiralBound data in the new location, it will be overwritten. To access existing data, use the features in the Import and Export submenus of the File menu.",
									@"SpiralBound Pro offers a Service, accessible from the Services submenu in any program's application menu, to begin a new SpiralBound Pro page. To use the service, first select the text or graphic you wish to use from any program. Then choose Make SpiralBound Pro Page from that program's Services submenu in that program's application menu. A window will pop up and allow you to select in which notepad you would like to make your new page. Change these settings here at any time.\n\nIn macOS 10.5, the service should be automatically enabled. To enable it in later versions of macOS, choose Services Preferences from the Services submenu in any program's application menu. A Keyboard Shortcuts window should appear. Inside, in the right scrollable view, locate \"Make SpiralBound Pro Page\" in the Text category and check the box to enable it. You may also wish to disable the \"Make New Sticky Note\" service at this time."};

- (IBAction)showTopicalHelp:(id)sender
{
	NSString *text;
	
	//NSAssert([sender tag] >= 10 && [sender tag] <= 13, @"-showTopicalHelp: sender has bad tag");

	text = TopicalHelpText[[sender tag]-HELP_OFFSET];
	
	id panel = NSGetAlertPanel(@"Preferences Help", @"%@", @"OK", nil, nil, text);
	
	[NSApp beginSheet:panel modalForWindow:[self window]
							 modalDelegate:self
							didEndSelector:@selector(closeTopicalHelpSheet:returnCode:contextInfo:)
							   contextInfo:nil];
	
}

-(void)closeTopicalHelpSheet:(id)panel returnCode:(NSInteger)code contextInfo:(void*)contextInfo
{
	[panel orderOut:nil];
	NSReleaseAlertPanel(panel);
}

#pragma mark Resetting Defaults

-(IBAction)resetPage:(id)sender
{
	static NSDictionary *perPageDefaultKeys=nil;
	if (perPageDefaultKeys == nil)
	{
		perPageDefaultKeys = [[NSDictionary alloc] initWithObjectsAndKeys:
							  [NSArray arrayWithObjects:DefaultsPreferencesUseSimpleDragbar,
														DefaultsPreferencesEquationHighlightColor,
														DefaultsPreferencesSolutionHighlightColor,
														nil],
							  @"Style",
							  
							  [NSArray arrayWithObjects:DefaultsPreferencesDataFolderPath,
														DefaultsPreferencesServicesAlwaysUseNotebookKey,
														DefaultsPreferencesServicesDefaultNotebookNameKey,
														nil],
							  @"Service",
							  
							  [NSArray arrayWithObjects:DefaultsPreferencesCalculatorIsEnabled,
														DefaultsPreferencesPutCalculatorResultInClipboard,
														DefaultsPreferencesCalculatorPrecision,
														DefaultsPreferencesEnterKeyCompletesEquation,
														nil],
							  @"Calculator",
							  
							  [NSArray arrayWithObjects:DefaultsPreferencesShouldScramble,
														DefaultsPreferencesScramblingDelay,
														DefaultsPreferencesScrambleImmediately,
														DefaultsPreferencesNeedsPasswordToUnscramble,
														DefaultsPreferencesEncryptData,
														DefaultsPreferencesRequirePasswordAtLaunch,
														nil],
							  @"Security",
							  
							  [NSArray arrayWithObjects:AutomaticallyCheckForUpdatesKey,
														UpdateCheckIntervalKey,
														nil],
							  @"Update",
							  [NSArray arrayWithObjects:DefaultsPreferencesMarkStyle,
														DefaultsPreferencesMarkColor,
														DefaultsPreferencesStrikeOutCheckedItem,
														nil],
							  @"To Do",
							   nil];
	}
	
	NSString *identifier = [[[self window] toolbar] selectedItemIdentifier];
	
	NSDictionary *allInitialValues = [[NSUserDefaultsController sharedUserDefaultsController] initialValues];
	NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
	NSArray *keys = [perPageDefaultKeys objectForKey:identifier];
	
	NSAssert(keys, @"Unable to find default keys for page");
	
	/*Special steps to take before resetting certain pages*/
	NSString *oldDataPath;
	if ([identifier isEqualToString:@"Data"])
	{
		oldDataPath = [ud objectForKey:DefaultsPreferencesDataFolderPath];
	}
	
	for (id key in keys)
	{
		id value = [allInitialValues objectForKey:key];
		//it appears to be OK to set nil values in user defaults
		//doing so effectively deletes the entry and is used here to
		//delete the ServicesDefaultNotebookName
		//NSAssert1(value,@"No initial default set for key: %@", key);
		[ud setObject:value forKey:key];
	}
	
	/*Special Steps to reset certain pages*/
	if ([identifier isEqualToString:@"Style"])
	{
		NSFont *font = [NSFont userFontOfSize: 12.0];
		[self setFontSize:[NSNumber numberWithFloat:[font pointSize]]];
		[self setFontName:[font fontName]];
		[self setTextColor:[NSColor blackColor]];
	}
	if ([identifier isEqualToString:@"Calculator"])
	{
		[KeyRecordField reset];
	}
	else if ([identifier isEqualToString:@"Data"])
	{
		
		[self deleteOldDataFolder:oldDataPath];
		
	}
	else if ([identifier isEqualToString:@"Update"])
	{
		//Sparkle doesn't user standard user defaults
	#if USE_SPARKLE
		SUUpdater *sparkle = [SUUpdater sharedUpdater];
		
		[sparkle setAutomaticallyChecksForUpdates:YES];
		[sparkle setUpdateCheckInterval:1209600];
		[sparkle setSendsSystemProfile:YES];
	#endif
	}
}

-(IBAction)resetAllPreferences:(id)sender
{
	[self resetPage:@"Style"];
	[self resetPage:@"Data"];
	[self resetPage:@"Calculator"];
	[self resetPage:@"Security"];
}

-(void)updateNotebookTitles:(NSNotification*)note
{
	[self setNotebookTitles:[SPROrganizer notebookTitles]];
}

- (NSArray *)notebookTitles
{
    return notebookTitles;
}

- (void)setNotebookTitles:(NSArray *)value
{
    if (notebookTitles != value)
	{
        notebookTitles = [value copy];
    }
}

-(IBAction)checkForUpdate:(id)sender
{
	RIVCheckForUpdate(NO);
}

@end
