//
//  SPRSearchEngine.h
//  CommonSpiralbound
//
//  Created by Philip on 5/31/10.
//  Copyright 2010 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SpiralBoundWindow;
@interface SPRSearchEngine : NSWindowController
{
	IBOutlet NSBox *pageTypeBox; //hide this if not pro
	IBOutlet NSBox *findOptionsBox; //this box is to the right of the pageTypeBox use its location
									//and that of the pageTypeBox to correctly resize the window
	IBOutlet NSBox *nonProFindOptionsBox; //remove the above with this if not running pro
	IBOutlet NSSearchField *searchField;
	
	IBOutlet NSTextField *resultsMessageField;
	
	NSMutableArray *results;
	NSUInteger selectedResult;		//-findNext: and -findPrevious: will set this to NSUInteger_Max if they are called
								//and the last or first result respectively has ALREADY been shown
								//then on a subsequent call they will show the first or last result, respectively
	
	NSArray *scopeTitles;
	NSString *searchString; //cache this for setting the resultsMessageField;
	
}

+(SPRSearchEngine*)soleInstance;
-(void)determineScopeTitles:(SpiralBoundWindow*)newMain;
-(NSArray *)scopeTitles;
-(void)setScopeTitles:(NSArray *)value;
-(NSArray*)pagesForSearchSettings;

-(IBAction)find:(id)sender;
-(IBAction)findNext:(id)sender;
-(IBAction)findPrevious:(id)sender;
-(void)selectResult:(NSUInteger)index;

-(NSString *)searchString;
-(void)setSearchString:(NSString *)value;



@end
