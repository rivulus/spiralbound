//
//  SPROrganizer+Sorting.h
//  SpiralBound Pro
//
//  Created by Philip White on 5/1/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SPROrganizer.h"

@interface SPROrganizer (Sorting)

//this method returns the page that is below the one currently visible in the notepad (stack)
//it steps through the array returned by notebook and returns the first one with an index greater than that
//of the currently visible one, wrapping back to zero if necessary while skipping page that are in torn off windows
+(void)movePagesWithHashes:(NSArray*)hashArray destinationOrganizer:(SPROrganizer*)destination firstPageIndex:(NSUInteger)firstPageIndex;
+(void)copyPagesWithHashes:(NSArray*)hashArray destinationOrganizer:(SPROrganizer*)destination firstPageIndex:(NSUInteger)firstPageIndex;

-(void)movePages:(NSArray*)pages toIndex:(NSUInteger)index;

+(NSArray*)orderedArrayOfWindows;

-(void)removePage:(SPRPage*)page;
-(void)addPages:(NSArray*)page atIndex:(NSUInteger)index;
-(SPRPage*)nextPageInStack;
-(SPRPage*)previousPageInStack;
- (void)returnPageFromTornOffWindow:(SpiralBoundWindow*)window;
-(void)removePageAtIndex:(NSUInteger)index;
-(void)movePageToTop:(SpiralBoundWindow*)window;
-(void)movePageToEnd:(SpiralBoundWindow*)window;
-(void)movePageToBeginning:(SpiralBoundWindow*)window;
@end
