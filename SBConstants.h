/*
 *  SBConstants.h
 *  SpiralBound
 *
 *  Created by Philip White on 2/2/09.
 *  Copyright 2009 Rivulus Software. All rights reserved.
 *
 */

#import <Cocoa/Cocoa.h>

//include the other constant-related includes
#import "SBTypes.h"
#import "DefaultsKeys.h"
#import "NotificationNames.h"
#import "PasteboardTypes.h"

#ifndef CONSTANTS_DEFINED

#define CONSTANTS_DEFINED 1

#define kBigResizerLineInset 14
#define kMediumResizerLineInset 9
#define kSmallResizerLineInset 4
#define kResizerDistanceFromEdge 2
#define kDividerLineWidth 1
#define kDividerLineInset 2
#define kStackEffectEdgeWidth 4.0
#define kPagesInStack 8
#define kNotKeyOpacity 0.85
#define kNotKeyLightness 0.2

#define kScrambleMenuItemTag 1
#define kLockButtonTag 1

#define kTrashButtonTag 11
#define kLeftArrowTag 13
#define kRightArrowTag 12
#define kReturnToNotebookButtonTag 12
#define kSheetButtonTag 14

#endif

extern NSString *kVersion, *OriginalSpiralBoundBundleID;

extern NSColor *SBYellow, *SBGreen, *SBBlue, *SBWhite, *SBLavender, *SBPeach, *SBBoldRed, *SBBoldPink,
*SBBoldYellow, *SBBoldGreen, *SBBoldPurple, *SBBoldBlue;

extern NSDictionary *SPRPasteboardToDocumentTypeTable;

extern NSString *SPRInvisibleAttributeHolder;

NSAttributedString *SPRGetAttributeHolder(NSDictionary *attrs); //returns an NSAttributedString containing the holder with the given attributes
												 //or, if nil, the default attributes

@interface SBConstants : NSObject
{
	
}


@end