//
//  SPRMultipleNotebookPurchaseAppeal.h
//  SpiralBound Pro
//
//  Created by Philip White on 4/9/18.
//

#import <Cocoa/Cocoa.h>

typedef NS_ENUM(NSInteger, SPRPurchaseAppealResult) {
	SPRPurchaseAppealTip,
	SPRPurchaseAppealBigTip,
	SPRPurchaseAppealExtraBigTip
};

typedef void(^SPRPurchaseAppealResultCallback)(SPRPurchaseAppealResult);

@interface SPRMultipleNotebookPurchaseAppeal : NSWindowController

- (void)reset;
- (void)finish;
- (void)showAsModalWithCompletion:(SPRPurchaseAppealResultCallback)appealCompletion;

@property (nonatomic) NSString *tipPrice;
@property (nonatomic) NSString *bigTipPrice;
@property (nonatomic) NSString *extraBigTipPrice;

@end
