//
//  DateTransformer.m
//  SpiralBound 1.1
//
//  Created by Philip on 11/13/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "DateTransformer.h"


@implementation DateTransformer

+ (Class)transformedValueClass
{
	return [NSString class];
}

+ (BOOL)allowsReverseTransformation { return NO; }

- (id)transformedValue:(id)value
{
	if (value == nil)
		return @"Never";
	
	NSDate *date = [NSUnarchiver unarchiveObjectWithData:value];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
	[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
	
	
	return ([dateFormatter stringFromDate:date]);
}


@end
