//
//  Exporter.m
//  SpiralBound Pro
//
//  Created by Philip White on 5/20/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import "Exporter.h"
#import "SBProtoFrameView.h"
#import "SPRPage.h"
#import "SPRNotebook.h"
#import "SBConstants.h"
#import "PrintingView.h"
#import "SPRLockableTextStorage.h"

static Exporter *SharedExporter=nil;

static NSString *ExporterNibName = @"Exporter_SBPro";

enum
{
	PLAIN_TEXT_TYPE,
	RTF_TYPE,
	RTFD_TYPE,
	HTML_TYPE,
	DOC_TYPE,
	WORDML_TYPE,
	DOCX_TYPE			//this one is only for SBPro because it requires OS X 10.5
};

NSString *SPRLArchiveExtension = @"sprl";

static NSString *DocumentTypeLookup[7];

@implementation Exporter

+(void)initialize
{
	DocumentTypeLookup[0] = NSPlainTextDocumentType; DocumentTypeLookup[1] = NSRTFTextDocumentType; DocumentTypeLookup[2] = NSRTFDTextDocumentType;
	DocumentTypeLookup[3] = NSHTMLTextDocumentType; DocumentTypeLookup[4] = NSDocFormatTextDocumentType; DocumentTypeLookup[5] = NSWordMLTextDocumentType;

	DocumentTypeLookup[6] = NSOfficeOpenXMLTextDocumentType;
}

+(Exporter*)sharedExporter
{
	if (SharedExporter == nil)
	{
		SharedExporter = [[self alloc] init];
	}
	
	return SharedExporter;
}

-(id)init
{
	self = [super init];
	
	if (self)
	{
		NSNib *nib = [[NSNib alloc] initWithNibNamed: ExporterNibName bundle:nil];
		BOOL success = [nib instantiateNibWithOwner:self topLevelObjects:NULL];
		if (!success)
		{
			NSLog(@"Failed to load Exporter.xib");
			return nil;
		}
	}

	return self;
}

-(void)showExportPanelForPage:(SPRPage*)page window:(NSWindow*)window
{
	NSSavePanel *savePanel = [NSSavePanel savePanel];
	
	[savePanel setAccessoryView:accessoryView];
	
	[fileTypeButton selectItemAtIndex:RTFD_TYPE];
	[savePanel setRequiredFileType:@"rtfd"];
	
	[savePanel beginSheetForDirectory:nil
								 file:[page title]
					   modalForWindow:window
						modalDelegate:self
					   didEndSelector:@selector(exportPanelDidEnd:returnCode:contextInfo:)
						  contextInfo:CFBridgingRetain(page)];
}

//this is for export
- (void)exportPanelDidEnd:(NSSavePanel *)sheet returnCode:(int)returnCode contextInfo:(void  *)contextInfo
{
	if (returnCode == NSCancelButton)
		return;
	
	NSString *path = [sheet filename];
	
	SPRPage *page = CFBridgingRelease(contextInfo);
	
	NSInteger type = [fileTypeButton indexOfSelectedItem];
	
	NSString *documentType = DocumentTypeLookup[type];
	
	//NSLog(@"Type = %@", documentType);
	
	NSAttributedString *contents = [page contentsAsAttributedString];
	
	id encodingKey=nil, encodingValue=nil;
	
	if (type == PLAIN_TEXT_TYPE)
	{
		//remove the attachment characters
		NSMutableAttributedString *mutableContents = [contents mutableCopy];
		NSString *refString = [contents string];
		NSInteger i;
		for (i=[refString length]-1;i>=0;i--)
		{
			if ([refString characterAtIndex:i] == 0xFFFC) //attachment character
			{
				NSRange range = {i,1};
				[mutableContents deleteCharactersInRange:range];
			}
		}
		
		contents = mutableContents;
		
		//also set the encoding to UTF8
		encodingKey = NSCharacterEncodingDocumentAttribute;
		encodingValue = [NSNumber numberWithInt: NSUTF8StringEncoding];
	}
	
	NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys: documentType, NSDocumentTypeDocumentAttribute, encodingValue, encodingKey, nil];
	NSRange range = {0,[contents length]};
	
	NSError *error=nil;
	NSFileWrapper *wrapper=nil;
	NSData *data=nil;
	
	if (type == DOCX_TYPE)
		data = [contents dataFromRange: range documentAttributes: attributes error: &error];
	else
		wrapper = [contents fileWrapperFromRange: range documentAttributes: attributes error: &error];
	
	if (error != nil)
	{
		NSLog(@"Error = %@", error);
		NSRunCriticalAlertPanel(@"Failed to Export Page", @"An error occurred while exporting: %@", @"OK", nil, nil, error);
		return;
	}
	
	//now fix the attachements if it's html
	if (type == HTML_TYPE)
	{
		NSDictionary *subfiles = [wrapper fileWrappers];
		NSFileWrapper *htmlFile = [subfiles objectForKey:@"index.html"];
		if (htmlFile != nil)
		{
			NSFileWrapper *newHTMLfile;
			NSMutableString *html = [[NSMutableString alloc] initWithData:[htmlFile regularFileContents] encoding:NSUTF8StringEncoding];
			[html replaceOccurrencesOfString:@"file:///" withString:@"" options:NSLiteralSearch range:NSMakeRange(0,[html length])];
			NSData *htmlData = [html dataUsingEncoding:NSUTF8StringEncoding];
			newHTMLfile = [[NSFileWrapper alloc] initRegularFileWithContents:htmlData];
			
			[newHTMLfile setPreferredFilename:@"index.html"];
			[wrapper removeFileWrapper:htmlFile];
			[wrapper addFileWrapper:newHTMLfile];
		}
	}
	
	BOOL success=NO;
	
	if (wrapper)
		success = [wrapper writeToFile:[path stringByExpandingTildeInPath] atomically:YES updateFilenames:NO];
	else if (data)
		success = [data writeToFile:[path stringByExpandingTildeInPath] atomically:YES];
	
	if (!success)
		NSRunCriticalAlertPanel(@"Failed to Export Page", @"An error occurred while saving the file", @"OK", nil, nil);
}

-(IBAction)setFileType:(id)sender
{
	NSString *fileType=nil;
	switch ([fileTypeButton indexOfSelectedItem])
	{
		case PLAIN_TEXT_TYPE:
			fileType = @"txt";
			break;
		
		case RTF_TYPE:
			fileType = @"rtf";
			break;
		
		case RTFD_TYPE:
			fileType = @"rtfd";
			break;
		
		case HTML_TYPE:
			fileType = @"html";
			break;
		
		case DOC_TYPE:
			fileType = @"doc";
			break;
		
		case WORDML_TYPE:
			fileType = @"doc";
			break;
		
		case DOCX_TYPE:
			fileType = @"docx";
			break;
	}
    NSAssert([[sender window] isKindOfClass:[NSSavePanel class]],@"Sender is not NSSavePanel");
	[(NSSavePanel*)[sender window] setAllowedFileTypes:[NSArray arrayWithObject:fileType]];
}

-(void)showSavePanelForNotebook:(SPRNotebook*)notebook window:(NSWindow*)window
{
	NSSavePanel *savePanel = [NSSavePanel savePanel];
	[savePanel setAllowedFileTypes:[NSArray arrayWithObject:SPRLArchiveExtension]];
	[savePanel setMessage:@"Save pages to open in another copy of SpiralBound or SpiralBound Pro."];
	[savePanel beginSheetModalForWindow:window
                      completionHandler:^(NSInteger result) {
                          if (result != NSFileHandlingPanelOKButton)
                              return;
                          [SPRNotebook beginEncodingWithoutSMarker];
                          
                          NSDictionary *dict = [NSDictionary dictionaryWithObject:[notebook pages]
                                                                           forKey:@"Pages"];
                          
                          NSMutableData *data = [[NSKeyedArchiver archivedDataWithRootObject:dict] mutableCopy];
                          
                          [SPRNotebook endEncodingWithoutSMarker];
                          
                          BOOL success = [data writeToURL: [savePanel URL] atomically: YES];
                          if (success == NO)
                          {
                              NSRunAlertPanel(@"Error saving file.", @"", @"OK", nil, nil);
                          }

                      }];
}

#pragma mark EMAIL

#define USER_DID_CLICK_SEND 0x0f0f
#define USER_DID_CLICK_CANCEL 0x0f0a

NSString* GetTemporaryFilePath(NSString *preferredTitle);

enum
{
	PDF_ATTACHMENT,
	RTF_ATTACHMENT,
	RTFD_ATTACHMENT
};

enum
{
	ATTACHMENT_EMAIL_TYPE,
	PLAIN_TEXT_EMAIL_TYPE,
	RICH_TEXT_EMAIL_TYPE
};

enum
{
	APPLE_MAIL_CLIENT,
	EUDORA_CLIENT,
	ENTOURAGE_CLIENT
};

-(void)emailPage:(SPRPage*)page inWindow: (NSWindow*)window
{
	if ([emailPanel isVisible])
	{
		NSBeep();
		[emailPanel makeKeyAndOrderFront:nil];
		return;
	}

	[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"UserCanEmailQuickly"];
	[NSApp beginSheet: emailPanel modalForWindow:window modalDelegate:self didEndSelector:@selector(emailSheetDidEnd:returnCode:contextInfo:) contextInfo:CFBridgingRetain(page)];
}

-(void)emailPageQuickly:(SPRPage*)page
{
	[self emailSheetDidEnd:nil returnCode: USER_DID_CLICK_SEND contextInfo: CFBridgingRetain(page)];
}

-(void)emailSheetDidEnd:(NSWindow*)sheet returnCode:(NSInteger)returnCode contextInfo:(void*)contextInfo
{
	[sheet orderOut:nil];
	
	if (returnCode == USER_DID_CLICK_CANCEL)
		return;
	
	SPRPage *page = CFBridgingRelease(contextInfo);
	
	NSString *title = [[page title] stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
	NSString *path = GetTemporaryFilePath(title);
	
	int type = [[self emailType] intValue];
	
	if (type == PLAIN_TEXT_EMAIL_TYPE)
	{
		//remove the attachment characters
		NSMutableAttributedString *contents = [[page contentsAsAttributedString] mutableCopy];
		NSString *refString = [contents string];
		NSInteger i;
		for (i=[refString length]-1;i>=0;i--)
		{
			if ([refString characterAtIndex:i] == 0xFFFC) //attachment character
			{
				NSRange range = {i,1};
				[contents deleteCharactersInRange:range];
			}
		}
		
		//create a mailto URL:
		NSURL *url;
		url = [NSURL URLWithString:[[NSString stringWithFormat: @"mailto:"
																 "?subject="
																 "&body=%@", [contents string]]
											stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
		[[NSWorkspace sharedWorkspace] openURL:url];
	}
	else
	{
		int format = [attachmentFormat intValue];
		if (format == PDF_ATTACHMENT)
		{
			PrintingView *printingView = [[PrintingView alloc] init];
			[printingView addPageContents:[page contentsAsAttributedString] title:[page title]];
			[printingView finish];
			NSData *pdfData = [printingView PDFData];
			
			if (pdfData == nil)
			{
				NSRunCriticalAlertPanel(@"Error Occurred", @"PDF could not be generated.", @"OK", nil, nil);
				return;
			}
			
			path = [path stringByAppendingString:@".pdf"];
			if ([pdfData writeToFile:path atomically:YES] == NO)
			{
				NSRunCriticalAlertPanel(@"Error Occurred", @"PDF attachment could not be written to disk.", @"OK", nil, nil);
				return;
			}
		}
		else
		{
			NSString *docType;
			if (format == RTF_ATTACHMENT)
			{
				docType = NSRTFTextDocumentType;
				path = [path stringByAppendingString:@".rtf"];
			}
			else
			{
				docType = NSRTFDTextDocumentType;
				path = [path stringByAppendingString:@".rtfd"];
			}
			
			NSAttributedString *text = [page contentsAsAttributedString];
			NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys: docType, NSDocumentTypeDocumentAttribute, nil];
			NSRange range = {0,[text length]};
			
			NSError *error=nil;
			NSFileWrapper *wrapper = [text fileWrapperFromRange: range documentAttributes: attributes error: &error];
			
			[wrapper writeToFile:path atomically:YES updateFilenames:NO];
		}
		
		NSString *scriptPath = [[NSBundle mainBundle] pathForResource:@"SendAttachmentMail" ofType:@"txt"];
				
		NSString *rawSource = [NSString stringWithContentsOfFile: scriptPath encoding:NSUTF8StringEncoding error:NULL];
		
		NSString *scriptSource = [NSString stringWithFormat:rawSource, path];
		
		if (scriptSource == nil)
		{
			NSRunCriticalAlertPanel(@"Error Occurred", @"Message could not be sent to mail client.", @"OK", nil, nil);
			return;
		}
		
		NSAppleScript *script = [[NSAppleScript alloc] initWithSource:scriptSource];
		
		NSDictionary *errorCodes;
		if ([script executeAndReturnError:&errorCodes] == nil) //error if returns nil
		{
			NSRunCriticalAlertPanel(@"Error Occurred", @"Message could not be sent to mail client.", @"OK", nil, nil);
			NSLog(@"%@", scriptSource);
			NSLog(@"%@", errorCodes);
		}
	}		
}

-(IBAction)cancelEmail:(id)sender
{
	[NSApp endSheet:emailPanel returnCode:USER_DID_CLICK_CANCEL];
}

-(IBAction)createEmail:(id)sender
{
	[NSApp endSheet:emailPanel returnCode:USER_DID_CLICK_SEND];
}

- (NSNumber *)attachmentFormat
{
	static BOOL firstCall=YES;
	
	if (firstCall)
	{
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		attachmentFormat = [defaults objectForKey:@"PreferredAttachmentFormat"];
		firstCall = NO;
	}

	return attachmentFormat;
}
				
- (void)setAttachmentFormat:(NSNumber *)value
{
	if (attachmentFormat != value)
	{
		attachmentFormat = [value copy];
		[[NSUserDefaults standardUserDefaults] setObject:attachmentFormat forKey:@"PreferredAttachmentFormat"];
	}
}
				
- (NSNumber *)emailType
{
	static BOOL firstCall=YES;
	
	if (firstCall)
	{
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		emailType = [defaults objectForKey:@"PreferredEmailType"];
		firstCall = NO;
	}

	return emailType;
}
				
- (void)setEmailType:(NSNumber *)value
{
	if (emailType != value)
	{
		emailType = [value copy];
		[[NSUserDefaults standardUserDefaults] setObject:emailType forKey:@"PreferredEmailType"];
	}
}

- (NSNumber *)emailClient
{
	return [NSNumber numberWithInt:0]; //Apple Mail
	/*static BOOL firstCall=YES;
	
	if (firstCall)
	{
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		emailClient = [defaults objectForKey:@"PreferredEmailClient"];
		firstCall = NO;
	}

	return [[emailClient retain] autorelease];*/
	
}
			
- (void)setEmailClient:(NSNumber *)value
{
	[NSException raise:@"ExporterException" format:@"Email Client no longer settable"];
	if (emailClient != value)
	{
		emailClient = [value copy];
		[[NSUserDefaults standardUserDefaults] setObject:emailClient forKey:@"PreferredEmailClient"];
	}
}

- (BOOL)appleMailExists
{
	if ([[NSWorkspace sharedWorkspace] fullPathForApplication:@"Mail"] != nil)
		return YES;
	else
		return NO;
}

- (BOOL)eudoraExists
{
	if ([[NSWorkspace sharedWorkspace] fullPathForApplication:@"Eudora"] != nil)
		return YES;
	else
		return NO;
}

- (BOOL)entourageExists
{
	if ([[NSWorkspace sharedWorkspace] fullPathForApplication:@"Microsoft Entourage"] != nil)
		return YES;
	else
		return NO;
}

NSString* GetTemporaryFilePath(NSString *preferredTitle)
{
	NSFileManager *fm = [NSFileManager defaultManager];
	NSString *tempFolder = NSTemporaryDirectory();
	
	NSString *actualPath = [tempFolder stringByAppendingFormat: @"/%@", preferredTitle];
	NSUInteger suffix=0;
	while ([fm fileExistsAtPath:actualPath])
	{
		suffix++;
		NSString *nextTitle = [preferredTitle stringByAppendingFormat:@"%u", suffix];
		actualPath = [tempFolder stringByAppendingFormat: @"/%@", nextTitle];
	}
	return actualPath;
}


@end

