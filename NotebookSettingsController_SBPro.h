//
//  NotebookSettingsController.h
//  SpiralBound Pro
//
//  Created by Philip White on 5/22/09.
//  Copyright 2009 Rivulus Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SPROrganizer;
@interface NotebookSettingsController : NSWindowController
{
	IBOutlet NSColorWell *colorWell;
	IBOutlet NSTextField *nameField;
	IBOutlet NSTextField *nameNotUniqueField;
	IBOutlet NSTextField *descriptionField;
    IBOutlet NSTabView *_colorsTabView;
	
	//bindings variables
	NSColor *color;
	NSString *title;
	
	NSColor *originalColor;
	SPROrganizer *organizer;
	
	BOOL titleUnique,
	     titleValid;
}

+(NotebookSettingsController*)sharedController;
-(void)showModallyForOrganizer:(SPROrganizer*)organizer;
-(IBAction)cancelButton:(id)sender;
-(IBAction)saveButton:(id)sender;
-(IBAction)colorButton:(id)sender;

- (NSColor *)color;
- (void)setColor:(NSColor *)value;

- (NSString *)title;
- (void)setTitle:(NSString *)value;

- (void)setTitleUnique:(BOOL)value;
- (void)setTitleValid:(BOOL)value;

- (BOOL)isTitleUnique;
- (BOOL)isTitleValid;

@end
